#!/bin/sh
APP_ROOT= /home/ubuntu/apps/life_circle/current
CMD="cd $APP_ROOT; RAILS_ENV=production ~/.rbenv/bin/rbenv exec bundle exec bin/delayed_job -n4 start "
AS_USER=ubuntu
set -u
su -c "$CMD" - $AS_USER
