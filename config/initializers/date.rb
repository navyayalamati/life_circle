class Date
  def as_json(options = nil) #:nodoc:
    if ActiveSupport.use_standard_json_time_format
      strftime("%d/%m/%Y")
    else
      strftime("%Y-%m-%d")
    end
  end
end
