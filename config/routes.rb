Rails.application.routes.draw do
  

  

  

  resources :reason_for_comings
  get 'care_giver_return_equipments/index'

  get 'care_giver_return_equipments/edit'

  get 'care_giver_return_equipments/new'

  resources :care_giver_equipments
  resources :care_giver_daily_attendances do
    collection do
      get :all_routes
      get :news
      get :today_attendance
      post :attendance
      post :update_attendance
      post :save_attendance
      post :attendance_of_cg
      get :payroll
      post :today_patients
      post :start
      post :care_giver_profile
      post :patient_care_giver_profile
      post :enquiries
      post :amount
      get :cg_attendance_in_date_range
    end
  end


  get 'track_vitals/index'

  get 'count_enquiry_managements/index'
  get 'count_enquiry_managements/all_cities'
  get 'count_enquiry_managements/all_jsons'
  

  resources :reports do
    collection do 
      post :get_report
      get :balance
      post :balance_report
      get :payment_received
      post :payment_report
      get :invoice
      post :invoice_details
      get :credit_notes
      post :credit
      get :due_date
      get :age_report
    end
  end
  
  get 'requested_services/index'

  get 'patient_disabilities/index'

  get 'time_of_administrations/index'

  resources :user_creations do
    collection do
      get :all_users
      post :save_user
      put :update_user
    end
  end

  resources :role_creations do
    collection do
      get :menu_bar
      post :save_menu
      post :get_menu
      get :role
      get :role_menu
      post :corresponding_menu
      post :update_menu
    end
  end
  get 'patient_rate_masters/index'

  get 'daily_checks/index'

  get 'care_giver_documents/index'

  resources :application_defaults


  get 'patients/index'

  get 'patients/show'

  get 'unit_masters/index'

  get 'route_masters/index'

  get 'form_masters/index'

  get 'routine_masters/index'

  get 'unit_masters/index'

  get 'route_masters/index'

  get 'form_masters/index'

  get 'routine_masters/index'

  get 'patients_care_plans/index'

  get 'patient_care_plan/index'

  get 'vital_trends/index'

  get 'key_vitals_masters/index'


  get 'drug_quantity_masters/index'

  get 'care_to_be_provided_plans/index'

  resources :transport_charge_masters do
    collection do
      get :charge_for_distance
    end
  end

  resources :city_masters do
    collection do
      post :save_city
      get :check_city_support
      post :check_location_support
    end
    resources :area_masters
  end

  resources :care_giver_enquiries do
    collection do
      get :approval_list
      get :cg
      get :cg_city_request
      get :approved
      get :closed
      get :new_city_request
      get :hold
      get :recruitment
      post :new_city
      post :create_enquiry
    end
    member do
      put :update_enq
      put :recruit
      put :holding
      get :cg_appointment
      get :appointment
      put :close
    end
  end
  
  resources :care_giver_masters do
    collection do
      post :cg_enquiries
      get :care_giver_enquiry
      get :attendance
      get :daily_attendance
      get :location
      get :enquiry
      post :init_enquiry
      get :from_to_date
      post :patients 
      get :payroll
      get :get_caregivers        
      get :get_caregivers_for_transfer   
    end
    member do
      put :finish
      get :cg_appointment
      get :primary_registraion
      put :update_job_type      
      put :update_status
      get :select_job_type
      get :approval_list
      get :month_plan      
    end
    
    resources :care_giver_return_equipments do
      collection do
        post :save_equipments
      end
    end
    
    resources :care_giver_additional_details do
      member do 
        put :approve_physical_description
        put :approve_helth_and_life
      end
      collection do
        get :health_and_lefe_style
        get :physical_description
        post :save_additional_details
      end
    end

    resources :care_giver_relatives do
      member do 
        put :approve
      end
    end

    resources :care_giver_services do
      collection do 
        get :services_for_registration
        post :save_services
        put :approve
      end
    end
    resources :care_giver_educations do
      member do 
        put :approve
      end

      collection do 
        get :cg_dob
        get :education_for_registration
        post :save_education
      end
    end
    
    resources :care_giver_documents do
      member do 
        put :approve
      end

      collection do 
        get :document_for_upload
        post :save_document
      end
    end

    resources :care_giver_employers do
      member do 
        put :approve
      end

      collection do 
        get :employer_for_registration
        post :save_employer
        post :save_fresher
      end
    end

    resources :care_giver_charges do
      member do 
        put :approve
      end

      collection do 
        get :new_or_existing_charges
        post :save_charges
      end
    end
    resources :care_giver_availability_masters
    resources :care_giver_leaves do
      member do 
        put :approve
        put :reject
      end
    end
  end

  get "care_giver_leaves/all_cg_leaves" => "care_giver_leaves#all_cg_leaves"
  
  resources :enquiries do
    collection do
      put :update_to_audit
      get :edit_values
      get :all_appointments
      get :appointments
      post :enquiry_details
      get :landing
      get :init_enquiry
      put :update_approach
      get :service_request
      put :update_service_request
      get :care_givers
      post :init_request
      post :list_care_givers
      post :book_care_giver
      get :new_appointment_for_enquiry
      post :create_appointment_for_enquiry
      post :capture_customer_on_empty_results
      get :new_city
      get :new_enquiry_from_missed_call_request
      get :missed_call_requests
      put :update_appointment
    end
    member do
      put :update_appointment_status
      get :appointment_edit
      put :update_appointment
      put :close
      post :check_cg_availability_on_modified_query
      get :show_appointment_for_enquiry
      post :update_pre_enquiry
    end
    
  end
  
  resources :enrollments do
    collection do
      get :reason_for_coming
      get :super_care_giver
      get :pcp
      get :cg_transfer
      post :save_disabilities
    end
    member do
      get :save_patients
      get :patient_edit
      get :change_care_giver      
      post :update_care_giver_change
      post :compare_payments_on_cg_change
      get :transfer_care_giver
      post :update_care_giver_transfer
      post :compare_payments_on_cg_transfer
      get :change_plan
      post :update_change_plan
      get :new_notification
      patch :post_notification
      get :close      
      patch :save_close
      
    end
    
    collection do
      get :incomplete_pcp
      get :closed_form
    end
    
    resources :payment_masters do
      member do
        get :payment_preview
        put :revoke_credit_note
        put :issue_credit_note
        put :update_credit_note_status
        delete :destroy_credit_note
        post :create_credit_note
        put :mark_as_pending_credit_note
        put :update_payment_discount_details
        post :recalculate
        get :credit_notes
      end

      collection do
        post :new_recurring_payment
        post :recurring_payment
        get :revise_care_giver_charge
        get :review_invoices
        get :payment_on_closing_enrollment
        post :save_closing_payment
      end
      
      resources :payment_bills
    end

    resource :patient_care_plan
    
    resource :patients_care_plans do
      collection do
        get :pcp_pdf
        get :all_jsons
      end
    end
    
    resources :track_vitals
    
    resources :patient_adaptive_equipments do
      collection do
        post :save_patient_equipments
      end
    end   
          
    resources :patient_disabilities do
      collection do
        post :save_patient_disabilities
      end
    end  
    
    resources :requested_services do
      collection do
        post :save_requested_services
        get :enrolled
      end
    end  
    
    resources :patient_selfcare_abilities
 
    resources :care_to_be_provided_plans do 
      collection do
         get :get_care_to_be_provided_plans
      end
    end
    
    resources :drug_quantity_masters do
     get :autocomplete_drug_master_names, :on => :collection
    end
    
    resources :vital_trends
        
    resources :patient_routines do
      collection do
        put :update_patient_routine
      end
    end
    
  end

  
  
  resources :service_masters do
    resources :sub_service_masters do
      collection do
        get :all_sub_services
      end
    end
  end
  
  get "sub_service_masters/all_sub_services" => "sub_service_masters#all_sub_services"
  
  get "/enrollments/:enrollment_id/payment_bills/enrollment_bills" => 'payment_bills#enrollment_bills'
  get "/enquiries/:enquiry_id/enrollments/new" => 'enrollments#new'
  post "/enquiries/:enquiry_id/enrollments" => 'enrollments#create'
  post "/enquiries/:enquiry_id/enrollments/safety_measures" => 'enrollments#safety_measures'
  
  # get '/enrollments/:enrollment_id/payment_masters' => 'payment_masters#index'
  # get '/enrollments/:enrollment_id/payment_masters/new' => 'payment_masters#new'
  # post '/enrollments/:enrollment_id/payment_masters' => 'payment_masters#create'
  # post '/enrollments/:enrollment_id/payment_masters/new_recurring_payment' => 'payment_masters#new_recurring_payment'
  # post '/enrollments/:enrollment_id/payment_masters/recurring_payment' => 'payment_masters#recurring_payment'
  # get '/enrollments/:enrollment_id/payment_masters/revise_care_giver_charge' => 'payment_masters#revise_care_giver_charge'
  # get '/enrollments/:enrollment_id/payment_masters/:id/payment_preview' => 'payment_masters#payment_preview'
  # put '/enrollments/:enrollment_id/payment_masters/:id/revoke_credit_note' => 'payment_masters#revoke_credit_note'
  # put '/enrollments/:enrollment_id/payment_masters/:id/issue_credit_note' => 'payment_masters#issue_credit_note'
  # put '/enrollments/:enrollment_id/payment_masters/:id/update_payment_mode_details' => 'payment_masters#update_payment_mode_details'
  # get '/enrollments/:enrollment_id/payment_masters/:id' => 'payment_masters#show'  

  resources :job_runs do
    collection do
      get "schedule_monthly_invoice"
    end
    member do
      get "mail_selected_payments"
      get "mail_all_payments"
      put "nullify_selected_payments"
    end
  end



  resources :service_masters
  resources :lost_reason_masters
  resources :enquiry_reference_masters
  resources :language_masters  

  resources :patient_selfcare_ability_masters
  resources :patient_adaptive_equipment_masters

  resources :key_vitals_masters
  
  resources :drug_masters
  
  resources :routine_masters
  
  resources :route_masters do
    collection do
      get :all_routes
    end
  end
  
  resources :form_masters
  
  resources :tickets do
    collection do 
      post :all_tickets
      get :all_patients
      get :all_care_givers
    end
  end
  
  resources :ticket_category_masters
  
  resources :ticket_conversations
  
  resources :daily_checks
  
  resources :unit_masters
  
  resources :patient_rate_masters
  
  resources :time_of_administrations


  devise_for :users, :controllers => {:registrations => 'registrations' , :sessions => 'sessions'}
  as :user do 
    get 'users/edit' => 'registrations#edit'
    put 'users' => 'registrations#update'
    get "authenticity" => "sessions#get_authenticity"
    get "get_current_user" => "sessions#get_current_user"
    get "check_user_exists" => "registrations#check_user_exists"
  end
  

  get "enquiry", :to => "home#welcome_enquiry"
  get "check_user_session", :to => "home#check_user_session"
  root to: "home#index"
  get "home/care_giver"
  get "home/services"
  get "home/team"
  get "home/foundation"
  get "home/news"
  get "home/careers"
  resources :disability_masters 
  resources :new_city_requests do
    member do
      get :acknowledge
    end
  end

  get 'auto_search/autocomplete_drug_master_names'
  get 'auto_search/autocomplete_caregiver_master_names'
  get 'auto_search/autocomplete_patient_names'

  resources :enquiry_conversations do
    collection do
      get :conversation_json
      post :conversation_save
    end
  end

  resources :patients do
    collection do
      get :get_patient_details_with_user_id
      post :create_patient_from_user_params
      get :service
      get :list_care_givers
    end
  resources :enquiry_conversations  
  end  
  
  resources :email_templates do
    member do
      get :show_sms
      get :edit_sms
    end
  end


  get "missed_call_listeners/listen" => "missed_call_listeners#listen"
  post "missed_call_listeners/post_listener" => "missed_call_listeners#post_listener"
  
  get 'auto_search/autocomplete_user_details'
  
  get "care_giver_charge_buckets/new" => "care_giver_charge_buckets#new"
  get "care_giver_charge_buckets" => "care_giver_charge_buckets#index"
  post "care_giver_charge_buckets" => "care_giver_charge_buckets#save"
  
  
end


