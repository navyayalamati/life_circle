json.array!(@reason_for_comings) do |reason_for_coming|
  json.extract! reason_for_coming, :id, :name
  json.url reason_for_coming_url(reason_for_coming, format: :json)
end
