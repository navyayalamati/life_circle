json.array!(@city_masters) do |city_master|
  json.extract! city_master, :id, :name, :latitude, :longitude
  json.url city_master_url(city_master, format: :json)
end
