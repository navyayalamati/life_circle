json.array!(@application_defaults) do |application_default|
  json.extract! application_default, :id, :description, :value
  json.url application_default_url(application_default, format: :json)
end
