json.array!(@payment_bills) do |payment_bill|
  json.extract! payment_bill, :id, :payment_master_id, :amount_paid, :payment_mode, :description_1, :description_2, :payment_done_by, :payment_date, :status
  json.url payment_bill_url(payment_bill, format: :json)
end
