json.array!(@care_giver_equipments) do |care_giver_equipment|
  json.extract! care_giver_equipment, :id, :name, :status
  json.url care_giver_equipment_url(care_giver_equipment, format: :json)
end
