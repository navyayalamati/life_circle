class CareGiverAvailabilityMaster < ActiveRecord::Base
  DAYS = [ :mon, :tue, :wed, :thu, :fri, :sat, :sun]
  belongs_to :care_giver_master
  
  scope :matching_care_giver, lambda{|care_giver_master_id| where(:care_giver_master_id => care_giver_master_id)}
  
  def self.new_day_wise_availabilities
    day_wise_availabillities = {}
    DAYS.each do |day|
      map = {}
      new_availability = new
      new_availability.day = day
      map[:availabilities] = [new_availability]
      map[:enabled] = false
      day_wise_availabillities[day] = map
    end
    day_wise_availabillities
  end

  def self.build_availabilities(care_giver_master_id)
    day_wise_availabillities = {}
    existing_availability = matching_care_giver(care_giver_master_id).to_a.group_by{|a| a.day}
    DAYS.each do |day|
      map = {}
      if existing_availability[day.to_s].present?
        
        map[:availabilities] = existing_availability[day.to_s].map{|a| CareGiverAvailabilityMasterDecorator.decorate(a) }
        map[:enabled] = true
      else
        new_availability = new
        new_availability.day = day
        map[:availabilities] = [new_availability]
        map[:enabled] = false
      end
      day_wise_availabillities[day] = map
    end
    day_wise_availabillities
  end

end
