class CareGiverEnquiry < ActiveRecord::Base
  has_many :enquiry_conversations, as: :conversation_master
end
