class RequestedService < ActiveRecord::Base
  belongs_to :enrollment
  belongs_to :enquiry
  belongs_to :service_master
  
  attr_accessor :assigned

  def service_name
    service_master.service
  end
  
  def as_json(options = {})   
    super(:methods => [:service_name, :assigned])   
  end
end
