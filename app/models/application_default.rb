class ApplicationDefault < ActiveRecord::Base

  after_save :reload_enrollment_fee, :if => Proc.new { |default| default.description.present? and  (default.description.downcase == "enrollment fee" or default.description.downcase == "enrollment_fee")}
  
  def reload_enrollment_fee
    self.class.load_enrollmet_fee
  end

  def self.load_enrollmet_fee
    @@enrollment_fee = self.where("lower(description) = 'enrollment_fee' OR lower(description) = 'enrollment fee'").first.value
  end

  def self.enrollment_fee
    @@enrollment_fee ||= load_enrollmet_fee
  end

  def self.default_cc_email
    self.where(:description => "default_cc_email").first.value
  end

  def self.invoice_due_date(date)
    invoice_due_row = self.where(:description => "invoice_due_date").first 
    (invoice_due_row.present? ? (date + invoice_due_row.value.to_i) : date)
  end

end
