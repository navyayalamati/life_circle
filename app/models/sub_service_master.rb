class SubServiceMaster < ActiveRecord::Base
  belongs_to :service_master
  has_many :pcp_services
  def service_name
    service_master.service
  end
  
  def as_json(options = {})   
    super(:methods => [:service_name])   
  end
end
