class MissedCallRequest < ActiveRecord::Base
  has_many :enquiry_conversations, as: :conversation_master
  belongs_to :enquiry
  ENQUIRY_CAPTURED = "ENQUIRY_CAPTURED"

  scope :match_by_mobile_no, lambda{|term| where("(mobile_no ILIKE '%#{term.downcase}%')")}
end
