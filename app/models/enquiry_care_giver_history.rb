class EnquiryCareGiverHistory < ActiveRecord::Base

  belongs_to :enquiry_history
  
  def total_charge
    (caregiver_charge.to_i + transport_charge.to_i)
  end

end
