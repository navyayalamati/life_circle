class PaymentBill < ActiveRecord::Base
  attr_accessor :total_amount_to_pay
  belongs_to :payment_master
  
  scope :completed, lambda{ where("status = 'COMPLETED'")}

  def bill_pre_process_and_save
    ActiveRecord::Base.transaction do
      if amount_paid == total_amount_to_pay.to_i 
        payment_master.mark_as_paid
      elsif amount_paid > total_amount_to_pay.to_i
        payment_master.mark_as_paid
        credit_note = CreditNote.new do |cr|
          cr.payment_master = payment_master
          cr.reason_for_creadit_note = CreditNote::EXESS_PAID_MSG
          cr.value_of_credit_note = (amount_paid - total_amount_to_pay.to_i)
          cr.credit_note_date = self.payment_date
          cr.status = "PENDING"
          cr.enrollment_id = payment_master.enrollment_id
        end
        credit_note.save
      elsif amount_paid < total_amount_to_pay.to_i
        payment_master.mark_as_partly_paid
      end
      self.status = "COMPLETED"
      self.save
    end
  end

end
