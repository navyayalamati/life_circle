class CareGiverService < ActiveRecord::Base
  belongs_to :care_giver_master
  belongs_to :service_master
  
  def self.all_serivces(care_giver_master_id)
    query = "SELECT service_masters.id as service_master_id, care_giver_services.id, service_masters.service, care_giver_services.care_giver_master_id, care_giver_services.approved FROM (SELECT * FROM care_giver_services where care_giver_master_id = #{care_giver_master_id}) as care_giver_services RIGHT OUTER JOIN service_masters on care_giver_services.service_master_id = service_masters.id  ORDER BY service"
    ActiveRecord::Base.connection.execute(query)
  end
  
  scope :belongs_to_care_giver, lambda{|care_giver| where(:care_giver_master_id => care_giver)}
  scope :having_services, lambda{|services_list| where(:service_master_id => services_list)}
  
end
