class CreditNote < ActiveRecord::Base

  belongs_to :payment_master
  belongs_to :enrollment
  
  scope :having_enrollment, lambda{|enrollment_id| where(:enrollment_id => enrollment_id)}
  scope :pending, lambda{where(:status => "PENDING")}
  scope :issued, lambda{where(:status => "ISSUED")}
  scope :leave_type, lambda{where(:reason_for_creadit_note => CreditNote::ABSENCE_REASON_MSG_FOR_SINGLE_DAY)}
  scope :excess_amount_paid_pendings, lambda{where(:reason_for_creadit_note => CreditNote::EXESS_PAID_MSG).pending}
  scope :not_excess_amount_paid, lambda{where("reason_for_creadit_note != ?",CreditNote::EXESS_PAID_MSG)}
  scope :credit_notes_on_date, lambda{|date| where(:credit_note_date => date)}
  scope :credit_note_of_enrollment, lambda{|enrollment_id| having_enrollment(enrollment_id).leave_type.order("credit_note_date DESC")}
  scope :having_reason, lambda{|reason| where(:reason_for_creadit_note => reason)}

  ABSENCE_REASON_MSG = "Went on leave for {days} days on {dates}. Client not accepted for replacement."
  ABSENCE_REASON_MSG_FOR_SINGLE_DAY = "LEAVE"
  EXESS_PAID_MSG = "Paid excess amount in prev month"
  
  def self.compose_reason_msg(absent_days, absent_dates_list)
    ABSENCE_REASON_MSG.gsub("{days}", absent_days.to_s).gsub("{dates}", absent_dates_list.join(", "))
  end


end
