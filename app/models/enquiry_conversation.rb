class EnquiryConversation < ActiveRecord::Base

  belongs_to :conversation_master , polymorphic: true
  belongs_to :author, :class_name => "User", :foreign_key => "entered_by"
  
  def author_name
    author.user_name
  end

  def formated_entered_at
    entered_at.strftime("%d/%m/%Y %H:%M") 
  end


  def as_json(options={})
    options[:methods] = [:author_name, :formated_entered_at]
    super(options)
  end
end
