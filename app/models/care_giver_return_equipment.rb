class CareGiverReturnEquipment < ActiveRecord::Base
  belongs_to :care_giver_master
  belongs_to :care_giver_equipment

  attr_accessor :assigned

  def adaptive_name
    care_giver_equipment.name
  end
  
  def as_json(options = {})   
    super(:methods => [:adaptive_name, :assigned])   
  end

end
