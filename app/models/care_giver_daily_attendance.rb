class CareGiverDailyAttendance < ActiveRecord::Base
  belongs_to :care_giver_master
  belongs_to :patient
  belongs_to :enquiry

  scope :attendance_on_date, lambda{|date| where("date = ?", date)}
  scope :exists_in_range, lambda{|period_from, period_to| where("date  BETWEEN ? AND ? ", period_from, period_to)}


end
