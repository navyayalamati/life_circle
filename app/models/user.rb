class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:user_name]
  #before_save :check_email_length
  
  has_one :care_giver_master
  has_one :patient

  before_save :ensure_authentication_token
  

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end


  L_C_AGENT = "l_c_agent"
  PATIENT = "patient"
  CARE_GIVER = "care_giver"
  validates :user_name, :presence => true

  attr_accessor :login, :registered_name, :registered_by, :area_master_id, :job_type, :first_name, :dob, :heighest_qualification, :expected_sal_per_day

  has_many :enquiries
  has_one :care_giver_master
  has_one :patient
  
  scope :having_user_name, lambda{|user_name| where(:user_name => user_name)}
  scope :having_no_active_enrollments, lambda{|user_id| User.where(:id => Patient.select(:user_id).where(:id => Enrollment.select(:patient_id).not_active.belongs_to_patient(Patient.where(:user_id => user_id))))}

  def patient?
    role == self.class::PATIENT
  end

  def care_giver?
    role == self.class::CARE_GIVER
  end

  def login=(login)
    @login = login
  end
  
  def login
    @login || self.mobile_no || self.email || self.user_name
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(mobile_no) = :value OR lower(email) = :value OR lower(user_name) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end
  
  def email_required?
    false
  end
  
  def care_giver_master_id
    care_giver_master.try(:id)
  end

  def patient_id
    patient.try(:id)
  end

  def as_json(options={})
    dependent_list = []
    if care_giver?
      dependent_list.push(:care_giver_master_id)
    elsif patient?
      dependent_list.push(:patient_id)
    end
    super(:methods => dependent_list)
  end

  def self.generate_password_on_admin_creation(size = 8)
    charset = %w{ @ # $ % & * 1 2 3 4 5 6 7 8 9 A C D E F G H J K M N P Q R T V W X Y Z}
    (0...size).map{ charset.to_a[rand(charset.size)] }.join
    #range = [(0..9), ('A'..'Z')].map { |i| i.to_a }.flatten
    #(0...8).map { range[rand(range.length)] }.join
  end


  private
  
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

  def check_email_length
    self.email = ((self.email.present? and self.email.length > 3) ? self.email : nil)
  end

end
