class PatientAdaptiveEquipment < ActiveRecord::Base
  belongs_to :enrollment
  belongs_to :patient_adaptive_equipment_master

  attr_accessor :assigned

  def adaptive_name
    patient_adaptive_equipment_master.adaptive_equipment
  end
  
  def as_json(options = {})   
    super(:methods => [:adaptive_name, :assigned])   
  end
end
