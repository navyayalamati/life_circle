class RoutineMaster < ActiveRecord::Base
  validates :name, :presence => true
  has_many :patient_routines
end
