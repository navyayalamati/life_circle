class PatientRoutine < ActiveRecord::Base
  belongs_to :enrollment
  belongs_to :routine_master
   
  def routine_name
      routine_master.name
   end
  
  def as_json(options = {})   
      super(:methods => [:routine_name])   
  end 
end
