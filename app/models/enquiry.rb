class Enquiry < ActiveRecord::Base
  REGISTRATIN_DONE_BY_OPTIONS = [['Patient', "PATIENT"], ['Relative', "RELATIVE"]]
  CAREGIVER_STAY_OPTIONS = ["LIVE_IN", "LIVE_OUT"]
  CAREGIVER_LIVE_IN_OPTION = "LIVE_IN"
  CAREGIVER_LIVE_OUT_OPTION = "LIVE_OUT"
  DUTY_STATION_OPTIONS = ["HOME", "HOSPITAL"]
  REGISTRATION_TYPE_OPTIONS = [['Let Me Fill', "LET_ME_FILL"], ['Visit My Home', "HOME_VISIT"], ['Call Me', "CALL_ME"]]
  STATUS = {:wait_for_appointment => "WAIT_FOR_APPOINTMENT", :wait_for_enrollement => "WAIT_FOR_ENROLLEMENT", :assessment_captured => "ASSESMENT_CAPTURED",:enrolled => "ENROLLED", :closed => "CLOSED", :rejected => "REJECTED", :pre_enquiry_appointment => "PRE_ENQUIRY_APPOINTMENT", :empty_results => "EMPTY_RESULTS", :sent_to_audit => "SENT_TO_AUDIT"}
  #in minutes
  MIN_HOLD_TIME = 30
  MAX_HOLD_TIME = (24*60)
  MAX_TIME_BREAK_FOR_DISTANCE_COVERAGE = 120
  LIVE_IN_CHECK_IN_TIME = "10:00"
  LIVE_IN_CHECK_OUT_TIME = "34:00"
  validate :validate_open_term_service_to
  belongs_to :user
  belongs_to :patient
  belongs_to :area_master
  has_many :requested_services
  has_many :requested_time_slots
  has_many :enquiry_languages
  has_many :enquiry_care_givers
  has_many :care_giver_masters, :through => "enquiry_care_givers"
  belongs_to :user 
  has_one :enrollment
  has_many :enquiry_conversations, as: :conversation_master
  has_many :enquiry_histories
  belongs_to :address
  has_one :missed_call_request

  accepts_nested_attributes_for :address
  
  attr_accessor :refered_mobile_no, :languages, :appointment_date, :missed_call_request_id

  scope :having_status, lambda{|status_code| where("status = ? or current_tag = ?", status_code, status_code)}
  scope :not_live_in, lambda{ where("caregiver_stay != 'LIVE_IN'")}
  scope :live_in, lambda{ where("caregiver_stay = 'LIVE_IN'")}
  scope :active, lambda{ where("status != '#{Enquiry::STATUS[:closed]}'")}
  scope :enrolled, lambda{ where("status = '#{Enquiry::STATUS[:enrolled]}'")}
  scope :not_enrolled, lambda{ where("status = '#{Enquiry::STATUS[:wait_for_appointment]}' or status = '#{Enquiry::STATUS[:wait_for_enrollement]}' or status = '#{Enquiry::STATUS[:pre_enquiry_appointment]}' or status = '#{Enquiry::STATUS[:empty_results]}'")}
  scope :dated_on, lambda{|date| where("CASE WHEN open_term = true THEN (? >= service_required_from) ELSE (? BETWEEN  service_required_from and service_required_to) END", date, date)}
  scope :after_start_date, lambda{|date| where("? >= service_required_from", date)}
  scope :before_end_date, lambda{|date| where("? <= service_required_to", date)}
  scope :during_last, lambda {|time| where("created_at > ?", (Time.now - time)) }
  scope :match_by_registered_user_name, lambda{|term| where("(lower(requested_user_name) ILIKE '%#{term.downcase}%') ")}

  def self.active_enquiries_in_date_range(start_date, end_date)
    where("(? >= service_required_from and ? <= service_required_to) or (? >= service_required_from and ? <= service_required_to)",start_date, start_date, end_date, end_date)
  end
  
  def blocked_by
    during_last(24.hours)
  end
  

  def live_in?
    self.caregiver_stay == "LIVE_IN"
  end
  
  def let_me_fill?
    self.mentioned_registration_type == "LET_ME_FILL"
  end 

  def update_appointment(appointment_time)
    self.update_attributes({:status => self.class::STATUS[:wait_for_enrollement], :appointment_time => appointment_time})
  end

  def mark_as_enrolled
    self.update_attributes({:status => self.class::STATUS[:enrolled]})
  end

  def patient_name
    if patient.present?
      patient.name
    end
  end

  def locality
    "#{area_master.name}, #{area_master.city_master.name}" if area_master.present?
  end

  def services_str
    services_list.join(", ") 
  end

  def services_list
    requested_services.map{|s| s.service_master.service}
  end

  def language_str
    language_list.join(", ") 
  end

  def language_list
    enquiry_languages.map{|l| l.language_master.language}
  end

  def address_str
    address.try(:full_address)
  end

  def address_new
    address.try(:full)
  end

  def time_slots_str
    requested_time_slots.map{|t| "#{t.decorated_time_from}-#{t.decorated_time_to}"}.join(", ") 
  end

  def appointment_time_str
    appointment_time.strftime("%d/%m/%Y %H:%M") if appointment_time.present?
  end

  def closed?
    status == self.class::STATUS[:closed]
  end

  def enrolled?
    status == self.class::STATUS[:enrolled]
  end

  def pre_enquiry?
    status == self.class::STATUS[:empty_results] or status == self.class::STATUS[:pre_enquiry_appointment]
  end

  def send_to_audit?
    current_tag.to_s.downcase == "to audit"
  end

  def closed_in_audit?
    current_tag.to_s.downcase == "closed"
  end

  def cg_blocked?
    return false if (closed? or send_to_audit? or closed_in_audit?)
    check_cg_blocking
  end

  def check_cg_blocking
    current_time_stamp = "'#{DateTime.now.strftime('%Y-%m-%d %H:%M')}'::timestamp"
    datetime_difference_in_minutes_query = "(DATE_PART('day', #{current_time_stamp} - r.booked_at) * 24 + DATE_PART('hour', #{current_time_stamp} - r.booked_at)) * 60 + DATE_PART('minute', #{current_time_stamp} - r.booked_at)"
    lock_check_query = "(CASE WHEN e.status = '#{Enquiry::STATUS[:wait_for_appointment]}' THEN #{datetime_difference_in_minutes_query} < #{Enquiry::MIN_HOLD_TIME} WHEN e.status = '#{Enquiry::STATUS[:wait_for_enrollement]}' THEN #{datetime_difference_in_minutes_query} < #{Enquiry::MAX_HOLD_TIME} ELSE true END)"
    Enquiry.where("id in (SELECT DISTINCT care_giver_master_id FROM requested_time_slots r, enquiries e WHERE e.id = r.enquiry_id and #{lock_check_query})").count > 0
  end
  


  def patient_name
    patient.try(:name)
  end


  def female_caregiver_required?
    care_giver_gender == "FEMALE"
  end

  def total_charge
    enquiry_care_givers.inject(0){|sum, cg| sum+ (cg.caregiver_charge.to_i+cg.transport_charge.to_i)}
  end

  def total_work_hours
    hours = 0
    scanned_cgs = []
    requested_time_slots.group_by(&:care_giver_master_id).values.first.each do |time_sot|
      end_time = time_sot.time_to
      unless time_sot.time_to.present?
        end_time = time_sot.time_from + 24.hours 
      end
      hours += ((end_time - time_sot.time_from)/1.hour)
    end
    hours
  end
  belongs_to :closed, :class_name => "User", :foreign_key => "closed_by"

  def closed_by_name
    closed.try(:user_name)
  end

  def care_giver_names
    enquiry_care_givers.map{|ecg| ecg.try(:care_giver_master).try(:name)}.join(",")
  end

  
  def as_json(options = {})
    if options.empty?
      super({:include => {:care_giver_masters => {:methods => [:name, :age, :professional_qualification_str, :education_details_str, :total_charge]}}, :methods => [:time_slots_str, :language_str, :services_str, :appointment_time_str, :total_charge, :patient_name]})
    else
      super(options)
    end
  end

  
  def detail_json
    care_giver_excludes_params = [:recruitment_mode, :place_of_birth, :father_name, :mother_name, :id_proof_no, :id_proof_doc, :id_proof_verified, :id_proof_verified_by, :address_proof_id, :address_proof_doc, :address_proof_verified, :address_proof_verified_by, :status, :job_type, :mentioned_registration_type, :created_at, :updated_at, :id_proof_type, :address_proof_type, :heighest_qualification, :profile_pic, :expected_sal_per_day, :reference_1, :reference_2, :approved]
    
    as_json( {:include => {:address => {:except => [:created_at, :updated_at]}, :requested_services => {:except => [:created_at, :updated_at]}, :requested_time_slots => {:except => [:created_at, :updated_at], :methods => [:decorated_time_from, :decorated_time_to]}, :enquiry_languages => {:except => [:created_at, :updated_at]}, :enquiry_care_givers => { :include => {:care_giver_master => {:methods => [:name, :services, :languages], :except => care_giver_excludes_params}}}}, :methods => [:patient_name, :locality, :services_str, :appointment_time_str, :language_list, :services_list, :time_slots_str, :language_str]})
  end

  def modified_json
    care_giver_excludes_params = [:recruitment_mode, :place_of_birth, :father_name, :mother_name, :id_proof_no, :id_proof_doc, :id_proof_verified, :id_proof_verified_by, :address_proof_id, :address_proof_doc, :address_proof_verified, :address_proof_verified_by, :status, :job_type, :mentioned_registration_type, :created_at, :updated_at, :id_proof_type, :address_proof_type, :heighest_qualification, :profile_pic, :expected_sal_per_day, :reference_1, :reference_2, :approved ]
    enquiry_params = [:created_at, :updated_at]
    
    as_json( {:methods => [:patient_name,  :appointment_time_str, :services_str, :time_slots_str, :language_str]})
  end

  private

  def validate_open_term_service_to
    unless status == Enquiry::STATUS[:pre_enquiry_appointment]
      unless open_term? 
        unless service_required_to.present?
          self.errors.add(:base, "Short term enquiries should have service required to")
        end
      end 
    end
  end

end

