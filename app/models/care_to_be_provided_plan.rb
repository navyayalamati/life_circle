class CareToBeProvidedPlan < ActiveRecord::Base
  belongs_to :enrollment
  has_many :pcp_services
  accepts_nested_attributes_for :pcp_services, :allow_destroy => true
  
  def services
    services_list.join(", ")
  end

  def services_list
    pcp_services.map{|y| y.sub_service_master.sub_service}
  end
  
  def as_json(options = {})   
    super(:methods => [:services])   
  end
end
