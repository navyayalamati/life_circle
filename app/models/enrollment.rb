class Enrollment < ActiveRecord::Base
  WillPaginate.per_page = 10
  belongs_to :patient
  belongs_to :enrollment
  belongs_to :enquiry
  has_many :enquiry_care_givers, :foreign_key => "enquiry_id", :primary_key => "enquiry_id"
  belongs_to :super_care_giver, :class_name => "CareGiverMaster", :foreign_key => "super_care_giver_id"
  has_many :requested_time_slots, :foreign_key => "enquiry_id", :primary_key => "enquiry_id"
  has_many :patient_disabilities
  has_many :payment_masters
  has_many :patient_selfcare_abilities
  has_many :patient_adaptive_equipments
  has_many :vital_trends
  has_many :track_vitals
  has_many :drug_quantity_masters
  has_many :credit_notes
  has_many :patient_routines
  has_many :care_to_be_provided_plans
  has_many :requested_services
  has_many :care_giver_daily_attendances, :foreign_key => "enquiry_id", :primary_key => "enquiry_id"
  
  ENROLLMENT_FEE = ApplicationDefault.enrollment_fee
  CLOSE_REASONS = [['Patient Dead', "PATIENT_DEAD"], ['Other', "OTHER"]]
  NOTICE_PERIOD_DAYS_ON_PATIENT_DEAD = 3
  NOTICE_PERIOD_MAX_DAYS = 7
  
  scope :enrolled_on, lambda{|date| where("to_char(created_at,'DD-MM-YYYY' ) = '#{date.strftime('%d-%m-%Y')}'") }
  scope :active, lambda{where(:status => "ACTIVE")}
  scope :not_active, lambda{where("status != 'ACTIVE'")}
  scope :not_closed, lambda{where("status != 'CLOSED'")}
  scope :belongs_to_patient, lambda{|patient_id| where(:patient_id => patient_id)}
  scope :belongs_to_care_giver, lambda{|care_giver_id| where(:enquiry_id => EnquiryCareGiver.select(:enquiry_id).belongs_to_care_giver(care_giver_id))}
  scope :belongs_to_enquiry, lambda{|enquiry_ids| where(:enquiry_id => enquiry_ids)}
  
  
  def patient_with_address_html
    "<b>#{patient.name}</b>"
    "</br>"
    "#{patient.address.full_address_html}"
    "</br>"
    "#{enquiry.mobile_no}"
    "</br>"
    "#{enquiry.email}"
  end
  
  def payments_exists?
    @payment_exist ||= (payment_masters.not_failed.count > 0)
  end


  def valid_period_to_date(date)
    if(enquiry.service_required_to.present? and date > enquiry.service_required_to)
      enquiry.service_required_to
    elsif self.close_request_date.present?
      (self.close_request_date > date ? date : self.close_request_date )
    else
      date
    end
  end
  
  def closed?
    self.status == 'CLOSED'
  end

  def close_pending?
    self.status == 'CLOSE_PENDING'
  end

  def close_request_registered?
    close_request_date.present?
  end

  def notice_period_days_when_close_request_registered(date)
    return self.class::NOTICE_PERIOD_MAX_DAYS unless close_request_registered?
    diff = (date-close_request_date).to_i.abs
    ( diff > self.class::NOTICE_PERIOD_MAX_DAYS ? 0 : diff)
  end

  def reason_based_notice_period_days
    ((reason_for_closing.present? and reason_for_closing == 'PATIENT_DEAD') ? self.class::NOTICE_PERIOD_DAYS_ON_PATIENT_DEAD : self.class::NOTICE_PERIOD_MAX_DAYS)
  end

  def safety_measures
    violation_list = []
    female_caregiver =  enquiry.female_caregiver_required?
    violation_list.push("Female caregiver cannot assigned to the patient who lives alone") if (female_caregiver and patient_home_setting.to_s == "LIVES_ALONE")
    violation_list.push("Female caregiver cannot assigned to the patient who lives only with male") if (female_caregiver and patient_home_setting.to_s == "LIVES_ALONE")
    violation_list.push("Suffering from infectious disease ") if suffering_from_infection_disease
    violation_list.push("Locality is not saf/well conttected") if (female_caregiver and !safe_and_well_connected_locality)
    violation_list.push("Not provides personal protective equipment") if !provides_personal_protective_equipment
    violation_list.push("Assigned with Female caregiver and no patient family member helps") if (female_caregiver and !family_member_helps)
    violation_list
  end

  def cross_gender_request?
    enquiry.caregiver_gender.to_s != enquiry.care_giver_masters.first.try(:gender).to_s
  end

  def generate_payment_date_period
    start_date = nil
    end_date = nil
    existing_payments = payment_masters.order("created_at asc")
    start_date = (existing_payments.present? ? (existing_payments.last.period_to+1) : enquiry.service_required_from) 
    end_date = (enquiry.open_term? ? start_date.end_of_month : enquiry.service_required_to)
    (start_date..end_date)
  end

  def validate_and_revise_payment_generation_period(start_date, end_date)
    existing_payments = payment_masters.order("created_at asc")
    last_payment = existing_payments.last
    result = {:valid => true}
    existing_start_date = (existing_payments.present? ? (last_payment.period_to) : enquiry.service_required_from) 
    if existing_start_date > start_date or end_date <= start_date
      result[:valid] = false
      return result
    end
    result[:start_date] = start_date
    result[:end_date] = valid_period_to_date(end_date)
    result
  end
  
  def address_str
    address.try(:full_address)
  end
  
  
  
  def previous_due_amount
    payment_masters.inject(0){|sum, bill| sum+bill.total_pending_payment.to_i}.to_i.round
  end

  def as_json(options = nil)
    super(options)
  end
  
  def detail_json
    as_json({:include => {:patient => {:except => [:created_at, :updated_at], :methods => [:address_str]}, :enquiry => {:include => {:requested_services => {:except => [:created_at, :updated_at]}, :requested_time_slots => {:except => [:created_at, :updated_at], :methods => [:decorated_time_from, :decorated_time_to]}, :enquiry_languages => {:except => [:created_at, :updated_at]}, :enquiry_care_givers => { :include => {:care_giver_master => {:methods => [:name, :services, :languages]}}}}, :methods => [:patient_name, :locality, :services_str, :appointment_time_str , :address_str]}}})
  end

end
