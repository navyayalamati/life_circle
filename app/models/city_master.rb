class CityMaster < ActiveRecord::Base
  validates :name, :presence => true
  geocoded_by :name
  after_validation :geocode 
  has_many :area_masters

  scope :matching_city, lambda{|city| where("lower(name) = ?", city.downcase)}
end
