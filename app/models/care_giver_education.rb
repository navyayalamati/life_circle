class CareGiverEducation < ActiveRecord::Base
  mount_uploader :attachment, DocumentUploader
  belongs_to :care_giver_master

  scope :proff_qualification,  lambda{ where(:professional_nursing_qualification => true)}
end
