class CareGiverLanguage < ActiveRecord::Base

  validates :care_giver_master_id, :presence => true
  validates :language_master_id, :presence => true
  belongs_to :care_giver_master
  belongs_to :language_master

end
