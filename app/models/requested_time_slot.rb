class RequestedTimeSlot < ActiveRecord::Base
  ENQUIRY_INIT_STATUS = "REQUESTED"

  belongs_to :enquiry
  belongs_to :care_giver_master
  belongs_to :patient

  def self.time_slots_history_on_given_date(date, start_time)
    time_stamp = "'#{start_time.strftime("%Y-%m-%d %H:%M")}'::timestamp"
    datetime_difference_in_time_query = "(DATE_PART('day', #{time_stamp} - time_to) * 24 + DATE_PART('hour', #{time_stamp} - time_to)) * 60 + DATE_PART('minute', #{time_stamp} - time_to) <= #{Enquiry::MAX_TIME_BREAK_FOR_DISTANCE_COVERAGE}"
    self.where(datetime_difference_in_time_query).where(:care_giver_master_id => Enquiry.select(:id).not_live_in.dated_on(date).order("time_to DESC"))
  end

  def decorated_time_from
    time_from.present? ? time_from.strftime("%H:%M") : ""
  end

  def decorated_time_to
    time_to.present? ? time_to.strftime("%H:%M") : ""
  end

  def addr(id)
    p 'aaaa'
    p id
    ad = Address.find_by_id(id)
    str = ad.line1.to_s
    str << ','
    str << ad.line2.to_s 
    str << ','
    str << ad.city.to_s 
    str << ','
    str << ad.state.to_s 
    str << ','
    str << ad.country.to_s 
    str << ','
    str << ad.pin_code.to_s
    return str
  end


  def first_name(id)
    p = Patient.find_by_id(id)
    return "#{p.first_name} #{p.last_name}"
  end

end
