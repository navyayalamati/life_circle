class PatientDisability < ActiveRecord::Base
  belongs_to :enrollment
  belongs_to :disability_master

  attr_accessor :assigned

  def disability_name
    disability_master.disability
  end
  
  def as_json(options = {})   
    super(:methods => [:disability_name, :assigned])   
  end
end
