class DrugMaster < ActiveRecord::Base
  validates :name, :presence => true
  has_many :drug_quantity_masters
  belongs_to :route_master
  belongs_to :form_master
  belongs_to :unit_master

  def route_name
    route_master.try(:name)
  end

  def form_name
    form_master.try(:name)
  end

  def unit_name
    unit_master.try(:name)
  end
  
  def as_json(options = {})   
    super(:methods => [:route_name,:form_name,:unit_name])   
  end
end
