class CareGiverMaster < ActiveRecord::Base
  validates :first_name, :presence => true
  # validates :telephone_no, :presence => true
  # validates :gender, :presence => true
  belongs_to :city_master
  JOB_TYPE_OPTIONS = [['Live In', "LIVE_IN"], ['Live Out', "LIVE_OUT"] , ['Both' , "BOTH"]]
  ELIGIBLE_AGE = 18
  CUT_OFF_QUALIFICATION = "BELOW_CLASS_8"
  
  
  belongs_to :address
  belongs_to :permanent_address, :class_name => "Address", :foreign_key => "permanent_address_id"
  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :permanent_address

 
  mount_uploader :id_proof_doc, DocumentUploader
  mount_uploader :address_proof_doc, DocumentUploader
  mount_uploader :profile_pic, AvatarUploader
  belongs_to :user
  belongs_to :status_changed_by, :class_name => "User", :foreign_key => "status_changed_by"
  has_many :care_giver_return_equipments ,  :dependent => :destroy

  has_one :care_giver_additional_detail, :dependent => :destroy
  has_many :care_giver_relatives, :dependent => :destroy
  has_many :care_giver_services, :dependent => :destroy
  has_many :care_giver_educations, :dependent => :destroy
  has_many :care_giver_documents, :dependent => :destroy
  has_many :care_giver_employers, :dependent => :destroy
  has_many :care_giver_availability_masters, :dependent => :destroy
  has_many :care_giver_languages, :dependent => :destroy
  has_many :care_giver_daily_attendances, :dependent => :destroy
  has_one :care_giver_charge, :dependent => :destroy
  has_many :requested_time_slots
  has_many :care_giver_leaves, :class_name => "CareGiverLeave"
  has_many :enquiry_care_givers
  has_many :enquiries, :through => :enquiry_care_givers
  has_many :daily_checks
  has_many :tickets
  attr_accessor :languages_known, :approximate_travel_distance

  def no_of_days_leave
    @date = Date.today
    leave = CareGiverLeave.where(:care_giver_master_id => self.id).where(:status => 'APPROVED').where("? <= to_date", @date)
    no_of_days = leave[0][:to_date] - leave[0][:from_date]
    no_of_days.to_i
  end

  def no_of_days_working
    self.id
  end
  
  def patient
   patient = Patient.select(:first_name).where(:id => Enquiry.select(:patient_id).where(:id => EnquiryCareGiver.select(:enquiry_id).where(:care_giver_master_id => self.id))).last
    patient.try([:first_name])
  end
  
  def risk_factor
    risk = EnrollmentSafetyViolation.select(:comments).where(:enrollment_id => Enrollment.select(:id).where(:enquiry_id => EnquiryCareGiver.select(:enquiry_id).where(:care_giver_master_id => self.id))) 
    if risk.exists?
      return "Low"
    else
      return "Nil"
    end
  end
  
  def no_of_days_on_bench
    date = EnquiryConversation.where(:tag => "Active").where(:conversation_master_id => self.id).where(:conversation_master_type => 'CareGiverEnquiry').last
    if date.present?
      return Date.today - date.created_at 
    else
      0
    end
    
  end

  def let_me_fill?
    self.mentioned_registration_type == "LET_ME_FILL"
  end 

  def name
    "#{first_name.to_s} #{last_name.to_s}"
  end

  def age
    dob.present? ? ((Date.today-dob)/365.0).to_i : "-"
  end

  def languages
    language_name_list.join(",")
  end

  def language_name_list
    self.care_giver_languages.map{|l| l.language_master.language}
  end

  def languages_html
    language_name_list.inject("<ul class='list1'>"){|result, l| "#{result}<li><i class='fa fa-chevron-circle-right'></i>#{l}</li>"}.concat("</ul>")
  end

  def languages_list
    @language_list ||= self.care_giver_languages.map(&:language_master_id)
  end

  def services
    services_list.join(", ")
  end

  def services_list
    care_giver_services.map{|service| service.service_master.service}
  end

  def services_html
    services_list.inject("<ul class='list1'>"){|result, s| "#{result}<li><i class='fa fa-chevron-circle-right'></i>#{s}</li>"}.concat("</ul>")
  end


  def professional_qualification_str
    professional_qualification_list.join(", ")
  end

  def professional_qualification_list
    care_giver_educations.proff_qualification.map{|ed| ed.exam_passed}
  end

  def professional_qualification_html
    professional_qualification_list.inject("<ul class='list1'>"){|result, pq| "#{result}<li><i class='fa fa-chevron-circle-right'></i>#{pq}</li>"}.concat("</ul>")
  end


  
  def education_details_str
    education_details_list.join(", ")
  end

  def education_details_list
    care_giver_educations.map{|ed| ed.exam_passed}
  end

  def education_details_html
    education_details_list.inject("<ul class='list1'>"){|result, ed| "#{result}<li><i class='fa fa-chevron-circle-right'></i>#{ed}</li>"}.concat("</ul>")
  end


  def total_charge
    care_giver_charge.try(:total_charge).to_i
  end

  def as_json(options={})
    super(:methods => [:name, :age, :total_charge])
  end

  def physical_description_approved?
    care_giver_additional_detail.try(:physical_description_approved?)
  end

  def health_approved?
    care_giver_additional_detail.try(:health_approved?)
  end

  def services_list_approved?
    (care_giver_services.present? and care_giver_services.map(&:approved?).all?)
  end

  def relatives_approved?
    (care_giver_relatives.present? and care_giver_relatives.map(&:approved?).all?)
  end

  def education_approved?
    (care_giver_educations.present? and care_giver_educations.map(&:approved?).all?)
  end

  def employers_approved?
    (care_giver_employers.present? and care_giver_employers.map(&:approved?).all?)
  end

  def charges_approved?
    care_giver_charge.try(:approved)
  end
 
  def check_children_approvals_and_update_self_approval
    children_approval_status = ( physical_description_approved? and health_approved? and services_list_approved? and relatives_approved? and education_approved? and employers_approved? and charges_approved?)
    self.update(:approved => children_approval_status)
  end
  
end
