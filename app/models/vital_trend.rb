class VitalTrend < ActiveRecord::Base
  belongs_to :enrollment
  belongs_to :key_vitals_master
  has_many :track_vitals
  
  def vital_name
    key_vitals_master.name
  end
  
  def vital_unit
      key_vitals_master.unit
  end
   
  def as_json(options = {})   
      super(:methods => [:vital_name, :vital_unit])   
  end

end
