class CareGiverCharge < ActiveRecord::Base
  validates :margin, presence: true
  validates :margin_type , presence: true
  before_save :calculate_and_update_total_charge
  belongs_to :care_giver_master


  def as_json(options)
    super(:methods => [:total_charge]);
  end

  private

  def calculate_and_update_total_charge
    if margin_type == 'PERCENTAGE'
      self.total_charge = (base_charge.to_i + (base_charge.to_i * (margin.to_f/100))).to_i
    else
      self.total_charge = (base_charge.to_i + margin.to_i)
    end
  end

  
end
