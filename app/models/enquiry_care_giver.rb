class EnquiryCareGiver < ActiveRecord::Base

  belongs_to :care_giver_master
  belongs_to :enquiry
  
  attr_accessor :adjustment_amount, :old_enquiry_care_giver_id, :consider_modified_charge
  
  scope :belongs_to_care_giver, lambda{|care_giver_id| where(:care_giver_master_id => care_giver_id)}

  def total_charge
    (caregiver_charge.to_i + transport_charge.to_i)
  end
end
