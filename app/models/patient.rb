class Patient < ActiveRecord::Base
  
  belongs_to :address, :dependent => :destroy
  belongs_to :guardian
  belongs_to :user
  has_many :daily_checks
  has_many :tickets
  accepts_nested_attributes_for :address
  
  scope :match_by_name, lambda{|term| where("(lower(patients.first_name) ILIKE '%#{term.downcase}%') OR (lower(patients.last_name) ILIKE '%#{term.downcase}%')")}

  def name
    "#{first_name} #{last_name}"
  end

  def age
    dob.present? ? ((Date.today-dob)/365.0).to_i : "-"
  end


  def address_str
    address.try(:full_address)
  end

  def build_patient_for_enrollment
    unless self.guardian.present?
      self.guardian = Guardian.new do |guardian|
        guardian.address = Address.new
      end
    else
      self.guardian
    end
    self.address = (self.address.present? ? self.address : Address.new)
  end
  
  def as_json(options = {})
    super(options)
  end
   
end
