class MenuBar < ActiveRecord::Base
  has_many :role_menu_mappings

  scope :matching_name, lambda{|name| where("name ILIKE ?", name)}
  scope :matching_tree, lambda{|name| where("id in (select parent_id from menu_bars where name ILIKE ?) or parent_id in (select parent_id from menu_bars where name ILIKE ?)", name, name)}

  def self.hirachichal_menu_bar(menu_name)
    query = "WITH RECURSIVE menu_nars_tree(id, name, url, parent_id, depth) AS (
 SELECT tn.id, tn.name, tn.url, tn.parent_id, 1::INT AS depth FROM menu_bars AS tn WHERE tn.id in (SELECT id from menu_bars where name ILIKE '#{menu_name}' limit 1)
UNION ALL                   
 SELECT c.id, c.name, c.url, c.parent_id, p.depth + 1 AS depth FROM menu_nars_tree AS p, menu_bars AS c WHERE c.id = p.parent_id
)                                                                
SELECT * FROM menu_nars_tree AS n order by id"
    MenuBar.connection.execute(query, :skip_logging)
  end

 
end
