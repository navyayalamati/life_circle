class TransportChargeMaster < ActiveRecord::Base
  validates :charge, :presence => true, numericality: true
  validates :from_distance, :presence => true, numericality: true
  validates :to_distance, allow_blank: true, numericality: true

  scope :having_distance, lambda{|distance| where("CASE WHEN (to_distance IS NULL OR to_distance <= 0)  THEN  #{distance}  >= from_distance ELSE #{distance}  >= from_distance and  #{distance} <= to_distance END")}
end
