// JavaScript Document
/* my Script */

$(function(){
	
	function window_height(){
		win_height = $(window).height();
		win_width = $(window).width();
		var div_height = $('.div-height').height();
		var headerHeight = $('header').height();
			$('.banner, #first-slide').css('height', win_height);
			$('.banner').css('margin-top', -100);
			$('#first-slide').css('width', win_width);
			$('.popupContainer').css('height', win_height);
			$('.popupContainer').css('width', win_width);
			$('.div-height-asign, .div-height').css('height', div_height);
			
			if($(window).width() > 768){
				$('.banner, .page_middle_table, .pgwSlideshow').css('height', win_height);
				$('.pgwSlideshow .ps-current > ul > li img').css('height', win_height);
				$('.krinss-banner .pgwSlideshow .ps-current > ul > li img').css('width', win_width);
				//$('section').css('min-height', win_height);
					}
				
				if($(window).width() <= 1024){
				$('.banner, #first-slide').css('height', "auto");
				$('section').css('min-height', "inherit");
				//$('.div-height-asign, .div-height').css('height', 'auto');
				
					}
					
				if($(window).width() <= 768){
				
				$('.div-height-asign, .div-height').css('height', 'auto');
					}

				}
		window_height(); 
		$(window).resize(function(){
    		window_height(); 
		});
		
		// anchor links
		$('#services .bg-green').click(function(){
			window.location.href='services.html'; 
			});
		
		
		
		// popup
		$('.login-popup').on('click', function(){
		$('#login-popup').show();
		$('.transparent-bg').show();
		$('body').addClass('');
		});
		
		$('#forgot-password').on('click', function(){
		$('#loginFieldsDiv').hide();
		$('#forgotPassword').show();
		
		});
		
		$('.cancel, .popup-close').on('click', function(){
		$(this).parents('.popupContainerFixed').hide();
		$('.transparent-bg').fadeOut(500);
		$('body').removeClass('');
		});
		
		$( document ).on( 'keydown', function ( e ) {
		if ( e.keyCode === 27 ) { // ESC
			$('.popupContainerFixed').hide();
			$('.transparent-bg').fadeOut(500);
			$('body').removeClass('overflow_none');
		}
		});
		
	$('.list-group-item').click(function(){
		$(this).removeClass('arrow-down');
		$(this).toggleClass('arrow-down');
		});
	
	// Details page slide show
      $("#slider3").responsiveSlides({
        manualControls: '#slider3-pager',
		auto: false
      });
	
	// Banner slidershow
     $("#banner_slider").responsiveSlides({
        speed: 800,
		pager: true
      });
	  
	  // testimonials
     $("#testimonial_slider").responsiveSlides({
        speed: 800,
		pager: true
      });
	  
	
		/* mobile navigation */
		$('.nav_mob_icon').on('click', function(){
			$('nav > ul').slideToggle();
			})
		  
		
	  	
	});
	

     




