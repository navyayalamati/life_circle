(function(angular, app) {
    "use strict";
    app.directive("timeofadministrationRenderer", function() {
	return {
	    restrict: 'E',
	    scope: {
                time1: '=',
                time2: '=',
                time3: '=',             
                slots: '=',
	    },
	    controller: ["$scope", function($scope){
                if(typeof $scope.slots != 'undefined' && $scope.slots.length > 0){
                }else{
                    $scope.slots = [{time1: "", time2: "", time3: ""}]
                }
                $scope.addMoreSlot = function(){
                    var newSlot = {}
                    angular.forEach($scope.slots[0], function(val, key){
                        newSlot[key] = ""
                    });
                    $scope.slots.push(newSlot)
                }

                $scope.removeTimeSlot = function(index){
                    if(index > 0){
                        $scope.slots.splice(index);
                    }
                }              
	    }],
	    template: "<div class='col-md-12' ng_repeat='slot in slots'><section class='col col-5'>"+
                "<label class='input'><input type='text' name='{{time1}}' placeholder='Time1' ng-model='slot.time' onFocus = 'getTimepicker(this)'/></label></section>"+
                "<section class='col col-5'><label class='input'><input type='text' name='{{time2}}' placeholder='Time2' ng-model='slot.time' onFocus = 'getTimepicker(this)'/></label>"+
                "<section class='col col-5'><label class='input'><input type='text' name='{{time3}}' placeholder='Time3' ng-model='slot.time' onFocus = 'getTimepicker(this)'/></label>"+
                "</section><section class='col col1' ng-show='$index > 0'><a href='#' title='close' ng-click ='removeTimeSlot($index)'><i class='fa fa-close fa-lg'></i></a></section></div>"+
                "<div class='col-md-4'><a href='#' ng-click='addMoreSlot()' class='button'>add More</a></div>"
	}
    });
})(angular, lifeCircleApp);
