(function(angular, app) {
    "use strict";
    app.directive("myDatepicker", ['$timeout', function($timeout){
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elem, attrs, ngModel){
                // timeout internals are called once directive rendering is complete
                
                var minDate = new Date()
                
                var maxDate = new Date()
                maxDate.setFullYear(maxDate.getFullYear()+1);
                if(typeof attrs.includePast != 'undefined'){
                    minDate = '1970/01/02'
                }
                if(typeof attrs.onlyTommorow != 'undefined'){
                    maxDate = '+1970/01/02'
                }
                
                $timeout(function(){                    
                    $(elem).datetimepicker({
                        timepicker:false,
                        format:'d/m/Y',
                        minDate: minDate,
                        maxDate: maxDate,
                        onSelect: function (dateText, inst) {
                            ngModel.$setViewValue(dateText);
                            ngModel.$render();
                        }
                    });
        });
            }
        };
    }]);
})(angular, lifeCircleApp);
