(function(angular, app) {
    "use strict";
    app.directive("timeSlotRenderer", function() {
	return {
	    restrict: 'E',
            transclude: false,
	    scope: {
		fromName: '=',
		toName: '=',
                slots: '=',
                invalidTimeSlot: '='
	    },
	    controller: ["$scope", function($scope){
                console.log($scope.invalidTimeSlot);
                if(typeof $scope.slots != 'undefined' && $scope.slots.length > 0){
                }else{
                    $scope.slots = [{time_from: "", time_to: ""}]
                    //alert(JSON.stringify($scope.slots))
                }
                $scope.addMoreSlot = function(){
                //alert("addmore")
                    var newSlot = {}
                    angular.forEach($scope.slots[0], function(val, key){
                        newSlot[key] = ""
                        //alert(key)
                    });
                    $scope.slots.push(newSlot)
                    //alert(JSON.stringify($scope.slots))
                }

                $scope.removeTimeSlot = function(index){
                    if(index > 0){
                        $scope.slots.splice(index);
                        $scope.validateSlots();
                    }
                }

                $scope.validateSlots = function(){
                    var minVal = null
                    var simpleDate = new Date()
                    $scope.invalidTimeSlot = false
                    angular.forEach($scope.slots, function(value, key){
                        if(value.time_to != null && typeof value.time_from != 'undefined'&& value.time_from.length > 1){
                            var fromDateParts = value.time_from.split(":")
                            var fromDateTime = new Date(simpleDate.getFullYear(), simpleDate.getMonth()-1, simpleDate.getDate(), fromDateParts[0], fromDateParts[1])
                            if(minVal == null){
                                minVal = fromDateTime
                            }
                            if(fromDateTime < minVal){
                                $scope.invalidTimeSlot = true
                                return false

                            }
                            minVal = fromDateTime
                        }else{
                            $scope.invalidTimeSlot = true
                            return false
                        }
                        if(value.time_to != null && typeof value.time_to != 'undefined' && value.time_to.length > 1){
                            var toDateParts = value.time_to.split(":")
                            var toDateTime = new Date(simpleDate.getFullYear(), simpleDate.getMonth()-1, simpleDate.getDate(), toDateParts[0], toDateParts[1])
                            if(minVal > toDateTime){
                                $scope.invalidTimeSlot = true
                                return false
                            }
                            minVal = toDateTime
                        }else{
                            $scope.invalidTimeSlot = true
                            return false
                        }
                    });
                }
	    }],
	    template: "<div class='col-md-12' ng_repeat='slot in slots'><section class='col col-5'>"+
                "<label class='input'><input type='text' name='{{fromName}}' placeholder='fromTime' ng-model='slot.time_from' ng-blur='validateSlots()' onFocus = 'getTimepicker(this)' my-tooltip tooltip='slot.time_from' tooltip-placement='bottom'/></label></section>"+
                "<section class='col col-5'><label class='input'><input type='text' name='{{toName}}' placeholder='toTime' ng-model='slot.time_to' ng-blur='validateSlots()' onFocus = 'getTimepicker(this)' my-tooltip tooltip='slot.time_to' tooltip-placement='bottom'/></label>"+
                "</section><section class='col col1' ng-show='$index > 0'><a href='#' title='close' ng-click ='removeTimeSlot($index)'><i class='fa fa-close fa-lg'></i></a></section></div><br><br/>"+
                "<div class='col-md-8'><span ng-show='invalidTimeSlot' class='error'> Invalid Time Slots</span></div><div class='col-md-4'><a href='#' ng-click='addMoreSlot()' class='button'>add More</a></div>"
	}
    });
})(angular, lifeCircleApp);

   
