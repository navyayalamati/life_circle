(function(angular, app) {
    "use strict";
    app.controller('welcomeEnquiryController',["$scope", "lifeCircleService", function($scope, lifeCircleService) {
	$scope.areas = []
	$scope.initCities = function(){
	    lifeCircleService.getCityMasters()
		.then(function(responce){
		    $scope.cities = responce.data
		    $scope.cities.push({name: "other", id:"OTHER"})
		})
	}

	$scope.fetchAreas = function(cityId){
	    $scope.areas.push({name:"other", id:"OTHER"})
	    if(cityId != 'OTHER'){
		lifeCircleService.getAreasInCity(cityId)
		    .then(function(responce){
			$scope.areas = responce.data
			$scope.areas.push({name:"other", id:"OTHER"})
		    })
	    }

	}

    }]);  
    
})(angular, lifeCircleApp);
