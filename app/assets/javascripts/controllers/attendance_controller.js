(function(angular, app) {
    "usestrict";
    app.controller('attendanceController',["$scope", "resources", "lifeCircleService" , "$http", "$window",function($scope, resources, lifeCircleService, $http, $window) {
        
	$scope.options = [{key:'W', value:'W'},
                          {key: 'L', value: 'L'}]
        $scope.pagination_start_at = 0;
        $scope.pagination_limit = 10;
        
                          
        $scope.att = function(){
            var url = "/care_giver_daily_attendances/attendance.json"
	    $http.post(url,{ attendance : attendance })
		.then(function(response){
                    $scope.att = response.data
                })
        }
	$scope.start = function(){
            var url = "/care_giver_daily_attendances/start.json"
	    $http.post(url,{ attendance : attendance })
		.then(function(response){
                    $scope.start = response.data
                })
        }
	$scope.atten = function(){
            var url = "/care_giver_daily_attendances/start.json"
	    $http.post(url,{ attendance : attendance })
		.then(function(response){
                    $scope.caregivers = response.data
                })
        }
	$scope.enq = function(){
            var url = "/care_giver_daily_attendances/enquiries.json"
	    $http.post(url,{ enquiry : enquiry })
		.then(function(response){
                    $scope.enqs = response.data
                })
        }
	
        $scope.get_payroll = function(from , to){
            $scope.from = from
            $scope.to = to
            lifeCircleService.payroll(from , to)
		.then(function(result){
		    $scope.reports = result.data
		    alert(JSON.stringify($scope.reports))
		});
	}


        $scope.amount = function(id){
            var url = "/care_giver_daily_attendances/amount.json"
	    $http.post(url,{ id : id , from : $scope.from , to : $scope.to  })
		.then(function(response){
                    $scope.amounts = response.data
                    $('#amountModal').modal('show');
                })
        }
        

	$scope.today_attendance = function(){
            var url = '/care_giver_daily_attendances/today_attendance.json'
            $http.get(url)
                .then(function(responce){
                    $scope.values = responce.data
		    $scope.TodayAttendance = true;
                })
        }

        


        $scope.date_click = function(key){
            var url = '/care_giver_daily_attendances/today_attendance.json?date='+key
            $http.get(url)
                .then(function(responce){
                    $scope.real_values = responce.data
		    $('#myModal').modal('show');
                })
        }

	$scope.update = function(date , id){
	    var url = "/care_giver_daily_attendances/update_attendance.json"
	    $http.post(url,{ id : id , date : date })
		.then(function(response){
                    //alert('')
		    $scope.times = response.data
		    //alert(JSON.stringify($scope.times))
		    $scope.date = date
                    $scope.id = id
                    
                    $('#getModal').modal('show');
                }) 
	}
	
        $scope.attendance_of_cg = function(id){
            var url = "/care_giver_daily_attendances/attendance_of_cg.json"
	    $http.post(url,{ id : id , from_date : $scope.from_date , to_date : $scope.to_date })
		.then(function(response){
                    //alert('')
		    $scope.cgs = response.data
		    $('#cgModal').modal('show');
                }) 
        }

        
        
        $scope.save_attendance = function(){
            //alert(JSON.stringify($scope.times))
            
            var url = "/care_giver_daily_attendances/save_attendance.json"
            $http.post(url,{ times : $scope.times  , date : $scope.date , id : $scope.id })
		.then(function(response){
		    $scope.times = response.data
		    //alert(JSON.stringify($scope.times))
		    //$('#getModal').modal('show');
                }) 

        }
	


	lifeCircleService.all_cg()
	    .then(function(result){
		$scope.care_giver_masters = result.data
	    });
	
	$scope.get_attendance = function(){
	  
	    $scope.TodayAttendance = false;
	    $scope.getAttendance = true;
	  
	    lifeCircleService.from_to_date($scope.from_date , $scope.to_date )
		.then(function(response){
		    $scope.reports = response.data
                    
                    $scope.pagination_start_at = 0;
                    var pagination_end_at = ($scope.reports[0].attendance_details.length > $scope.pagination_limit  ? $scope.pagination_limit : $scope.reports[0].attendance_details.length);
                    $scope.total_date_headers = $scope.reports[0].attendance_details.length;
                    $scope.end_limit = getPaginationEndDate()
                    processChangedDates($scope.pagination_start_at, $scope.end_limit)
		});
	}

        $scope.getAttendaceDetailsAtIndex = function(index, details){
            if(typeof details[index] != 'undefined'){
                return details[index].status
            }
            
        }

        $scope.previousDates = function(){
            $scope.pagination_start_at = ($scope.pagination_start_at - $scope.pagination_limit)
            if($scope.pagination_start_at <= 0){
                $scope.pagination_start_at = 0;
            }
            $scope.end_limit = getPaginationEndDate()
            
            processChangedDates($scope.pagination_start_at, $scope.end_limit)
        }

        $scope.nextDates = function(){
            $scope.pagination_start_at  = ($scope.pagination_start_at + $scope.pagination_limit)

            if($scope.pagination_start_at >= $scope.total_date_headers){
                return false;
            }
            $scope.end_limit = getPaginationEndDate()
            processChangedDates($scope.pagination_start_at, $scope.end_limit)
        }

        var processChangedDates = function(lower_limit, upper_limit){
            $scope.headers = []
            for(var i = lower_limit; i < upper_limit; i++){
                $scope.headers.push($scope.reports[0].attendance_details[i].date);
            }
        }

        var getPaginationEndDate = function(){
            var end_limit = ($scope.pagination_start_at + $scope.pagination_limit)
            if(end_limit >= $scope.total_date_headers){
                end_limit = $scope.total_date_headers
            }
            return end_limit;
        }


    }]);  
    
})(angular, lifeCircleApp);
