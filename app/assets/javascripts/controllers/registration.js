(function(angular, app) {
    "use strict";
    app.controller('RegistrationController',["$scope", "$http", "$sce", function($scope, $http, $sce) {
        $scope.registrars = [{key: 'Patient', value: "PATIENT"}, {key:'Relative', value:"RELATIVE"}]
        $scope.user = {role: "patient", first_name: "", email: "", mobile_no: "", user_name: "", password: "", password_confirmation: "", registered_by: ""}
        $scope.validateUser = function(userName){
            $scope.invalidUserIcon = null
            console.log($scope.user)
            if(typeof userName == 'undefined' || userName.length <= 1){
                $scope.invalidUserIcon = 'show'
                $scope.invalidUserName = true;
                return false
            }
            var url = "/check_user_exists.json?user_name="+userName
            $http.get(url).then(function(response){
                console.log(response.data)
                if(response.data.availability){
                    $scope.invalidUserIcon = 'hide'
                    $scope.invalidUserName = false;
                }else{
                    $scope.invalidUserIcon = 'show'
                    $scope.invalidUserName = true;
                }
                console.log(response.data)
            })
        }
        
        $scope.register = function(){
            var url = "/users.json"
            $http.post(url, {user: $scope.user}).
                success(function(response){
                    $scope.$parent.bookCareGiver()
                    $scope.error = false
                    
                }).
                error(function(data, status, headers, config) {
                    console.log(data)
                    $scope.error = true
                    var error_str = ""
                    angular.forEach(data.errors, function(value, key){
                        error_str += (key+": "+value+" <br/>")
                    });
                    console.log(error_str)
                    $scope.error_msg = $sce.trustAsHtml(error_str)
                });
        }
        
    }]);  
    
})(angular, lifeCircleApp);
