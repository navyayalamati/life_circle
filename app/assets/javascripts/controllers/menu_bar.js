(function(angular, app) {
    "use strict";
    app.controller('MenuController',["$scope", "lifeCircleService", "$location", "$state",  "$rootScope", function($scope, lifeCircleService, $location, $state,  $rootScope) {
        var initiateForm = function(){
            $scope.myShowFormValue = true;
            $scope.myFormValue = false;
	    $scope.myEditValue = false;
        };
        initiateForm();

	
	lifeCircleService.role()
	    .then(function(result){
		$scope.roles = result.data
	    });
	
	lifeCircleService.role_menu()
	    .then(function(result){
		$scope.role_menus = result.data
		//alert($scope.role_menus)
	    });
	
	$scope.MenuMappings = function(){
	    $scope.myFormValue = true;
	    $scope.myShowFormValue = false;
            $scope.myEditValue = false;
	}

	lifeCircleService.menu_bar()
	    .then(function(result){
		$scope.menus = result.data
		$scope.menu_array = []
		//alert(JSON.stringify($scope.menus))
		
	    });
	lifeCircleService.getCityMasters()
	    .then(function(result){
		$scope.city_masters = result.data
	    });


	
	
	$scope.editMenuMappings = function(role ){
	    
	    //alert(JSON.stringify(role))
	    $scope.myFormValue = false;
	    $scope.myShowFormValue = false;
	    $scope.myEditValue = true;
	    lifeCircleService.corresponding_menu(role)
		.then(function(result){
		    $scope.menu_arrays = result.data
		    //alert(JSON.stringify($scope.menu_arrays))
		    // for ( var i = 0; i < $scope.menus.length; i++ ){         
		// 	for ( var j = 0; j < $scope.menu_arrays.length; j++ ) {                   
		// 	    if  ($scope.menus[i]['id'] == $scope.menu_arrays[j]['menu_bar_id'] )
		// 	    {
		// 		$scope.main_menu.checked_value = true;                              
		// 	    }    
		// 	}
		//}            
		});
	}
	

	$scope.edit_menu = function(){
	    //alert(JSON.stringify($scope.menu_arrays))
	    lifeCircleService.update_menu($scope.menu_arrays)
	    	.then(function(result){
		    $scope.menu_arrays = result.data
		    window.location.reload()
		    initiateForm();
		});
	}
	
	$scope.menu_bar = function(id){
	    lifeCircleService.getMenu(id)
		.then(function(result){
		    $scope.bars = result.data
		    //alert(JSON.stringify($scope.bars))
		});
	}
	
	$scope.checkAll = function(y){
	    //alert(JSON.stringify(y.mark_as_parent))
	    if(y.mark_as_parent == 'YES')
	    {
		
	    }
	}
	
	$scope.addArray = function(checked_value , id){
	    
	    if (!checked_value)
	    {
		for(var i = 0; i < $scope.menu_array.length; i++)
		{
		    if ($scope.menu_array[i]['id'] == id)
                    {                                     
                        $scope.menu_array.splice(i, 1);
                    }                   
                }
	    } 
	    
            else
            {
		
                $scope.menu_array.push({
                    id: id,
                });  
		
            } 
            //alert(JSON.stringify($scope.menu_array))
        };    
	
	$scope.save_menu = function(menu){
	    //alert()
	    //alert(JSON.stringify($scope.menu_array))
	    lifeCircleService.save_menu(menu ,$scope.menu_array )
		.then(function(result){
		    $scope.city_masters = result.data
		    initiateForm();
		    window.location.reload();

		});
	}
        
    }]);  
    
})(angular, lifeCircleApp);
