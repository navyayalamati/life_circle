(function(angular, app) {
    "use strict";
    app.controller('EnrollmentController',["$scope", "lifeCircleService", "$state", "$http",  "$window",function($scope, lifeCircleService, $state, $http, $window){
        $scope.enrollment = null
        $scope.availabilityStatus = true
        $scope.onlyNumbers = /^\d+$/;
        $('#patientDetailId').bind('railsAutocomplete.select', function(event, data){
            $("#selectedUserId").val(data.item.id)
        });

	$scope.status = [{key: "OPEN", value: "OPEN"}, { key: "CLOSE", value:"CLOSE"}]

        $scope.fetchPatientDetails = function(){
            var user_id = $("#selectedUserId").val()
            lifeCircleService.getPatientDetailsWithUserId(user_id)                
                .then(function(responce){
                    $scope.enrollment.patient = responce.data
        
                });
        }
	
	lifeCircleService.getSuperCareGiver()                
            .then(function(responce){
                $scope.super_care_givers = responce.data
		
            });

        lifeCircleService.getReasons()                
            .then(function(responce){
                $scope.reason_for_comings = responce.data
		
            });
	
	$scope.auditState = function(){
            lifeCircleService.updateAuditEnquiry($scope.enquiry)
                .then(function(responce){
                    if(responce.data.status == true){
                        alert('Enrollemnt has been moved to Audit')
                        $window.location.href = '/enquiries'
                    }
                });
        }
        
	$scope.incomplete_pcp = function(){
	    lifeCircleService.incomplete_pcp()
                .then(function(responce){
		    $scope.incomplete_pcp = responce.data
		    
                });  
	    
	}

	$scope.all_appointments = function(){
	    lifeCircleService.all_appointments()
                .then(function(responce){
		    $scope.all_appointments = responce.data
		    
                });  
	}
	
	$scope.edit = function(id){
	    lifeCircleService.edit_values(id)
                .then(function(responce){
		    $scope.update_enquiries = responce.data[0]
		    
		});  
	}
	
	$scope.save_appointment = function(){
	    lifeCircleService.update_appointment($scope.update_enquiries)
	    	.then(function(response){
		    $scope.appointments = response.data
		    $window.location.href  = "/enquiries/appointments"
		});
	}
	
        $scope.newEnrollment = function(enquiryId){
            $scope.enquiryId = enquiryId
            lifeCircleService.newEnrollment($scope.enquiryId)
                .then(function(responce){
                    $scope.enrollment = responce.data.enrollment
		    $scope.enquiry = $scope.enrollment.enquiry;
                    $scope.enrollment.patient.first_name = $scope.enrollment.enquiry.requested_user_name
                    $scope.enrollment.patient.mobile_no = $scope.enrollment.enquiry.mobile_no
                    $scope.enrollment.email_for_official_comminication = $scope.enrollment.enquiry.email
                    angular.forEach($scope.enquiry.requested_time_slots, function(time_slot){
                        time_slot.time_from = time_slot.decorated_time_from
                        time_slot.time_to = time_slot.decorated_time_to
                    });
                    $scope.enquiry.place_details = $scope.enrollment.patient.address_str
                    $scope.loadDisabilities()
                    $scope.loadServices();
                    $scope.loadLanguages();
                });
        }

        $scope.checkCgAvailabilityOnModifiedQuery = function(enquiry){
            lifeCircleService.checkCgAvailabilityOnModifiedQuery($scope.enquiry)
                .then(function(responce){
                    $scope.availabilityStatus = responce.data.availablity
                });
            
        }

        $scope.updateEnquiry = function(){
            $scope.errors = []
            lifeCircleService.updateEnquiry($scope.enquiry)
                .then(function(responce){
                    if(responce.data.status == true){
                        $scope.enquiryUpdated = true
                        $scope.enquiryUpdatesuccess = true
                        $scope.newEnrollment($scope.enquiry.id);
                    }else{
                        console.log("error")
                        $scope.enquiryUpdated = true
                        $scope.enquiryUpdatesuccess = false
                        $scope.errors =  responce.data.errors
                    }
                });
        }

        $scope.loadServices = function(){
            var reg_service_ids = []
            angular.forEach($scope.enquiry.requested_services, function(service){
                reg_service_ids.push(service.service_master_id)
            })

            lifeCircleService.getServiceMasters()
                .then(function(responce){
                    $scope.enquiry.services =  responce.data
                    console.log(reg_service_ids)
                    angular.forEach($scope.enquiry.services, function(service){
                        if(reg_service_ids.indexOf(service.id) != -1){
                            service.assigned = true
                        }
                    })
                    
                });
        }

        $scope.loadLanguages = function(){
            var reg_language_ids = []
            angular.forEach($scope.enquiry.enquiry_languages, function(lang){
                reg_language_ids.push(lang.language_master_id)
            })
            lifeCircleService.getLanguageMasters()
                .then(function(responce){
                    $scope.enquiry.languages =  responce.data
                    angular.forEach($scope.enquiry.languages, function(lang){
                        if(reg_language_ids.indexOf(lang.id) != -1){
                            lang.assigned = true
                        }
                    })
                    
                });
        }

        
        $scope.loadDisabilities = function(){
            lifeCircleService.getDisabilityMasters()
                .then(function(responce){
                    $scope.enrollment.disabilities = responce.data
                    console.log($scope.enrollment.disabilities)
                });
        }

        $scope.saveEnrollement = function(){
            $scope.patientFormSubitted = true
            if ($scope.EnrollmentInputForm.$valid){
                lifeCircleService.createEnrollment($scope.enquiryId, $scope.enrollment)
                    .then(function(responce){
                        if(responce.data.status == true){
                            $window.location.href = "/enrollments/"+responce.data.enrollment_id+"/payment_masters/revise_care_giver_charge"
                        }else{
                            alert("Some error prevented enrollment saving")
                        }

                    });
            }
        }
        
       $scope.gotoStep0 = function(){
            $scope.collapseOneShown = true
            $scope.collapseTwoShown = false
            $scope.collapseThreeShown = false
            $("#collapseOne").collapse('toggle')
            if($("#collapseTwo").hasClass("in")){
                $("#collapseTwo").collapse('hide')
            }
            if($("#collapseThree").hasClass("in")){
                $("#collapseThree").collapse('hide')
            }

        }

       $scope.checkSafety = function(){
           $scope.safetyCheckSubmitted = true;
           //$scope.EnrollmentInputForm.patient_home_setting.$error.required ||
           var validStatus = ( $scope.EnrollmentInputForm.cg_work_alone_with_patient.$error.required || $scope.EnrollmentInputForm.suffering_from_infection_disease.$error.required || $scope.EnrollmentInputForm.safe_and_well_connected_locality.$error.required || $scope.EnrollmentInputForm.provides_personal_protective_equipment.$error.required || $scope.EnrollmentInputForm.family_member_helps.$error.required)
           console.log(validStatus)
           if(typeof validStatus == 'undefined' || validStatus == false){
               var safety_params = { patient_home_setting: $scope.enrollment.patient_home_setting, safe_and_well_connected_locality: $scope.enrollment.safe_and_well_connected_locality, suffering_from_infection_disease: $scope.enrollment.suffering_from_infection_disease, cg_work_alone_with_patient: $scope.enrollment.cg_work_alone_with_patient, provides_personal_protective_equipment: $scope.enrollment.provides_personal_protective_equipment, family_member_helps: $scope.enrollment.family_member_helps}
	      
               lifeCircleService.safetyMeasures($scope.enrollment.enquiry.id, safety_params)
               
                   .then(function(responce){
                       //alert(JSON.stringify($scope.safetyMeassures))
                       $scope.safetyMeassures = responce.data.safety_measures                       
                       console.log($scope.safetyMeassures)
                       if(responce.data.safety == false){
                           $("#modal").modal("show")
                       }else{
                           
                           $scope.collapseOneShown = false
                           $scope.collapseTwoShown = true
                           $scope.collapseThreeShown = false
                           $scope.isStep1Submitted = true
                           $("#collapseOne").collapse('hide')
                           $("#collapseTwo").collapse('show')
                           if($("#collapseThree").hasClass("in")){
                               $("#collapseThree").collapse('hide')
                           }
                       }
                   });
           }
       }
        
        $scope.gotoStep1 = function(){
            $("#enrollment-safety-pop-up").modal("hide")
            $scope.collapseOneShown = false
            $scope.collapseTwoShown = true
            $scope.collapseThreeShown = false
            $scope.isStep1Submitted = true
            $("#collapseOne").collapse('hide')
            $("#collapseTwo").collapse('show')
            if($("#collapseThree").hasClass("in")){
                $("#collapseThree").collapse('hide')
            }
       }

        

        $scope.gotoStep2 = function(){
            $scope.isDisabilitySelectionEmpty = true
            var assignedAtleastOne = 0
            angular.forEach($scope.enrollment.disabilities, function(disability){
                if(typeof disability.assigned != 'undefined' && disability.assigned == true){
                    assignedAtleastOne += 1;
                }
            });
            
           if(assignedAtleastOne > 0){
               $scope.isDisabilitySelectionEmpty = false
               $scope.collapseThreeClicked = true
               $scope.collapseOneShown = false
               $scope.collapseTwoShown = false
               $scope.collapseThreeShown = true
               $scope.isStep2Submitted = true

               if($("#collapseOne").hasClass("in")){
                   $("#collapseOne").collapse('hide')
               }
               if($("#collapseTwo").hasClass("in")){
                   $("#collapseTwo").collapse('hide')
               }
                
                $("#collapseThree").collapse('toggle')

           }else{
               $scope.isDisabilitySelectionEmpty = true
           }
        }
        
        $scope.modify_enquiry = function(){
            $("#enquiryModificationDiv").modal()
            $state.go("service_request")
        }
               
        $scope.changeEnrollmentPlan = function(enrollmentId){
            $scope.enrollmentId = enrollmentId
            lifeCircleService.getEnrollment(enrollmentId)
                .then(function(responce){
                    $scope.enrollment = responce.data.enrollment
                    $scope.enquiry = $scope.enrollment.enquiry;
                    angular.forEach($scope.enquiry.requested_time_slots, function(time_slot){
                        time_slot.time_from = time_slot.decorated_time_from
                        time_slot.time_to = time_slot.decorated_time_to
                    });
                    $scope.enquiry.place_details = $scope.enrollment.patient.address_str
                    $scope.loadDisabilities()
                    $scope.loadServices();
                    $scope.loadLanguages();
                });
        }


        $scope.updateChangePlan = function(){
            alert('hello')
            lifeCircleService.changeEnrollmentPlan($scope.enrollment.id, $scope.enquiry)
                .then(function(responce){
                    if(responce.data.status){
                        $window.location.href = "/enrollments/"+$scope.enrollment.id
                    }else{
                        alert("Cannot update")
                    }
                    
                });
            
        }

        $scope.calculatePatientAge = function(dob){
            var ageDifMs = Date.now() - Date.parse(dob);
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            $scope.enrollment.patient.age = Math.abs(ageDate.getUTCFullYear() - 1970);
        }
        
    }]);  
})(angular, lifeCircleApp);


