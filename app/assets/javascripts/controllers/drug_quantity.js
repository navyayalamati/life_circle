(function(angular, app) {
    "usestrict";
    app.controller('DrugQuantityController',["$scope", "$filter", "resources", "$window",function($scope, $filter, resources, $window) {
        $scope.success = false
        $scope.warning = false
        $scope.alertMessage = ""
        $scope.quantity = 0
        $scope.dose = 0
        $scope.drug_quantity_master_id
        $scope.loaddrugquantityMasters = function(enrollment_id){
            //alert("load")
            $scope.enrollment_id = enrollment_id 
           $scope.drugquantitymasters = resources.DrugQuantity.query({enrollment_id:enrollment_id});
        }
        
        $scope.newdrugquantitymaster = function(){               
            $scope.drugquantitymaster = new resources.DrugQuantity({enrollment_id: $scope.enrollment_id})
            $scope.drugquantitymaster.isNew = true 
            // var date = new Date();
            // $scope.drugquantitymaster.refill_date = $filter('date')(date, "dd/MM/yyyy");
            //$scope.trackvital.entered_at = (new Date).toLocaleFormat("%m %e, %Y");
            //alert(true)
            $scope.drugquantitymaster.slots = [{time1: "", time2: "", time3: ""}]   
        }
        
        $scope.changeRefillDate = function(q,d){
            //someDate = new Date()
            //someDate.setDate(someDate.getDate() + )
            //alert(someDate)
            if(q != 0){
                $scope.quantity = q}
            if(d != 0){
                $scope.dose = d}
            if($scope.quantity != 0 && $scope.dose != 0){
                //alert($scope.quantity + $scope.dose)
                //var v = $window.Math.round($scope.quantity / $scope.dose);
                //alert(v)
                var date = new Date()
                date.setDate(date.getDate() + $window.Math.round($scope.quantity / $scope.dose) + 1 )
                $scope.drugquantitymaster.refill_date = $filter('date')(date, "dd/MM/yyyy");
            }
        }
        
        $scope.close = function(){
             $scope.drugquantitymaster.isNew  = false;
        }
        
        $scope.removeTimeSlot = function(index){
            if(index > 0){
                $scope.drugquantitymaster.slots.splice(index);
            }
        }
        
        $scope.addMoreSlot = function(){
            //alert("addmore")
            var newSlot = {}
            angular.forEach($scope.drugquantitymaster.slots[0], function(val, key){
                newSlot[key] = ""
                //alert(key)
            });
            $scope.drugquantitymaster.slots.push(newSlot)
            //alert(JSON.stringify($scope.slots))
        }

        $scope.time_of_admins = function(id){
            $scope.drug_quantity_master_id = id
            $('#timeModal').modal('show')
            $scope.loadTimeofadmins(id)
        }
        
        $scope.closeupdate = function(drugquantitymaster){
            drugquantitymaster.edit = false;
        }
        
        $scope.edit = function(drugquantitymaster){
            drugquantitymaster.edit = true;
        }

        $scope.save = function(){
        	//alert()
            //alert($('#id').val())
            $scope.drugquantitymaster.drug_master_id = $("#id").val();
            // if($scope.drugquantitymaster.check){
            //     alert($scope.drugquantitymaster.check)
            //     var date = new Date();               
            //     alert(date)
            //     alert(date.setYear(date.getFullYear(2115)))
            //     $scope.drugquantitymaster.review_date =  date.setYear(date.getFullYear() + 1);
            //     alert($scope.drugquantitymaster.review_date)
            // }
            $scope.drugquantitymaster.$save()
                .then(function(value) {
                    //alert("value")
                    $scope.success = true
		    $scope.alertMessage = "Save Successfull"
                    $scope.loaddrugquantityMasters($scope.enrollment_id);
                }, function(reason) {
                   $scope.warning = true
		   $scope.alertMessage = "Error!"
                    // handle failure
                });
        }
        
        $scope.update = function(drugquantitymaster){
            drugquantitymaster.$update()
                .then(function(value) {
                    drugquantitymaster.edit = false;
                    $scope.success = true
		    $scope.alertMessage = "Update Successfull"
                    $scope.loaddrugquantityMasters($scope.enrollment_id);
                }, function(reason) {
                   $scope.warning = true
		   $scope.alertMessage = "Error!"
                    // handle failure
                });
        }

        
        
        $scope.destroy = function(drugquantitymaster){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                drugquantitymaster.$delete()
                    .then(function(responce){
                        $scope.drugquantitymasters.splice($scope.drugquantitymasters.indexOf(drugquantitymaster), 1)
                        $scope.success = true
		    $scope.alertMessage = "Delete Successfull"
                    }, function(reason) {
                       $scope.warning = true
		       $scope.alertMessage = "Error!"
                    // handle failure
                });
            };
        }

        $scope.loadTimeofadmins = function(id){
            $scope.drug_quantity_master_id = id
            //alert(id)
            $scope.timeofadmins = resources.Timeofadmin.query({drug_quantity_master_id: $scope.drug_quantity_master_id});
            console.log($scope.timeofadmins)
        }
        
        $scope.newTimeofadmin = function(){
            //alert($scope.drug_quantity_master_id)
            $scope.timeofadmin = new resources.Timeofadmin({time: null, drug_quantity_master_id: $scope.drug_quantity_master_id})
            $scope.timeofadmin.isNew = true
        }

        $scope.edittime = function(timeofadmin){
            timeofadmin.edit = true;
        }

        $scope.updatetime = function(timeofadmin){
            timeofadmin.$update()
                .then(function(value) {
                    timeofadmin.edit = false;
                    $scope.loadTimeofadmins($scope.drug_quantity_master_id);
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.savetime = function(){
            $scope.timeofadmin.$save()
                .then(function(value) {
                    $scope.loadTimeofadmins($scope.drug_quantity_master_id);
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroytime = function(timeofadmin){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                timeofadmin.$delete()
                    .then(function(responce){
                        $scope.timeofadmins.splice($scope.timeofadmins.indexOf(timeofadmin), 1)
                    })
            };
        }
        
    }]);  
    
})(angular, lifeCircleApp);
