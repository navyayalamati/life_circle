(function(angular, app) {
    "use strict";
    app.controller('LanguageController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadLanguages = function(){
            $scope.languages = resources.Language.query();
        }
        
        $scope.newLanguage = function(){
            $scope.language = new resources.Language({language: null})
            $scope.language.isNew = true
        }

        $scope.edit = function(language){
            language.edit = true;
        }

        $scope.update = function(language){
            language.$update()
                .then(function(value) {
                    language.edit = false;
                    $scope.loadLanguages();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.language.$save()
                .then(function(value) {
                    $scope.loadLanguages();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(language){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                language.$delete()
                    .then(function(responce){
                        $scope.languages.splice($scope.languages.indexOf(language), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
