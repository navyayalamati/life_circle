(function(angular, app) {
    "usestrict";
    app.controller('RequestedservicesController',["$scope" ,"lifeCircleService", "resources", function($scope,lifeCircleService, resources) {
        $scope.success = false
        $scope.warning = false
        $scope.services =[]
        $scope.not_saved = []
        $scope.alertMessage = ""
        $scope.loadServices = function(id){
            //alert(id)
            $scope.enrollment_id = id
            lifeCircleService.getRequestedservices($scope.enrollment_id)
		.then(function(result){
		    $scope.services  = result.data		    
                    console.log($scope.services)
		});           
        }

        $scope.save = function(){
            //alert(JSON.stringify($scope.services))
            lifeCircleService.saveRequestedservices($scope.enrollment_id, $scope.services)
		.then(function(result){                    
		    $scope.services  = result.data.requested_services
		    $scope.not_saved  = JSON.stringify(result.data.not_saved)
		    
		    if($scope.not_saved.length != 0)
		    {
		      $scope.warning = true
		      $scope.success = false
		    }
		    else{
		     $scope.success = true
		     }
                    $scope.loadServices($scope.enrollment_id)
                    //alert(JSON.stringify(result.data))
                    console.log($scope.services)
		}, function(reason) {
			$scope.warning = true
			$scope.loadServices($scope.enrollment_id)
                    // handle failure
                });
        }

        // $scope.update = function(equipment){
        //     //alert(JSON.stringify(abilities))
        //     lifeCircleService.updateRequestedservices($scope.enrollment_id, $scope.services)
	// 	.then(function(result){
        //             equipment.edit = false
        //            // alert(result.data)
	// 	    $scope.updateDisabilitie = result.data
	// 	});
        // }
       
        
    }]);  
    
})(angular, lifeCircleApp);
