(function(angular, app) {
    "use strict";
    app.controller('DisabilityController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadDisabilities = function(){
            $scope.disabilities = resources.Disability.query();
        }
        
        $scope.newDisability = function(){
            $scope.disability = new resources.Disability({disability: null})
            $scope.disability.isNew = true
        }

        $scope.edit = function(disability){
            disability.edit = true;
        }

        $scope.update = function(disability){
            disability.$update()
                .then(function(value) {
                    disability.edit = false;
                    $scope.loadDisabilities();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.disability.$save()
                .then(function(value) {
                    $scope.loadDisabilities();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(disability){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                disability.$delete()
                    .then(function(responce){
                        $scope.disabilities.splice($scope.disabilities.indexOf(disability), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
