(function(angular, app) {
    "use strict";
    app.controller('careGiverChargeBucketsController',["$scope", "lifeCircleService", "$window", function($scope, lifeCircleService, $window) {
        
        $scope.loadBuckets = function(){
            lifeCircleService.loadCareGiverChargeBuckets()
                .then(function(response){
                    console.log(response.data)
                    $scope.charge_buckets = response.data

                })
        }

        $scope.saveBuckets = function(){
            if(validate()){
                lifeCircleService.saveCareGiverChargeBuckets($scope.charge_buckets)
                    .then(function(response){
                        $window.location = "/care_giver_charge_buckets"
                    })
            }else{
                alert("Invalid Ranges")
            }
        }

        $scope.addMore = function(){
            $scope.charge_buckets.push({hrs_from: null, hrs_to:null, charge_percentage: null})
        }

        var validate = function(){
            var status = false;
            var minVal = null
            for(var i = 0 ; i < $scope.charge_buckets.length; i++){
                var value = $scope.charge_buckets[i];
                console.log(value)
                if(value.hrs_to != null && typeof value.hrs_from != 'undefined'&& value.hrs_from != null){
                    if(minVal == null){
                        minVal = value.hrs_from
                    }
                    if(value.hrs_from < minVal){
                        return false

                    }
                    minVal = value.hrs_from
                }else{
                    status = true
                }
                if(value.hrs_to != null && typeof value.hrs_to != 'undefined'){
                    if(minVal > value.hrs_to){
                        return false
                    }
                    minVal = value.hrs_to
                }else{
                    status = true
                }
            };
            return status;
        }
        
    }]);  
    
})(angular, lifeCircleApp);

