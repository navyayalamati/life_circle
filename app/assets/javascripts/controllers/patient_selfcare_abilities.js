(function(angular, app) {
    "usestrict";
    app.controller('PatientSelfcareAbilitesController',["$scope" ,"lifeCircleService", "resources", function($scope,lifeCircleService, resources) {        
        $scope.abilities =[]
        $scope.loadSelfcares = function(id){
            //alert(id)
            $scope.enrollment_id = id
            lifeCircleService.getPatientAbilities($scope.enrollment_id)
		.then(function(result){
		    $scope.abilities = result.data
                    console.log($scope.abilities)
		});           
        }

        $scope.newloadSelfcares = function(id){
            $scope.selfcares = resources.Selfcare.query();            
        
        $scope.selfcares.$promise.then(function(data){
            $scope.selfcares = data;
            //console.log(data)
            $scope.newPatientSelfcareAbilities()
        })
        }
        
        
        $scope.newPatientSelfcareAbilities = function(){
            angular.forEach($scope.selfcares, function(selfcare){
                $scope.ability = { selfcare_masters_id: selfcare.id, name: selfcare.selfcare_ability, independent: false, description: ''}
                $scope.abilities.push($scope.ability)                 
            });
                
        }

        $scope.save = function(){
            //alert($scope.enrollment_id)
            lifeCircleService.savePatientAbilities($scope.enrollment_id, $scope.abilities)
		.then(function(result){
		    $scope.saveabilities = result.data		    
		}, function(reason) {			
                    // handle failure
                });
        }

        $scope.update = function(ability){
            //alert(JSON.stringify(abilities))
            lifeCircleService.updatePatientAbilities($scope.enrollment_id, ability)
		.then(function(result){
                    ability.edit = false                    
                   // alert(result.data)
		    $scope.updateabilities = result.data
		}, function(reason) {			
                    // handle failure
                });
        }
       
        
    }]);  
    
})(angular, lifeCircleApp);
