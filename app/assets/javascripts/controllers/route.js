(function(angular, app) {
    "usestrict";
    app.controller('RouteController',["$scope", "resources", "$window", "$http",function($scope, $http ,resources, $window) {
        
        $scope.loadRoutes = function(){
            var url = "/route_masters.json"
	    $http.get(url)
vfgn		.then(function(response){
                    $scope.routes = response.data
                })
            //$scope.routes = resources.Route.query();
            alert(JSON.stringify($scope.routes))
        }
        
        $scope.newRoute = function(){
            $scope.route = new resources.Route({route: null})
            $scope.route.isNew = true
        }

        $scope.edit = function(route){
            route.edit = true;
        }

        $scope.update = function(route){
            route.$update()
                .then(function(value) {
                    route.edit = false;
                    $scope.loadRoutes();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.route.$save()
                .then(function(value) {
                    $scope.loadRoutes();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(route){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                route.$delete()
                    .then(function(responce){
                        $scope.routes.splice($scope.routes.indexOf(route), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
