(function(angular, app) {
    "use strict";
    app.controller('CareGiverApprovalController',["$scope", "lifeCircleService", "$http", "params", "$state", function($scope, lifeCircleService, $http, params, $state){
        $scope.careGiverMasterId = params.careGiverMasterId
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/approval_list.json";
        $http.get(url)
            .then(function(response){
                $scope.approvals = response.data.approvals
                console.log($scope.approvals)
                $scope.allApproved = true
                for(var key in $scope.approvals) {
                    var value = $scope.approvals[key];
                    if(value == false){
                        $scope.allApproved = false
                        break
                    }
                }            
            })
    }]);  
    
})(angular, lifeCircleApp);
