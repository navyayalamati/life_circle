(function(angular, app) {
    "usestrict";
    app.controller('RoutineController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadRoutines = function(){
            $scope.routines = resources.Routine.query();
        }
        
        $scope.newRoutine = function(){
            $scope.routine = new resources.Routine({routine: null})
            $scope.routine.isNew = true
        }

        $scope.edit = function(routine){
            routine.edit = true;
        }

        $scope.update = function(routine){
            routine.$update()
                .then(function(value) {
                    routine.edit = false;
                    $scope.loadRoutines();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.routine.$save()
                .then(function(value) {
                    $scope.loadRoutines();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(routine){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                routine.$delete()
                    .then(function(responce){
                        $scope.routines.splice($scope.routines.indexOf(routine), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
