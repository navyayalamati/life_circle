(function(angular, app) {
    "usestrict";
    app.controller('UnitController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadUnits = function(){
            $scope.units = resources.Unit.query();
        }
        
        $scope.newUnit = function(){
            $scope.unit = new resources.Unit({unit: null})
            $scope.unit.isNew = true
        }

        $scope.edit = function(unit){
            unit.edit = true;
        }

        $scope.update = function(unit){
            unit.$update()
                .then(function(value) {
                    unit.edit = false;
                    $scope.loadUnits();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.unit.$save()
                .then(function(value) {
                    $window.location.reload();
                    $scope.loadUnits();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(unit){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                unit.$delete()
                    .then(function(responce){
                        $scope.units.splice($scope.units.indexOf(unit), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
