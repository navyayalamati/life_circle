(function(angular, app) {
    "use strict";
    app.controller('CareGiverChargesController',["$scope", "lifeCircleService", "$http", "params", "$state","$window" , "$location", function($scope, lifeCircleService, $http, params, $state , $window , $location){
        $scope.options = [{key: 'Percentage', value: "PERCENTAGE"}, {key:'Absolute', value:"ABSOLUTE"}]
      
        $scope.careGiverMasterId = params.careGiverMasterId
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_charges/new_or_existing_charges.json";
        $http.get(url)
            .then(function(response){
                $scope.care_giver_charge = response.data.care_giver_charge
                console.log($scope.care_giver_charge)
            })
        
        $scope.calculateTotal = function(){
            if($scope.care_giver_charge.margin_type == "PERCENTAGE"){
                $scope.care_giver_charge.total_charge = ($scope.care_giver_charge.base_charge + ($scope.care_giver_charge.base_charge * ($scope.care_giver_charge.margin/100)))}
            else{
                $scope.care_giver_charge.total_charge = ($scope.care_giver_charge.base_charge + $scope.care_giver_charge.margin)
            }      
            console.log($scope.care_giver_charge.total_charge) 
        }
        
        $scope.save = function(){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_charges/save_charges.json";
            $http.post(url, {care_giver_charge: $scope.care_giver_charge})
                .then(function(response){
                    $state.go('careGiverDocument', {careGiverMasterId: $scope.careGiverMasterId})
                })
        }

        $scope.approve = function(charge){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_charges/"+charge.id+"/approve.json/";
            $http.put(url)
		.then(function(response){
                    charge.approved = response.data.status
                });
        }
	


    }]);  
    
})(angular, lifeCircleApp);


