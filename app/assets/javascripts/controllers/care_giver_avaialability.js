(function(angular, app) {
    "use strict";
    app.controller('CareGiverAvailabilityController',["$scope", "lifeCircleService", "$http", "params", "$state", function($scope, lifeCircleService, $http, params, $state){
        $scope.careGiverMasterId = params.careGiverMasterId
        
        $scope.newAvailability = function(){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_availability_masters/new.json";
            $http.get(url)
                .then(function(responce){
                    $scope.availabilities = responce.data.availabilities
		    console.log($scope.availabilities)
                });
        }

	$scope.all = function(){
	    $scope.availabilities.mon.enabled = true;
	    $scope.availabilities.tue.enabled = true;
	    $scope.availabilities.wed.enabled = true;
	    $scope.availabilities.thu.enabled = true;
	    $scope.availabilities.fri.enabled = true;
	    $scope.availabilities.sat.enabled = true;
	    $scope.availabilities.sun.enabled = true;
	}

	$scope.mark = function(){
	    $scope.availabilities.mon.availabilities[0].time_to = "23:59"
	    $scope.availabilities.mon.availabilities[0].time_from = "00:00"
	    $scope.availabilities.tue.availabilities[0].time_to = "23:59"
	    $scope.availabilities.tue.availabilities[0].time_from = "00:00"
	    $scope.availabilities.wed.availabilities[0].time_to = "23:59"
	    $scope.availabilities.wed.availabilities[0].time_from = "00:00"
	    $scope.availabilities.thu.availabilities[0].time_to = "23:59"
	    $scope.availabilities.thu.availabilities[0].time_from = "00:00"
	    $scope.availabilities.fri.availabilities[0].time_to = "23:59"
	    $scope.availabilities.fri.availabilities[0].time_from = "00:00"
	    $scope.availabilities.sat.availabilities[0].time_to = "23:59"
	    $scope.availabilities.sat.availabilities[0].time_from = "00:00"
	    $scope.availabilities.sun.availabilities[0].time_to = "23:59"
	    $scope.availabilities.sun.availabilities[0].time_from = "00:00"
	    alert('')
	}
        $scope.save = function(){
	    alert(JSON.stringify($scope.availabilities))
	    
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_availability_masters.json";
            $http.post(url, {availabilities: $scope.availabilities})
                .then(function(responce){
                    $scope.availabilities = responce.data.availabilities
                    console.log($scope.availabilities)
                });
            
            console.log($scope.availabilities)
            $state.go('careGiverCharges',{careGiverMasterId: $scope.careGiverMasterId})
        }
        
    }]);  
    
})(angular, lifeCircleApp);
