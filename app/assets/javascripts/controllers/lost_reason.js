(function(angular, app) {
    "use strict";
    app.controller('LostReasonController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadLostReasons = function(){
            $scope.lostReasons = resources.LostReason.query();
        }
        
        $scope.newLostReason = function(){
            $scope.lostReason = new resources.LostReason({reason: null})
            $scope.lostReason.isNew = true
        }

        $scope.edit = function(lostReason){
            lostReason.edit = true;
        }

        $scope.update = function(lostReason){
            lostReason.$update()
                .then(function(value) {
                    lostReason.edit = false;
                    $scope.loadLostReasons();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.lostReason.$save()
                .then(function(value) {
                    $scope.loadLostReasons();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(lostReason){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                lostReason.$delete()
                    .then(function(responce){
                        $scope.lostReasons.splice($scope.lostReasons.indexOf(lostReason), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
