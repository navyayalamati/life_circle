(function(angular, app) {
    "use strict";
    app.controller('SubServiceMasterController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadSubServices = function(serviceMasterId){
            //alert(serviceMasterId)
            $scope.serviceMasterId = serviceMasterId
            $scope.sub_services = resources.SubServiceMaster.query({service_master_id:serviceMasterId});
        }
        
        $scope.newSubService = function(){
            $scope.sub_service = new resources.SubServiceMaster({sub_service: null, service_master_id: $scope.serviceMasterId})
            $scope.sub_service.isNew = true
        }

        $scope.edit = function(sub_service){
            sub_service.edit = true;
        }

        $scope.update = function(sub_service){
            sub_service.$update()
                .then(function(value) {
                    sub_service.edit = false;
                    $scope.loadSubServices($scope.serviceMasterId);
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.sub_service.$save()
            //alert($scope.sub_service)
                .then(function(value) {
                    $scope.loadSubServices($scope.serviceMasterId);
                    //alert(value.data)
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(sub_service){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                sub_service.$delete()
                    .then(function(responce){
                        $scope.sub_services.splice($scope.sub_services.indexOf(sub_service), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
