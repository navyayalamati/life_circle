(function(angular, app) {
    "use strict";
    app.controller('CareGiverEmployerController',["$scope", "$http", "params", "FileUploader", "$state",function($scope, $http, params, FileUploader, $state){
        $scope.careGiverMasterId = params.careGiverMasterId
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_employers/employer_for_registration.json";
        var saveUrl = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_employers.json";
        $scope.reference1 = "" 
        $scope.reference2 = ""
       
        
        $http.get(url)
            .then(function(response){
                console.log(response)
                $scope.employers = response.data.employers
                $scope.reference1 = response.data.reference_1
                $scope.reference2 = response.data.reference_2
                $scope.uploader = new FileUploader({
                    url: saveUrl,
                    autoUpload: false,
                    removeAfterUpload: true,
                    alias: "attachment",
                    headers: {
                        "X-CSRF-Token" : $('meta[name="csrf-token"]').attr('content')
                    }
                 });
            })
        
        $scope.addMore = function(){
            var newEmployer = {}
            angular.forEach($scope.employers[0], function(val, key){
                newEmployer[key] = "";
            })
            newEmployer.care_giver_master_id = $scope.careGiverMasterId
            $scope.employers.push(newEmployer)
        }

        $scope.save = function(){
            var employers_without_files = []
            var employers_with_files = []
            angular.forEach($scope.employers, function(employer, index){
                              
                if(typeof employer.employer != 'undefined' && employer.employer != null && employer.employer.length > 1){
                    var isFileAttached = $("#file_field_"+index).val()
                    if(typeof isFileAttached != 'undefined' && isFileAttached.length > 1){
                        employers_with_files.push(employer)
                    }else{
                        employers_without_files.push(employer)
                    }
                }
            });
                if(employers_with_files.length > 0){
                    angular.forEach($scope.uploader.queue, function(val, key){
                        angular.forEach(employers_with_files[key], function(val, keyInner){
                            if(val == null || typeof val == 'undefined'){
                                employers_with_files[key][keyInner] = ""
                            }
                        });
                        console.log(employers_with_files[key])
                        val.formData.push(employers_with_files[key])
                        val.formData.push({reference_1: $scope.reference1, referece_2: $scope.reference2 , fresher:$scope.fresher})
                        val.upload();
                    });
                    
                    
                }
                
                var saveUrl = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_employers/save_employer.json?reference_1="+$scope.reference1+"&reference_2="+$scope.reference2
                if(employers_without_files.length > 0){
                    $http.post(saveUrl, {care_giver_employers: employers_without_files})
                        .then(function(response){
                            console.log(response)
                            $state.go('healthAndLifeStyle', {careGiverMasterId: $scope.careGiverMasterId})
                        })
                }
            

        }

        $scope.approve = function(employer){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_employers/"+employer.id+"/approve.json/";
            $http.put(url)
                .then(function(response){
                    employer.approved = response.data.status
                });
        }
        
        $scope.save_fresher = function(){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_employers/save_fresher.json?fresher="+$scope.fresher
            $http.post(url)
                .then(function(response){
                    $state.go('healthAndLifeStyle', {careGiverMasterId: $scope.careGiverMasterId})
                    
                });
        }
        
    }]);  
    
})(angular, lifeCircleApp);
