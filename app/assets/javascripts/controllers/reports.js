(function(angular, app) {
    "use strict";
    app.controller('ReportsController',["$scope", "lifeCircleService", "$location", "$state",  "$rootScope", function($scope, lifeCircleService, $location, $state,  $rootScope) {
	
	$scope.report_types = [{value: 'This Month', key: "this_month"}, { value: 'Prev Month', key:"prev_month"},  { value: 'This Year', key:"this_year"}]

	$scope.credit_types = [{value: 'All', key: "all"}, { value: 'Open', key:"PENDING"}, { value: 'Closed', key:"CLOSED"}]
	
	$scope.types = [{value: 'Sale by Customer', key: "sale_by_customer"}, { value: 'Sale by Item', key:"sale_by_item"}, {value:'Sale by Sales Person', key:"sale_by_sale_person"}]


	$scope.get_dates = function(){
	    var date = new Date()
	    var dateFormat = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()
	    if($scope.report.report_type == 'this_month'){
		
		var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
		var Firstdate = firstDay.getFullYear()+"-"+(firstDay.getMonth()+1)+"-"+firstDay.getDate() 
		$scope.report.to_date = dateFormat;
		$scope.report.from_date = Firstdate;
		
	    }
	    if($scope.report.report_type == 'this_year'){
		var year = new Date(date.getFullYear(), 0, 1);
		var yeardate = year.getFullYear()+"-"+(year.getMonth()+1)+"-"+year.getDate() 
		$scope.report.to_date = dateFormat;
		$scope.report.from_date = yeardate;
	    }
	    if($scope.report.report_type == 'prev_month'){
		var first = new Date(date.getFullYear(), date.getMonth()-1, 1);
		var First = first.getFullYear()+"-"+(first.getMonth()+1)+"-"+first.getDate() 
		var last = new Date(date.getFullYear(), date.getMonth(), 0);
		var Last = last.getFullYear()+"-"+(last.getMonth()+1)+"-"+last.getDate() 
		$scope.report.to_date = Last;
		$scope.report.from_date = First;
	    }
	    if($scope.report.report_type == 'this_quarter'){
		$scope.report.to_date = dateFormat;
		var firstDay = new Date(date.getFullYear(), date.getMonth(), 9);
		var Firstdate = firstDay.getFullYear()+"-"+(firstDay.getMonth()+1)+"-"+firstDay.getDate() 
		$scope.report.from_date = yeardate;
	    }
	    
	    
	}


	$scope.get_report = function(){
	    lifeCircleService.get_report($scope.report)
                .then(function(responce){
                    $scope.reports = responce.data  
		    $scope.myReport = true;
		    $scope.total_amount = [];
                    for(var i=0; i < responce.data.length; i++){
                        $scope.total_amount.push(
                            ($scope.reports[i].amount)
			)}
                    if ($scope.total_amount)
                    {
                        $scope.total_debit_value = 0;
                        for (var i = 0; i < $scope.total_amount.length; i++) {
                            $scope.total_debit_value += $scope.total_amount[i] << 0;
                        }
                    }
                })
	}


	$scope.balance_report = function(){
	    //alert(JSON.stringify($scope.report))
	    lifeCircleService.balance_report($scope.report)
                .then(function(responce){
                    $scope.reports = responce.data  
		    $scope.balanceReport = true;
		});
	}
	
	
	$scope.payment_received = function(){
	    //alert(JSON.stringify($scope.report))
	    lifeCircleService.payment_report($scope.report)
                .then(function(responce){
                    $scope.payments = responce.data  
		    alert('')
		    $scope.paymentReport = true;
		});
	}
	
	
	$scope.invoice = function(){
	    //alert(JSON.stringify($scope.report))
	    lifeCircleService.invoice_report($scope.report)
                .then(function(responce){
                    $scope.invoice_details = responce.data  
		    alert('')
		    $scope.invoiceReport = true;
		});
	}
	
	
	$scope.credit_report = function(){
	    //alert(JSON.stringify($scope.report))
	    lifeCircleService.credit_report($scope.report)
                .then(function(responce){
                    $scope.credit_notes = responce.data  
		    alert('')
		    $scope.creditReport = true;
		});
	}
	
	$scope.due_report = function(){
	    lifeCircleService.due_report()
                .then(function(responce){
                    $scope.due_reports = responce.data  
		    $scope.dueReport = true;
		});
	}
	




	
    }]);  
    
})(angular, lifeCircleApp);
