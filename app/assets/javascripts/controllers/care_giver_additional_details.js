(function(angular, app) {
    "use strict";
    app.controller("careGiverAdditionalDetailsController",["$scope", "lifeCircleService", function($scope, lifeCircleService){
        $scope.loadCurrentUser = function(){
            lifeCircleService.getCurrentUser()
                .then(function(responce){
                    $scope.current_user = responce.data.current_user
                })
        }

        $scope.isAdmin = function(){
            console.log($scope.current_user)
            if(typeof $scope.current_user != 'undefined'){
                return ($scope.current_user.role == "l_c_agent")
            }else{
                return false;
            }
        }
    }]);  
    
})(angular, lifeCircleApp);
