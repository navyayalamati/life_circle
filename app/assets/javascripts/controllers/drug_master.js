(function(angular, app) {
    "usestrict";
    app.controller('DrugController',["$scope", "resources", "$window",function($scope, resources, $window) {
        $scope.loaddrugMasters = function(){
            //alert("load")
            $scope.drugmasters = resources.DrugMaster.query();
            $scope.route_masters = resources.Route.query();
            $scope.form_masters = resources.Form.query();
            $scope.unit_masters = resources.Unit.query();
            console.log($scope.form_masters)
            
        }
       
        $scope.newdrugMaster = function(){
            //alert("new")       
            $scope.drugmaster = new resources.DrugMaster({drugmaster: null})
            $scope.drugmaster.isNew = true 
        }

        $scope.edit = function(drugmaster){
            drugmaster.edit = true;
        }

        $scope.update = function(drugmaster){
            drugmaster.$update()
                .then(function(value) {
                    drugmaster.edit = false;
                    $scope.loaddrugMasters();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.drugmaster.$save()
                .then(function(value) {
                    $scope.loaddrugMasters();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(drugmaster){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                drugmaster.$delete()
                    .then(function(responce){
                        $scope.drugmasters.splice($scope.drugmasters.indexOf(drugmaster), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
