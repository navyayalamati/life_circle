(function(angular, app) {
    "use strict";
    app.controller('CareGiverDocumentController',["$scope", "$http", "params", "FileUploader","$state", "$window", function($scope, $http, params, FileUploader, $state , $window){
        $scope.careGiverMasterId = params.careGiverMasterId
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_documents/document_for_upload.json";
        var saveUrl = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_documents.json";
        $http.get(url)
            .then(function(response){
                console.log(response)
                $scope.documents = response.data.documents
                //alert(JSON.stringify($scope.documents))
                $scope.uploader = new FileUploader({
                    url: saveUrl,
                    autoUpload: false,
                    removeAfterUpload: true,
                    alias: "attachment",
                    headers: {
                        "X-CSRF-Token" : $('meta[name="csrf-token"]').attr('content')
                    }
                }
                );
            })
        
        $scope.finish_enquiry = function(){
	    var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/finish.json/";
            $http.put(url)
                .then(function(response){
		    $window.location.href = '/care_giver_enquiries/recruitment'
                    charge.finished = response.data.status
                });
        }

        $scope.addMore = function(){            
            var newDocument = {}
            angular.forEach($scope.documents[0], function(val, key){
                newDocument[key] = "";
            })
            newDocument.care_giver_master_id = $scope.careGiverMasterId
            $scope.documents.push(newDocument)
        }

        $scope.save = function(){
            //alert(JSON.stringify($scope.documents))
	    var documents_without_files = []
            var documents_with_files = []
            angular.forEach($scope.documents, function(document, index){
                if(typeof document.document_name != 'undefined' && document.document_name != null && document.document_name.length > 1){
                    var isFileAttached = $("#file_field_"+index).val()
                    if(typeof isFileAttached != 'undefined' && isFileAttached.length > 1){
                        documents_with_files.push(document)
                    }else{
                        documents_without_files.push(document)
                    }
                }
            })
            if(documents_with_files.length > 0){
                angular.forEach($scope.uploader.queue, function(val, key){                
                    angular.forEach(documents_with_files[key], function(val, keyInner){               
                        if(val == null || typeof val == 'undefined'){
                            documents_with_files[key][keyInner] = ""
                        }
                    });
                    val.formData.push(documents_with_files[key])
                    val.upload();
                });
            }
            
            var saveUrl = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_documents/save_document.json";
            if(documents_without_files.length > 0){
                $http.post(saveUrl, {care_giver_documents: documents_without_files})
                    .then(function(response){
                        console.log(response)                       
                    })

            }
          $state.go('careGiverApproval', {careGiverMasterId: $scope.careGiverMasterId})
        }

        $scope.approve = function(document){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_documents/"+document.id+"/approve.json/";
            $http.put(url)
                .then(function(response){
                    document.approved = response.data.status
                });
        }

    }]);  
    
})(angular, lifeCircleApp);
