(function(angular, app) {
    "use strict";
    app.controller('CareGiverRelativeController',["$scope", "lifeCircleService", "$http", "params", "$state", function($scope, lifeCircleService, $http, params, $state){
        $scope.careGiverMasterId = params.careGiverMasterId
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_relatives.json";
        $http.get(url)
            .then(function(response){
                console.log(response)
                $scope.relatives = response.data.relatives
            })
        
        $scope.addMore = function(){
            var newRelative = {}
            angular.forEach($scope.relatives[0], function(val, key){
                newRelative[key] = null;
            })
            newRelative.care_giver_master_id = $scope.careGiverMasterId
            $scope.relatives.push(newRelative)
        }
        
	


        $scope.save = function(){
	    $scope.count = 0
	    for(var i = 0 ; i < $scope.relatives.length ; i++)
	    {
		if($scope.relatives[i].emergency_contact)
		{ $scope.count += 1}
		
	    }
	    if($scope.count > 1)
	    {
		alert("Select only one Emergency Contact")
	    }
	    else
	    {
		var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_relatives.json";
		console.log($scope.relatives)
		$http.post(url, {care_giver_relatives: $scope.relatives})
                    .then(function(response){
			console.log(response)
			//$scope.relatives = response.data.relatives
			$state.go('careGiverEducation',{careGiverMasterId: $scope.careGiverMasterId})	
                    })
	    }
        }

        $scope.approve = function(relative){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_relatives/"+relative.id+"/approve.json/";
            $http.put(url)
                .then(function(response){
                    relative.approved = response.data.status
                });
        }

    }]);  
    
})(angular, lifeCircleApp);
