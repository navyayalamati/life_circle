(function(angular, app) {
    "use strict";
    app.controller('PhysicalDescriptionController',["$scope", "lifeCircleService", "$http", "params", "$state", function($scope, lifeCircleService, $http, params, $state){
        $scope.careGiverMasterId = params.careGiverMasterId
        var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_additional_details/physical_description.json";
        $http.get(url)
            .then(function(response){
                $scope.description = response.data.care_giver_details
            })
        
        $scope.save = function(){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_additional_details/save_additional_details.json";
            $scope.description.physical_description_approved = false
            $http.post(url, {care_giver_additional_detail: $scope.description})
                .then(function(response){
                    $scope.description = response.data.care_giver_details
                    $state.go('careGiverRelatives',{careGiverMasterId: $scope.careGiverMasterId})
                })
	
        }

        $scope.approve = function(){
            var url = "/care_giver_masters/"+$scope.careGiverMasterId+"/care_giver_additional_details/"+$scope.description.id+"/approve_physical_description.json/";
            $http.put(url)
                .then(function(response){
                    $scope.description.physical_description_approved = response.data.status
                    
                });
        }
    }]);  
    
})(angular, lifeCircleApp);
