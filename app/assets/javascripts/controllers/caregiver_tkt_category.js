(function(angular, app) {
    "usestrict";
    app.controller('CaregivertktcatgController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadCaregivertktcatgs = function(){
            $scope.caregivertktcatgs = resources.Caregivertktcatg.query();
        }
        
        $scope.newCaregivertktcatg = function(){
            $scope.caregivertktcatg = new resources.Caregivertktcatg({caregivertktcatg: null})
            $scope.caregivertktcatg.isNew = true
        }

        $scope.edit = function(caregivertktcatg){
            caregivertktcatg.edit = true;
        }

        $scope.update = function(caregivertktcatg){
            caregivertktcatg.$update()
                .then(function(value) {
                    caregivertktcatg.edit = false;
                    $scope.loadCaregivertktcatgs();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.caregivertktcatg.$save()
                .then(function(value) {
                    $scope.loadCaregivertktcatgs();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(caregivertktcatg){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                caregivertktcatg.$delete()
                    .then(function(response){
                        $scope.caregivertktcatgs.splice($scope.caregivertktcatgs.indexOf(caregivertktcatg), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
