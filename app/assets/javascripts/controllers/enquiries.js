(function(angular, app) {
    "use strict";
    app.controller('enquiriesController',["$scope", "lifeCircleService", function($scope, lifeCircleService) {
        
        
        $scope.getEnquiries = function(page){
            lifeCircleService.enquiries(page, $scope.page_status)
                .then(function(response){
                    injectValues(response);
                })
        }

        $scope.findEnquiry = function(){
            lifeCircleService.enquiriesWithFilter(1, $scope.page_status, $scope.patientNameFilter)
                .then(function(response){
                    injectValues(response);
                })
        }

        var injectValues = function(response){
            $scope.enquiries = response.data.enquiries
            $scope.from_index = response.data.from_index
            $scope.to_index = response.data.to_index
            $scope.total_entries = response.data.total_entries;
            $scope.current_page = parseInt(response.data.current_page)

        }

    }]);  
    
})(angular, lifeCircleApp);
