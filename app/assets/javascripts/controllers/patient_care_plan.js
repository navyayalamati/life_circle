(function(angular, app) {
    "use strict";
    app.controller("patientCarePlanController",["$scope", "lifeCircleService", function($scope, lifeCircleService){
        //alert("")
        $scope.loadCurrentUser = function(){
            lifeCircleService.getCurrentUser()
                .then(function(responce){
                    $scope.current_user = responce.data.current_user
                })
        }

        // $scope.isAdmin = function(){
        //     console.log($scope.current_user)
        //     if(typeof $scope.current_user != 'undefined'){
        //         return ($scope.current_user.role == "admin")
        //     }else{
        //         return false;
        //     }
        // }
    }]);  
    
})(angular, lifeCircleApp);
