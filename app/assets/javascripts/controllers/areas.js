(function(angular, app) {
    "use strict";
    app.controller('AreasMasterController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadAreas = function(cityMasterId){
            $scope.cityMasterId = cityMasterId
            $scope.areas = resources.AreasMaster.query({city_master_id: cityMasterId});
        }
        
        $scope.newArea = function(){
            $scope.area = new resources.AreasMaster({name: null, city_master_id: $scope.cityMasterId})
            $scope.area.isNew = true
        }

        $scope.edit = function(area){
            area.edit = true;
        }

        $scope.update = function(area){
            area.$update()
                .then(function(value) {
                    area.edit = false;
                    $scope.loadAreas($scope.cityMasterId);
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.area.$save()
                .then(function(value) {
                    $scope.loadAreas($scope.cityMasterId);
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(area){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                area.$delete()
                    .then(function(responce){
                        $scope.areas.splice($scope.areas.indexOf(area), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);


