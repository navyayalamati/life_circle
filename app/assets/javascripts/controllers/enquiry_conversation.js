(function(angular, app) {
    "use strict";
    app.controller('EnquiryConversationController', ["$scope", "lifeCircleService", "resources", "$window", "$http", "$q", "$location" ,function($scope, lifeCircleService, resources, $window, $http, $q , $location) {
        $scope.enquiry_tags = [{value: 'Potential-High', key: "Potential-High"}, { value: 'Potential-Low', key:"Potential-Low"}, {value:'To Audit', key:"Lost"}, {value:'Non-Prospect', key:"Non-Prospect"}]
	$scope.audit_tags = [{value: 'Flag', key: "Flag"}, { value: 'Closed', key:"Closed"}]
        $scope.labelClasses = {"Potential-High": 'label-success',  "Potential-Low": 'label-warning', "Non-Prospect": 'label-info', "Lost": 'label-danger', "Flag":'label-info' ,"Saftey-Check" : 'label-danger', "Closed": 'label-danger', "To Audit" : 'label-info',"Recruit": 'label-success', "Hold": 'label-warning', "Reject": 'label-danger'}
                
        $scope.cg_master_status = []
        $scope.patients = []
        
        $scope.loadEnquiryConversations = function(conversation_master_id, conversation_master_type, care_giver_id, type, cg_status, cg_leave_id, conversation_for){
            //alert(cg_status)
           
            $scope.conversation_master_type = conversation_master_type
            $scope.conversation_master_id = conversation_master_id
            $scope.cg_status = cg_status
            $scope.cg_leave_id = cg_leave_id
            $scope.conversation_for = conversation_for
            $scope.care_giver_id = care_giver_id
            $scope.type = type
            //alert($scope.cg_status)
            //alert($scope.conversation_for)
            resources.EnquiryConversation.query({conversation_master_id: conversation_master_id, conversation_master_type: conversation_master_type}).$promise.then(function(result){
                console.log(result)
                $scope.enquiryConversations = result.conversations
                //alert(JSON.stringify($scope.enquiryConversations))
                $scope.parent_tag = result.parent_tag
            })
            if ($scope.cg_status != null){
                $scope.cg_master_status =  $scope.options = [{key:'Active', value:'Active'},{key:'Working', value:'Working'},
                          {key: 'Inactive', value: 'Leave'},
                          {key:'Terminated', value:'Resign'}]
            }
          
	    if($scope.conversation_for != null)
            {
               if($scope.type == "HOLD")
               {
                $scope.patients = [{value: 'Recruit', key: "Recruit"}, { value: 'Reject', key:"Reject"}]
                }
                else{
                $scope.patients = [{value: 'Recruit', key: "Recruit"},{value: 'Hold', key: "Hold"}, { value: 'Reject', key:"Reject"}]
                }
            }
	    
         
            // var url = "/care_giver_masters/get_caregivers.json?care_giver_enquiry_id="+conversation_master_id
            // $http.get(url)
            //     .then(function(response){
            //         //alert('')
            //         $scope.caregivers = response.data.care_giver_master
            //         console.log($scope.caregiver)
            //     })
            $scope.newEnquiryConversation();
        }
        
	
        $scope.addTag = function(){
            $scope.enquiryConversation.tag = $scope.tag
        }

        $scope.newEnquiryConversation = function(){
            $scope.enquiryConversation = new resources.EnquiryConversation({conversation_master_id: $scope.conversation_master_id, conversation_master_type: $scope.conversation_master_type, description: ""})
            //$scope.enquiryConversation.cg_status = $scope.cg_status
            //$scope.enquiryConversation.conversation_for = $scope.conversation_for
        }

        $scope.edit = function(enquiryConversation){
            enquiryConversation.edit = true;
        }

        $scope.update = function(enquiryConversation){
            enquiryConversation.$update()
                .then(function(value) {
                    enquiryConversation.edit = false;
                    $scope.loadEnquiryConversations($scope.parent_key);
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            
            if($scope.enquiryConversation.description == ""){
                $scope.description_empty = true
                return false;
            }
            
            $scope.enquiryConversation.$save()
                .then(function(value) {
                    
		    $window.history.back();
                    if($scope.conversation_for != null && $scope.tag != null)
                    {
                        var url = "/care_giver_enquiries/"+$scope.conversation_master_id+"/update_enq.json?status="+$scope.tag
                        $http.put(url)
			    .then(function(response){
			
                                // $scope.cgenquiry = response.data.data
                                // console.log($scope.cgenquiry)
                                // alert($scope.cgenquiry)
                                // alert(response.data)
                            });
                        $scope.loadEnquiryConversations($scope.conversation_master_id, $scope.conversation_master_type, $scope.cg_status);
                      	
			//$window.location.reload();
		    }
                }, function(reason) {
                    // handle failure
                });
        }

        $scope.eqStatus = function(){
            if ($scope.cg_status == "Leave"){
                $scope.check()
            }
            else{
                var url = "/care_giver_masters/"+$scope.care_giver_id+"/update_status.json?cg_status="+$scope.cg_status;
                $http.put(url)
                    .then(function(response){
                        //alert("chekk")
                        //console.log(response)
                    },function(result){
                        //alert("king")
                        console.log(result.data)
                    })
            }
             
         }

        $scope.check = function(){
             var url = "/care_giver_masters/get_caregivers_for_transfer.json?care_giver_enquiry_id="+$scope.conversation_master_id+"&care_giver_leave_id="+$scope.cg_leave_id
            var responsePromise =  $http.get(url);
            responsePromise.then(function(response){
            // lifeCircleService.checkCgTransfer($scope.conversation_master_id, $scope.cg_leave_id)
            //     .then(function(response){
                     //alert('')
                $scope.caregiver = response.data.care_giver_master
                $scope.cg_enroll = response.data.cg_enrolled
                $scope.count = response.data.count
                $scope.status = response.data.status
                $scope.cg_leave =response.data.cg_leave
                    if($scope.count > 1 )
                    {
                       
                        for(var i=0; i<$scope.status.length; i++){
                            if($scope.status[i] == "Go"){
                                    var confirm = $window.confirm("Trasfer Caregiver Before Approving Leave?");
                                    if(confirm){
                                        $window.open("/enrollments/"+$scope.cg_enroll[i]['id']+"/transfer_care_giver?care_giver_master_id="+$scope.caregiver[0].id+"&leave_from="+$scope.cg_leave[0].from_date+"&leave_to="+$scope.cg_leave[0].to_date, 'Caregiver Transfer','width=1024,height=890')
                                    };
                                }
                            else if($scope.status[i] == "Ok")
                                {
                                    alert("Matching Caregiver not found")
                                }
                            }
                        
                        
                    }
                    else if($scope.count == 1)
                    {
                        if($scope.status == "Go")
                        {
                            var confirm = $window.confirm("Trasfer Caregiver Before Approving Leave?");
                            if(confirm){
                                $window.open("/enrollments/"+$scope.cg_enroll[0].id+"/transfer_care_giver?care_giver_master_id="+$scope.caregiver[0].id, 'Caregiver Transfer','width=1024,height=890')
                            };
                        }
                        else if($scope.status == "Ok")
                        {
                            alert("Matching Caregiver not found")
                        }
                    }
            });
            
            // if($scope.count > 1 )
            // {
            //     alert()
            //     angular.foreach($scope.status, function(s)
            //                     {
            //                         if(s == "Go"){
            //                             alert()
            //                             var confirm = $window.confirm("Are you sure to delete?");
            //                             if(confirm){
                                            
            //                             };
            //                         }
            //                     });
                
            // }
            
        }
        
    }]);  
    
})(angular, lifeCircleApp);


