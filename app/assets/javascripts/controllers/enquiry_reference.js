(function(angular, app) {
    "use strict";
    app.controller('EnquiryReferenceController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadEnquiryReferences = function(){
            $scope.enquiryReferences = resources.EnquiryReference.query();
        }
        
        $scope.newEnquiryReference = function(){
            $scope.enquiryReference = new resources.EnquiryReference({reference: null})
            $scope.enquiryReference.isNew = true
        }

        $scope.edit = function(enquiryReference){
            enquiryReference.edit = true;
        }

        $scope.update = function(enquiryReference){
            enquiryReference.$update()
                .then(function(value) {
                    enquiryReference.edit = false;
                    $scope.loadEnquiryReferences();
                }, function(reason) {
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.enquiryReference.$save()
                .then(function(value) {
                    $scope.loadEnquiryReferences();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(enquiryReference){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                enquiryReference.$delete()
                    .then(function(responce){
                        $scope.enquiryReferences.splice($scope.enquiryReferences.indexOf(enquiryReference), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);


