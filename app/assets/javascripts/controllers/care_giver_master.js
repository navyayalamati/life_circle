(function(angular, app) {
    "usestrict";
    app.controller('CareGiverController',["$scope", "resources", "$http", "$window",function($scope, resources, $http, $window) {

        //alert()

        $scope.options = [{key:'Working', value:'Working'},
                          {key:'Active', value:'Active'},
                          {key: 'Leave', value: 'Leave'},
                          {key:'Bench', value:'Bench'},
                          {key: 'Resign', value: 'Resign'}]

        $scope.Woptions = [{key:'Working', value:'Working'},
                           {key: 'Leave', value: 'Leave'},
                           {key:'Bench', value:'Bench'},
                           {key: 'Resign', value: 'Resign'}]

        $scope.Loptions = [{key: 'Leave', value: 'Leave'},
                           {key:'Working', value:'Working'},
                           {key:'Bench', value:'Bench'},
                           {key: 'Resign', value: 'Resign'}]
        
        $scope.Roptions = [{key: 'Resign', value: 'Resign'},
                           {key:'Working', value:'Working'},
                           {key:'Bench', value:'Bench'}]
        
        $scope.Boptions = [{key:'Bench', value:'Bench'},
                           {key: 'Leave', value: 'Leave'},
                           {key: 'Resign', value: 'Resign'}]
        
        $scope.loadCaregivers = function(status){
            // $scope.givers = new resources.Caregiver.query()
            // $scope.caregivers = $scope.givers.care_giver_masters
            //var url =
            // alert("vjdsa")
            var url = "/care_giver_masters.json"
            $http.get(url)
                .then(function(response){
                    //alert('')
                    $scope.caregivers = response.data.care_giver_masters
                    console.log($scope.caregivers)
                })
        }

        $scope.change = function(care_giver){
            // if(care_giver.status == "Resign"){
            //     care_giver.reason_for_status_change = ""
            //     $scope.cgm = angular.copy(care_giver)
            //     $("#close-cgm-pop-up").modal("show")
            //     // $("#closing_caregivermaster_id").val(care_giver.id);
            //     // $("#close_caregivermaster_comments").val("");
            //     // alert(care_giver.id)
            //     //closecgmEnquiry(care_giver)
            //     //alert($("#closing_caregivermaster_id").val())
            // }
            // else if(care_giver.status == "Leave"){
            // }
            
            switch (care_giver.status)
            {
                case 'Resign':
                //alert(care_giver.status)
                care_giver.reason_for_status_change = ""
                $scope.cgm = angular.copy(care_giver)
                $("#close-cgm-pop-up").modal("show")
                break;
                case 'Leave':
                var confirm = $window.confirm("Are you sure to delete?", 'transfer');
                if(confirm){
                    alert()
                }
                else
                {
                    $scope.loadCaregivers();
                }
                //alert(care_giver.status)
                break;
                case 'Transfer': 
                //alert(care_giver.status)
                break;
                case 'Active':    
                //alert(care_giver.status)
                break;
                case 'Bench':    
                //alert(care_giver.status)
                break;
                case 'Working':   
                //alert(care_giver.status)
                break;
                default:
                //alert()
                break;
               
            }
            

            // $scope.update(care_giver)
        }

        $scope.closecgmEnquiry = function(cgm)
        {
            $("#close-cgm-pop-up").modal("hide")
            //alert(cgm.id)
            //alert(cgm.reason_for_status_change)
            $scope.update(cgm)
            // $scope.closecgm_id = $("#closing_caregivermaster_id").val()
            // var closingComments = $("#close_caregivermaster_comments").val()
            // $("#close-cgm-pop-up").modal("hide")
            // alert(care_giver.id)
            // alert($scope.closecgm_id)
            // console.log($scope.closecgm_id)
        }
        
        $scope.update = function(care_giver){
            //alert("K")
            //alert(care_giver.id)
            var url = "/care_giver_masters/"+care_giver.id+"/update_status.json";
            $http.put(url,{ care_giver : care_giver })
                .then(function(response){
                    //alert("chekk")
                    $scope.caregivers = response.data
                    $scope.loadCaregivers();
                    //console.log($scope.caregivers)
                },function(result){
                    //alert("king")
                    console.log(result.data)
                })
        }
        

        $scope.save = function(){
            $scope.caregiver.$save()
                .then(function(value) {
                    $scope.loadCaregivers();
                }, function(reason) {
                    // handle failure
                });
        }
        
        
        
    }]);  
    
})(angular, lifeCircleApp);
