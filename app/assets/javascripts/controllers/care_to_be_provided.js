(function(angular, app) {
    "usestrict";
    app.controller('CareprovidesController',["$scope", "lifeCircleService", "resources","$http", "$window",function($scope, lifeCircleService, resources, $http, $window) {
        $scope.success = false
        $scope.warning = false
        $scope.alertMessage = ""
        //$scope.subservices = []
        $scope.sub_services = []
        $scope.serviceArray = []
         //$scope.example4settings = {displayProp: 'sub_service', idProp: 'id'};
        $scope.loadCareprovides = function(enrollment_id){
            //alert("load")
            $scope.enrollment_id = enrollment_id
            //$scope.subService = resources.SubServiceMaster.query();                
                lifeCircleService.getEnrolledservices($scope.enrollment_id)
		.then(function(result){
		    $scope.services  = result.data
		    angular.forEach($scope.services, function(service){
                         $scope.serviceArray.push(service.service_master_id)  
                         //$scope.sub_services.push(resources.SubServiceMaster.query({service_master_id: service.service_master_id}))               
                     });
                    var url = "/sub_service_masters/all_sub_services.json?array="+$scope.serviceArray;
                    $http.get(url)
                        .then(function(response){
                            //alert("chekk")
                            $scope.sub_services = response.data                            
                        },function(result){
                            //alert("king")
                            //console.log(result.data)
                        })                     
                    console.log($scope.services)
		});        
            // $scope.services = resources.ServiceMaster.query();
            // alert($scope.services)
            // var urll = "/enrollments/"+$scope.enrollment_id+"/care_to_be_provided_plans.json"
            // $http.get(urll)
            //     .then(function(response){
            //         $scope.careprovides = response.data.care;
            //         $scope.careprovides.sub_service_master = response.data.pcp
            //         console.log(response)})
            $scope.careprovides = resources.Careprovide.query({enrollment_id:enrollment_id});
            $scope.careprovides.$promise.then(function(data) {
                $scope.careprovides = data;
            });
        }
         $scope.filtertype = function(){
            return function (item) {
                if (item.assigned == true)
                {
                    return true;
                }
                else
                return false;
            };
        }
        
        $scope.loadSubservice = function(id){
        alert(id)
        //$scope.sub_services = resources.SubServiceMaster.query({service_master_id: id});        
        }
        $scope.newCareprovide = function(){
            //alert("new")    
            //$scope.sub_services = resources.SubServiceMaster.query({service_master_id:[$scope.serviceArray]});           
            $scope.careprovide = new resources.Careprovide({enrollment_id: $scope.enrollment_id})
            $scope.careprovide.isNew = true 
        }
        
        $scope.close = function(){
             $scope.careprovide.isNew = false;
        }
	
	$scope.closeupdate = function(careprovide){
            careprovide.edit = false;
        }
	
        $scope.edit = function(careprovide){
            careprovide.edit = true;
        }

        $scope.update = function(careprovide){
            //alert(careprovide.enrollment_id)
            careprovide.$update()
                .then(function(value) {
                    careprovide.edit = false;
                    $scope.success = true
                    $scope.warning = false
		    $scope.alertMessage = "Update Successfully"
                    $scope.loadCareprovides($scope.enrollment_id);
                }, function(reason) {
                    $scope.warning = true
                    $scope.success = false
		    $scope.alertMessage = "Error!"
                    // handle failure
                });
        }

        $scope.save = function(){
            //alert($scope.careprovide)
            $scope.careprovide.$save()
                .then(function(value) {
                    $scope.success = true
                    $scope.warning = false
		    $scope.alertMessage = "Saved Successfully"
                    $scope.loadCareprovides($scope.enrollment_id);
                }, function(reason) {
                    $scope.warning = true
                    $scope.success = false
		   $scope.alertMessage = "Error!"
                    // handle failure
                });
        }
        
        $scope.destroy = function(careprovide){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                careprovide.$delete()
                    .then(function(responce){
                        $scope.careprovides.splice($scope.careprovides.indexOf(careprovide), 1)
                        $scope.success = true
                        $scope.warning = false
		        $scope.alertMessage = "Delete Successfull"
                    }, function(reason) {
                        $scope.warning = true
                        $scope.success = false
		        $scope.alertMessage = "Error!"
                    // handle failure
                });
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
