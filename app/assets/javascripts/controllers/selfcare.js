(function(angular, app) {
    "usestrict";
    app.controller('SelfcareController',["$scope", "resources", "$window",function($scope, resources, $window) {
        
        $scope.loadSelfcares = function(){
            $scope.selfcares = resources.Selfcare.query();
            alert(JSON.stringify($scope.selfcares))
        }
        
        $scope.newSelfcare = function(){
            $scope.selfcare = new resources.Selfcare({selfcare: null})
            $scope.selfcare.isNew = true
        }

        $scope.edit = function(selfcare){
            selfcare.edit = true;
        }

        $scope.update = function(selfcare){
            selfcare.$update()
                .then(function(value) {
                    selfcare.edit = false;
                    $scope.loadSelfcares();
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.selfcare.$save()
                .then(function(value) {
                    $scope.loadSelfcares();
                }, function(reason) {
                    // handle failure
                });
        }
        
        $scope.destroy = function(selfcare){
            var confirm = $window.confirm("Are you sure to delete?");
            if(confirm){
                selfcare.$delete()
                    .then(function(responce){
                        $scope.selfcares.splice($scope.selfcares.indexOf(selfcare), 1)
                    })
            };
            }
        
    }]);  
    
})(angular, lifeCircleApp);
