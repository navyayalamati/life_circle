(function(angular, app) {
    "use strict";
    app.controller('TicketconversationController', ["$scope", "resources", "$window", "$http", function($scope, resources, $window, $http) {
        
        $scope.ticket_tags = [{value: 'Waiting for Inputs', key: "Waiting for Inputs"}, { value: 'Waiting for Closed', key:"Waiting for Closed"},{value:'Open', key:"Open"}, {value:'Closed', key:"Closed"}]
        
        $scope.labelClasses = {"Waiting for Inputs": 'label-primary',  "Waiting for Closed": 'label-info', "Closed": 'label-success', "New": 'label-danger', "Open": 'label-warning'}
                
        
        $scope.loadTicketconversations = function(ticket_id){
            $scope.ticket_id = ticket_id
            resources.Ticketconversation.query({ticket_id: ticket_id}).$promise.then(function(result){
                console.log(result)
                $scope.ticketconversations = result.conversations
                $scope.parent_tag = result.parent_tag
            })
            $scope.newTicketconversation();
        }
        
        $scope.addTag = function(){
            
            $scope.ticketconversation.tag = $scope.tag
        }

        $scope.newTicketconversation = function(){
            $scope.ticketconversation = new resources.Ticketconversation({ticket_id: $scope.ticket_id, description: ""})
            //$scope.ticketconversation.cg_status = $scope.cg_status
            //$scope.ticketconversation.conversation_for = $scope.conversation_for
        }
        
        $scope.rolefilter = function(role){
            return function (item) {
                if(role == 'l_c_agent'){
                    if (item.key == "Waiting for Inputs")
                    {
                        return true;
                    }
                    else if (item.key == "Waiting for Closed")
                    {
                        return true;
                    }
                }
                else if (role == 'patient' || role == 'care_giver')
                {
                    if (item.key =="Closed")
                    {
                        return true;
                    }    
                    else if (item.key =="Open")
                    {
                        return true;
                    }                   
                }
                return false;
            };
        }


        $scope.edit = function(ticketconversation){
            ticketconversation.edit = true;
        }

        $scope.update = function(ticketconversation){
            ticketconversation.$update()
                .then(function(value) {
                    ticketconversation.edit = false;
                    $scope.loadTicketconversations($scope.parent_key);
                }, function(reason) {
                    
                    // handle failure
                });
        }

        $scope.save = function(){
            $scope.ticketconversation.$save()
                .then(function(value) {
                        $scope.loadTicketconversations($scope.ticket_id);
                }, function(reason) {
                    // handle failure
                });
        }        
    }]);  
    
})(angular, lifeCircleApp);

