(function(angular, app) {
    "use strict";
    app.controller('CityController',["$scope", "lifeCircleService", "$location", "$http", "$state",  function($scope, lifeCircleService, $location, $state , $http) {
        
	
        
        $scope.submit_location_all = function(){
            $scope.address = address_auto_complete()
            $scope.address_final = [];
	    lifeCircleService.save_city($scope.address , $scope.address_str)
		.then(function(responce){
                    $scope.city = responce.data
                    window.location.href = '/city_masters'
		})
        }
        var get_all_locations_from_auto_complete = function() {
	    var add = [];
	    
	    
	    
	    var address = []
            var line1 = ""
            var line2 = ""
	    for(var i=0; i < add.length ; i++)
	    {
		if(typeof add[i].route != 'undefined' && add[i].route.length > 1){
                    line1 += (add[i].route + ", ")
		}
		if(typeof add[i].sublocality_level_3 != 'undefined' && add[i].sublocality_level_3.length > 1){
                    line1 += add[i].sublocality_level_3
		}
		if(typeof add[i].sublocality_level_2 != 'undefined' && add[i].sublocality_level_2.length > 1){
                    line2 += (add[i].sublocality_level_2 + ", ")
		}
		if(typeof add[i].sublocality_level_1 != 'undefined' && add[i].sublocalit1y_level_1.length > 1){
                    line2 += add[i].sublocality_level_1
		}
		if(line1.length > 1){
                    address.line1 = line1
		}
		if(line2.length > 1){
                    address.line2 = line2
		}
		
		if(typeof add[i].locality != 'undefined' && add[i].locality.length > 1){
                    address.city = add[i].locality
		}
		
		if(typeof add[i].country != 'undefined' && add[i].country.length > 1){
                    address.country = add[i].country
		}
		
		if(typeof add[i].postal_code != 'undefined' && add[i].postal_code.length > 1){
                    address.postal_code = add[i].postal_code
		}
		
		address.push(address.city);
	    
	    }
	    return address
	}
        var address_auto_complete = function(){
	    var gmapAddress = JSON.parse($("#place_details_object").val())
            
            var address = {}
            var line1 = ""
            var line2 = ""
            if(typeof gmapAddress.route != 'undefined' && gmapAddress.route.length > 1){
                line1 += (gmapAddress.route + ", ")
            }
            if(typeof gmapAddress.sublocality_level_3 != 'undefined' && gmapAddress.sublocality_level_3.length > 1){
                line1 += gmapAddress.sublocality_level_3
            }
            if(typeof gmapAddress.sublocality_level_2 != 'undefined' && gmapAddress.sublocality_level_2.length > 1){
                line2 += (gmapAddress.sublocality_level_2 + ", ")
            }
            if(typeof gmapAddress.sublocality_level_1 != 'undefined' && gmapAddress.sublocality_level_1.length > 1){
                line2 += gmapAddress.sublocality_level_1
            }
            if(line1.length > 1){
                address.line1 = line1
            }
            if(line2.length > 1){
                address.line2 = line2
            }

            if(typeof gmapAddress.locality != 'undefined' && gmapAddress.locality.length > 1){
                address.city = gmapAddress.locality
            }

            if(typeof gmapAddress.country != 'undefined' && gmapAddress.country.length > 1){
                address.country = gmapAddress.country
            }

            if(typeof gmapAddress.postal_code != 'undefined' && gmapAddress.postal_code.length > 1){
                address.postal_code = gmapAddress.postal_code
            }
            return address;
        }
        
        
    }]);  
    
})(angular, lifeCircleApp);


