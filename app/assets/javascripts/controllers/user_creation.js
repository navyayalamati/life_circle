(function(angular, app) {
    "use strict";
    app.controller('UserCreationController',["$scope", "lifeCircleService", "$location", "$state",  "$rootScope", "$http", function($scope, lifeCircleService, $location, $state,  $rootScope , $http ) {
        var initiateForm = function(){
            $scope.userShow = true;
            $scope.userForm = false;
	    
	    
        };
        initiateForm();
	
	$scope.all_users = function(){
	    var url = '/user_creations/all_users.json'
	    $http.get(url)
                .then(function(response){
		    $scope.users = response.data
		})
	};
	
	lifeCircleService.role()
	    .then(function(result){
		$scope.roles = result.data
	    });

	$scope.edit = function(user){
            user.edit = true;
        }
	
	$scope.update = function(user){
            lifeCircleService.updateUserRole(user)
                .then(function(value) {
		    $scope.all_users();
                    user.edit = false;
                }, function(reason) {
                    // handle failure
                });
        }
	
	$scope.save = function(user){
	    lifeCircleService.save_user(user)
                .then(function(response){
                    $scope.user = response.data
		    $scope.userShow = true;
		    $scope.userForm = false;
		    $scope.all_users();
		})
	
        }
	
	$scope.new_user = function(){
	    $scope.userShow = false;
            $scope.userForm = true;
	}

        
    }]);  
    
})(angular, lifeCircleApp);
