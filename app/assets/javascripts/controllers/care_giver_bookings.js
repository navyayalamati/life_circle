(function(angular, app) {
    "use strict";
    app.controller('CareGiverBookingsController',["$scope", "lifeCircleService", "$location",  "$state", "localStorageService", "$rootScope", function($scope, lifeCircleService, $location, $state, localStorageService, $rootScope) {
        $scope.AttendanceForm = false;
        $scope.enquiry = {}
        $scope.spinnerActivated = true;
        $scope.registration_types = [{value: 'Let Me Fill', key: "LET_ME_FILL"}, { value: 'Visit My Home', key:"HOME_VISIT"}, {value:'Call Me', key:"CALL_ME"}]
        $scope.price_ranges = [{operator: '<', l_value: 200, key: "<200"}, {operator: '<', l_value: 300, key: "<300"}, {operator: '<', l_value: 400, key: "<400"}, {operator: '<', l_value: 500, key: " <500"}, {operator: '>', h_value: 1000, key: "<1000"}]
        
        $scope.selected_care_givers = []
        $scope.appointment_date = ""
        $scope.appointment_time = ""
        $scope.showLogin = false;
        $scope.invalidTimeSlot = false;
	
	
        lifeCircleService.getCurrentUser()
            .then(function(responce){
                $scope.current_user = responce.data.current_user
            })

	lifeCircleService.all_cg()
	    .then(function(result){
		$scope.care_giver_masters = result.data
		
	    });
	

	
        
        $scope.load_enquiry_from_missed_call = function(missed_call_request_id, mobile_no){
            $scope.enquiry.missed_call_request_id = missed_call_request_id;
            $scope.enquiry.mobile_no = mobile_no;
            $scope.load_defaults();
            $state.go("city_and_area")
        }
        
        $scope.load_defaults = function(){
            $scope.enquiry.appointment_time = "";
            $scope.loadServices();
            $scope.loadLanguages();
            $scope.spinnerActivated = false;
        }

        $scope.load_existing_enquiry = function(enquiryId){
            //alert('')	
	    lifeCircleService.getEnquiry(enquiryId)
		.then(function(result){
		    $scope.enquiry = result.data.enquiry
                    $scope.load_defaults();
                   
                    $state.go("city_and_area")
		    //alert(JSON.stringify($scope.reports))
		});
        }

        $scope.checkState = function(){
            var current_state = localStorageService.get("currentState")
            if(current_state == null || typeof current_state == 'undefined' || (current_state.length <= 1)){
                localStorageService.set("currentState", "city_and_area")
                $state.go("city_and_area")
            }
	    else{
                
		if(localStorageService.get("enquiry") != null){
                    $scope.enquiry = JSON.parse(localStorageService.get("enquiry"))
                }
		if(current_state == 'check_city_support'){
		    
		    check_city_support();
		    
		} else{		
                    if(current_state == 'list_care_givers'){
			$scope.list_care_givers();
                    }else if(current_state == 'city_and_area'){
			$scope.enquiry = {}
                    }
                    $state.go(current_state)
		}
            }
        }
        
	$scope.isAdmin = function(){
            if(typeof $scope.current_user != 'undefined' & $scope.current_user != null){
                return ($scope.current_user.role == "l_c_agent")
            }else{
                return false;
            }
        }
        
        $scope.submit_city_and_area = function(){
	    $scope.enquiry.address_attributes = get_address_details_from_auto_complete()
	    $scope.address_final = [];
	    var a = $scope.enquiry.address_attributes
	    for (var key in a) {
		$scope.address_final.push(a[key]) ;
	    }
	    
            if($scope.enquiry.address_attributes  == 0){
	       
            }
	    else{
		check_city_support();
	    }
        }
        
	var check_city_support = function(){
	    lifeCircleService.check_city_support($scope.enquiry.address_attributes.city)
		.then(function(responce){
		    var isCityExists = responce.data.isCityExists
		    if(typeof isCityExists != 'undefined' && isCityExists === true){
			localStorageService.set("currentState", "service_request")
			//window.location.href = "/enquiry#/care_giver_preference"
			$state.go("service_request")
		    }else{
			localStorageService.set("currentState", "new_city_request")
			//window.location.href = "/enquiry#/new_city_request"
			$state.go("new_city_request")
		    }
		})
	    
	}

        
	    
	
        var get_address_details_from_auto_complete = function(){
	    var gmapAddress = JSON.parse($("#place_details_object").val())
            var address = {}
            var line1 = ""
            var line2 = ""
            if(typeof gmapAddress.route != 'undefined' && gmapAddress.route.length > 1){
                line1 += (gmapAddress.route + ", ")
            }
            if(typeof gmapAddress.sublocality_level_3 != 'undefined' && gmapAddress.sublocality_level_3.length > 1){
                line1 += gmapAddress.sublocality_level_3
            }
            if(typeof gmapAddress.sublocality_level_2 != 'undefined' && gmapAddress.sublocality_level_2.length > 1){
                line2 += (gmapAddress.sublocality_level_2 + ", ")
            }
            if(typeof gmapAddress.sublocality_level_1 != 'undefined' && gmapAddress.sublocality_level_1.length > 1){
                line2 += gmapAddress.sublocality_level_1
            }
            if(line1.length > 1){
                address.line1 = line1
            }
            if(line2.length > 1){
                address.line2 = line2
            }

            if(typeof gmapAddress.locality != 'undefined' && gmapAddress.locality.length > 1){
                address.city = gmapAddress.locality
            }

            if(typeof gmapAddress.country != 'undefined' && gmapAddress.country.length > 1){
                address.country = gmapAddress.country
            }

            if(typeof gmapAddress.postal_code != 'undefined' && gmapAddress.postal_code.length > 1){
                address.postal_code = gmapAddress.postal_code
            }
            return address;
        }


        $scope.submit_enquiry_type = function(){
            localStorageService.set("currentState", "service_request")
            $state.go("service_request")
        }
        
        $scope.loadServices = function(){
	    var reg_service_ids = []
            angular.forEach($scope.enquiry.requested_services, function(service){
                reg_service_ids.push(service.service_master_id)
            })

            if (typeof $scope.enquiry.services == 'undefined' || (typeof $scope.enquiry.services != 'undefined' && $scope.enquiry.services.length <= 0)){
                lifeCircleService.getServiceMasters()
                    .then(function(responce){
                        $scope.enquiry.services =  responce.data
			//alert(JSON.stringify($scope.enquiry.services))
			angular.forEach($scope.enquiry.services, function(service){
                            if(reg_service_ids.indexOf(service.id) != -1){
                                service.assigned = true
                            }
                        })

                    });
            }
        }

        $scope.loadLanguages = function(){
            var reg_language_ids = []
            angular.forEach($scope.enquiry.enquiry_languages, function(lang){
                reg_language_ids.push(lang.language_master_id)
            })

            if (typeof $scope.enquiry.languages == 'undefined' || (typeof $scope.enquiry.languages != 'undefined' && $scope.enquiry.languages.length <= 0)){
                lifeCircleService.getLanguageMasters()
                    .then(function(responce){
                        $scope.enquiry.languages =  responce.data
                        angular.forEach($scope.enquiry.languages, function(lang){
                            if(reg_language_ids.indexOf(lang.id) != -1){
                                lang.assigned = true
                            }
                        })
                    });
            }
        }
        
        $scope.gotoAppointment = function(){
            if($scope.selected_care_givers.length < $scope.enquiry.no_of_care_givers){
                alert("Please select required number of caregivers")
            }else{
                $scope.grandTotal = 0;
                angular.forEach($scope.selected_care_givers, function(cg){
                    $scope.grandTotal += cg.total_charge
                })
                $state.go("appointment")
            }
        }

        $scope.toggleCgInSelectList = function(care_giver){
            if($scope.enquiry.no_of_care_givers == 1){
                $scope.selected_care_givers = []
                $scope.selected_care_givers.push(care_giver)
                $scope.gotoAppointment();
            }else{
                var isAlreadyAdded = false;
                var index = -1;
                for(var i = 0 ; i< $scope.selected_care_givers.length ; i++){
                    if($scope.selected_care_givers[i].id == care_giver.id){
                        isAlreadyAdded = true;
                        index = i;
                        break;
                    }
                }

                if(isAlreadyAdded){
                    $scope.selected_care_givers.splice(index, 1);
                    care_giver.selected = false;
                }else{
                    if($scope.selected_care_givers.length < $scope.enquiry.no_of_care_givers){
                        care_giver.selected = true;
                        $scope.selected_care_givers.push(care_giver)
                    }else{
                        alert("Maxumum number of caregivers has been selected.")
                    }
                }
                
                
            }
        }
                                                  
        $scope.appointment_login = true;
        $scope.bookCareGiver = function(isValidForm){
            if(isValidForm ){
                $scope.BookingCgForm = true;
                var service;
                if(typeof $scope.appointment_time == 'undefined' || $scope.appointment_time.length <= 2){
                    $scope.appointment_time = "00:00"
                }
                var appointmentTime = $scope.appointment_date+"-"+$scope.appointment_time
                $scope.enquiry.appointment_time = appointmentTime
                if(typeof $scope.enquiry.id != 'undefined' && $scope.enquiry.id > 0){
                    service = lifeCircleService.updateExistingEnquiry($scope.enquiry.id, $scope.enquiry, $scope.selected_care_givers)
                }else{
                    service = lifeCircleService.bookCareGiver($scope.enquiry, $scope.selected_care_givers)
                }
                service.then(function(responce){
                    $scope.appointment_captured = true
                    $scope.showLogin = false
                    $scope.appointment_login = false
                    $scope.showEnroll = false
                    var data = responce.data
                    if(data.status == true){
                        if($scope.enquiry.form_filled_location == 'PATIENT_HOME')
                        {
                            window.location.href = '/enquiries/'+$scope.enquiry.id+'/enrollments/new'
                        }
                        
                    }else{
                        //alert("Something went wrong..!!")
                    }
                });
            }
        }
	
        $scope.updateAppointment = function(){
            var appointmentTime = $scope.appointment_date+"-"+$scope.appointment_time
            $scope.enquiry.appointment_time = appointmentTime
            $scope.showLogin = true
        }
        
        $scope.list_care_givers = function(){
	    if($scope.validateDates() && $scope.validateTimeSlots()){
                lifeCircleService.list_care_givers($scope.enquiry)
                    .then(function(response){
			$scope.care_givers = response.data
                        if($scope.care_givers.length <= 0){
                           
			    $state.go('customer_on_empty_results')
                        }else{
                            $scope.selected_care_givers = []
                            localStorageService.set("currentState", "list_care_givers")
                            $state.go("list_care_givers")
                        }
                    });
            }
        }

        $scope.capture_customer_on_empty_results = function(){
            $scope.enquiry.appointment_time = $scope.enquiry.appointment_date+"-"+$scope.enquiry.appointment_time
            lifeCircleService.captureCustomerOnEmptyResults($scope.enquiry)
                .then(function(responce){
                    var data = responce.data
                    if(data.status == true){
                        $scope.appointment_captured = true
                        $scope.showLogin = false
                    }else{
                        //alert("Something went wrong..!!")
                    }
                });

        }

        window.onbeforeunload = function () {
            localStorageService.set("enquiry", JSON.stringify($scope.enquiry))
        };

        $scope.gotoStep0 = function(){
            $scope.servicesShown = true
            $scope.cgOptionsShown = false
            $scope.timeRegionShown = false
            $("#collapseOne").collapse('toggle')
        }
        
        $scope.gotoStep1 = function(){
           var assignedAtleastOne = 0
           angular.forEach($scope.enquiry.services, function(service){
               if(typeof service.assigned != 'undefined' && service.assigned == true){
                   assignedAtleastOne += 1;
               }
           });
	   
           if(assignedAtleastOne > 0){
               localStorageService.set("currentState", "care_giver_preference")
               $state.go("care_giver_preference")
           }
       }
        
        $scope.gotoStep2 = function(){
            var assignedAtleastOne = 0
            $scope.genderCaptured = false
            if(typeof $scope.enquiry.care_giver_gender == 'undefined'){
                $scope.genderCaptured = true
            }
            $scope.price_empty = false
            
            //if($scope.enquiry.affordable_price == null){
            //    $scope.price_empty = true
            //    return false;
            //}
            

            if( !validateNoOfCareGivers() ){
                return false;
            }
            
            angular.forEach($scope.enquiry.languages, function(language){
                if(typeof language.assigned != 'undefined' && language.assigned == true){
                    assignedAtleastOne += 1;
                }
            });
            
            if(assignedAtleastOne  > 0  ){
		localStorageService.set("currentState", "schedule")
		$state.go("schedule")
                
            }else{
                $scope.language_empty = true
            }           
        }

        var validateNoOfCareGivers = function(){
            $scope.no_of_caregivers_exceeded = false;
            $scope.no_of_caregivers_empty = false;

            if($scope.enquiry.no_of_care_givers > 2){
                $scope.no_of_caregivers_exceeded = true;
                return false;
            }else if(typeof $scope.enquiry.no_of_care_givers == 'undefined' || $scope.enquiry.no_of_care_givers <1){
                $scope.no_of_caregivers_empty = true;
                return false;
            }else{
                return true;
            }
        }


        $scope.validateDates = function(){
            $scope.invalidDate = false
            $scope.invalidServiceRequiredFromDate = false;
            var from_date_parts;
            if(typeof $scope.enquiry.open_term != 'undefined' && ($scope.enquiry.open_term == false || $scope.enquiry.open_term == "false") && (typeof $scope.enquiry.service_required_to == 'undefined' || $scope.enquiry.service_required_to.length <= 1)){
                $scope.invalidDate = true;
                return false;
            }
            if(typeof $scope.enquiry.service_required_from == 'undefined' || $scope.enquiry.service_required_from == null || $scope.enquiry.service_required_from.length <= 1){
                $scope.invalidServiceRequiredFromDate = true;
                return false;
            }else{
                from_date_parts= $scope.enquiry.service_required_from.split("/")
            }
            
            var fromDate = new Date(from_date_parts[2], parseInt(from_date_parts[1])-1, from_date_parts[0])
            if(typeof $scope.enquiry.service_required_to != 'undefined' && $scope.enquiry.service_required_to != null && $scope.enquiry.service_required_to != 'null' && $scope.enquiry.service_required_to.length > 1){
                var to_date_parts = $scope.enquiry.service_required_to.split("/")
                var toDate = new Date(to_date_parts[2], parseInt(to_date_parts[1])-1, to_date_parts[0])
                if(fromDate > toDate){
                    $scope.invalidDate = true
                    return false
                }
            }
            return true;
        }

        $scope.validateTimeSlots = function(){
            $scope.requiredTimeSlotsError = false;
            if(typeof $scope.enquiry.requested_time_slots != 'udefined' && $scope.enquiry.requested_time_slots.length > 0){
                if((typeof $scope.enquiry.requested_time_slots[0].time_from == 'undefined' || $scope.enquiry.requested_time_slots[0].time_from.length <= 1) && ($scope.enquiry.caregiver_stay != 'LIVE_IN')){
                    $scope.requiredTimeSlotsError = true;
                }else{
                    return !$scope.invalidTimeSlot
                }
            }
            
        }

       

        $scope.applyAccordian = function(){
            accordian.invoke()
        }

        
    }]);  
    
})(angular, lifeCircleApp);
