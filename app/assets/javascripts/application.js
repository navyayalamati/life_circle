

/*******************************************/


//= require jquery.min
//= require responsiveslides.min
/*******************************************/


//= require jquery_ujs
//= require jquery-ui
//= require bootstrap.min


//= require jquery-ui/autocomplete
//= require autocomplete-rails

//= require styleselector
//= require fhmm
//= require totop
//= require modernizr.custom.75180

//= require jquery.themepunch.plugins.min
//= require jquery.themepunch.revolution.min
//= require jquery.jcarousel.min
//= require underscore
//= require gmaps/google
//= require moment
//= require fullcalendar
//= require jquery.readyselector


//= require jquery.modal

//= require responsive-tabs.min
//= require accordion_custom

//= require angular
//= require readmore
//= require angular-resource
//= require angular-ui-router.min
//= require angular-rails-templates
//= require angular-file-upload.min
//= require angular-file-upload.min.map
//= require angular-local-storage.min
//= require bootstrap-wysihtml5
//= require jquery.datetimepicker
//= require_tree ./templates

//= require_tree .





$(document).ready(function() {
    
    $('#carousel-example').carousel();
    
    $('.wysiHtml5textarea').wysihtml5({
        toolbar: {
            "fa": true,
            "p": true,
            "image": false,
            "link": false
        }
    });
});
