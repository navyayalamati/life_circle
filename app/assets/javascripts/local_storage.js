(function(angular, app) {
    "use strict";
    app.config(["localStorageServiceProvider", function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('lefeCircleApp')
            .setStorageType('sessionStorage')
            .setNotify(true, true)
    }]);
    
})(angular, lifeCircleApp);

