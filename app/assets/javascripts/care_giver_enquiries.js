function init_close_cg_enquiry(caregiverId, name){    
   
    $("#closing_caregiver_id").val(caregiverId);
    $("#closing_comments").val("");
    $("#close-cg-pop-up").modal("show")   
   
}




function closecgEnquiry(){

    var closecaregiverId = $("#closing_caregiver_id").val()
    var closingComments = $("#closing_comments").val()
    var url = "/care_giver_enquiries/"+closecaregiverId+"/close.json"
    
    $.ajax({
        method: "PUT",
        data: {"closing_comments": closingComments},
        url: url,
        success: function( data ) {
            location.reload();
        },
        error: function(ermsg){
            console.log(ermsg)
        }
    });
}

function hold_enquiry(care_giver_id){
    var url = "/care_giver_enquiries/"+care_giver_id+"/holding.json"
    $.ajax({
        method: "PUT",
        data: {"closing_comments": ""},
        url: url,
        success: function( data ) {
            location.reload();
        },
        error: function(ermsg){
            console.log(ermsg)
        }
    });

}


function recruit_enquiry(care_giver_id){
    var url = "/care_giver_enquiries/"+care_giver_id+"/recruit.json"
    $.ajax({
        method: "PUT",
        data: {"closing_comments": ""},
        url: url,
        success: function( data ) {
            location.reload();
        },
        error: function(ermsg){
            console.log(ermsg)
        }
    });

}



