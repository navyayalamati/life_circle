(function(angular, app) {
    "use strict";
    app.service("resources",["$resource", function($resource) {
        var DisabilityMaster = $resource('/disability_masters/:id.json', {id: '@id'},
                                         {
                                             "update": { method: "PUT"}
                                         }
                                        );
        var AreasMaster = $resource('/city_masters/:city_master_id/area_masters/:id.json', {id: '@id', city_master_id: '@city_master_id'},
                                    {
                                        "update": { method: "PUT"}
                                    }
                                   );

        var DrugMaster = $resource('/drug_masters/:id.json', {id: '@id'},
                                      {
                                          "update": { method: "PUT"}
                                      }
                                  );

        var Routine = $resource('/routine_masters/:id.json', {id: '@id'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                                  );
        var Patientabilities = $resource('/enrollments/:enrollment_id/patient_selfcare_abilities/:id.json', {id: '@id', enrollment_id: '@enrollment_id'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                                  );                                  
        var Route = $resource('/route_masters/:id.json', {id: '@id'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                             );
	var Formmaster = $resource('/form_masters/:id.json', {id: '@id'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                               );
        var Unit = $resource('/unit_masters/:id.json', {id: '@id'},
                                {
                                    "update": { method: "PUT"}
                                }
                               );
        var PatientRoutine = $resource('/enrollments/:enrollment_id/patient_routines/:id.json', {id: '@id', enrollment_id: '@enrollment_id' },
                                       {                                      
                                           "update": { method: "PUT"}
                                       }
                                      );
        var Careprovide = $resource('/enrollments/:enrollment_id/care_to_be_provided_plans/:id.json', {id: '@id', enrollment_id: '@enrollment_id' },
                                       {                                      
                                           "update": { method: "PUT"}
                                       }
                                      );
        
        var DrugQuantity = $resource('/enrollments/:enrollment_id/drug_quantity_masters/:id.json', {id: '@id', enrollment_id: '@enrollment_id'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                                    );

        var KeyvitalsMaster = $resource('/key_vitals_masters/:id.json', {id: '@id'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                                  );
        var ServiceMaster = $resource('/service_masters/:id.json', {id: '@id'},
                                         {
                                             "update": { method: "PUT"}
                                         }
                                     );

        var SubServiceMaster = $resource('/service_masters/:service_master_id/sub_service_masters/:id.json', {id: '@id', service_master_id: '@service_master_id' },
                                         {
                                             "update": { method: "PUT"}
                                    }
                                        );        
        var Caregiver = $resource('/care_giver_masters/:id.json', {id: '@id'},
                                         {
                                             "update_status": { method: "PUT"}
                                         }
                                        );
        var Selfcare = $resource('/patient_selfcare_ability_masters/:id.json', {id: '@id'},
                                      {
                                          "update": { method: "PUT"}
                                      }
                                      );

        
        var Vitaltrend = $resource('/enrollments/:enrollment_id/vital_trends/:id.json', {id: '@id', enrollment_id: '@enrollment_id'},
                                   { 
                                       "update": { method: "PUT"},                                         
                                       "query": {
                                           method: 'GET',
                                           isArray: true,
                                       },               
                                   }
                                  );
        var Trackvital = $resource('/enrollments/:enrollment_id/track_vitals/:id.json', {id: '@id', enrollment_id: '@enrollment_id'},
                                   { 
                                       "update": { method: "PUT"},                                         
                                       "query": {
                                           method: 'GET',
                                           isArray: false,
                                       },               
                                   }
                                  );        

        var AdaptiveEquipmentMaster = $resource('/patient_adaptive_equipment_masters/:id.json', {id: '@id'},
                                       {
                                           "update": { method: "PUT"}
                                       }
                                      );

        var EnquiryReferenceMaster = $resource('/enquiry_reference_masters/:id.json', {id: '@id'},
                                         {
                                             "update": { method: "PUT"}
                                         }
                                        );
        var LostReasonMaster = $resource('/lost_reason_masters/:id.json', {id: '@id'},
                                         {
                                             "update": { method: "PUT"}
                                         }
                                        );
        var LanguageMaster = $resource('/language_masters/:id.json', {id: '@id'},
                                         {
                                             "update": { method: "PUT"}
                                         }
                                      );
        var Ticketcateg = $resource('/ticket_category_masters/:id.json', {id: '@id'},
                                       {
                                           "update": { method: "PUT"}
                                       }
                                      );
        var Ticket = $resource('/tickets/:id.json?role=:role&current_tag=:current_tag', {id: '@id', role: '@role', current_tag:'@current_tag'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                               );
        var Dailycheck = $resource('/daily_checks/:id.json?role=:role&check_id=:check_id', {id: '@id', check_id: '@check_id', role: '@role'},
                                   {
                                       "update": { method: "PUT"},
                                       "query": {
                                                    method: 'GET',
                                                    isArray: false,
                                                }
                                   }
                               );                      			
        var Ticketconversation = $resource('/ticket_conversations/:id.json?ticket_id=:ticket_id', {id: '@id', ticket_id: '@ticket_id'},
                                            {
                                                query: {
                                                    method: 'GET',
                                                    isArray: false,
                                                }
                                            }
                                     );
        var TransportChargeMaster = $resource('/transport_charge_masters/:id.json', {id: '@id'},
                                         {
                                             "update": { method: "PUT"}
                                         }
                                        );
        var Patientratemaster = $resource('/patient_rate_masters/:id.json', {id: '@id'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                               );
        var Timeofadmin = $resource('/time_of_administrations/:id.json?drug_quantity_master_id=:drug_quantity_master_id', {id: '@id', drug_quantity_master_id: '@drug_quantity_master_id'},
                                   {
                                       "update": { method: "PUT"}
                                   }
                               );
        var CareGiverLeave = $resource('/care_giver_masters/:care_giver_master_id/care_giver_leaves/:id.json', {id: '@id', care_giver_master_id: '@care_giver_master_id'},
                                       {
                                           "update": { method: "PUT"},
                                           "approve": {url: "/care_giver_masters/:care_giver_master_id/care_giver_leaves/:id/approve.json", method: "PUT"},
                                           "reject": {url: "/care_giver_masters/:care_giver_master_id/care_giver_leaves/:id/reject.json", method: "PUT"},                                           
                                           "allCGLeaves": {url: "/care_giver_leaves/all_cg_leaves.json", isArray: true}                                           
                                    });

        var EnquiryConversation = $resource('/enquiry_conversations/:id.json?conversation_master_id=:conversation_master_id&conversation_master_type=:conversation_master_type', {id: '@id', conversation_master_type: '@conversation_master_type', conversation_master_id: '@conversation_master_id'},
                                            {
                                                query: {
                                                    method: 'GET',
                                                    isArray: false,
                                                }
                                            }
                                           );                                     
        
        
        return {
            Disability :  DisabilityMaster,
            AreasMaster : AreasMaster,
            Service : ServiceMaster,
            EnquiryReference : EnquiryReferenceMaster,
            LostReason : LostReasonMaster,
            Language : LanguageMaster,
            TransportCharge : TransportChargeMaster,
            CareGiverLeave : CareGiverLeave,
            EnquiryConversation : EnquiryConversation,
            Selfcare : Selfcare,
            AdaptiveEquipment : AdaptiveEquipmentMaster,
            SubServiceMaster : SubServiceMaster,
            PatientRoutine :  PatientRoutine,
            DrugMaster : DrugMaster,
            DrugQuantity : DrugQuantity,
            KeyvitalsMaster : KeyvitalsMaster,
            Vitaltrend : Vitaltrend,
            Trackvital : Trackvital,
            Routine : Routine,
            Route : Route,
            Formmaster : Formmaster,
            Unit : Unit,
            Careprovide : Careprovide,
            Caregiver : Caregiver,
            Ticketcateg : Ticketcateg,            
            Ticket : Ticket,
            Ticketconversation : Ticketconversation,
            Dailycheck : Dailycheck,
            Patientratemaster : Patientratemaster,
            Timeofadmin : Timeofadmin,
            Patientabilities : Patientabilities

        };
    }]);
})(angular, lifeCircleApp);
