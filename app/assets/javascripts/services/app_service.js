(function(angular, app) {
    "use strict";
    app.service("lifeCircleService",["$http", function($http) {
	
        
        var getAllEnrollments = function(){
	    var url = "/enrollments.json"
	    return $http.get(url);
	}
	
	var edit_values = function(id){
	    var url = "/enquiries/edit_values?id="+id
	    return $http.get(url);
	}
        var physicalDescription = function(careGiverMasterId){
            var url = "/care_giver_masters/"+careGiverMasterId+"/care_giver_additional_details/physical_description.json";
            return $http.get(url);
        }        

        var updatePatientRoutine = function(enrollment_id, patient_routine){
            var url = "/enrollments/"+enrollment_id+"/patient_routines/update_patient_routine.json";
            return $http.put(url, { patient_routine : patient_routine });
            //alert(JSON.stringify(patient_routine))
       }

	var updateUserRole  = function(user){
	    var url = "/user_creations/update_user.json";
	    return $http.put(url, {user : user});

	}
	
	var update_appointment = function(enquiries){
	    var url = "/enquiries/update_appointment.json";
	    return $http.put(url, {enquiries : enquiries});
	    
	}

	var incomplete_pcp = function(){
            var url = "/enrollments/pcp.json";
            return $http.get(url);
        }   
	var all_appointments = function(){
            var url = "/enquiries/all_appointments.json";
            return $http.get(url);
        }   
	
	
        var getPatientRoutines = function(enrollment_id){
            var url = "/enrollments/"+enrollment_id+"/patient_routines.json";
            return $http.get(url, { enrollment_id : enrollment_id });
        }
        
        var destroyPatientRoutine = function(enrollment_id, patient_routine){
            //alert("services")
	    var url = "/enrollments/"+enrollment_id+"/patient_routines/"+patient_routine.id+".json"
	    return $http.delete(url);
	};

        var savePatientAbilities = function(enrollment_id,abilities){
            var url = "/enrollments/"+enrollment_id+"/patient_selfcare_abilities/save_patient_abilities.json";
            return $http.post(url, { enrollment_id : enrollment_id, abilities : abilities });
        }

        var updatePatientAbilities = function(enrollment_id,ability){
            var url = "/enrollments/"+enrollment_id+"/patient_selfcare_abilities/update_patient_abilities.json";
           // alert(enrollment_id)
            //alert(JSON.stringify(abilities))
            return $http.put(url, { ability : ability });
       }

        var getPatientAbilities = function(enrollment_id){
            var url = "/enrollments/"+enrollment_id+"/patient_selfcare_abilities.json";
            return $http.get(url, { enrollment_id : enrollment_id });
        }

	var savePatientEquipments = function(enrollment_id,equipments){
            var url = "/enrollments/"+enrollment_id+"/patient_adaptive_equipments/save_patient_equipments.json";
            return $http.post(url, { equipments : equipments });
        }

        var updatePatientEquipments = function(enrollment_id,equipment){
            var url = "/enrollments/"+enrollment_id+"/patient_adaptive_equipments/update_patient_equipments.json";
            // alert(enrollment_id)
            //alert(JSON.stringify(abilities))
            return $http.put(url, {equipment : equipment });
        }

        var getPatientEquipments = function(enrollment_id){
            var url = "/enrollments/"+enrollment_id+"/patient_adaptive_equipments.json";
            return $http.get(url);
        }
        var savePatientDisabilities = function(enrollment_id,disabilities){
            var url = "/enrollments/"+enrollment_id+"/patient_disabilities/save_patient_disabilities.json";
            return $http.post(url, { disabilities : disabilities });
        }

        var updatePatientDisabilities = function(enrollment_id,equipment){
            var url = "/enrollments/"+enrollment_id+"/patient_disabilities/update_patient_disabilities.json";
            // alert(enrollment_id)
            //alert(JSON.stringify(abilities))
            return $http.put(url, {disabilities : disabilities });
        }
        var getEquipments = function(care_giver_id){
            var url = "/care_giver_masters/"+care_giver_id+"/care_giver_return_equipments.json";
            return $http.get(url);
        }
        var saveEquipments = function(care_giver_id,equipments){
            alert(JSON.stringify(care_giver_id))
            var url = "/care_giver_masters/"+care_giver_id+"/care_giver_return_equipments/save_equipments.json";
            return $http.post(url, { equipments : equipments });
        }

        var updateEquipments = function(care_giver_id,equipment){
            var url = "/care_giver_masters/"+care_giver_id+"/care_giver_return_equipments/update_equipments.json";
            // alert(enrollment_id)
            //alert(JSON.stringify(abilities))
            return $http.put(url, {equipment : equipment });
        }
     

        var getPatientDisabilities = function(enrollment_id){
            var url = "/enrollments/"+enrollment_id+"/patient_disabilities.json";
            return $http.get(url);
        }
        
        var saveRequestedservices = function(enrollment_id,services){
            var url = "/enrollments/"+enrollment_id+"/requested_services/save_requested_services.json";
            return $http.post(url, { services : services });
        }

        var getRequestedservices = function(enrollment_id){
            var url = "/enrollments/"+enrollment_id+"/requested_services.json";
            return $http.get(url);
        }
        
        var getEnrolledservices = function(enrollment_id){
            var url = "/enrollments/"+enrollment_id+"/requested_services/enrolled.json";
            return $http.get(url);
        }
        
	var getAreasInCity = function(city_id){
	    var url = "/city_masters/"+city_id+"/area_masters.json";
            return $http.get(url);

	}

	var admin_approval_cg = function(){
	    var url = "/care_giver_enquiries.json";
	    return $http.get(url);
	}
	
	var all_cg = function(){
	    var url = "/care_giver_masters.json";
	    return $http.get(url);
	}
	
	
	var getCityMasters = function(city_id){
	    var url = "/city_masters.json";
            return $http.get(url);
	}
	var getServiceMasters = function(){
	    var url = "/service_masters.json";
            return $http.get(url);
	}

        var getDisabilityMasters = function(){
	    var url = "/disability_masters.json";
            return $http.get(url);
	}
        
        var getLanguageMasters = function(){
	    var url = "/language_masters.json";
            return $http.get(url);
	}

        var checkUserSessionExists = function(){
	    var url = "/check_user_session.json";
            return $http.get(url);
            
        }
	var check_location_support = function(city){
	    var url = "/city_masters/check_location_support.json"
            return $http.post(url , {city : city});
	}
	
	var enquiry_details = function(enquiry){
	    var url = "/enquiries/enquiry_details.json"
            return $http.post(url , {enquiry : enquiry});
	}
	
        var list_care_givers = function(enquiry){
            var url = "/enquiries/list_care_givers.json"
            return $http.post(url, {enquiry: enquiry});
        }
        
        var checkCgAvailabilityOnModifiedQuery = function(enquiry){
            var url = "/enquiries/"+enquiry.id+"/check_cg_availability_on_modified_query.json"
            return $http.post(url, {enquiry: enquiry});
        }
        
	var from_to_date = function(from , to){
	    var url = '/care_giver_daily_attendances/cg_attendance_in_date_range.json?from='+from+'&to='+to
	    return $http.get(url);
	}
        var daily_attendance = function(attendance){
            var url = "/care_giver_masters/daily_attendance.json"
            return $http.get(url);
        }

        var bookCareGiver = function(enquiry, care_givers){
            var url = "/enquiries/book_care_giver.json"
            return $http.post(url, {enquiry: enquiry, care_givers: care_givers});
        }

        var updateAppointment= function(enquiry_id, appointment_date_time){
            var url = "/enquiries/"+enquiry_id+"/update_appointment.json"
            return $http.put(url, {appointment_time: appointment_date_time});
        }

        var newEnrollment = function(enquiry_id){
            var url = "/enquiries/"+enquiry_id+"/enrollments/new.json"
            return $http.get(url);
        }

        var createEnrollment = function(enquiry_id, enrollment){
            var url = "/enquiries/"+enquiry_id+"/enrollments.json"
            return $http.post(url, {enrollment: enrollment});
        }

        var check_city_support = function(city){
            var url = "/city_masters/check_city_support.json?city="+city
            return $http.get(url);

        }

        var submit_new_City_request = function(new_city_request){
            var url = "/new_city_requests.json"
            return $http.post(url, {new_city_request: new_city_request});

        }

        var updateEnquiry = function(enquiry){
            var url = "/enquiries/"+enquiry.id+".json"
            return $http.put(url, {enquiry: enquiry});
        }
        var updateAuditEnquiry = function(enquiry){
            var url = "/enquiries/update_to_audit.json"
            return $http.put(url, {enquiry: enquiry});
        }
        
        var safetyMeasures = function(enquiryId, safety_params){
            var url = "/enquiries/"+enquiryId+"/enrollments/safety_measures.json"
            return $http.post(url, {safety_params: safety_params});
        }

        var getEnrollment = function(enrollmentId){
            var url = "/enrollments/"+enrollmentId+".json"
            return $http.get(url)
        }

        var changeEnrollmentPlan  = function(enrollmentId, enquiry){
            var url = "/enrollments/"+enrollmentId+"/update_change_plan.json"
            return $http.post(url, {enquiry: enquiry})
        }

        var getCurrentUser = function(){
            var url = "/get_current_user.json"
            return $http.get(url)
        }

        var getPatientDetailsWithUserId = function(userId){
            var url = "/patients/get_patient_details_with_user_id.json?user_id="+userId
            return $http.get(url)
        }

        var getNewPaymentForReview  = function(enrollmentId, generationDate){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/review_invoices.json?generation_date="+generationDate
            return $http.get(url)
        }

        var issueCreditNote = function(enrollmentId, paymentId, creditNoteId){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/"+paymentId+"/issue_credit_note.json"
            return $http.put(url, {credit_note_id: creditNoteId})

        }

        var revokeCreditNote = function(enrollmentId, paymentId, creditNoteId){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/"+paymentId+"/revoke_credit_note.json"
            return $http.put(url, {credit_note_id: creditNoteId})

        }

        var markAsPendingCreditNote = function(enrollmentId, paymentId, creditNoteId){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/"+paymentId+"/mark_as_pending_credit_note.json"
            return $http.put(url, {credit_note_id: creditNoteId})

        }

        var updatePaymentDiscountDetails = function(enrollmentId, paymentId, params){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/"+paymentId+"/update_payment_discount_details.json"
            return $http.put(url, params)
        }

	var cgCorrespondingPatients = function(care_giver_details){
	    var url = '/care_giver_masters/patients.json'
	    return $http.post(url, {enquiry: enquiry})
	}
	
        var recalculatePayment = function(enrollmentId, payment_master, generation_date){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/"+payment_master.id+"/recalculate.json"
            return $http.post(url, {payment_master: payment_master, generation_date: generation_date})
        }

        var captureCustomerOnEmptyResults = function(enquiry){
            var url = "/enquiries/capture_customer_on_empty_results.json"
            return $http.post(url, {enquiry: enquiry});
        }

        var getEnquiry = function(enquiryId){
            var url = "/enquiries/"+enquiryId+".json"
            return $http.get(url);
        }

        var updateExistingEnquiry = function(enquiryId, enquiry, care_givers){
            var url = "/enquiries/"+enquiryId+"/update_pre_enquiry.json"
            return $http.post(url, {enquiry: enquiry, care_givers: care_givers});
        }
         
        var checkCgTransfer = function(conversation_master_id, cg_leave_id){
             var url = "/care_giver_masters/get_caregivers.json?care_giver_enquiry_id="+conversation_master_id+"&care_giver_leave_id="+cg_leave_id
             return $http.get(url)
        }
	
	var payroll = function(from ,to){
            var url = "/care_giver_daily_attendances/payroll.json?from="+from+"&to="+to
            return $http.get(url);
        }
	
	var menu_bar = function(){
	    var url = "/role_creations/menu_bar.json"
            return $http.get(url);
        }

	var loadCareGiverChargeBuckets = function(){
            var url = "/care_giver_charge_buckets.json"
            return $http.get(url);

        }

        var saveCareGiverChargeBuckets = function(charge_buckets){
            var url = "/care_giver_charge_buckets.json"
            return $http.post(url, {care_giver_charge_buckets: charge_buckets});
        }
        
	
	var save_menu = function(menu , array){
            var url = "/role_creations/save_menu.json"
            return $http.post(url, {menu: menu , array: array});
        }
	
	var getMenu = function(id){
            var url = "/role_creations/get_menu.json"
            return $http.post(url, {id: id});
        }

	var role = function(){
	    var url = "/role_creations/role.json"
            return $http.get(url);
	}
	
	var role_menu = function(){
	    var url = "/role_creations/role_menu.json"
            return $http.get(url);
	}
	
	var corresponding_menu = function(role){
	    var url = "/role_creations/corresponding_menu.json"
            return $http.post(url, {role: role});
	    
	}
	var save_user = function(user){
	    var url = "/user_creations/save_user.json";
            return $http.post(url, {user: user});
	}


	var update_menu = function(array){
	    var url = "/role_creations/update_menu.json"
            return $http.post(url, {array: array});
	    
	}
	

	var get_report = function(report){
	    alert(JSON.stringify(report))
	    var url = "/reports/get_report.json"
            return $http.post(url, {report: report});
	    
	}

	var balance_report = function(report){
	    var url = "/reports/balance_report.json"
            return $http.post(url, {report: report});
	    
	}

	var payment_report = function(report){
	    var url = "/reports/payment_report.json"
            return $http.post(url, {report: report});
	}
	
	var invoice_report = function(report){
	    var url = "/reports/invoice_details.json"
            return $http.post(url, {report: report});
	}
	
	var credit_report = function(report){
	    var url = "/reports/credit.json"
            return $http.post(url, {report: report});
	}
	
	var due_report = function(){
	    var url = "/reports/age_report.json"
            return $http.get(url);
	}
        var getClosingEnrollmentPaymentAttributes = function(enrollment_id, close_date){
            var url = "/enrollments/"+enrollment_id+"/payment_masters/payment_on_closing_enrollment.json?close_date="+close_date
            return $http.get(url);
        }
        var create_credit_note = function(enrollmentId, paymentId, creditNoteParams){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/"+paymentId+"/create_credit_note.json"
            return $http.post(url, {credit_note: creditNoteParams})
        }
        var save_cg_enquiry = function(enquiry){
            var url = "/care_giver_enquiries/create_enquiry.json"
            return $http.post(url, {enquiry: enquiry})
        }
        var destroy_credit_note = function(enrollmentId, paymentId, creditNoteId){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/"+paymentId+"/destroy_credit_note.json?credit_note_id="+creditNoteId
            return $http.delete(url)
        }
        var getReasons = function(){
            var url = "/enrollments/reason_for_coming.json"
            return $http.get(url);
        }
        
        var update_credit_note_status = function(enrollmentId, paymentId, creditNoteId, status){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/"+paymentId+"/update_credit_note_status.json"
            return $http.put(url, {credit_note_id: creditNoteId, status: status})
        }

        var saveClosingPayment = function(enrollmentId, credit_notes, payment_master){
            var url = "/enrollments/"+enrollmentId+"/payment_masters/save_closing_payment.json"
            return $http.post(url, {credit_notes: credit_notes, payment_master: payment_master})
        }
        
        var missedCallRequests = function(page, page_status){
            return $http.get("/enquiries/missed_call_requests.json", {params: {page: page, status: page_status}})
        }

        var missedCallRequestsWithFilter = function(page, page_status, filter){
            return $http.get("/enquiries/missed_call_requests.json", {params: {page: page, status: page_status, filter: filter}})
        }


        var enquiries = function(page, page_status){
            return $http.get("/enquiries.json", {params: {page: page, status: page_status}})
        }

        var enquiriesWithFilter = function(page, page_status, filter){
            return $http.get("/enquiries.json", {params: {page: page, status: page_status, filter: filter}})
        }

        var getJobPayments = function(page, jobRunId){
            return $http.get("/job1_runs/"+jobRunId+".json", {params: {page: page}})
        }

        

        var mailSelectedPayments = function(jobRunId, paymentIds){
            return $http.get("/job_runs/"+jobRunId+"/mail_selected_payments.json?payment_master_ids="+paymentIds)
        }

        var nullifySelectedPayments = function(jobRunId, paymentIds){
            return $http.put("/job_runs/"+jobRunId+"/nullify_selected_payments.json?payment_master_ids="+paymentIds)
        }

        var mailAllPayments = function(jobRunId){
            return $http.get("/job_runs/"+jobRunId+"/mail_all_payments.json")
        }

	var getSuperCareGiver = function(){
	    var url = "/enrollments/super_care_giver.json"
            return $http.get(url);
	}
	    
        var save_city = function(city , address) {
            var url = "/city_masters/save_city.json"
            return $http.post(url, {city: city , address:address})
        }

        return {
            getReasons : getReasons,
            updateAuditEnquiry : updateAuditEnquiry,
            save_city : save_city,
            save_cg_enquiry : save_cg_enquiry,
            mailAllPayments : mailAllPayments,
            nullifySelectedPayments : nullifySelectedPayments,
            mailSelectedPayments : mailSelectedPayments,
            getJobPayments : getJobPayments,
            missedCallRequestsWithFilter : missedCallRequestsWithFilter,
	    update_appointment: update_appointment,
	    edit_values :edit_values,
            enquiriesWithFilter : enquiriesWithFilter,
            enquiries : enquiries,
            missedCallRequests : missedCallRequests,
            updateCreditNoteStatus : update_credit_note_status,
            destroyCreditNote : destroy_credit_note,
            createCreditNote : create_credit_note,
	    due_report : due_report,
	    credit_report : credit_report,
	    invoice_report : invoice_report,
	    payment_report : payment_report,
	    balance_report : balance_report,
	    get_report : get_report,
	    enquiry_details : enquiry_details,
	    cgCorrespondingPatients : cgCorrespondingPatients,
	    getAllEnrollments : getAllEnrollments,
	    all_cg :all_cg,
            physicalDescription :  physicalDescription,
	    getAreasInCity : getAreasInCity,
	    getCityMasters : getCityMasters,
            checkUserSessionExists : checkUserSessionExists,
            getServiceMasters : getServiceMasters,
            getLanguageMasters : getLanguageMasters,
            list_care_givers : list_care_givers,
            bookCareGiver : bookCareGiver,
            updateAppointment : updateAppointment,
            getDisabilityMasters : getDisabilityMasters,
            newEnrollment : newEnrollment,
            createEnrollment : createEnrollment,
            check_city_support : check_city_support,
            submit_new_City_request : submit_new_City_request,
            checkCgAvailabilityOnModifiedQuery : checkCgAvailabilityOnModifiedQuery,
            updateEnquiry : updateEnquiry,
            safetyMeasures : safetyMeasures,
            getEnrollment : getEnrollment,
            changeEnrollmentPlan : changeEnrollmentPlan,
            getCurrentUser : getCurrentUser,
            getPatientDetailsWithUserId : getPatientDetailsWithUserId,
	    check_location_support :  check_location_support,
            daily_attendance : daily_attendance,
	    admin_approval_cg : admin_approval_cg,
            getCurrentUser : getCurrentUser,
            savePatientAbilities : savePatientAbilities,
            getPatientAbilities : getPatientAbilities,
            updatePatientAbilities : updatePatientAbilities,
            savePatientEquipments : savePatientEquipments,
            getPatientEquipments : getPatientEquipments,
            updatePatientEquipments : updatePatientEquipments,
            savePatientDisabilities : savePatientDisabilities,
            getPatientDisabilities : getPatientDisabilities,
            updatePatientDisabilities : updatePatientDisabilities,
            saveRequestedservices : saveRequestedservices,
            getRequestedservices : getRequestedservices,
            getNewPaymentForReview : getNewPaymentForReview,
            issueCreditNote : issueCreditNote,
            revokeCreditNote : revokeCreditNote,
            updatePaymentDiscountDetails : updatePaymentDiscountDetails,
	    from_to_date : from_to_date,
            getPatientRoutines : getPatientRoutines,
            updatePatientRoutine : updatePatientRoutine,
            destroyPatientRoutine : destroyPatientRoutine,
            markAsPendingCreditNote : markAsPendingCreditNote,
            recalculate : recalculatePayment,
            captureCustomerOnEmptyResults : captureCustomerOnEmptyResults,
            getEnquiry : getEnquiry,
            updateExistingEnquiry : updateExistingEnquiry,
            checkCgTransfer : checkCgTransfer,
	    payroll:payroll,
	    menu_bar :menu_bar,
	    save_menu :save_menu,
            saveCareGiverChargeBuckets : saveCareGiverChargeBuckets,
            loadCareGiverChargeBuckets  : loadCareGiverChargeBuckets,
	    getMenu :getMenu,
	    role : role,
	    role_menu : role_menu,
	    corresponding_menu : corresponding_menu,
	    save_user : save_user,
	    updateUserRole : updateUserRole,
	    update_menu : update_menu,
            getClosingEnrollmentPaymentAttributes : getClosingEnrollmentPaymentAttributes,
            saveClosingPayment : saveClosingPayment,
	    incomplete_pcp : incomplete_pcp,
	    all_appointments : all_appointments,
            getEnrolledservices : getEnrolledservices,
            getEquipments :getEquipments,
            saveEquipments : saveEquipments,
            updateEquipments : updateEquipments,
	    getSuperCareGiver : getSuperCareGiver
         };
    }]);
})(angular, lifeCircleApp);
