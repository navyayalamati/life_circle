(function(angular, app) {
    "use strict";
    app.config(["$stateProvider", function($stateProvider) {
        
        $stateProvider.state('city_and_area', {
            url: "/city_and_area",
            templateUrl: 'city_and_area_enquiry.html',
            controller: "welcomeEnquiryController",
            resolve: {
                clear: ["localStorageService", function(localStorageService) {
		    console.log("clearing local storage======")
                    localStorageService.clearAll();
                }]
            },
        });
        $stateProvider
            .state('appointment', {
		url: "/appointment",
		templateUrl: 'appointment.html'
            })            
            .state('appointment.login', {
                url: "/appointment/login",
                templateUrl: 'login.html',
                controller: "loginController"
            })
            .state('appointment.registration', {
                url: "/appointment/registration",
                templateUrl: 'registration.html',
                controller: "RegistrationController"
            })
            .state('appointment.admin_patient_create', {
                url: "/appointment/admin_patient_create",
                templateUrl: 'admin_patient_creation.html',
                controller: "AdminPatientCreationController"
            });


        $stateProvider.state('confirmBooking', {
            url: "/confirm_booking",
            templateUrl: 'confirm_booking.html'
        });

        $stateProvider.state('new_city_request', {
            url: "/new_city_request",
            templateUrl: 'new_city_request.html',
            controller: "NewCityRequestController"
        });
        $stateProvider.state('service_request', {
            url: "/service_request",
            templateUrl: 'service_request.html'
        });
	$stateProvider.state('care_giver_preference', {
            url: "/care_giver_preference",
            templateUrl: 'care_giver_preference.html'
        });
	$stateProvider.state('schedule', {
            url: "/schedule",
            templateUrl: 'schedule.html'
        });
        $stateProvider
            .state('list_care_givers', {
                url: "/list_care_givers",
                templateUrl: 'list_care_givers.html'
            });
        $stateProvider
            .state('customer_on_empty_results', {
                url: "/customer_on_empty_results",
                templateUrl: 'capture_customer_on_empty_results.html'
            });

        $stateProvider.state('careGiverCharges', {
            url: "/charges/:careGiverMasterId",
            templateUrl: 'care_giver_charge.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#charges_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "CareGiverChargesController"
            
        });
        
        $stateProvider.state('physicalDescription', {
            url: "/new_physical_description/:careGiverMasterId",
            templateUrl: 'physical_description_form.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active");
                    $("li#physical_description_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "PhysicalDescriptionController"
            
        });
        $stateProvider.state('healthAndLifeStyle', {
            url: "/health_and_lefe_style/:careGiverMasterId",
            templateUrl: 'health_and_life_style_form.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active");
                    $("li#health_and_life_style_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "HealthAndLifeStyleController"
            
        });

        $stateProvider.state('careGiverRelatives', {
            url: "/relatives/:careGiverMasterId",
            templateUrl: 'care_giver_relatives_form.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#family_background_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "CareGiverRelativeController"
            
        });
        $stateProvider.state('careGiverEducation', {
            url: "/education/:careGiverMasterId",
            templateUrl: 'care_giver_education_form.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#education_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "CareGiverEducationController"
            
        });
        $stateProvider.state('careGiverDocument', {
            url: "/document/:careGiverMasterId",
            templateUrl: 'care_giver_document_form.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#document_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "CareGiverDocumentController"
            
        });
        $stateProvider.state('careGiverServices', {
            url: "/services/:careGiverMasterId",
            templateUrl: 'care_giver_services.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#care_giver_services_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "CareGiverServicesController"
            
        });
        $stateProvider.state('careGiverEmployer', {
            url: "/employer/:careGiverMasterId",
            templateUrl: 'care_giver_employer_form.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#employer_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "CareGiverEmployerController"
            
        });
        $stateProvider.state('careGiverAvailability', {
            url: "/availability/:careGiverMasterId",
            templateUrl: 'care_giver_availability_form.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#availability_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "CareGiverAvailabilityController"
            
        });
        $stateProvider.state('careGiverApproval', {
            url: "/approval/:careGiverMasterId",
            templateUrl: 'care_giver_approval.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#approve_tab").addClass("active")
                    return {careGiverMasterId: $stateParams.careGiverMasterId};
                }]
            },
            controller: "CareGiverApprovalController"
            
        });

        $stateProvider.state('PatientSelfCareAbility', {
            url: "/patient_selfcare_ability/:enrollmentId",
            templateUrl: 'patient_selfcare_abilities.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#patient_self_care_tab").addClass("active")
                    return {enrollmentId: $stateParams.enrollmentId};
                }]
            },
            controller: "PatientSelfcareAbilitesController"
            
        });

        $stateProvider.state('AdaptiveDevices', {
            url: "/adaptive_devices/:enrollmentId",
            templateUrl: 'patient_adaptive_equipments.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#adaptive_devices_tab").addClass("active")
                    return {enrollmentId: $stateParams.enrollmentId};
                }]
            },
            controller: "PatientAdaptiveEquipmentsController"
            
        });

        $stateProvider.state('PatientRoutine', {
            url: "/routines/:enrollmentId",
            templateUrl: 'patient_routines.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#patient_routine_tab").addClass("active")
                    return {enrollmentId: $stateParams.enrollmentId};
                }]
            },
            controller: "PatientRoutineController"
            
        });

        $stateProvider.state('DrugManagement', {
            url: "/drug_management/:enrollmentId",
            templateUrl: 'drug_quantity_master.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#drug_management_tab").addClass("active")
                    return {enrollmentId: $stateParams.enrollmentId};
                }]
            },
            controller: "DrugQuantityController"
            
        });

        $stateProvider.state('VitalstobeTracked', {
            url: "/vitals_tracked/:enrollmentId",
            templateUrl: 'vital_trends.html',
            resolve: {
                params: ["$stateParams", "$state", function($stateParams, $state) {
                    $("li[rel='tablistElement']").removeClass("active")
                    $("li#vitals_to_be_tracked_tab").addClass("active")
                    return {enrollmentId: $stateParams.enrollmentId};
                }]
            },
            controller: "VitalTrendsController"
            
        });
        

    }]);
})(angular, lifeCircleApp);
