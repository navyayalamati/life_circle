class CareGiverAvailabilityMasterDecorator < Draper::Decorator
  delegate_all

  def time_from
    model.time_from.strftime("%H:%M") if model.time_from.present?
  end

  def time_to
    model.time_to.strftime("%H:%M") if model.time_to.present?
  end


  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

end
