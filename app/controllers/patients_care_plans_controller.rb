class PatientsCarePlansController < ApplicationController
  before_action :load_enrollment 
  
  def index
  end

  def show
  end
  
  def all_jsons
    @patient_disabilities = DisabilityMaster.all.map do |equ|
      exists = @enrollment.patient_disabilities.where(:disability_master_id => equ.id).first
      @disability_string = []
      #p @enrollment.patient_adaptive_disabilitys.count
      #p "check exists"
      p exists
      if exists.present?
        exists.assigned = true
        exists
        end
    end
    @patient_disabilities.each do |c|
      if c.present?
        @disability_string.push(c.disability_name)
      end
    end

    @requested_services = ServiceMaster.all.map do |equ|
      exists = @enrollment.enquiry.requested_services.where(:service_master_id => equ.id).first
      @services_string = []
      #p @enrollment.patient_adaptive_services.count
      #p "check exists"
      p exists
      if exists.present?
        exists.assigned = true
        exists      
      end
    end

    @requested_services.each do |c|
      if c.present?
        @services_string.push(c.service_name)
      end
    end

    @patient_equipments = PatientAdaptiveEquipmentMaster.all.map do |equ|
      exists = @enrollment.patient_adaptive_equipments.where(:patient_adaptive_equipment_master_id => equ.id).first
      @equipments_string = []
      #p @enrollment.patient_adaptive_equipments.count
      #p "check exists"
      p exists
      if exists.present?
        exists.assigned = true
        exists      
      end
    end

    @patient_equipments.each do |c|
      if c.present?
        @equipments_string.push(c.adaptive_name)
      end
    end

    @abilities = @enrollment.patient_selfcare_abilities
    
    @routine = @enrollment.patient_routines.order("time")
    
    @care = @enrollment.care_to_be_provided_plans
    
    p '12121200'
    p @enrollment.drug_quantity_masters
    @drug_quantit = @enrollment.drug_quantity_masters 
    
    @vitals = @enrollment.vital_trends  
    
    @track_vitals = @enrollment.track_vitals.group_by(&:entered_at)      
    respond_to do |format|
      format.html do
      end
      format.json do
        # @last = []
        # sorted_array = @vitals.keys.sort()
        # @last << sorted_array
        # @array = []
        # sorted_array.each do |x|
        #   @vitals[x].each do |c|
        #     if c.vital_name == "BP"
        #       @array << {vital_trend_id: c.vital_trend_id , entered_at: c.entered_at , value: c.value, bp1: c.bp1.to_i, bp2: c.bp2.to_i ,  vital_name: c.vital_name, upper_limit: c.upper_limit.to_i,lower_limit: c.lower_limit.to_i, last: @last}
        #     else
        #       @array << {vital_trend_id: c.vital_trend_id , entered_at: c.entered_at , value: c.value, bp1: c.bp1, bp2: c.bp2 ,  vital_name: c.vital_name, upper_limit: c.upper_limit,lower_limit: c.lower_limit, last: @last}
        #     end
        #   end
        # end
        
        render :json => {:disabilities => @disability_string.join(","), :services => @services_string.join(","), :equipments => @equipments_string.join(","), :selfcare_abilities => @abilities, :patient_routine => @routine, :care_tobe_provided => @care, :drugs => @drug_quantit, :vitals => @vitals, :track_vitals => @track_vitals}
      end
    end
  end
  
  def pcp_pdf
    @patient_disabilities = DisabilityMaster.all.map do |equ|
        exists = @enrollment.patient_disabilities.where(:disability_master_id => equ.id).first
        #p @enrollment.patient_adaptive_disabilitys.count
        #p "check exists"
        p exists
        if exists.present?
          exists.assigned = true
          exists
        else
        disability = PatientDisability.new()
        disability.disability_master_id = equ.id
        disability.enrollment_id = @enrollment_id
        disability.patient_id = @enrollment.patient.id
        disability
        end
      end
      
    @requested_services = ServiceMaster.all.map do |equ|
        exists = @enrollment.enquiry.requested_services.where(:service_master_id => equ.id).first
        #p @enrollment.patient_adaptive_services.count
        #p "check exists"
        p exists
        if exists.present?
          exists.assigned = true
          exists
        else
        service = RequestedService.new()
        service.service_master_id = equ.id
        service.enquiry_id = @enrollment.enquiry_id
        service.patient_id = @enrollment.patient.id
        service
        end
      end
      
      @patient_equipments = PatientAdaptiveEquipmentMaster.all.map do |equ|
        exists = @enrollment.patient_adaptive_equipments.where(:patient_adaptive_equipment_master_id => equ.id).first
        #p @enrollment.patient_adaptive_equipments.count
        #p "check exists"
        p exists
        if exists.present?
          exists.assigned = true
          exists
        else
        p "check__________________________"
        equipment = PatientAdaptiveEquipment.new()
        equipment.patient_adaptive_equipment_master_id = equ.id
        equipment.enrollment_id = @enrollment_id
        equipment.created_by = current_user.id
        p equipment
        equipment
        end
      end
      
    @abilities = @enrollment.patient_selfcare_abilities
    
    @routine = @enrollment.patient_routines.order("time")
    
    @care = @enrollment.care_to_be_provided_plans
    
    @drug_quantit = @enrollment.drug_quantity_masters
    
    @vitals = @enrollment.track_vitals
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pcpPdf.pdf",
        debug: true,
        :page_size => 'A4',
        :margin => {:top => 0,
          :bottom => 0,
          :left => '5mm',
          :right => '5mm'
          }     
      end
    end
  end
  
   private

  def load_enrollment
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
  end
end
