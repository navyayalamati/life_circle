class CareToBeProvidedPlansController < ApplicationController
  load_resource
  before_action :load_enrollment
  
  def index
    @care = @enrollment.care_to_be_provided_plans.select(:id,:enrollment_id,:care_time,:comments)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @care
      end
    end
  end
  
  def get_care_to_be_provided_plans
  @array =[]
  @requested_services = @enrollment.enquiry.requested_services.select(:service_master_id)
   
   @requested_services.each do |c|
     @array.push(c.service_master_id)
   end
   p "++++++++++++++=========+++++++"
  p @requested_services
  p @array
  @sub_service_master = SubServiceMaster.select(:id,:sub_service,:service_master_id).where(:service_master_id => @array) 
  p @sub_service_master
  respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @sub_service_master
      end
    end
  end
  
  def create
    p "================="    
    @care_provide = CareToBeProvidedPlan.new(care_to_be_provided_plan_params)
    @care_provide.created_by = current_user.id
    respond_to do |format|
      format.html do
      end
      format.json do
        if @care_provide.save
          @services = params[:sub_service_master_id]         
          @services.each do |x|
              @pcp_service = PcpService.new()
              @pcp_service.care_to_be_provided_plan_id = @care_provide.id
              @pcp_service.sub_service_master_id = x[:id]
              @pcp_service.service_master_id = x[:service_master_id] 
              @pcp_service.save         
          end
        end
        render json: {status: @care_provide.save}
      end
    end

  end

  def update
    #p care_provide_params
    @care_provide = CareToBeProvidedPlan.find(params[:id])
    if params[:sub_service_master].present?
      @care_provide.pcp_services.destroy_all
      @services = params[:sub_service_master]          
      @services.each do |x|
              @pcp_service = PcpService.new()
              @pcp_service.care_to_be_provided_plan_id = @care_provide.id
              @pcp_service.sub_service_master_id = x[:id]
              @pcp_service.service_master_id = x[:service_master_id] 
              @pcp_service.save           
      end
    end
    respond_to do |format|
      format.json do
        render json: {status: @care_provide.update(care_to_be_provided_plan_params)}
      end
    end
  end

  def destroy
    @care_provide = CareToBeProvidedPlan.find(params[:id])
    @care_provide.pcp_services.destroy_all
    if @care_provide.destroy
       @status = true
    else 
       @status = false
    end
    respond_to do |format|
      format.json do
        render json: {status: @status}
      end
    end
  end

  private

  def load_enrollment
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
  end
  
  def care_to_be_provided_plan_params
    params.require(:care_to_be_provided_plan).permit(:care_time, :comments, :enrollment_id)#, pcp_services_attributies: [:care_to_be_provided_plan_id, :service_id, :sub_service_id])
  end
end
