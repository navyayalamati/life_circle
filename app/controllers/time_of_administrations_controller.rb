class TimeOfAdministrationsController < ApplicationController
  load_resource
  before_action :load_enrollment

  def index
    @time_of_administrations = @drug_quantity_master.time_of_administrations.order("time")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @time_of_administrations
      end
    end
  end
  
  def create    
    @time_of_administration = TimeOfAdministration.new(time_of_administration_params)
    #p @time_of_administration
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @time_of_administration.save}
      end
    end

  end

  def update
     respond_to do |format|
      format.json do
        render json: {status: @time_of_administration.update(time_of_administration_params)}
      end
     end
     #render :json => {:status => true, :time_of_administration =>@time_of_administration}    
  end
 
  def destroy
    respond_to do |format|
      format.json do
        # p 'sdgmlmgdslddl'
     	# p params[:id]
        # new = PatientRoutine.find(params[:id])
        # new.delete
        # render :json => true
        if @time_of_administration.destroy
          @status = true
        else 
          @status = false
        end
       render json: {status: @status}
      end
    end
  end


  private

  def load_enrollment
    @drug_quantity_master_id = params[:drug_quantity_master_id]
    @drug_quantity_master = DrugQuantityMaster.find(params[:drug_quantity_master_id])
  end

  def time_of_administration_params
    params.require(:time_of_administration).permit(:drug_quantity_master_id, :time)
  end
end
