class RouteMastersController < ApplicationController
  load_resource
  
  
  def index
    @routes = RouteMaster.select(:id, :name).all.order("id")
    respond_to do |format|
      format.html do
      end
      format.json do
        @route = RouteMaster.select(:id, :name).all.order("id")
        render :json => @route
      end
    end
  end

  def edit
    @route = RouteMaster.find(params[:id])
  end

  def update
    @route = RouteMaster.find(params[:id])
    @route.name = params[:route_master][:name] 
    @route.save
    if @route.save
      redirect_to route_masters_path
    else
      render "edit"
    end
  end
  def destroy
    @route = RouteMaster.find(params[:id])
    @route.destroy
    redirect_to route_masters_path
  end
  def all_routes
    @route = RouteMaster.select(:id, :name).all.order("id")
    respond_to do |format|
      format.html do
      end
      format.json do
        
        @route = RouteMaster.select(:id, :name).all.order("id")
        render :json => @route
      end
    end
  end
  def create
    @route_master = RouteMaster.new(route_master_params)
    #@route_master.created_by = current_user.id
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @route_master.save}
      end
    end

  end

  # def update
  #   respond_to do |format|
  #     format.json do
  #       render json: {status: @route_master.update(route_master_params)}
  #     end
  #   end
  # end

  # def destroy
  #   respond_to do |format|
  #     format.json do
  #       render json: {status: @route_master.destroy}
  #     end
  #   end
  # end

  private
  
  def route_master_params
    params.require(:route_master).permit(:name)
  end
end
