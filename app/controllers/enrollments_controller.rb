require "enquiry_enrollments_controller"
class EnrollmentsController < EnquiryEnollmentsController
  before_action :load_enquiry, :only => [:new, :create, :safety_measures]
  load_resource
  skip_load_resource :only => [:create]
  helper_method :sort_column, :sort_direction

  def index
    @enquiries = Enquiry.includes(:patient).enrolled.order(sort_column + " " + sort_direction).paginate(:page => params[:page].present? ? params[:page] : 1)
    respond_to do |format|
      format.html do
      end
      format.json do
        @enrollments = Enrollment.where(:status =>'ACTIVE')
        data = @enrollments.map do |c|
          {:enrollment_id => c.id , :patient_id => c.patient_id , :name => c.patient.first_name , :phone => c.patient.mobile_no ,  :location => c.patient.address.address_json  , :care_giver_name => c.enquiry.try(:care_giver_names)}
        end
        render :json =>  data 
      end
      format.js do
      end
    end
  end

  def super_care_giver
    respond_to do |format|
      format.html do
      end
      format.json do
        @cgs = []
        @users = User.where(:role => 'super_care_giver').each do |x|
          @care == CareGiverMaster.where(:user_id => x.id).map do |y|
            @cgs << {
              :id => y.id ,:name => y.name
            }
          end
        end
        render :json => @cgs
      end
    end
  end
  def reason_for_coming
    respond_to do |format|
      format.html do
      end
      format.json do
        @care = ReasonForComing.all.map do |y|
          {
            :id => y.id ,:name => y.name
          }
        end
        render :json => @care
      end
    end
  end

  def incomplete_pcp
    @enrollments = Enrollment.where(:status =>'ACTIVE')
    @all = []
    @enrollments.each do |x|
      if PatientAdaptiveEquipment.where(:enrollment_id => x.id).exists? and PatientSelfcareAbility.where(:enrollment_id => x.id).exists? and PatientRoutine.where(:enrollment_id => x.id).exists? and DrugQuantityMaster.where(:enrollment_id => x.id).exists? and VitalTrend.where(:enrollment_id => x.id).exists? and CareToBeProvidedPlan.where(:enrollment_id => x.id).exists?
      else
        @all << x
      end
    end
  end

  def pcp
    respond_to do |format|
      format.html{}
      format.json do
        @enrollments = Enrollment.where(:status =>'ACTIVE')
        @all = []
        @json = []
        p 'qqqqqqqqqqqqqqq'
        p @enrollments
        @enrollments.each do |x|
          if PatientAdaptiveEquipment.where(:enrollment_id => x.id).exists? and PatientSelfcareAbility.where(:enrollment_id => x.id).exists? and PatientRoutine.where(:enrollment_id => x.id).exists? and DrugQuantityMaster.where(:enrollment_id => x.id).exists? and VitalTrend.where(:enrollment_id => x.id).exists? and CareToBeProvidedPlan.where(:enrollment_id => x.id).exists?
          else
            @all << x
          end
        end
        @all.each do |y|
          if PatientAdaptiveEquipment.where(:enrollment_id => y.id).exists? 
            @adaptive_equipment = true
          else
            @adaptive_equipment = false
          end
          if PatientSelfcareAbility.where(:enrollment_id => y.id).exists? 
            @self_care = true
          else
            @self_care = false
          end
          if PatientRoutine.where(:enrollment_id => y.id).exists? 
            @patient_routine = true
          else
            @patient_routine = false
          end
          if DrugQuantityMaster.where(:enrollment_id => y.id).exists? 
            @drug = true
          else
            @drug = false
          end
          if VitalTrend.where(:enrollment_id => y.id).exists?
            @vital = true
          else
            @vital = false
          end
          if CareToBeProvidedPlan.where(:enrollment_id => y.id).exists?
            @care_to_be_provided = true
          else
            @care_to_be_provided = false
          end
          @json << {:enrollment_id => y.id,:patient_name => y.patient.name,:adaptive_equipment => @adaptive_equipment , :self_care  => @self_care  ,:patient_routine => @patient_routine , :drug => @drug , :vital => @vital , :care_to_be_provided  => @care_to_be_provided  }
        end
        
        render :json => @json
      end
    end
  end
  
  def show
     @en = params[:id]
     p @en
    respond_to do |format|
      format.html{}
      format.json do
        render :json => {:enrollment => @enrollment.detail_json}
      end
    end
  end
  
  def save_patients
    p '111111111111111'
    @enrollment = Enrollment.find(params[:id])
    @patient = Patient.find_by_id(@enrollment.patient_id)
    @patient.first_name = params[:patient_first_name]
    @patient.last_name = params[:patient_last_name]
    @patient.age = params[:age]
    @patient.gender = params[:gender]
    @patient.mobile_no = params[:mobile]
    @patient.alternate_contact_no = params[:alternate_no]
    @address = Address.new()
    @address.state = params[:patient_state]
    @address.line1 = params[:patient_line1]
    @address.line2 = params[:patient_line2]
    @address.city_master_id = params[:patient_city_master_id]
    @address.country = params[:patient_country]
    @address.pin_code = params[:patient_pincode]
    @address.save
    @patient.address_id = @address.id
    @patient.save
    @guardian = Guardian.find_by_id(@patient.guardian_id)
    @guardian.first_name = params[:guadian_first_name]
    @guardian.last_name = params[:guadian_last_name]
    @guardian.relation = params[:guadian_relation]
    @guardian.age = params[:guadian_age]
    @guardian.gender = params[:guadian_gender]
    @guardian.mobile_no = params[:mobile_no]
    @guardian.save
    redirect_to enrollment_path(@enrollment)
  end
  
  def patient_edit
    @enrollment = Enrollment.find(params[:id])
  end
  
  def new
    respond_to do |format|
      format.html{}
      format.json do
        @patient = Patient.new
        @enrollment = Enrollment.new
        @patient.build_patient_for_enrollment
        @enrollment.patient = @patient
        @enrollment.enquiry = @enquiry
        p 'navya'
        p @enrollment.detail_json
        render :json => {:enrollment => @enrollment.detail_json}
      end
    end
  end

  def closed_form
    respond_to do |format|
      format.html do
        @enrollments = Enrollment.where(:status => 'CLOSED')
      end
      format.json do
        @enrollments = Enrollment.where(:status => 'CLOSED').map do |c|
          {:patient_id => c.patient_id , :name => c.patient.first_name , :start_date => c.enquiry.try(:service_required_from) , :end_date => c.closed_date ,:reason => c.reason_for_closing ,  }
        end
        render :json => @enrollments
      end
    end
  end

  def create
    respond_to do |format|
      format.html{}
      format.json do
        p 'params'
        p params[:enrollment]
        p @enquiry
        
        enrollment_creator = LifeCircle::EnrollmentCreator.new(@enquiry, params[:enrollment], current_user)
        status = enrollment_creator.create
        if status
          
          caregiver_status = @enquiry.care_giver_masters.each do |care_giver_master|
            care_giver_master.status = "Working"
            care_giver_master.save
          end
          render :json => {:status => status, :enrollment_id => enrollment_creator.enrollment.id}
        else
          render :json => {:status => false}
        end
      end
    end
  end


  def safety_measures
    respond_to do |format|
      format.html{}
      format.json do
        enrollment = Enrollment.new(safety_params)
        enrollment.enquiry = @enquiry
        render :json => {:safety => enrollment.safety_measures.empty?, :safety_measures => enrollment.safety_measures}
      end
    end
  end

  def change_care_giver
    @old_care_giver_master_id = params[:care_giver_master_id]
    @care_giver_masters = LifeCircle::CareGiverMatcher.new(prepare_search_params_from_enquiry(@enrollment.enquiry)).care_giver_masters
    @care_giver_masters = CareGiverMasterDecorator.decorate_collection(@care_giver_masters)
  end 

  def update_care_giver_change
    history_creator = LifeCircle::EnrollmentHistoryCreator.new(@enrollment, current_user)
    status = history_creator.excecute_care_giver_replacement([params[:enquiry_care_giver]])
    if status
      flash[:success] = "Successfully replaced"
    else
      flash[:error] = "Some problem prevented the  replacement"
    end
    redirect_to @enrollment
  end 

  def compare_payments_on_cg_change
    @old_care_giver_enquiry = @enrollment.enquiry_care_givers.where(:care_giver_master_id => params[:old_care_giver_master_id]).first
    @new_care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
    @new_enquiry_care_giver = EnquiryCareGiver.new do |e_cg|
      e_cg.enquiry_id = @enrollment.enquiry_id
      e_cg.care_giver_master_id = @new_care_giver_master.id
      e_cg.approx_travel_distance = params[:approximate_travel_distance]
      e_cg.caregiver_charge = @new_care_giver_master.care_giver_charge.total_charge

      e_cg.transport_charge = TransportChargeMaster.having_distance(params[:approximate_travel_distance]).first.try(:charge) if params[:approximate_travel_distance].present?
      e_cg.adjustment_amount = (@old_care_giver_enquiry.total_charge - e_cg.total_charge)
      e_cg.old_enquiry_care_giver_id = @old_care_giver_enquiry.id
      e_cg.consider_modified_charge = false
    end
  end
  
  def transfer_care_giver
    @old_care_giver_master_id = params[:care_giver_master_id]
    @care_giver_masters = LifeCircle::CareGiverMatcher.new(prepare_search_params_for_leave_enquiry(@enrollment.enquiry)).care_giver_masters
    @care_giver_masters = CareGiverMasterDecorator.decorate_collection(@care_giver_masters)
  end
  
  def update_care_giver_transfer
    history_creator = LifeCircle::EnrollmentHistoryCreator.new(@enrollment, current_user)
    status = history_creator.excecute_care_giver_replacement([params[:enquiry_care_giver]])
    if status
      flash[:success] = "Successfully replaced"
    else
      flash[:error] = "Some problem prevented the  replacement"
    end
    redirect_to @enrollment
  end
  
  def compare_payments_on_cg_transfer
    @old_care_giver_enquiry = @enrollment.enquiry_care_givers.where(:care_giver_master_id => params[:old_care_giver_master_id]).first
    @new_care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
    @new_enquiry_care_giver = EnquiryCareGiver.new do |e_cg|
      e_cg.enquiry_id = @enrollment.enquiry_id
      e_cg.care_giver_master_id = @new_care_giver_master.id
      e_cg.approx_travel_distance = params[:approximate_travel_distance]
      e_cg.caregiver_charge = @new_care_giver_master.care_giver_charge.total_charge

      e_cg.transport_charge = TransportChargeMaster.having_distance(params[:approximate_travel_distance]).first.try(:charge) if params[:approximate_travel_distance].present?
      e_cg.adjustment_amount = (@old_care_giver_enquiry.total_charge - e_cg.total_charge)
      e_cg.old_enquiry_care_giver_id = @old_care_giver_enquiry.id
      e_cg.consider_modified_charge = false
    end
  end

  def change_plan
    
  end
  
  def update_disability
    @enrollment.patient_disabilities.destroy_all
    @enrollment.patient_disabilities = new_patient_disabilities(@params, @enrollment)
    @enrollment.patient_disabilities.map(&:save!)
  
  end

  def update_change_plan
    history_creator = LifeCircle::EnrollmentHistoryCreator.new(@enrollment, current_user)
    status = history_creator.update_enrollment_plan(required_enquiry_params, search_params)
    respond_to do |format|
      format.html do
        if status
          flash[:success] = "Successfully updated"
        else
          flash[:error] = "Some problem prevented the updation"
        end
        redirect_to change_plan_enrollment_path(@enrollment)
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def new_notification
    @enrollment.notification_given_date = Date.today
  end

  def post_notification
    status = @enrollment.update(notification_params.merge(:status => "NOTIFIED"))
    respond_to do |format|
      format.html do
        if status
          flash[:success] = "Successfully updated"
          redirect_to @enrollment
        else
          flash[:error] = "Some problem prevented the updation"
          render "new_notification"
        end
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def close
    @enrollment.closed_date = Date.today
  end 
  
  def save_close
    status = @enrollment.update(closing_params.merge(:status => "CLOSE_PENDING"))
    @enquiry = @enrollment.enquiry.update_attributes(:status => "CLOSE_PENDING") 
    redirect_to payment_on_closing_enrollment_enrollment_payment_masters_path(@enrollment)
  end

  private
  def sort_column
    Enquiry.enrolled.column_names.include?(params[:sort]) ? params[:sort] : "service_required_from"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end


  def load_enquiry
    @enquiry = Enquiry.find(params[:enquiry_id])
  end

  def safety_params
    params.require(:safety_params).permit(:patient_home_setting, :cg_work_alone_with_patient, :suffering_from_infection_disease, :safe_and_well_connected_locality, :family_member_helps, :provides_personal_protective_equipment)
  end

  def prepare_search_params_from_enquiry(enquiry)
    search_params = {}
    search_params[:languages] = enquiry.enquiry_languages.map(&:language_master_id)
    search_params[:requested_services] = enquiry.requested_services.map(&:service_master_id)
    search_params[:service_required_from] = enquiry.service_required_from.to_s
    search_params[:service_required_to] = enquiry.service_required_to.to_s
    search_params[:gender] = enquiry.care_giver_gender
    search_params[:time_slots] = enquiry.requested_time_slots.map{|ts| {"time_from" => ts.decorated_time_from, "time_to" => ts.decorated_time_to}}
    
    search_params[:patient_address] =  enquiry.patient.address_str
    search_params[:open_term] = enquiry.open_term?
    search_params[:affordable_price] = split_affordable_price(enquiry.affordable_price)
    search_params[:caregiver_stay] = enquiry.caregiver_stay
    search_params
  end
  
  def prepare_search_params_for_leave_enquiry(enquiry)
    search_params = {}
    search_params[:languages] = enquiry.enquiry_languages.map(&:language_master_id)
    search_params[:requested_services] = enquiry.requested_services.map(&:service_master_id)
    search_params[:service_required_from] = params[:leave_from]
    search_params[:service_required_to] = params[:leave_to]
    search_params[:gender] = enquiry.care_giver_gender
    search_params[:time_slots] = enquiry.requested_time_slots.map{|ts| {"time_from" => ts.decorated_time_from, "time_to" => ts.decorated_time_to}}
    
    search_params[:patient_address] =  enquiry.patient.address_str
    search_params[:open_term] = enquiry.open_term?
    p "split_affordable_price(enquiry.affordable_price)============#{split_affordable_price(enquiry.affordable_price)}"
    search_params[:affordable_price] = split_affordable_price(enquiry.affordable_price)
    search_params[:caregiver_stay] = enquiry.caregiver_stay
    search_params
  end

  def notification_params
    params.require(:enrollment).permit(:notification_given_date, :close_request_date, :comments_on_notification)
  end

  def closing_params
    params.require(:enrollment).permit(:closing_comments, :reason_for_closing, :closed_date)
  end

  def split_affordable_price(price)
    split = price.split("<")
    if split.count > 1
      return {:l_value => split[1], :operator => "<"}
    end
    split = price.split(">")
    if split.count > 1
      return {:h_value => split[1], :operator => ">"}
    end
    split = price.split("-")
    if split.count > 1
      return {:l_value => split[0], :h_value => split[1], :operator => "to"}
    end
    return {:h_value => price, :operator => ">"}
  end

    
  
end
