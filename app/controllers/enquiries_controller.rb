require "enquiry_enrollments_controller"
class EnquiriesController < EnquiryEnollmentsController
  load_resource :exept => [:list_care_givers]
  before_action :authenticate_user!, :only => [:index, :close]

  def index
    @params = params[:status]
    respond_to do |format|
      format.html do
      end

      format.json do
        if @params == 'To Audit'
          query = Enquiry.includes(:patient).where.not(status: "EMPTY_RESULTS").where("current_tag = ?  OR current_tag = ?", @params, 'Flag')
        elsif @params == 'No Caregiver Found'
          query = Enquiry.includes(:patient).where(:status => "EMPTY_RESULTS")
        else
          query = Enquiry.includes(:patient).where.not(status: "EMPTY_RESULTS")
          if @params.present?
            query = query.where(:current_tag => @params) 
          else
            query = query.where('current_tag is NULL') 
          end
            
        end
        if (params[:filter].present?)
          query = query.match_by_registered_user_name(params[:filter])
        end
        if params[:having_status].present? and params[:having_status] != 'ALL'
          query = query.having_status(params[:having_status])
        else
          query = query.not_enrolled
        end
        page = params[:page].present? ? params[:page] : 1
        
        @enquiries =  query.paginate(:page => page, :per_page => 10)

        enquiries_data = @enquiries.map do |en|
          blocked = (en.cg_blocked? ? 'Y' : 'N')
          {:id => en.id, :requested_user_name => (en.patient_name.present? ? en.patient_name : en.requested_user_name), :mobile_no => en.mobile_no, :patient_id => en.patient_id , :blocked => blocked , :source => en.type_of_enquiry , :closed_by =>  en.closed_by_name , :email => en.email , :address_str => en.address_new , :services_str => en.services_str , :current_tag => en.current_tag}
        end
        render :json => JsonPagination.pagination_entries(@enquiries).merge!(:enquiries => enquiries_data)
      end
    end
  end

  def edit_values
    @id = params[:id]
    @enq = Enquiry.where(:id => params[:id]).map do |x|
      {:id => x.id ,:apponitment_status => x.appointment_status ,:appointment_time => x.appointment_time.strftime("%H:%M") , :appointment_date => x.appointment_time.strftime("%d/%m/%y")}
    end
    render :json => @enq
  end

  def missed_call_requests
    respond_to do |format|
      format.html do
      end
      format.json do
        page = params[:page].present? ? params[:page] : 1
        if params[:status].present?
          @missed_call_requests = MissedCallRequest.where(:current_tag => params[:status], :status => 'PENDING').paginate(:page => page, :per_page => 20)
        else
          @missed_call_requests = MissedCallRequest.where( :status => 'PENDING').where(:current_tag => '').paginate(:page => page, :per_page => 20)
        end
        @missed_call_requests = @missed_call_requests.match_by_mobile_no(params[:filter]) if params[:filter].present?
        render :json => JsonPagination.pagination_entries(@missed_call_requests).merge!(:missed_call_requests => @missed_call_requests)
      end
    end
  end
  
  def update_to_audit
    @enq  = Enquiry.find_by_id(params[:enquiry][:id])
    @enq.current_tag = "To Audit"
    @enq.save
    respond_to do |format|
      format.json do 
        render :json => {:status => @enq.save} 
      end
    end
  end

  def new_city
    respond_to do |format|
      format.html do
        @enquiry = NewCityRequest.all
      end
      format.json do
        @enquiry = NewCityRequest.map do |c|
          {
            :city => c.city , :email => c.email , :mobile_no => c.mobile_no , :name => c.name
          }
        end
        render :json => @enquiry
      end
    end
  end

  def close
    @enquiry = Enquiry.find(params[:id])
    status = @enquiry.update_attributes({:closed_at => Date.today, :closing_comments => params[:closing_comments], :closed_by => current_user.id, :status => Enquiry::STATUS[:rejected] , :current_tag => 'Lost'})
    respond_to do |format|
      format.html do
        redirect_to enquiries_path
      end
      format.json do
        render :json => {:status => status}
      end
    end
    
  end
  
  def show
    @enquiry = Enquiry.find(params[:id])
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:enquiry => @enquiry.modified_json}
      end
      format.js do
      end
      format.pdf do
        render :pdf => "Enquiry Details",
        :formats => [:pdf, :haml],
        :page_size => 'A4',
        :margin => {:top => '8mm',
          :bottom => '8mm',
          :left => '10mm',
          :right => '10mm'}
      end
    end
  end

  def landing
  end

  def enquiry_details
    respond_to do |format|
      format.json do
        
        render :json => {:enquiry => @enquiry.modified_json}
      end
    end
  end
  
  def list_care_givers
    @care_giver_masters = LifeCircle::CareGiverMatcher.new(search_params).care_giver_masters
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @care_giver_masters.to_json({:only => [:id, :id_proof_type], :methods => [:approximate_travel_distance, :image_url, :name, :services, :languages, :expected_sal_per_day, :education_details_str, :professional_qualification_str, :age, :total_charge, :language_name_list, :services_list, :professional_qualification_list, :education_details_list, :image_xs_url]})
      end
    end
  end


  def check_cg_availability_on_modified_query
    @care_giver_matcher = LifeCircle::SingleCareGiverMatcher.new(search_params, @enquiry.care_giver_masters, @enquiry)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:availablity => @care_giver_matcher.availabile_to_patient_on_changed_criteria?}
      end
    end
  end

  def update
    respond_to do |format|
      enquiry_params = required_enquiry_params
      enquiry_creator = LifeCircle::EnquiryCreator.new(enquiry_params, search_params, @enquiry.care_giver_masters, current_user, @enquiry)
      format.html do
      end
      format.json do
        status = enquiry_creator.update
        enquiry =  enquiry_creator.enquiry
        if status == true 
          #enquiry_json = enquiry.to_json()
          render :json => {:status => status, :enquiry => enquiry}
        else
          render :json => {:status => status, :errors => enquiry_creator.errors}
        end
      end
    end
  end
  
  # post enquiries/book_care_giver
  def book_care_giver
    respond_to do |format|
      enquiry_params = required_enquiry_params.merge(:appointment_time => params[:enquiry][:appointment_time])
      care_givers = params[:care_givers].map do |cg_params| 
        cg = CareGiverMaster.find(cg_params[:id])
        cg.approximate_travel_distance = cg_params[:approximate_travel_distance]
        #cg.status = "Booked"
        cg
      end

      enquiry_creator = LifeCircle::EnquiryCreator.new(enquiry_params, search_params, care_givers, current_user)
      format.html do
      end
      format.json do
        status = enquiry_creator.save
        enquiry =  enquiry_creator.enquiry
        if status == true 
          #enquiry_json = enquiry.to_json()
          render :json => {:status => status}
        else
          render :json => {:status => status, :enquiry => enquiry_creator.errors}
        end
      end
    end
  end

  def appointment_edit
    @id = params[:id]
    @enquiry = Enquiry.find(params[:id])
  end

  def update_appointment
    
    @enq  = Enquiry.find_by_id(params[:enquiries][:id])
    @enq.appointment_status = params[:enquiries][:apponitment_status]
    appointment = params[:enquiries][:appointment_date] + "-" +params[:enquiries][:appointment_time]
    appointment_datetime = DateTime.strptime("#{appointment}", '%d/%m/%Y-%H:%M')
    @enq.appointment_time = appointment_datetime
    @enq.save
    respond_to do |format|
      format.json do 
        render :json => {:status => @enq.save} 
      end
    end
  end

  def update_appointment_status
    @enquiry = Enquiry.find(params[:id])
    appointment_datetime = params[:enquiry][:appointment_time].to_datetime
    
    @enquiry.update_attributes({:appointment_status => params[:enquiry][:appointment_status], :appointment_time => appointment_datetime })
    p @enquiry
    
    redirect_to appointments_enquiries_path
    #p = @enquiry.update_appointment(appointment_datetime)} 
  end  
  #render :json => {:status => @enquiry.update_appointment(appointment_datetime)} 

  
  def appointments
    #@enquiry = Enquiry.where('appointment_time > ?', Date.yesterday)
    @enquiry = Enquiry.where(:appointment_status => "OPEN").order("created_at DESC")
  end
  
  def all_appointments
    @enquiry = Enquiry.where(:appointment_status => "OPEN").where.not(:current_tag => 'To Audit').where.not(:current_tag => 'Closed')
    respond_to do |format|
      format.json do 
        enquiries_data = @enquiry.map do |en|
          if DateTime.parse(en.created_at.to_s) < (Time.now - 24.hours) or en.care_giver_names.empty?
            blocked = 'N'
          else
            blocked = 'Y'
          end
          {:id => en.id, :requested_user_name => (en.patient_name.present? ? en.patient_name : en.requested_user_name), :mobile_no => en.mobile_no, :patient_id => en.patient_id , :blocked => blocked , :source => en.type_of_enquiry , :closed_by =>  en.closed_by_name , :email => en.email , :address_str => en.address_str , :services_str => en.services_str , :current_tag => en.current_tag , :appointment_time => en.try(:appointment_time).try(:strftime , "%d/%m/%Y-%H:%M") }
        end
        render :json => enquiries_data
      end
    end
  end

  def update_pre_enquiry
    respond_to do |format|
      enquiry_params = required_enquiry_params
      care_givers = params[:care_givers].map do |cg_params| 
        cg = CareGiverMaster.find(cg_params[:id])
        cg.approximate_travel_distance = cg_params[:approximate_travel_distance]
        #cg.status = "Booked"
        cg
      end

      enquiry_creator = LifeCircle::EnquiryCreator.new(enquiry_params, search_params, care_givers, current_user, @enquiry)
      format.html do
      end
      format.json do
        status = enquiry_creator.update_pre_enquiry
        enquiry =  enquiry_creator.enquiry
        if status == true 
          #enquiry_json = enquiry.to_json()
          render :json => {:status => status, :enquiry => enquiry}
        else
          render :json => {:status => status, :errors => enquiry_creator.errors}
        end
      end
    end
  end

  def new_appointment_for_enquiry
    @enquiry = Enquiry.new
    if params[:mobile_no].present?
      @enquiry.mobile_no = params[:mobile_no]
      @enquiry.missed_call_request_id = params[:missed_call_request_id]
    end
  end

  def show_appointment_for_enquiry
    @enquiry = Enquiry.find(params[:id])
  end

  def create_appointment_for_enquiry
    appointment_related_params = create_appointment_for_enquiry_params()
    @enquiry = Enquiry.new(appointment_related_params)
    @enquiry.status = Enquiry::STATUS[:pre_enquiry_appointment]
    @enquiry.type_of_enquiry = "Walkin"
    @enquiry.appointment_status = "OPEN"
    p 'params'
    p params[:enquiry][:address]
    
    address = Address.new()
    address.line1 = params[:enquiry][:address]
    address.save
    @enquiry.address_id = address.id
    if @enquiry.appointment_date.present? and @enquiry.appointment_time.present?
      @enquiry.appointment_time = DateTime.strptime("#{@enquiry.appointment_date}-#{appointment_related_params[:appointment_time]}", '%d/%m/%Y-%H:%M')
    end
    status = @enquiry.save
    if(appointment_related_params[:missed_call_request_id].present?)
      missed_call_request = MissedCallRequest.find(appointment_related_params[:missed_call_request_id])
      missed_call_request.update(:enquiry_id => @enquiry.id)
    end
    respond_to do |format|
      format.html do
        if status
          flash[:sucess] = "Sucessfully Created"
          redirect_to show_appointment_for_enquiry_enquiry_path(@enquiry)
        else
          flash[:error] = "Some error prevented saving"
          render "new_appointment_for_enquiry"
        end
      end
      format.json do
         appointment_related_params = create_appointment_for_enquiry_params()
        @enquiry = Enquiry.new(appointment_related_params)
        @enquiry.status = Enquiry::STATUS[:pre_enquiry_appointment]
        @enquiry.type_of_enquiry = "Walkin"
        @enquiry.appointment_status = "OPEN"
        p 'params'
        p params[:enquiry][:address]
        
        address = Address.new()
        address.line1 = params[:enquiry][:address]
        address.save
        @enquiry.address_id = address.id
        if @enquiry.appointment_date.present? and @enquiry.appointment_time.present?
          @enquiry.appointment_time = DateTime.strptime("#{@enquiry.appointment_date}-#{appointment_related_params[:appointment_time]}", '%d/%m/%Y-%H:%M')
        end
        status = @enquiry.save
        if(appointment_related_params[:missed_call_request_id].present?)
          missed_call_request = MissedCallRequest.find(appointment_related_params[:missed_call_request_id])
          missed_call_request.update(:enquiry_id => @enquiry.id)
        end
        render :json => {:status => status} 
      end
    end
    
  end

  # post enquiries/capture_customer_on_empty_results
  def capture_customer_on_empty_results
    respond_to do |format|
      enquiry_params = required_enquiry_params.merge(:appointment_time => params[:enquiry][:appointment_time])
     
      enquiry_creator = LifeCircle::EnquiryCreator.new(enquiry_params, search_params, [], current_user)
      format.html do
      end
      format.json do
        status = enquiry_creator.save_enquiry_without_cgs
        enquiry =  enquiry_creator.enquiry
        if status == true 
          #enquiry_json = enquiry.to_json()
          render :json => {:status => status}
        else
          render :json => {:status => status, :enquiry => enquiry_creator.errors}
        end
      end
    end
  end

  def new_enquiry_from_missed_call_request
    @missed_call_request = MissedCallRequest.find(params[:missed_call_request_id])
  end
 
  private

  def create_appointment_for_enquiry_params
    params.require(:enquiry).permit(:type_of_enquiry , :requested_user_name, :email, :mobile_no, :appointment_date, :appointment_time, :missed_call_request_id)
  end
  
end
