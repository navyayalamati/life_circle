class ApplicationDefaultsController < ApplicationController
  before_action :set_application_default, only: [:show, :edit, :update, :destroy]

  # GET /application_defaults
  # GET /application_defaults.json
  def index
    @application_defaults = ApplicationDefault.all
  end

  # GET /application_defaults/1
  # GET /application_defaults/1.json
  def show
  end

  # GET /application_defaults/new
  def new
    @application_default = ApplicationDefault.new
  end

  # GET /application_defaults/1/edit
  def edit
  end

  # POST /application_defaults
  # POST /application_defaults.json
  def create
    @application_default = ApplicationDefault.new(application_default_params)

    respond_to do |format|
      if @application_default.save
        format.html { redirect_to @application_default, notice: 'Application default was successfully created.' }
        format.json { render :show, status: :created, location: @application_default }
      else
        format.html { render :new }
        format.json { render json: @application_default.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /application_defaults/1
  # PATCH/PUT /application_defaults/1.json
  def update
    respond_to do |format|
      if @application_default.update(application_default_params)
        format.html { redirect_to @application_default, notice: 'Application default was successfully updated.' }
        format.json { render :show, status: :ok, location: @application_default }
      else
        format.html { render :edit }
        format.json { render json: @application_default.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /application_defaults/1
  # DELETE /application_defaults/1.json
  def destroy
    @application_default.destroy
    respond_to do |format|
      format.html { redirect_to application_defaults_url, notice: 'Application default was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application_default
      @application_default = ApplicationDefault.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def application_default_params
      params.require(:application_default).permit(:description, :value)
    end
end
