class PatientSelfcareAbilityMastersController < ApplicationController
  load_resource
  
  def index
    @selfcare = PatientSelfcareAbilityMaster.select(:id, :selfcare_ability).all
    respond_to do |format|
      format.html do
      end
      format.json do
        p '1111111111111111111'
        p @selfcare
        render json: @selfcare
      end
    end
  end
  def create
    @patient_selfcare_ability_master = PatientSelfcareAbilityMaster.new(patient_selfcare_ability_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @patient_selfcare_ability_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @patient_selfcare_ability_master.update(patient_selfcare_ability_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @patient_selfcare_ability_master.destroy}
      end
    end
  end

  private
  
  def patient_selfcare_ability_master_params
    params.require(:patient_selfcare_ability_master).permit(:selfcare_ability)
  end
end
