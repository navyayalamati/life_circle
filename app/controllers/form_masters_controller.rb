class FormMastersController < ApplicationController
  load_resource
  
  def index
    @form = FormMaster.select(:id, :name).all.order("id")
    respond_to do |format|
      format.html do
        @form = FormMaster.select(:id, :name).all.order("id")
      end
      format.json do
        render :json => @form
      end
    end
  end
  def create
    p params
    @form_master = FormMaster.new(form_master_params)
    #@form_master.created_by = current_user.id
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @form_master.save}
      end
    end

  end
  
  def edit
    @form = FormMaster.find(params[:id])
  end

  def update
    @form = FormMaster.find(params[:id])
    @form.name = params[:form_master][:name] 
    @form.save
    if @form.save
      redirect_to form_masters_path
    else
      render "edit"
    end
  end
  # def update
  #   respond_to do |format|
  #     format.json do
  #       render json: {status: @form_master.update(form_master_params)}
  #     end
  #   end
  # end
  
  # def destroy
  #   respond_to do |format|
  #     format.json do
  #       render json: {status: @form_master.destroy}
  #     end
  #   end
  # end
  def destroy
    @form = FormMaster.find(params[:id])
    @form.destroy
    redirect_to form_masters_path
  end

  private
  
  def form_master_params
    params.require(:form_master).permit(:name)
  end
end
