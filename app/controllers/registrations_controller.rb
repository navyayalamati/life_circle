class RegistrationsController < Devise::RegistrationsController
  skip_before_filter :require_no_authentication, only: [:new, :create]
  respond_to :json
  def check_user_exists
    respond_to do |format|
      format.json do
        requesting_name = params[:user_name]
        status = true
        if User.having_user_name(params[:user_name]).count >0
          status = false
        end
        render :json => {availability: status}
      end
    end
  end
  
  def new
    respond_to do |format|
      super do |user|
        if params[:register_for].present?
          if params[:register_for] == 'PT'
            user.role =  User::PATIENT
          elsif params[:register_for] == 'CG'
            user.role = User::CARE_GIVER
            user.heighest_qualification = params[:highest_qualification]
            user.dob = params[:dob]
            @@care_giver_enquiry_id = params[:care_giver_enquiry_id]
            @@care_giver_enquiry_phone = params[:care_giver_enquiry_phone]
            @@care_giver_enquiry_gender = params[:care_giver_enquiry_gender]
            p params[:care_giver_enquiry_phone]
            p @@care_giver_enquiry_phone
            p @@care_giver_enquiry_gender
            p status
          else
            user.role =  User::PATIENT
          end
        else
          user.role =  User::PATIENT
        end
      end
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def create
    ActiveRecord::Base.transaction do
      super do |user|
        if user.patient?
          Patient.create({:mobile_no => user.mobile_no, :first_name => params[:user][:first_name], :user_id => user.id})
        elsif user.care_giver?
          p "======================================creatinh "
          p @@care_giver_enquiry_id      
          p @@care_giver_enquiry_gender
          cg = CareGiverMaster.new(care_giver_master_params.merge!(:user_id => user.id))
          cg.care_giver_enquiry_id = @@care_giver_enquiry_id
          cg.telephone_no = @@care_giver_enquiry_phone
          cg.gender = @@care_giver_enquiry_gender
          status = cg.save
          #UserMailer.cg_login( params[:user][:email] ,params[:user][:user_name] , params[:user][:password]).deliver
          @enquiry = CareGiverEnquiry.find(@@care_giver_enquiry_id)
          status = @enquiry.update_attributes({:closed_at => Date.today, :closed_by => current_user.id, :status => "Appointed"})
          p cg.errors
          p status
          
        end
      end
    end
  end
  
  def sign_up(resource_name, resource)
    
  end

  def after_sign_up_path_for(resource)
    respond_to do |format|
      format.html do
        if resource.care_giver?
          p "#################"
          p resource.care_giver_master
          v = {:user => resource, :care_giver => resource.care_giver_master}
          select_job_type_care_giver_master_path(resource.care_giver_master)
        else
          root_path
         # init_enquiry_enquiry_path(resource.enquiries.last)
        end
      end
      format.json do
        if resource.care_giver?
          #render :json => {:user => resource, :care_giver => resource.care_giver_master}
        else
          #render :json => {:user => resource, :patient => resource.patient}
        end
      end
    end
  end

  private

  def care_giver_master_params
    params.require(:user).permit(:first_name, :dob, :heighest_qualification, :expected_sal_per_day)
  end

  def enquiry_stpe1_params
    params.require(:user).permit(:registered_name, :registered_by, :area_master_id)
  end
  
end
