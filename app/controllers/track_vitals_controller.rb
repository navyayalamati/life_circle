class TrackVitalsController < ApplicationController
  load_resource
  before_action :load_enrollment
  
  def index
    @vitals = @enrollment.track_vitals.group_by(&:entered_at)
    p @vitals
    respond_to do |format|
      format.html do
      end
      format.json do
        @last = []
        sorted_array = @vitals.keys.sort()
        @last << sorted_array
        @array = []
        sorted_array.each do |x|
          @vitals[x].each do |c|
            if c.vital_name == "BP"
              @array << {vital_trend_id: c.vital_trend_id , entered_at: c.entered_at , value: c.value, bp1: c.bp1.to_i, bp2: c.bp2.to_i ,  vital_name: c.vital_name, upper_limit: c.upper_limit.to_i,lower_limit: c.lower_limit.to_i, last: @last}
            else
              @array << {vital_trend_id: c.vital_trend_id , entered_at: c.entered_at , value: c.value, bp1: c.bp1, bp2: c.bp2 ,  vital_name: c.vital_name, upper_limit: c.upper_limit,lower_limit: c.lower_limit, last: @last}
            end
          end
        end
        render :json => @vitals
      end
    end
  end
  def create
    #@key_vitals = KeyVitalsMaster.where(:id => params[:key_vitals_master_id])
    @track_vital = TrackVital.new(track_vital_params)
    #@track_vital.created_by = current_user.id
    #p abc
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @track_vital.save}
      end
    end

  end

  def update
    @track_vital = TrackVital.find(params[:id])
    #p track_vital_params
    respond_to do |format|
      format.json do
        render json: {status: @track_vital.update(track_vital_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @track_vital.destroy}
      end
    end
  end

  private

  def load_enrollment
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
  end
  
  def track_vital_params
    params.require(:track_vital).permit(:vital_trend_id, :value, :entered_at , :bp1, :bp2,:enrollment_id)
  end
end
