class PatientAdaptiveEquipmentMastersController < ApplicationController
  load_resource

  def index
    @adaptive_equipments = PatientAdaptiveEquipmentMaster.select(:id, :adaptive_equipment).all.order("adaptive_equipment")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @adaptive_equipments
      end
    end
  end

  def create
    @patient_adaptive_equipment_master = PatientAdaptiveEquipmentMaster.new(patient_adaptive_equipment_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @patient_adaptive_equipment_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @patient_adaptive_equipment_master.update(patient_adaptive_equipment_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @patient_adaptive_equipment_master.destroy}
      end
    end
  end

  private
  
  def patient_adaptive_equipment_master_params
    params.require(:patient_adaptive_equipment_master).permit(:adaptive_equipment)
  end
end
