class LostReasonMastersController < ApplicationController
  load_resource
  def index
    @lost_reasons = LostReasonMaster.all.order("reason")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @lost_reasons
      end
    end
  end
  

  def create
    @lost_reason_master = LostReasonMaster.new(lost_reason_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @lost_reason_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @lost_reason_master.update(lost_reason_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @lost_reason_master.destroy}
      end
    end
  end

  private
  
  def lost_reason_master_params
    params.require(:lost_reason_master).permit(:reason)
  end
end
