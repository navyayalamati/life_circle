class TicketsController < ApplicationController
   load_resource
   before_action :load_params
  def index
    if current_user.role == 'l_c_agent'
    p params[:current_tag]
      @tickets = Ticket.where(:role => params[:role], :current_tag => params[:current_tag]).order("id")
    elsif current_user.role == 'patient'     
      @patient = Patient.where(:user_id => current_user.id)
      p @patient[0][:id]
      @tickets = Ticket.where(:patient_id => @patient[0][:id]).order("current_tag")
    elsif current_user.role == 'care_giver'
      @caregiver = CareGiverMaster.where(:user_id => current_user.id)
      p @caregiver[0][:id]
      @tickets = Ticket.where(:care_giver_master_id => @caregiver[0][:id]).order("current_tag")
    end
    respond_to do |format|
      format.html do
      end
      format.json do
       data = @tickets.map do |c|       
        if c.patient_id
         {:id => c.id, :description => c.description, :patient_id => c.patient_id, :raised_date => c.raised_date, :category_name => c.category_name,:role => c.role, :current_tag => c.current_tag , :name => c.patient_name}
        else
         {:id => c.id, :description => c.description, :care_giver_master_id => c.care_giver_master_id, :raised_date => c.raised_date, :role => c.role, :category_name => c.category_name, :current_tag => c.current_tag , :name => c.care_giver_master_name}
         end
       end
        render :json => data
      end
    end
  end
  
  
  
  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.status = 'New'
    @ticket.raised_date =  Date.today
    @ticket.raised_by= current_user.user_name    
    @ticket.current_tag = 'New'
    if current_user.role == 'patient'  
      patient = Patient.where(:user_id => current_user.id)   
      @ticket.patient_id = patient[0][:id]
      @ticket.role = 'Patient'
    elsif current_user.role == 'care_giver'
      caregiver = CareGiverMaster.where(:user_id => current_user.id)
      @ticket.care_giver_master_id = caregiver[0][:id]
      @ticket.role = 'Caregiver'
    end
    status = @ticket.save
    if status
      if @ticket.patient_id.present? 	
       @template = EmailTemplate.where(:id => 13)
        #UserMailer.ticket_to_admin(@template ,@ticket.patient.name , @ticket.role ,@ticket.ticket_category_master.name ,@ticket.raised_date).deliver
       @temp = EmailTemplate.where(:id => 12)
        #UserMailer.ticket_to_user(current_user.email , @temp ,@ticket.patient.name , @ticket.role ,@ticket.ticket_category_master.name ,@ticket.raised_date).deliver
      else
        @template = EmailTemplate.where(:id => 13)
        #UserMailer.ticket_to_admin(@template ,@ticket.care_giver_master.name , @ticket.role ,@ticket.ticket_category_master.name ,@ticket.raised_date).deliver
        @temp = EmailTemplate.where(:id => 12)
        #UserMailer.ticket_to_user(current_user.email , @temp ,@ticket.care_giver_master.name , @ticket.role ,@ticket.ticket_category_master.name ,@ticket.raised_date).deliver
      end
      @ticket_conversation = TicketConversation.new()
      @ticket_conversation.entered_at = DateTime.now
      @ticket_conversation.entered_by = current_user.id
      @ticket_conversation.ticket_id = @ticket[:id]
      @ticket_conversation.description = @ticket[:description]
      @ticket_conversation.tag = 'New'
      @ticket_conversation.save
    end
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: status}
      end
    end
  end

  def update 
    @ticket = Ticket.find(params[:id]) 
    status = @ticket.update(ticket_params.merge(:current_tag => params[:status], :opened_date => Date.today))
    respond_to do |format|      
     format.html do
      if status
       p "================"
       p status
       p @ticket
      else
      p '++++++++++++++++'
      end
      end
      format.json do        
        render json: {status: status, ticket: @ticket}
      end
    end
  end

  
  #def update
    #respond_to do |format|
      #format.json do
        #render json: {status: @ticket.update(ticket_params.merge(:status => params[:status], :opened_date => Date.today))}
      #end
    #end
  #end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @ticket.destroy}
      end
    end
  end

  def all_patients
    respond_to do |format|
      format.json do
        @patients = Patient.all.map do |x|
          @s = x.name
          @s << ','
          @s << x.mobile_no 
          {
            :id => x.id , :name => @s
          }
        end
        @patient_category = TicketCategoryMaster.where(:category_type => "Patient").map do |x|
          {
            :id => x.id , :name => x.name
          }
        end
        render json: {patients: @patients , categories: @patient_category}
      end
    end
  end

  def all_care_givers
    respond_to do |format|
      format.json do
        @patients = CareGiverMaster.all.map do |x|
          @s = x.name.to_s
          @s << ','
          @s << x.telephone_no.to_s 
          {
            :id => x.id , :name => @s
          }
        end
        @cg_category = TicketCategoryMaster.where(:category_type => "Caregiver").map do |x|
          {
            :id => x.id  , :name => x.name
          }
        end
        render json: {care_givers: @patients , categories: @cg_category}
      end
    end
  end 
  
  def all_tickets
    respond_to do |format|
      format.html do
      end
      format.json do
        @params = params[:ticket]
        if @params[:role] == 'l_c_agent'
          if @params[:status] == 'Patient'
            @new_tickets  = Ticket.where(:role => "Patient").where(:current_tag => @params[:current_tag]).map do |c|
              {:id => c.id, :description => c.description,  :raised_date => c.raised_date, :category_name => c.category_name, :status => c.current_tag , :name => c.try(:patient).try(:name)}
            end
          else @params[:status] == 'Caregiver'
            @new_tickets  = Ticket.where(:role => "Caregiver").where(:current_tag => @params[:current_tag]).map do |c|
              {:id => c.id, :description => c.description, :raised_date => c.raised_date, :category_name => c.category_name, :status => c.current_tag , :name => c.try(:care_giver_master).try(:name)}
            end
          end
        elsif @params[:role] == 'patient'      
          @patient = Patient.where(:user_id => @params[:user_id])
          p @patient[0][:id]
          @new_tickets = Ticket.where(:patient_id => @patient[0][:id]).where(:current_tag => @params[:current_tag]).map do |c|
            {:id => c.id, :description => c.description,  :raised_date => c.raised_date, :category_name => c.category_name, :status => c.current_tag , :name => c.try(:patient).try(:name)}
            
          end
        elsif @params[:role] == 'care_giver'
          @caregiver = CareGiverMaster.where(:user_id => @params[:user_id])
          p @caregiver[0][:id]
          @new_tickets = Ticket.where(:care_giver_master_id => @caregiver[0][:id]).where(:current_tag => @params[:current_tag]).map do |c|
            {:id => c.id, :description => c.description, :raised_date => c.raised_date, :category_name => c.category_name, :status => c.current_tag , :name => c.try(:care_giver_master).try(:name)}
          end
        end
        render :json => @new_tickets
      end
    end
  end

  private
  
  def load_params
   @role = params[:role]
   @current_tag = params[:current_tag]
  end
  
  def ticket_params
    params.require(:ticket).permit(:description, :ticket_category_master_id, :role, :patient_id, :care_giver_master_id)
  end
end
