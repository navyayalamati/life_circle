class PatientAdaptiveEquipmentsController < ApplicationController
  def index
    @enrollment_id = params[:enrollment_id]
    @enrollment = Enrollment.find(params[:enrollment_id])
    if @enrollment.patient_adaptive_equipments.empty?
      @patient_equipments = PatientAdaptiveEquipmentMaster.all.map do |equ|
        equipment = PatientAdaptiveEquipment.new()
        equipment.patient_adaptive_equipment_master_id = equ.id
        equipment.enrollment_id = @enrollment_id
        equipment.created_by = current_user.id
        equipment
      end    
     
    else
      @patient_equipments = PatientAdaptiveEquipmentMaster.all.map do |equ|
        exists = @enrollment.patient_adaptive_equipments.where(:patient_adaptive_equipment_master_id => equ.id).first
        #p @enrollment.patient_adaptive_equipments.count
        #p "check exists"
        p exists
        if exists.present?
          exists.assigned = true
          exists
        else
        p "check__________________________"
        equipment = PatientAdaptiveEquipment.new()
        equipment.patient_adaptive_equipment_master_id = equ.id
        equipment.enrollment_id = @enrollment_id
        equipment.created_by = current_user.id
        equipment.assigned = false
        equipment
        end
      end
    end

    respond_to do |format|
      format.html do
      end
      format.json do
       data = @patient_equipments.map do |c| {:patient_adaptive_equipment_master_id => c.patient_adaptive_equipment_master_id, :adaptive_name => c.adaptive_name, :assigned => c.assigned}
        end
        render :json => data
      end
    end
    
  end

  def save_patient_equipments
    p "check equipments"
    p params[:equipments]
    @patient_equipments = []
    @equipments = params[:equipments].select {|x| x[:assigned].present? && x[:assigned] == true}
    @equipments.each do |y|
      equipment = PatientAdaptiveEquipment.new()
      equipment.patient_adaptive_equipment_master_id = y[:patient_adaptive_equipment_master_id]
      equipment.enrollment_id = params[:enrollment_id]
      equipment.created_by = y[:created_by]
      equipment.assigned = y[:assigned]
      @patient_equipments.push(equipment)
    end
    @enrollment = Enrollment.find(params[:enrollment_id])
    @enrollment.patient_adaptive_equipments.destroy_all
    @patient_equipments.map(&:save)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @patient_equipments
      end
    end
  end 
end
