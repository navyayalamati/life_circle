class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  #acts_as_token_authentication_handler_for User
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token, if: :json_request?
  before_filter :authenticate_user_from_token!
  before_filter :update_breadcrumb
  before_action :configure_permitted_parameters, if: :devise_controller?
  after_filter :set_csrf_cookie_for_ng
  
  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:user_name, :mobile_no, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:role, :mobile_no, :user_name, :email, :password, :password_confirmation)}
    devise_parameter_sanitizer.for(:account_update) { |u| 
      u.permit(:password, :password_confirmation, :current_password) 
    }
  end
  
  protected

  def json_request?
    request.format.json?
  end
  
  def verified_request?
    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
  end

  private
  
  # For this example, we are simply using token authentication
  # via parameters. However, anyone could use Rails's token
  # authentication features to get the token from a header.
  def authenticate_user_from_token!
    user_token = params[:authentication_token].presence
    user       = user_token && User.find_by_authentication_token(user_token.to_s)
    if user
      # Notice we are passing store false, so the user is not
      # actually stored in the session and a token is needed
      # for every request. If you want the token to work as a
      # sign in token, you can simply remove store: false.
      sign_in user, store: false
    end
  end


  def update_breadcrumb
    @breadcrumbs = session[:breadcrumbs]
    @breadcrumbs = [] unless @breadcrumbs.present?
    if(params[:breadcrumb_param].present?)
      @breadcrumbs = [ ]
      Breadcrumb.new(@breadcrumbs, params[:breadcrumb_param]).push_element
    end
    session[:breadcrumbs] = @breadcrumbs
  end
end
