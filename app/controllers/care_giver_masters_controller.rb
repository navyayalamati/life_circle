class CareGiverMastersController < ApplicationController
  load_resource
  
  def index
    respond_to do |format|
      format.html do
        @params = params[:status]
        if current_user.role == 'l_c_agent'
          if params[:status].present?
            @care_giver_masters = CareGiverMaster.where(:status => params[:status]).order("first_name")
          else
            @care_giver_masters = CareGiverMaster.all.order("first_name")
          end
        end
        if current_user.role == 'care_giver'
          @care_giver_masters = CareGiverMaster.where(:user_id => current_user.id)
        end
        if current_user.role == 'patient'
          patient = Patient.where(:user_id => current_user.id)
          @enrollments = Enrollment.where(:patient_id => patient[0][:id])
          if @enrollments.present?
            @enrollments.each do |x|
              @care = EnquiryCareGiver.where(:enquiry_id => x[:enquiry_id])
            end
            @care.each do |y|
              @care_giver_masters = CareGiverMaster.where(:id => y.care_giver_master_id)
            end
          end
        end
      end
      format.json do
        @care_giver_masters = CareGiverMaster.all.order("first_name")
        data = @care_giver_masters.map do |c|
          {:id => c.id , :name => c.first_name , :gender => c.gender, :services => c.services, :languages => c.languages , :phone => c.telephone_no , :profile_pic => c.profile_pic, :status => c.status, :care_giver_enquiry_id => c.care_giver_enquiry_id }
        end
        render :json => {:care_giver_masters => data}
      end
    end
  end
  
  def patients
    respond_to do |format|
      format.html do
        @care_giver_masters = CareGiverMaster.where(:user_id => current_user.id)
        p '1111111111111111111111111111111'
        p @care_giver_masters
        @enq = EnquiryCareGiver.where(:care_giver_master_id => @care_giver_masters[0][:id])
        p @enq
        @enrollment = []
        
      end
      format.json do
        @params = params[:enquiry]
        @enq = EnquiryCareGiver.where(:care_giver_master_id => @params[:care_giver_master_id])
        if @enq
          @data = @enq.map do |c|
            @enrollment = Enrollment.where(:enquiry_id => c[:enquiry_id])
            {:enrollment_id => @enrollment[0][:id] , :patient_name => c.enquiry.patient.first_name , :patient_id => c.enquiry.patient_id ,  :phone => c.enquiry.patient.mobile_no , :address => c.enquiry.patient.address.full_address }
          end
        else
          @data = "No patients for this care giver"
        end
        render :json => {:patients => @data}
      end
    end
  end
  
  def finish
    respond_to do |format|
      format.json do
        @care = CareGiverMaster.find(params[:id])
        @care.status_changed_by = current_user
        @care.status_changed_at = Date.today
        @care.status = 'Documents Collected'
        @care.save
      end
      render :json => {:status => true}
    end
  end
  
  def init_enquiry						
    if(params[:dob].present?)
      @dob= Date.strptime(params[:dob], "%d/%m/%Y")
      to_day = Date.today
      @valid_age = (((to_day-@dob)/365.0).to_i >= CareGiverMaster::ELIGIBLE_AGE) 
    end

    if(params[:highest_qualification].present?)
      @heighest_qualification = params[:highest_qualification]
      @invalid_qualification = (params[:highest_qualification] == CareGiverMaster::CUT_OFF_QUALIFICATION)
    end

    respond_to do |format|
      format.html do
        if(@valid_age == false or @invalid_qualification == true)
          render "regret_enquiry"
        else
          redirect_to new_user_registration_path(:register_for => "CG", :dob => "#{@dob.to_s}", :heighest_qualification => @heighest_qualification)
        end
      end
      format.json do
        aligiblity = true
        aligiblity = false if (@valid_age == false or @invalid_qualification == true)
        render :json => {:aligiblity => aligiblity}
      end
    end


  end

  def select_job_type
  end

  
  def update_job_type
    respond_to do |format|
      status = @care_giver_master.update(registration_job_type_params)
      
      format.html do
        if status
          redirect_to primary_registraion_care_giver_master_path(@care_giver_master)
        else
          render "init_enquiry"
        end
      end
      format.json do
        if status
          render :json => {:status => status, :care_giver_master => @care_giver_master}
        else
          render :json => {:status => status, :errors => @care_giver_master.errors}
        end
      end
    end
  end

  def primary_registraion
    @care_giver_master.address = Address.new unless @care_giver_master.address.present?
  end
  
  
  def edit
  end

  # This will be used by only admin
  def new
    @care_giver_master = CareGiverMaster.new
    @care_giver_master.user = User.new 
    @care_giver_master.address = Address.new 
    @care_giver_master.permanent_address = Address.new 
  end
  
  # This will be used by only admin
  def create
    status = false
    cg_creator = LifeCircle::CareGiverCreationService.new(params)
    status = cg_creator.create
    @care_giver_master = cg_creator.care_giver_master
    respond_to do |format|
      format.html do
        if status
          flash[:success] = "Successfully updated"
          redirect_to @care_giver_master
        else
          flash[:error] ="Some problem occured while updating.. <br>#{cg_creator.errors}"
          render "new"
        end
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def show
  end
  
  def cg_transfer
  end

  def get_caregivers
    respond_to do |format|
      format.html do
      end
      format.json do
        @care_giver_masters = CareGiverMaster.all.order("first_name")
        data = @care_giver_masters.map do |c|
          {:id => c.id , :name => c.first_name , :gender => c.gender, :services => c.services, :languages => c.languages , :phone => c.telephone_no , :profile_pic => c.profile_pic, :status => c.status  }
        end
        render :json => {:care_giver_masters => data}
      end
    end
  end
  
  def get_caregivers_for_transfer
    p "+++===+++===+++===+++===+++"
    p params[:care_giver_enquiry_id]
    p params[:care_giver_leave_id]
    @status = []
    respond_to do |format|
      format.html do
      end
      format.json do
         @care_giver_master = CareGiverMaster.where(:care_giver_enquiry_id => params[:care_giver_enquiry_id])
         p @cg_leave = CareGiverLeave.where(:id => params[:care_giver_leave_id])

        p @care_giver_master
        data = @care_giver_master.map do |c|
          {:id => c.id , :name => c.first_name , :gender => c.gender, :services => c.services, :languages => c.languages , :phone => c.telephone_no , :profile_pic => c.profile_pic, :status => c.status  }            
        end
        if @care_giver_master.present?   
          @cg_enquiry = EnquiryCareGiver.where(:care_giver_master_id => @care_giver_master[0][:id])
          if @cg_enquiry.present?
            p "=============================================S"
            @cg_enquiry.each do |cgenq|
              @cg_enrolled = Enrollment.where(:enquiry_id => cgenq[:id])
              if @cg_enrolled.present?
                p @cg_enrolled
                @cg_enrolled.each do |cgenroll|
                  @cg_enquiries = Enquiry.where(:id => cgenroll[:enquiry_id])
                  p @cg_enquiries
                  if @cg_enquiries.present?
                    @cg_enquiries.each do |cgenqs|
                      #if cgenqs[:caregiver_stay] == "LIVE_OUT" || cgenqs[:caregiver_stay] == "LIVE_IN"
                      if cgenqs[:open_term] == true
                        p "opentre"
                        #if @cg_leave[0][:from_date] >
                        if @cg_leave[0][:from_date] >= cgenqs[:service_required_from] && @cg_leave[0][:to_date] <= cgenqs[:service_required_to]
                          p "Live out working"
                          @status << "Go"
                        elsif @cg_leave[0][:from_date] <= cgenqs[:service_required_from] && @cg_leave[0][:to_date] <= cgenqs[:service_required_to]
                          @status << "Go"
                        elsif @cg_leave[0][:from_date] <= cgenqs[:service_required_from] && @cg_leave[0][:to_date] >= cgenqs[:service_required_to]
                          @status << "Go"
                        else
                          @status << "Ok"
                        end
                      else
                        if(@cg_leave[0][:from_date] >= cgenqs[:service_required_from])
                          p "Live in working"
                          @status << "Go"
                        else
                          @status << 'Ok'
                        end
                      end
                    end
                  end
                end
              end
            end
          else
          end
        end
        render :json => {:care_giver_master => data, :cg_enrolled => @cg_enrolled, :cg_leave => @cg_leave, :count => @cg_enrolled.try(:count), :status => @status}
      end
    end
  end
  
  def update_status
    p "+++++++==========++++++++++++"
    p params[:id]   
    p params[:cg_status] 
     @care_giver_master = CareGiverMaster.find(params[:id])
     status = @care_giver_master.update_attributes({:status_changed_at => Date.today, :status_changed_by => current_user, :status => params[:cg_status]})    
     respond_to do |format|
       format.html do
       end
       format.json do
         render :json => {:status => status}
       end
     end
  end

  def update
    status = false
    ActiveRecord::Base.transaction do
      @care_giver_master.assign_attributes(care_giver_master_params)
      p '1111111111'
      p params[:care_giver_master][:profile_pic]
      @care_giver_master.profile_pic = params[:care_giver_master][:profile_pic]
      if @care_giver_master.save
        if params[:care_giver_master][:languages_known].present?
          care_giver_languages = params[:care_giver_master][:languages_known].map do |lang_id|
            CareGiverLanguage.new do |cgl|
              cgl.care_giver_master_id = @care_giver_master.id
              cgl.language_master_id = lang_id
            end
          end
          @care_giver_master.care_giver_languages.destroy_all
          status = care_giver_languages.map(&:save).all?
        end
      end
    end
    if params[:type_of_care_giver] == 'on'
      user = User.where(:id => @care_giver_master.user_id)
      user[0][:role] = 'super_care_giver'
      user[0].save
    end
    p 'nanaananannananana'
    p  params[:type_of_care_giver]
   
    respond_to do |format|
      format.html do
        if @care_giver_master.save
          flash[:success] = "Successfully updated"
          render "show"
        else
          flash[:error] = "Some problem occured while updating.."
          render "primary_registraion"
        end
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end


  def daily_attendance
    params[:attendance] = {:care_giver_master_id => 1 , :patient_id => 1 , :time => '2015-09-11 13:30:00' , :status => 'START'}
    if params[:attendance][:status] == 'START'
      @req = RequestedTimeSlot.where(:care_giver_master_id => params[:attendance][:care_giver_master_id] , :patient_id => params[:attendance][:patient_id] , :time_from => params[:attendance][:time])
      p '11111111'
      p @req
      @new = CareGiverDailyAttendance.new()
      @new.care_giver_master_id =  params[:attendance][:care_giver_master_id]
      @new.patient_id = params[:attendance][:patient_id]
      @new.date = params[:attendance][:time]
      @new.from_time = @req[0][:time_from]
      @new.to_time = @req[0][:time_to]
      @new.reached_from_time = params[:attendance][:time]
      @new.actual_latitude  = params[:attendance][:latitude]
      @new.actual_longitude = params[:attendance][:longitude]
      @new.save
    elsif  params[:attendance][:status] == 'STOP'
      @new.left_to_time = params[:attendance][:time]
      @new.latitude = params[:attendance][:latitude]
      @new.longitude = params[:attendance][:longitude]
      @new.save
    end
    render :json => true
  end

  def payroll
    respond_to do |format|
      format.html do
      end
      format.json do
        @new = CareGiverDailyAttendance.where(:date => params[:from].to_date..params[:to].to_date).group_by(&:care_giver_master_id)
        p '11111111111'
        # @new.values.each do |x|
        #   @count = []
        #   @count << x.count
        #   p @count
        # end
        @enrollment = Enquiry.enrolled.where(:updated_at => params[:from].to_date..params[:to].to_date).each do |x|
          no = (x[:service_required_to].to_date - x[:service_required_from].to_date).to_i
          @payroll = EnquiryCareGiver.where(:enquiry_id => x.id).map do |y|
            
            {
              care_giver_name: y.care_giver_master.name , no_of_days: no , patient: x.patient_id , charge: y.caregiver_charge , transport: y.transport_charge             
            }
          end
        end
         
       
        
        render :json =>  @payroll
      end
    end
  end
  
  

  def approval_list
    respond_to do |format|
      format.json do
        approval = { physical_description_approved: @care_giver_master.physical_description_approved?, health_approved: @care_giver_master.health_approved?, services_list_approved: @care_giver_master.services_list_approved?, relatives_approved: @care_giver_master.relatives_approved?, education_approved: @care_giver_master.education_approved?, employers_approved: @care_giver_master.employers_approved?, charges_approved: @care_giver_master.charges_approved?}
        
        @cg = CareGiverMaster.find(params[:id])
        if (@care_giver_master.physical_description_approved? and @care_giver_master.health_approved? and  @care_giver_master.services_list_approved? and  @care_giver_master.relatives_approved? and @care_giver_master.education_approved? and @care_giver_master.employers_approved? and @care_giver_master.charges_approved? )
          @cg.status = 'Active'
          @cg.save
        end
        
        render :json => {:approvals => approval}
      end
    end
    
  end

  def month_plan
    respond_to do |format|
      format.html {}
      format.json do
        start_date = Date.strptime(params[:start], "%Y-%m-%d")
        end_date = Date.strptime(params[:end], "%Y-%m-%d")
        events = [{"title":"Free Pizza","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Free Pizza.</p><p>Nothing to see!</p>","start":"2015-09-30T09:05:28","end":"2015-09-30T10:05:28","url":"http://www.mikesmithdev.com/blog/worst-job-titles-in-internet-and-info-tech/"},{"title":"Free Pizza","allday":"false","borderColor":"#5173DA","color":"#99ABEA","textColor":"#000000","description":"<p>This is just a fake description for the Free Pizza.</p><p>Nothing to see!</p>","start":"2015-09-04T09:05:28","end":"2015-09-04T10:05:28","url":"http://www.mikesmithdev.com/blog/worst-job-titles-in-internet-and-info-tech/"}]
        render :json => events
      end
    end
  end

  private

  def registration_job_type_params
    params.require(:care_giver_master).permit(:job_type)
  end

  def user_params(care_giver_params)
    care_giver_params.require(:user).permit(:email, :user_name, :password, :password_confirmation)
  end

  
  def care_giver_master_params
    params.require(:care_giver_master).permit(:first_name, :last_name, :father_name, :mother_name, :place_of_birth, :dob, :telephone_no, :id_proof_type, :id_proof_no, :id_proof_doc, :address_proof_type,:address_proof_id, :address_proof_doc, :maritual_status, :status, :gender, :profile_pic ,:address_attributes => [:line1, :line2, :city_master_id,:city, :area, :state, :country, :pin_code], :permanent_address_attributes => [:line1, :line2, :city,:city_master_id ,:area, :state, :country, :pin_code])
  end
end
