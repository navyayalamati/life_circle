class KeyVitalsMastersController < ApplicationController
  load_resource
  
  def index
    @vital = KeyVitalsMaster.select(:id, :name, :unit, :upper_limit, :lower_limit).all.order("id")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @vital
      end
    end
  end
  def create
    @vital_master = KeyVitalsMaster.new(key_vitals_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @vital_master.save}
      end
    end

  end

  def update
    p key_vitals_master_params
    respond_to do |format|
      format.json do
        render json: {status: @vital_master.update(key_vitals_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @vital_master.destroy}
      end
    end
  end

  private
  
  def key_vitals_master_params
    params.require(:key_vitals_master).permit(:name, :unit, :upper_limit, :lower_limit)
  end
end
