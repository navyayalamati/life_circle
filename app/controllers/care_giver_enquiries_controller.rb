class CareGiverEnquiriesController < ApplicationController
  
  def create
    if(params[:dob].present?)
      @dob= Date.strptime(params[:dob], "%d/%m/%Y")
      to_day = Date.today
      @valid_age = (((to_day-@dob)/365.0).to_i >= CareGiverMaster::ELIGIBLE_AGE) 
    end
    respond_to do |format|
      format.html do
        if(@valid_age == false )
          render "regret_enquiry"
        else
          @new = CareGiverEnquiry.new()
          @new.name = params[:name]
          @new.location = params[:location]
          @new.dob = params[:dob]
          @new.phone = params[:phone]
          @new.email = params[:email]
          @new.highest_qualification = params[:highest_qualification]
          @new.gender = params[:gender]
          @new.city = params[:new_city].split(',').uniq
          @new.apt_date = params[:apt_date]
          @new.apt_time = params[:apt_time]
          @new.status = 'Open'
          @new.current_tag = 'Open'
          @new.save
          flash[:alert] =  "Thank you for applying. We will get back to you soon."
          redirect_to care_giver_enquiry_path(@new)
          #redirect_to root_path
        end
      end
      format.json do
        params = params[:care_giver_enquiry]
        new = CareGiverEnquiry.new()
        new.name = params[:name]
        new.location = params[:location]
        new.dob = params[:dob]
        new.phone = params[:phone]
        new.email = params[:eamil]
        new.highest_qualification = params[:highest_qualification]
        new.status = 'Open'
        new.current_tag = 'Open'
        new.save
        render :json => {:status => new.save , :id => new.id}
      end
    end
  end

  def create_enquiry
    respond_to do |format|
      format.json do
        p '111111111111111'
        p params[:enquiry]
        
        eq_params = params[:enquiry]
        new = CareGiverEnquiry.new()
        new.name = eq_params[:name]
        new.location = eq_params[:location]
        new.dob = eq_params[:dob]
        new.phone = eq_params[:phone_no]
        new.email = eq_params[:email]
        new.gender = eq_params[:gender]
        new.highest_qualification = eq_params[:qualification]
        new.city =  eq_params[:patient_address][:city]
        new.status = 'Open'
        new.current_tag = 'Open'
        new.save
        render :json => { :id => new.id , :status => new.save}
      end
    end
  end

  def show
   p "params[:id]"
   p params[:id]
   @enquiry = CareGiverEnquiry.find(params[:id])
  end
  def cg_city_request
    respond_to do |format|
      format.html do
        @enquiries = CgNewCityRequest.all
      end
      format.json do
        @enquiries = CgNewCityRequest.all.map do |x|
          {
            :name => x.name , :email => x.email , :phone => x.phone , :city => x.city
          }
        end
        render :json => @enquiries

      end
    end
  end

  def approval_list
    respond_to do |format|
      format.html do
      end
      format.json do
        @care_giver_masters = CareGiverMaster.where(:status => 'Documents Collected').order("first_name")
        data = @care_giver_masters.map do |c|
          {:care_giver_master_id => c.id , :name => c.first_name ,  :phone => c.telephone_no , :profile_pic => c.profile_pic.thumb.url, :care_giver_enquiry_id => c.care_giver_enquiry_id ,:entered_by => c.try(:status_changed_by).try(:user_name) , :entered_at => c.status_changed_at     }
        end
        render :json => data
      end
    end
  end
  
  def recruitment
    respond_to do |format|
      format.html do
        @enquiries = CareGiverEnquiry.where(:current_tag => 'Recruit')        
      end
      format.json do
        
        @enquiries = CareGiverEnquiry.where(:current_tag => 'Recruit')
        data = @enquiries.map do |c|
          {:id => c.id , :name => c.name , :location => c.location , :phone => c.phone , :qualification => c.highest_qualification}
        end
        render :json =>  data
      end
    end
  end

  def closed
    respond_to do |format|
      format.html do
        @enquiries = CareGiverEnquiry.where(:current_tag => "Reject")
      end
      format.json do
        data = CareGiverEnquiry.where(:current_tag => "Reject").map do |c|
          {:id => c.id , :name => c.name , :location => c.location , :phone => c.phone , :qualification => c.highest_qualification}
        end
        render :json =>  data
      end
    end
  end


  def recruit
    @enquiry = CareGiverEnquiry.find(params[:id])
    status = @enquiry.update_attributes({:status => "Recruit"})
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def index
    respond_to do |format|
      format.html do
        @enquiries = CareGiverEnquiry.all.where(:current_tag => 'Open')
      end
      format.json do
        @enquiries = CareGiverEnquiry.all.where(:current_tag => 'Open')
        data = @enquiries.map do |c|
          {:id => c.id , :name => c.name , :location => c.location , :phone => c.phone , :qualification => c.highest_qualification}
        end
        render :json =>  data
      end
    end
  end
  
  def update_enq
    @enquiry = CareGiverEnquiry.find(params[:id])
    status = @enquiry.update_attributes({:status => params[:status]})
    respond_to do |format|
      format.html do
      end
      format.json do
      render :json => {:status => status}
      end
    end
  end

  def holding
    @enquiry = CareGiverEnquiry.find(params[:id])
    status = @enquiry.update_attributes({:status => "Hold"})
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
    
  end

  def new_city
    @new_city = CgNewCityRequest.new()
    @new_city.name = params[:name]
    @new_city.email = params[:email]
    @new_city.phone = params[:phone]
    @new_city.city = params[:new_city].split(',').uniq 
    @new_city.save
    redirect_to root_path
  end
  
  def hold
    respond_to do |format|
      format.html do
        @enquiries = CareGiverEnquiry.where(:current_tag => 'Hold')
      end
      format.json do
        @enquiries = CareGiverEnquiry.where(:current_tag => 'Hold').map do |c|
          {:id => c.id , :name => c.name , :location => c.location , :phone => c.phone , :qualification => c.highest_qualification}
        end
        render :json => @enquiries
      end
    end
  end
  
  def close
    @enquiry = CareGiverEnquiry.find(params[:id])
    status = @enquiry.update_attributes({:closed_at => Date.today, :closed_by => current_user.id, :reason_for_closing => params[:closing_comments], :status => "Rejected" })
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end
  
  

end
