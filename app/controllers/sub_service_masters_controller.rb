class SubServiceMastersController < ApplicationController
  load_resource
  before_action :load_service_master, :except => [:all_sub_services]

  def index
    @sub_service_master = @service_master.sub_service_masters.order("id")
    respond_to do |format|
      format.html do
      end
      format.json do
      data = @sub_service_master.map do |c|
       {
         :id => c.id, :sub_service => c.sub_service
       }
       end
        render :json => data
      end
    end
  end
  
  def create
    @sub_service_master = SubServiceMaster.new(sub_service_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @sub_service_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @sub_service_master.update(sub_service_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @sub_service_master.destroy}
      end
    end
  end
  
  def all_sub_services
    p "=========="
    p params
    p params[:array]
    p params[:array].split(",").map {|i| i.to_i}
    #@sub_service_master = @service_master.sub_service_masters.order("id")
    @sub_service_master = SubServiceMaster.all.where(:service_master_id => params[:array].split(",").map {|i| i.to_i})
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @sub_service_master
      end
    end
  end

  private

  def load_service_master
    @service_master = ServiceMaster.find(params[:service_master_id])
  end

  def sub_service_master_params
    params.require(:sub_service_master).permit(:sub_service, :service_master_id)
  end
end
