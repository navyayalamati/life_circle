class CareGiverLeavesController < ApplicationController
  before_action :load_care_giver_master, :except => [:all_cg_leaves]
  before_action :load_care_giver_leave, :only => [:show, :edit, :update, :approve, :reject] 

  def all_cg_leaves
    respond_to do |format|
      format.html do
      end
      format.json do
        @care_giver_leaves = CareGiverLeave.all.order("applied_at desc")
        render :json => @care_giver_leaves.to_json(:methods => [:care_giver_name, :care_giver_img_url, :care_giver_enquiry_id, :care_giver_status])
      end
    end

  end

  def index
    @care_giver_leaves = @care_giver_master.care_giver_leaves.order("from_date desc")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @care_giver_leaves
      end
    end
  end
  
  def create
    p "==============+++++++++++++++++++++++++==========="
    p care_giver_leave_params
    @care_giver_leave = CareGiverLeave.new(care_giver_leave_params)
    @care_giver_leave.status = "PENDING"
    @care_giver_leave.applied_at = Date.today
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @care_giver_leave.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @care_giver_leave.update(care_giver_leave_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @care_giver_leave.destroy}
      end
    end
  end


  def approve
    respond_to do |format|
      format.json do
        render json: {status: @care_giver_leave.approve}
      end
    end
  end

  def reject
    respond_to do |format|
      format.json do
        render json: {status: @care_giver_leave.reject}
      end
    end
  end


  private

  def load_care_giver_leave
    @care_giver_leave = CareGiverLeave.find(params[:id])
  end

  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end

  def care_giver_leave_params
    leave_params = nil
    if params[:care_giver_leave].present?
      leave_params = params.require(:care_giver_leave)
    else
      leave_params = params.require(:care_giver_leafe)
    end
    leave_params.permit(:care_giver_master_id, :from_date, :to_date, :description)
  end
  


  
end
