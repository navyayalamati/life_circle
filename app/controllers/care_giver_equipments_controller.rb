class CareGiverEquipmentsController < ApplicationController
  before_action :set_care_giver_equipment, only: [:show, :edit, :update, :destroy]

  # GET /care_giver_equipments
  # GET /care_giver_equipments.json
  def index
    @care_giver_equipments = CareGiverEquipment.all
  end

  # GET /care_giver_equipments/1
  # GET /care_giver_equipments/1.json
  def show
  end

  # GET /care_giver_equipments/new
  def new
    @care_giver_equipment = CareGiverEquipment.new
  end

  # GET /care_giver_equipments/1/edit
  def edit
  end

  # POST /care_giver_equipments
  # POST /care_giver_equipments.json
  def create
    @care_giver_equipment = CareGiverEquipment.new(care_giver_equipment_params)

    respond_to do |format|
      if @care_giver_equipment.save
        format.html { redirect_to @care_giver_equipment, notice: 'Care giver equipment was successfully created.' }
        format.json { render :show, status: :created, location: @care_giver_equipment }
      else
        format.html { render :new }
        format.json { render json: @care_giver_equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /care_giver_equipments/1
  # PATCH/PUT /care_giver_equipments/1.json
  def update
    respond_to do |format|
      if @care_giver_equipment.update(care_giver_equipment_params)
        format.html { redirect_to @care_giver_equipment, notice: 'Care giver equipment was successfully updated.' }
        format.json { render :show, status: :ok, location: @care_giver_equipment }
      else
        format.html { render :edit }
        format.json { render json: @care_giver_equipment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /care_giver_equipments/1
  # DELETE /care_giver_equipments/1.json
  def destroy
    @care_giver_equipment.destroy
    respond_to do |format|
      format.html { redirect_to care_giver_equipments_url, notice: 'Care giver equipment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_care_giver_equipment
      @care_giver_equipment = CareGiverEquipment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def care_giver_equipment_params
      params.require(:care_giver_equipment).permit(:name, :status)
    end
end
