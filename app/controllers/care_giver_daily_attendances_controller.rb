class CareGiverDailyAttendancesController < ApplicationController
  def index
    @date = Date.today
  end

  def show
  end

  def new
    locations = []
    Patient.all.each do |x|
      locations << x.address
    end
    gmap_data = Gmaps4rails.build_markers(locations) do |location, marker|
      marker.lat location.latitude
      marker.lng location.longitude
      marker.infowindow location.line1
      marker.picture({
                       :url => "http://maps.google.com/mapfiles/ms/icons/blue-dot.png" ,
                       :width   => 40,
                       :height  => 40,
                       
                     })
         
    end
    gon.gmap_data = gmap_data.to_json
    gon.width = "800px"
    gon.height = "400px"
  end

  def edit
  end
  
  def attendance
    respond_to do |format|
      format.html do
      end
      format.json do
        @time =  []
      end
    end
  end
  
  def payroll
    respond_to do |format|
      format.html do
      end
      format.json do
        @payroll = []
        @new = CareGiverDailyAttendance.where(:date => params[:from].to_date..params[:to].to_date).group_by(&:care_giver_master_id)
        @count = []
        @new.keys.each do |x|
          @cg = @new[x]
          @total_days = @new[x].map(&:date).uniq.count
          @time = []
          @new[x].map do |y|
            if y.to_time.present?
              @time << y.to_time - y.from_time 
            end
            @hours = @time.inject(0){|sum,x| sum + x }
          end
          @care_giver = CareGiverMaster.find_by_id(x)
          @name = @care_giver.name
          @c = CareGiverCharge.find_by_care_giver_master_id(x)
          @charge = @c.try(:base_charge)
          @amount = EnquiryCareGiver.where(:enquiry_id  =>1 )
          @amount = []
          @new[x].map(&:enquiry_id).each do |x|
            @a = EnquiryCareGiver.find_by_enquiry_id(x)
            if @a.present?
              @amount << @a.try(:caregiver_charge)
            end
          end
          @total_amount = @amount.inject(0){|sum,x| sum + x }
          @payroll << {:cg_id => x,:name => @name , :days => @total_days , :hours => @hours.round/60 , :rate => @charge , :amount => @total_amount}
        end
        render :json =>  @payroll     
      end
    end
  end
  
  def amount
    respond_to do |format|
      format.html do
      end
      format.json do
        @new = CareGiverDailyAttendance.where(:date => params[:from].to_date..params[:to].to_date).where(:care_giver_master_id => params[:id]).map do |x|
          @enq = EnquiryCareGiver.where(:enquiry_id => x.enquiry_id)
          {
            
            :date => x.date , :patient => x.enquiry.try(:patient).try(:name) , :amount => @enq[0][:caregiver_charge]  
          }
        end
        render :json => @new
      end
    end
  end

  def attendance_of_cg
    respond_to do |format|
      format.html do
      end
      format.json do
        @att = CareGiverDailyAttendance.where(:care_giver_master_id => params[:id] ).where(:date => params[:from_date].to_date..params[:to_date].to_date).map do |c|
          {
            :id => c.id , :patient => c.patient.try(:name), :planned_in => c.try(:from_time).try(:strftime, " %I:%M%p"), :actual_in => c.reached_from_time.try(:strftime, "%I:%M%p") , :planned_out => c.try(:to_time).try(:strftime, "%I:%M%p") , :actual_out =>  c.left_to_time.try(:strftime, "%I:%M%p") , :date => c.date
          }
        end
        render :json => @att
      end
    end
  end

  def save_attendance
    respond_to do |format|
      format.html do
      end
      format.json do
        params[:times].each do |x|
          if x[:from].present? and x[:to].present?
            @at = CareGiverDailyAttendance.new()
            @at.care_giver_master_id = params[:id]
            @at.date = params[:date]
            @req = RequestedTimeSlot.where(:id => x[:id])
            @at.patient_id = x[:patient_id]
            @at.from_time = @req[0][:time_from]
            @at.to_time = @req[0][:time_to]
            @at.reached_from_time = x[:from]
            @at.left_to_time    = x[:to]
            @at.status = x[:status]
            @at.save
          end
        end
        render :json => @at.save
      end
    end
  end

  def today_patients
    respond_to do |format|
      format.html do
      end
      format.json do
        @time = []
        @params = params[:enquiry]
        @enq = EnquiryCareGiver.where(:care_giver_master_id => @params[:care_giver_master_id]).each do |x|
          @enquiries = Enquiry.where(:id => x.enquiry_id)
          @date = Date.today
          @en = @enquiries.find_by_sql("SELECT * FROM enquiries WHERE '#{@date.strftime("%m/%d/%y")}' BETWEEN service_required_from AND service_required_to")
          if @en.present?
            @enrollment = Enrollment.where(:enquiry_id => x.enquiry_id)
            RequestedTimeSlot.where(:enquiry_id => x.enquiry_id).map do |c|
              @att = CareGiverDailyAttendance.where(:date => @date).where(:patient_id => @enquiries[0][:patient_id])
              if @att.present? and @att[0][:reached_from_time].present? and @att[0][:left_to_time].blank?
                @start = 'STOP'
                @attendance_id = @att[0][:id]
              else
                @start = 'START'
              end
              @time << {
                :enrollment_id => @enrollment[0][:id],:patient_id => @enquiries[0][:patient_id] ,:name => c.first_name(@enquiries[0][:patient_id]), :from_time => c.time_from.strftime('%H:%M %p') , :time_slot_id => c.id , :time =>  "#{c.time_from.try(:strftime ,'%H:%M %p')} - #{c.time_to.try(:strftime ,'%H:%M %p')}" ,  :address => c.addr(@enquiries[0][:address_id]) , :phone => @enquiries[0][:patient_id].try(:mobile_no) , :status => @start , :attendance_id => @attendance_id
              }
            end
          end
        end
        render :json => @time
      end
    end
  end
  
  def update_attendance
    respond_to do |format|
      format.html do
      end
      format.json do
        @time = []
        @enq = EnquiryCareGiver.where(:care_giver_master_id => params[:id]).each do |x|
          enquiries = Enquiry.where(:id => x.enquiry_id)
          @date = params[:date].to_date
          @en = enquiries.find_by_sql("SELECT * FROM enquiries WHERE '#{@date.strftime("%m/%d/%y")}' BETWEEN service_required_from AND service_required_to")
          if @en.present?
            RequestedTimeSlot.where(:enquiry_id => x.enquiry_id).map do |c|
              @time << {
                :id => c.id ,:name => x.try(:patient).try(:name), :from_time => c.time_from.strftime('%H:%M %p') , :to_time => c.time_to.strftime('%H:%M %p') 
              }
            end
          end
        end
        render :json => @time
      end
    end
  end
  
  def care_giver_profile
    respond_to do |format|
      format.html do
      end
      format.json do
        @params = params[:enquiry]
        @cg = CareGiverMaster.where(:id => @params[:care_giver_master_id]).map do |x|
          {

            :name => x.name , :profile_pic => x.profile_pic , :mother_name => x.mother_name , :father_name => x.father_name , :dob => x.dob , :place_of_birth => x.place_of_birth , :maritual_status => x.maritual_status , :languages => x.languages , :services => x.services  , :gender => x.gender , :address => x.permanent_address.address_json , :id_proof_no => x.id_proof_no , :id_proof_type => x.id_proof_type            
          }
        end
        render :json => @cg
      end
    end
  end
  
  def patient_care_giver_profile
    respond_to do |format|
      format.html do
      end
      format.json do
        @params = params[:enquiry]
        @enquiry = Enquiry.where(:patient_id => @params[:patient_id])
        if @enquiry.present?
          @enquiry.each do |z|
            @care = EnquiryCareGiver.where(:enquiry_id => z[:id])
          end
          @care.each do |y|
            @care_giver_masters = CareGiverMaster.where(:id => y.care_giver_master_id).map do |x|
              {

                :name => x.name , :profile_pic => x.profile_pic , :mother_name => x.mother_name , :father_name => x.father_name , :dob => x.dob , :place_of_birth => x.place_of_birth , :maritual_status => x.maritual_status , :languages => x.languages , :services => x.services  , :gender => x.gender , :address => x.permanent_address.try(:address_json) , :id_proof_no => x.id_proof_no , :id_proof_type => x.id_proof_type            
              }
            end
          end
        end
        render :json => @care
      end
    end
  end
  
  

  def start
    respond_to do |format|
      format.html do
      end
      format.json do
        if params[:attendance][:status] == 'START'
          # @req = RequestedTimeSlot.where(:care_giver_master_id => params[:attendance][:care_giver_master_id] , :patient_id => params[:attendance][:patient_id] , :time_from => params[:attendance][:time])
          @req = RequestedTimeSlot.where(:id => params[:attendance][:time_slot_id])
          @new = CareGiverDailyAttendance.new()
          @new.care_giver_master_id =  params[:attendance][:care_giver_master_id]
          @new.patient_id = params[:attendance][:patient_id]
          @new.date = Date.today
          @new.from_time = @req[0][:time_from]
          @new.to_time = @req[0][:time_to]
          @new.reached_from_time = params[:attendance][:time]
          @new.actual_latitude  = params[:attendance][:latitude]
          @new.actual_longitude = params[:attendance][:longitude]
          @new.save
          render :json => {:status => @new.save , :attendance_id => @new.id , :attendance_status => 'START'}
        elsif  params[:attendance][:status] == 'STOP'
          @new  = CareGiverDailyAttendance.find_by_id(params[:attendance][:attendance_id])
          @new.left_to_time = params[:attendance][:time]
          @new.latitude = params[:attendance][:latitude]
          @new.longitude = params[:attendance][:longitude]
          @new.save
          render :json => {:status => @new.save , :attendance_status => 'STOP'}
        end
      end
    end
  end



  def first_attendance
    respond_to do |format|
      format.html do
      end
      format.json do
        @date = params[:date]
        @care_giver_attendance = CareGiverDailyAttendance.where(:date => Date.today)
        data = @care_giver_attendance.map do |c|
          {:id => c.id , :name => c.care_giver_master.name , :patient => c.patient.name, :planned_in => c.from_time.try(:strftime, " %I:%M%p"), :actual_in => c.reached_from_time.try(:strftime, "%I:%M%p") , :planned_out => c.to_time.try(:strftime, "%I:%M%p") , :actual_out => c.left_to_time.try(:strftime, "%I:%M%p") }
        end
        render :json => data
      end
    end
  end


  def today_attendance
    respond_to do |format|
      format.html do
      end
      format.json do
        if params[:date]
          @date = params[:date].to_date
        else
          @date = Date.today
        end
        enquiries = Enquiry.enrolled
        @en = enquiries.find_by_sql("SELECT * FROM enquiries WHERE '#{@date.strftime("%m/%d/%y")}' BETWEEN service_required_from AND service_required_to")
        
        @c = @en.map do |e|
          @time = RequestedTimeSlot.where(:enquiry_id => e.id).map do |c|
            @cg = e.care_giver_masters.map do |cg|
              @att = CareGiverDailyAttendance.where(:care_giver_master_id => cg.id ).where(:patient_id => e.patient_id).where(:date => @date).where(:from_time => c.time_from)
              if @att.blank? and c.time_from  > Time.now
                @alert_in = 'White'
              elsif @att.blank? and c.time_from < Time.now
                @alert_in = 'Red'
              else
                @alert_in = 'Green'
              end
              if @att.blank? 
                @alert_out = 'White'
              elsif @att.present? and c.time_to > @att[0][:left_to_time]
                @alert_out = 'Red'
              else
                @alert_out = 'Green'
              end
              {
                :id => e.id , :name => cg.try(:name) , :patient => e.patient.try(:name), :planned_in => c.try(:time_from).try(:strftime, " %I:%M%p"), :actual_in => @att.try(:reached_from_time).try(:strftime, "%I:%M%p") ,:alert_in => @alert_in , :planned_out => c.try(:time_to).try(:strftime, "%I:%M%p") , :actual_out =>  @att.try(:left_to_time).try(:strftime, "%I:%M%p") , :alert_out => @alert_out
              }
            end
          end
        end
        render :json => @c
      end
    end
  end

  def enquiries
    respond_to do |format|
      format.html do
      end
      format.json do
        
        if params[:enquiry][:status] == 'Enquiries'
          @missed = MissedCallRequest.all.map do |x|
            {
              :id => x.id ,:phone_no => x.mobile_no , :time => x.requested_at.strftime("%d/%m/%y - %H:%M")
            }
          end
          @enquiry  = Enquiry.all.map do |x|
            {
              :id => x.id, :name => (x.patient_name.present? ? x.patient_name : x.requested_user_name)  , :mobile_no => x.mobile_no , :email => x.email , :address => x.address_str
            }
          end
        else
          @m = MissedCallRequest.where(:current_tag => params[:enquiry][:status])
          @missed = @m.map do |x|
            {
              :id => x.id ,:phone_no => x.mobile_no , :time => x.requested_at.strftime("%d/%m/%y - %H:%M")
            }
          end
          @e = Enquiry.where(:current_tag => params[:enquiry][:status] )
          @enquiry = @e.map do |x|
            {
              :id => x.id, :name => (x.patient_name.present? ? x.patient_name : x.requested_user_name)  , :mobile_no => x.mobile_no , :email => x.email , :address => x.address_str
            }
          end
        end
        render :json => {:missed => @missed , :enquiry => @enquiry }
      end
    end
  end

  def cg_attendance_in_date_range
    respond_to do |format|
      format.json do
        data = []
        given_date_range = (params[:from].to_date..params[:to].to_date)
        care_giver_masters = CareGiverMaster.includes(:care_giver_daily_attendances).where("care_giver_daily_attendances.date".to_s => given_date_range)
        care_giver_masters.each do |care_giver_master|
          grouped_attendaces =  care_giver_master.care_giver_daily_attendances.group_by(&:date)
          cg_data = {id: care_giver_master.id, care_giver_master: care_giver_master.first_name, :attendance_details => []}
          given_date_range.each do |date|
            attendance = grouped_attendaces[date].try(:first)
            inner_data = {date: date}
            if attendance.present?
              inner_data[:status] = "W"
              registered_attendances = Enrollment.not_closed.belongs_to_care_giver(attendance.care_giver_master_id).belongs_to_enquiry(Enquiry.after_start_date(given_date_range.first))
              if registered_attendances.count > 0
                inner_data[:active] = "Yes"
                inner_data[:time_attributes] = grouped_attendaces[date].map{|a| a.slice(:id, :from_time, :to_time, :reached_from_time, :left_to_time)}
              end
            else
              if CareGiverLeave.leave_on_date(date).approved.count > 0
                inner_data[:status] = "L"
              else
                inner_data[:status] = "NA"
              end
            end
             cg_data[:attendance_details].push(inner_data)
          end
          data.push(cg_data)
        end
        render :json => data
      end
    end
  end
  
end
