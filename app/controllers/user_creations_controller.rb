class UserCreationsController < ApplicationController
  
  def save_user
    respond_to do |format|
      
      format.html{}
      
      format.json do
        @role =  params[:user]
        @r = Role.find_by_id(@role[:role] )
        @user =  User.new(:user_name => @role[:user_name], :email => @role[:email] , :password =>User.generate_password_on_admin_creation  , :role => @r.name , :role_id => @r.id)
        if @user.save
          UserMailer.welcome_email(@user , @user.password).deliver_now
        end
      end
      p 'user creation save user'
      render :json => @user
    end
  end
  
  def update_user
    respond_to do |format|
      format.json do
        @user_params = params[:user]
        @user = User.find_by_id(@user_params[:id])
        @user.role_id = @user_params[:role_id]
        @role = Role.where(:id => @user_params[:role_id] )
        @user.role = @role[0][:name]
        @user.save
        #render json: {status: @patient_routine)} 
      end
    end
    p 'user creation update user'
    render :json => @user
    
  end

  def all_users
    respond_to do |format|
      format.html{}
      format.json do
        @users = User.where.not(role: "care_giver").map do |x|
          { id: x.id, 
            name: x.user_name, 
            email: x.email, 
            role_id: x.role_id, 
            role: x.role }
        end
        p 'user creation  user'
        render :json => @users
      end
    end 
  end
  
  def new
  end
end
