class RoleCreationsController < ApplicationController
  def index
  end

  def new
  end

  def show
  end
  
  def update_menu
    @array = []
    @checked = params[:array].select {|x| x[:checked_value].present? && x[:checked_value] == true}
    @checked.each do |y|
      @m = MenuBar.find_by_id(y[:menu_bar_id])
      x = RoleMenuMapping.new()
      x.menu_bar_id = y[:menu_bar_id]
      x.role_id = y[:role_id]
      @role = Role.find_by_id(y[:role_id])
      @array.push(x)
      
      if @m.parent_id != 0
        @men  = RoleMenuMapping.new()
        @men.role_id = @role.id
        @men.menu_bar_id = @m.parent_id
        @array.push(@men)
        @n = MenuBar.find_by_id(@m.parent_id)
        if @n.parent_id != 0 
          @menu = RoleMenuMapping.new()
          @menu.role_id = @role.id
          @menu.menu_bar_id = @n.parent_id
          @array.push(@menu)
        end
      end
    end
    
    @role.role_menu_mappings.destroy_all
    @array.map(&:save)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @array
      end
    end
  end 
  

  def corresponding_menu
    respond_to do |format|
      format.html{}
      format.json do
        @role  = Role.find_by_id(params[:role])
        @menu = MenuBar.all.map do |x|
          role = @role.role_menu_mappings.where(:menu_bar_id => x.id).first 
          if role.present?
            role.checked_value = true
            role
          else
            e = RoleMenuMapping.new()
            e.checked_value = false
            e.menu_bar_id = x.id
            e.role_id = @role.id
            e
          end
        end
        render :json => @menu
      end
    end
  end

  def save_menu
    respond_to do |format|
      format.html{}
      format.json do
        p '1111111111111111111111111111navya'
        p params[:main_menu]
        p params[:sub_menu]
        menu_creator = LifeCircle::Menu.new( params[:menu] ,params[:array], current_user)
      
        render :json => menu_creator.create
      end
    end
  end
  
  def role
    respond_to do |format|
      format.html{}
      format.json do
        role = Role.all 
        render :json => role
      end
    end
  end

  def role_menu
    respond_to do |format|
      format.html{}
      format.json do
        @role_menu = RoleMenuMapping.select(:menu_bar_id , :role_id).uniq.map do |x|
          { id: x.id , role_id: x.role_id , menu_id: x.menu_bar_id , menu_name: x.menu_bar.name  , parent_id: x.menu_bar.parent_id}
        end
        render :json => @role_menu
      end
    end
  end

  def get_menu
    respond_to do |format|
      format.html do
      end
      format.json do
        @menu  = []
        RoleMenuMapping.where(:role_id => current_user.role_id).select(:menu_bar_id , :role_id).uniq.each do |x| 
          @menu << MenuBar.find_by_id(x.menu_bar_id)
        end
       
        render :json => @menu
      end
    end
  end

  def menu_bar
    respond_to do |format|
      format.html do
      end
      format.json do
        @menu = MenuBar.all
        render :json => @menu 
      end
    end
  end
end

