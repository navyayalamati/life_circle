class ServiceMastersController < ApplicationController
  load_resource

  def index
    @services = ServiceMaster.select(:id, :service).all.order("service")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @services
      end
    end
  end
  
  def show
  end
  
  def create
    @service_master = ServiceMaster.new(service_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @service_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @service_master.update(service_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @service_master.destroy}
      end
    end
  end

  private
  
  def service_master_params
    params.require(:service_master).permit(:service, :description)
  end

end
