class HomeController < ApplicationController

  def welcome_enquiry
    session[:new_enquiry] = nil
  end

  def check_user_session
    respond_to do |format|
      format.json do
        render :json => {:status => current_user.present?, :user => current_user }
      end
    end
  end

end
