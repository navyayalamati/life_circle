class UnitMastersController < ApplicationController
  load_resource
  
  def index
    @unit = UnitMaster.select(:id, :name).all.order("id")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @unit
      end
    end
  end
  def create
    @unit_master = UnitMaster.new(unit_master_params)
    #@unit_master.created_by = current_user.id
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @unit_master.save}
      end
    end

  end
  def edit
    @unit = UnitMaster.find(params[:id])
  end

  def update
    @unit = UnitMaster.find(params[:id])
    @unit.name = params[:unit_master][:name] 
    @unit.save
    if @unit.save
      redirect_to unit_masters_path
    else
      render "edit"
    end
  end
  # def update
  #   respond_to do |format|
  #     format.json do
  #       render json: {status: @unit_master.update(unit_master_params)}
  #     end
  #   end
  # end

  # def destroy
  #   respond_to do |format|
  #     format.json do
  #       render json: {status: @unit_master.destroy}
  #     end
  #   end
  # end
  def destroy
    @unit = UnitMaster.find(params[:id])
    @unit.destroy
    redirect_to unit_masters_path
  end

  private
  
  def unit_master_params
    params.require(:unit_master).permit(:name)
  end
end
