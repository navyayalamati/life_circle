class PaymentMastersController < ApplicationController
  before_action :load_enrollment
  
  def index
    @payment_masters = @enrollment.payment_masters
  end

  def show
    @payment_master = PaymentMaster.find(params[:id])
    respond_to do |format|
      format.html do
      end
      format.pdf do
        render :pdf => "invoice_for_the_month_#{@payment_master.invoice_generation_date.strftime('%m-%Y')}",
        :formats => [:pdf],
        :page_size => 'A4'
      end
    end
  end
  
  def new
    payment_date_period = @enrollment.generate_payment_date_period
    payment_calculator = LifeCircle::PaymentCalculator.new(@enrollment, Date.today)
    @payment_master_form = payment_calculator.new_payment(payment_date_period.first, payment_date_period.last).first
    respond_to do |format|
      format.html do
      end
      format.json do
      end
    end
  end

  def revise_care_giver_charge
    respond_to do |format|
      format.html do
        @form_object = LifeCircle::CareGiverChargeReviseFormObject.new({:enrollment_id => @enrollment.id, :enquiry_care_givers => @enrollment.enquiry.enquiry_care_givers})
        payment_date_period = @enrollment.generate_payment_date_period
        @form_object.period_from = payment_date_period.first
        if (@enrollment.close_request_date.present? and @form_object.period_from>= @enrollment.close_request_date) 
          redirect_to @enrollment, {:error => "Cannot proceed to payment since enrollment closed"}
        elsif @form_object.period_from > payment_date_period.last
          flash[:fail] = "Cannot proceed to payment. Invalid payment date period"
          redirect_to enrollment_payment_masters_path(@enrollment)
        end
        
        @form_object.period_to = payment_date_period.last
        @form_object.no_of_days = (@form_object.period_from .. @form_object.period_to).count
      end
      format.json do
      end
    end
  end

  def recurring_payment
    cg_params = params[:life_circle_care_giver_charge_revise_form_object][:enquiry_care_givers]
    history_creator = LifeCircle::EnrollmentHistoryCreator.new(@enrollment, current_user) 
    status = history_creator.check_and_update_care_giver_charge_changes(cg_params)
    #generation_date = Date.today #Date.strptime("25/10/2015", "%d/%m/%Y") #Date.today
    if status
      start_date = params[:life_circle_care_giver_charge_revise_form_object][:period_from]
      end_date = params[:life_circle_care_giver_charge_revise_form_object][:period_to]
      #generation_date_str = params[:life_circle_care_giver_charge_revise_form_object][:generation_date]
      generation_date_str = Date.today
      start_date = Date.strptime(start_date, "%d/%m/%Y")
      end_date = Date.strptime(end_date, "%d/%m/%Y")
      generation_date = Date.today
      #generation_date = Date.strptime(generation_date_str, "%d/%m/%Y")
      status = generate_new_payment(start_date, end_date, generation_date)
    end
    respond_to do |format|
      format.html do
        if status
          redirect_to review_invoices_enrollment_payment_masters_path(:enrollment_id => @enrollment.id, :generation_date => generation_date)
        else
          flash[:error] = "Invalid date range"
          redirect_to revise_care_giver_charge_enrollment_payment_masters_path(@enrollment)
        end
      end
      format.json do
      end
    end
  end
  
  def payment_on_closing_enrollment
    respond_to do |format|
      format.html do
        @close_date = Date.today #strptime("18/04/2016", "%d/%m/%Y")
      end
      format.json do
        close_date = Date.strptime(params[:close_date], "%d/%m/%Y")
        attributes = LifeCircle::PaymentMasterCreationService.new({}, @enrollment, current_user).generate_and_return_closing_payment_atributes(close_date)
        render :json => attributes 
      end
    end
  end
  
  def review_invoices
    respond_to do |format|
      format.html do
        @generation_date = params[:generation_date]
      end
      format.json do
        @payment_masters = @enrollment.payment_masters.generated_on(Date.strptime(params[:generation_date], "%d/%m/%Y"))
        render :json => {:payment_masters => @payment_masters}
      end
    end
  end

  def payment_preview
    @payment_master = PaymentMaster.find(params[:id])
    respond_to do |format|
      format.html do
        if @payment_master.completed?
          redirect_to enrollment_payment_master_path(@enrollment, @payment_master)
        end
      end
      format.json do
        render :json => {:payment_master => @payment_master, :credit_notes => @enrollment.credit_notes.pending}
      end
    end
  end

  def create_credit_note
    @payment_master = PaymentMaster.find(params[:id])
    @credit_note = CreditNote.new(credit_note_params)
    status = @credit_note.save
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status, :credit_note => @credit_note}
      end
    end
  end

  def destroy_credit_note
    @payment_master = PaymentMaster.find(params[:id])
    @credit_note = CreditNote.find(params[:credit_note_id])
    status = @credit_note.destroy
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def update_credit_note_status
    @payment_master = PaymentMaster.find(params[:id])
    @credit_note = CreditNote.find(params[:credit_note_id])
    status = @credit_note.update({:status => params[:status]})
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end
  

  def issue_credit_note
    @payment_master = PaymentMaster.find(params[:id])
    @credit_note = CreditNote.find(params[:credit_note_id])
    status = @credit_note.update({:status => "ISSUED", :payment_master_id => @payment_master.id})
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def mark_as_pending_credit_note
    @payment_master = PaymentMaster.find(params[:id])
    @credit_note = CreditNote.find(params[:credit_note_id])
    status = @credit_note.update({:status => "PENDING", :payment_master_id => @payment_master.id})
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end


  def revoke_credit_note
    @payment_master = PaymentMaster.find(params[:id])
    @credit_note = CreditNote.find(params[:credit_note_id])
    status = @credit_note.update({:status => "REVOKED", :payment_master_id => @payment_master.id})
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def update_payment_discount_details
    @payment_master = PaymentMaster.find(params[:id])
    payment_discount_params = params.permit(:discount, :discount_type, :discount_every_time)
    # payment_mode_params = payment_mode_params.merge(:payment_date => Date.today, :payment_done_by => current_user.id, :status => "COMPLETED")
    if(payment_discount_params[:discount_every_time].present? and payment_discount_params[:discount_every_time])
      @enrollment.update(payment_discount_params)
    end 
    status = @payment_master.update(payment_discount_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end

  def create
    payment_creation_service = LifeCircle::PaymentMasterCreationService.new(params.require(:life_circle_payment_master_form_object), @enrollment, current_user)
    status = payment_creation_service.save
    @payment_master = payment_creation_service.payment_master
    respond_to do |format|
      format.html do
        if status
          flash[:success] = "Payment has been done successfully"
          redirect_to "/enrollments/#{@enrollment.id}/payment_masters/#{@payment_master.id}"
        else
          flash[:success] = "Payment has not been done successfully"
          render "new"
        end
      end
      format.json do
        render :json => {:status => status}
      end
    end
  end


  def recalculate
    @payment_master = PaymentMaster.find(params[:id])
    status = false
    ActiveRecord::Base.transaction do
      @payment_master.credit_notes.not_excess_amount_paid.destroy_all
      @payment_master.destroy
      payment_master_params = params[:payment_master]
      @enrollment.discount = params[:payment_master][:discount]
      @enrollment.discount_type = params[:payment_master][:discount_type]
      start_date = payment_master_params[:period_from]
      end_date = payment_master_params[:period_to]
      start_date = Date.strptime(start_date, "%d/%m/%Y")
      end_date = Date.strptime(end_date, "%d/%m/%Y")
      generation_date = Date.strptime(params[:generation_date], "%d/%m/%Y")
      status = generate_new_payment(start_date, end_date, generation_date)
    end
      respond_to do |format|
      format.html do
        if status
          redirect_to payment_preview_enrollment_payment_master_path(@enrollment, @payment_master)
        else
          flash[:error] = "Invalid date range"
          redirect_to revise_care_giver_charge_enrollment_payment_masters_path(@enrollment)
        end
      end
      format.json do
        render :json => {status: status, :payment_master => @payment_master}
      end
    end

  end

  def save_closing_payment
    respond_to do |format|
      format.json do
        payment_creation_service = LifeCircle::PaymentMasterCreationService.new(params, @enrollment, current_user)
        if payment_creation_service.save_closing_payment
          render :json => {:status => true, :payment_master => payment_creation_service.payment_master}
        else
          render :json => {:status => false}
        end
      end
    end
  end

  def credit_notes
    @payment_master = PaymentMaster.find(params[:id])
    @credit_notes = @payment_master.credit_notes 
    respond_to do |format|
      format.html do
      end
      format.pdf do
        render :pdf => "credit_notes_of_invoice#{@payment_master.invoice_no}",
        :formats => [:pdf],
        :page_size => 'A4'
      end
    end
  end

  private

  def load_enrollment
    @enrollment = Enrollment.find(params[:enrollment_id])
  end

  def payment_master_params
    params.require(:payment_master).permit(:enrollment_id, :payment_mode, :amount_in_rupees, :period_from, :period_to, :description_1, :description_2, :status, :total_amount_in_rupees)
  end

  def credit_note_params
    params.require(:credit_note).permit(:payment_master_id, :reason_for_creadit_note, :value_of_credit_note, :status, :credit_note_date, :enrollment_id)
  end


  def generate_new_payment(start_date, end_date, generation_date)
    date_status_attributes = @enrollment.validate_and_revise_payment_generation_period(start_date, end_date)
    status = date_status_attributes[:valid]
    if status
      payment_creation_service = LifeCircle::PaymentMasterCreationService.new({}, @enrollment, current_user)
      @payment_master = payment_creation_service.build_payment(generation_date, date_status_attributes[:start_date], date_status_attributes[:end_date]) # Date.today
    end
    status
  end

end
