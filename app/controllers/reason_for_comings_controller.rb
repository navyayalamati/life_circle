class ReasonForComingsController < ApplicationController
  before_action :set_reason_for_coming, only: [:show, :edit, :update, :destroy]

  # GET /reason_for_comings
  # GET /reason_for_comings.json
  def index
    @reason_for_comings = ReasonForComing.all
  end

  # GET /reason_for_comings/1
  # GET /reason_for_comings/1.json
  def show
  end

  # GET /reason_for_comings/new
  def new
    @reason_for_coming = ReasonForComing.new
  end

  # GET /reason_for_comings/1/edit
  def edit
  end

  # POST /reason_for_comings
  # POST /reason_for_comings.json
  def create
    @reason_for_coming = ReasonForComing.new(reason_for_coming_params)

    respond_to do |format|
      if @reason_for_coming.save
        format.html { redirect_to reason_for_comings_path}
        format.json { render :show, status: :created, location: @reason_for_coming }
      else
        format.html { render :new }
        format.json { render json: @reason_for_coming.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reason_for_comings/1
  # PATCH/PUT /reason_for_comings/1.json
  def update
    respond_to do |format|
      if @reason_for_coming.update(reason_for_coming_params)
        format.html { redirect_to reason_for_comings_path }
        format.json { render :show, status: :ok, location: @reason_for_coming }
      else
        format.html { render :edit }
        format.json { render json: @reason_for_coming.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reason_for_comings/1
  # DELETE /reason_for_comings/1.json
  def destroy
    @reason_for_coming.destroy
    respond_to do |format|
      format.html { redirect_to reason_for_comings_url, notice: 'Reason for coming was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reason_for_coming
      @reason_for_coming = ReasonForComing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reason_for_coming_params
      params.require(:reason_for_coming).permit(:name)
    end
end
