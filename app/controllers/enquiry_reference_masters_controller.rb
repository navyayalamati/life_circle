class EnquiryReferenceMastersController < ApplicationController
  load_resource
  def index
    @enquiry_references = EnquiryReferenceMaster.all.order("reference")
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @enquiry_references
      end
    end
  end
  

  def create
    @enquiry_reference_master = EnquiryReferenceMaster.new(enquiry_reference_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @enquiry_reference_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @enquiry_reference_master.update(enquiry_reference_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @enquiry_reference_master.destroy}
      end
    end
  end

  private
  
  def enquiry_reference_master_params
    params.require(:enquiry_reference_master).permit(:reference)
  end
end
