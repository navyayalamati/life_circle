class TransportChargeMastersController < ApplicationController
  load_resource
  def index
    @transport_charge_masters = TransportChargeMaster.select(:id, :from_distance, :to_distance,:charge).all
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => @transport_charge_masters
      end
    end
  end
  

  def create
    @transport_charge_master = TransportChargeMaster.new(transport_charge_master_params)
    respond_to do |format|
      format.html do
      end
      format.json do
        render json: {status: @transport_charge_master.save}
      end
    end

  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @transport_charge_master.update(transport_charge_master_params)}
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json do
        render json: {status: @transport_charge_master.destroy}
      end
    end
  end

  def charge_for_distance
    distance = params[:distance]
    charge = TransportChargeMaster.having_distance(distance).first.try(:charge).to_i
    respond_to do |format|
      format.json do
        render json: {charge: charge}
      end
    end
  end



  private
  
  def transport_charge_master_params
    params.require(:transport_charge_master).permit(:from_distance, :to_distance, :charge)
  end
end
