class MissedCallListenersController < ApplicationController

  #This action method is configured at imimobile account
  # URL configured at IMIMobile is 
  # https://staging-lifecircle.herokuapp.com/missed_call_listeners/listen.json?msisdn=!MOBILENO!&serviceName=MisscallTOSMS
  # Request params which we get are {"msisdn"=>"8008417878", "serviceName"=>"MisscallTOSMS"} 
  def listen
    respond_to do |format|
      format.json do
        missed_call_request = MissedCallRequest.new({:mobile_no => params[:msisdn], :requested_at => DateTime.now, :status => "PENDING"})
        missed_call_request.save
        render :json => {:status => true}
      end
    end
  end

end
