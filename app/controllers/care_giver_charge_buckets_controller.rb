class CareGiverChargeBucketsController < ApplicationController


  def index
    @care_giver_charge_buckets = CareGiverChargeBucket.all 
    unless @care_giver_charge_buckets.present?
      @care_giver_charge_buckets = (4.times.map do |i|
        CareGiverChargeBucket.new
      end)
    end
    respond_to do |format|
      format.html{}
      format.json do
        render :json => @care_giver_charge_buckets
      end
    end
  end

  def new
  end

  def save
    @care_giver_charge_buckets = []
    params[:care_giver_charge_buckets].each do |param|
      if(param[:hrs_from].present?)
        charge_bucket =  CareGiverChargeBucket.new(param.permit(:hrs_from, :hrs_to, :charge_percentage))
        @care_giver_charge_buckets.push(charge_bucket)
      end
    end
    status = false
    ActiveRecord::Base.transaction do
      CareGiverChargeBucket.destroy_all
      status = @care_giver_charge_buckets.map(&:save!).all?
    end
    respond_to do |format|
      format.html{}
      format.json do
        render :json => status
      end
    end

  end

end
