class EnquiryConversationsController < ApplicationController
  load_resource
  before_action :load_parent
  before_action :authenticate_user!

  def index
    @enquiry_conversations = @parent.enquiry_conversations.order("entered_at desc")
    if @conversation_master_type =='Enquiry' and @enrollment_saftey_violations.present?
      @enquiry_conversations << {:description => @enrollment_saftey_violations[0][:comments], :tag => "Saftey-Check", :formated_entered_at => @enrollment_saftey_violations[0][:created_at].strftime("%d/%m/%Y %H:%M"), :entered_at => @enrollment_saftey_violations[0][:created_at]}
      @enquiry_conversations.order("entered_at desc")
    end
    if @conversation_master_type =='Enquiry' 
      @enquiry = Enquiry.find_by_id(params[:conversation_master_id])
      if(@enquiry.missed_call_request.present?)
        @enquiry_conversations = (@enquiry.missed_call_request.enquiry_conversations.order("entered_at desc") + @enquiry_conversations)
      end
    end
    respond_to do |format|
      format.html do
      end
      format.json do
        render :json => {:conversations => @enquiry_conversations, :parent_tag => @parent.current_tag}
      end
    end
  end

  def conversation_json
    
    respond_to do |format|
      format.html do
      end
      format.json do
        @enquiry = Enquiry.find_by_id(params[:conversation_master_id])
        @enquiry_conversations = @parent.enquiry_conversations.order("entered_at desc").map do |x|
          {
            :description => x.description, :entered_by => x.author_name , :entered_at => x.formated_entered_at , :tag => x.tag
          }
        end
        if @enquiry.present? and @enquiry.pre_enquiry?
          @status = "Add criteia"
        elsif @enquiry.blank? 
          @status = "Caregiver"
        else
          @status = "Enroll"
          
        end
        render :json => {:conversations => @enquiry_conversations, :status => @status }
      end
    end
  end

  
  
  def create
    @enquiry_conversation = EnquiryConversation.new(enquiry_conversation_params)
    @enquiry_conversation.save
    @enquiry_conversation.entered_at = DateTime.now
    if @enquiry_conversation.tag == 'To Audit'
      @enquiry_conversation.conversation_master.closed_by = current_user.id
      #@enquiry_conversation.conversation_master.closed_at = DateTime.now
      @enquiry_conversation.conversation_master.save
    end
    @enquiry_conversation.entered_by = current_user.id
    status = false
    ActiveRecord::Base.transaction do
      status = @enquiry_conversation.save
      if status and @enquiry_conversation.tag.present?
        
        if(@enquiry_conversation.tag.to_s.downcase == 'to audit' or @enquiry_conversation.tag.to_s.downcase == 'closed')
          enquiry_status = ((@enquiry_conversation.tag.to_s.downcase == 'to audit') ? Enquiry::STATUS[:sent_to_audit] : Enquiry::STATUS[:closed] ) 
          @parent.update_attributes({:current_tag => @enquiry_conversation.tag, :status => enquiry_status})
        else
          @parent.update_attributes({:current_tag => @enquiry_conversation.tag})
        end
      end
    end
    respond_to do |format|
      format.html do
        #redirect_to :back
      end
      format.json do
        render json: {status: status}
      end
    end
    
  end

  def update
    respond_to do |format|
      format.json do
        render json: {status: @enquiry_conversation.update(enquiry_conversation_params)}
      end
    end
  end

  private

  def load_parent
    @conversation_master_type = params[:conversation_master_type]
    @conversation_master_id = params[:conversation_master_id]
    @cg_status = params[:cg_status]
    @cg_leave_id = params[:cg_leave_id]
    @name = params[:name]
    @conversation_for = params[:conversation_for]
    @care_giver_id = params[:care_giver_id]
    @type = params[:type]
    if @conversation_master_type =='Enquiry'
      
      @parent = Enquiry.find(params[:conversation_master_id])
      @enrollment = Enrollment.where(:enquiry_id => params[:conversation_master_id])
      if @enrollment.present?
      @enrollment_saftey_violations = EnrollmentSafetyViolation.where(:enrollment_id => @enrollment[0][:id])
      end
      
    elsif @conversation_master_type == 'MissedCallRequest'
      @parent = MissedCallRequest.find(params[:conversation_master_id])
    elsif @conversation_master_type == 'CareGiverEnquiry'
      @parent = CareGiverEnquiry.find(params[:conversation_master_id])
    end
  end

  def enquiry_conversation_params
    params.require(:enquiry_conversation).permit(:conversation_master_id, :conversation_master_type, :description, :tag)
  end
  

end
