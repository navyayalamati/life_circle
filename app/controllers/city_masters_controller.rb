class CityMastersController < ApplicationController
  before_action :set_city_master, only: [:show, :edit, :update, :destroy]

  def check_city_support
    respond_to do |format|
      format.json do
        city = params[:city]
        render :json => {isCityExists: (CityMaster.matching_city(city).count > 0)}
      end
    end
  end

  def save_city
    respond_to do |format|
      format.json do
        p 'qqqqqqqqqq'
        city = CityMaster.new()
        city.name = params[:city][:city]
        city.address = params[:address]
        #city.address = params[:city]
        city.save
        render :json => city
      end
    end
  end

  def  check_location_support
    respond_to do |format|
      format.json do
        city = params[:city]
        @a = []
        if city.present?
          city.each do |x|
            if CityMaster.matching_city(x).count > 0
              @a << CityMaster.matching_city(x)
              p 'count'
              p @a.count
            end
          end
        end
        render :json => {status: (@a.count > 0) }
      end
    end
  end

  # GET /city_masters
  # GET /city_masters.json
  def index
    @city_masters = CityMaster.all
  end

  # GET /city_masters/1
  # GET /city_masters/1.json
  def show
  end

  # GET /city_masters/new
  def new
    @city_master = CityMaster.new
  end

  # GET /city_masters/1/edit
  def edit
  end

  # POST /city_masters
  # POST /city_masters.json
  def create
    @city_master = CityMaster.new(city_master_params)

    respond_to do |format|
      if @city_master.save
        format.html { redirect_to @city_master, notice: 'City master was successfully created.' }
        format.json { render :show, status: :created, location: @city_master }
      else
        format.html { render :new }
        format.json { render json: @city_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /city_masters/1
  # PATCH/PUT /city_masters/1.json
  def update
    respond_to do |format|
      if @city_master.update(city_master_params)
        format.html { redirect_to city_masters_path }
        format.json { render :show, status: :ok, location: @city_master }
      else
        format.html { render :edit }
        format.json { render json: @city_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /city_masters/1
  # DELETE /city_masters/1.json
  def destroy
    @city_master.destroy
    respond_to do |format|
      format.html { redirect_to city_masters_url, notice: 'City master was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city_master
      @city_master = CityMaster.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def city_master_params
      params.require(:city_master).permit(:name , :address)
    end
end
