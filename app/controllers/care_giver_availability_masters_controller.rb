class CareGiverAvailabilityMastersController < ApplicationController
  before_action :load_care_giver_master

  def new
    respond_to do |format|
      format.json do
        render :json => { :availabilities => CareGiverAvailabilityMaster.build_availabilities(@care_giver_master.id) }
      end
    end
  end

  

  def index
  end


  def create
    respond_to do |format|
      format.json do
        status = false
        ActiveRecord::Base.transaction do
          availabilities = []
          p '111111111111111111111'
          p params[:availabilities]
          p '222222222222222222222'
          params[:availabilities].each do |day, value_params|
            if value_params[:enabled].present? and value_params[:enabled] == true and day != "all"
              value_params[:availabilities].each do |avail_params|
                availability = CareGiverAvailabilityMaster.new(availability_params(avail_params))
                availability.care_giver_master = @care_giver_master
                availability.day = day
                availabilities.push(availability)
              end
            end
          end
          
          if @care_giver_master.care_giver_availability_masters.destroy_all
            if availabilities.map(&:valid?).all?
              status = availabilities.map(&:save).all?
            end
          end
        end
        render :json => {:status => status}
      end
    end
  end

  def show
  end

  private
  
  def availability_params(local_params)
    local_params.permit(:care_giver_master_id,:day, :time_from, :time_to)
  end
  
  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end
  
end
