class CareGiverServicesController < ApplicationController
  before_action :load_care_giver_master
  load_resource :only => [:approve]
  before_action :authenticate_user!
  
  def index
    respond_to do |format|
      format.html{}
      format.json do
        render :json => {:care_giver_services => @care_giver_master.care_giver_services}
      end
    end
  end

  def services_for_registration
    respond_to do |format|
      format.html{}
      format.json do
        services = CareGiverService.all_serivces(@care_giver_master.id)
        @data = services.map{|service| service.merge!(:assigned => service["id"].present?)}
        
        render :json => {:care_giver_services => @data}
      end
    end
  end

  def save_services
    ActiveRecord::Base.transaction do
      @care_giver_master.care_giver_services.destroy_all
      care_giver_services = []
      params[:care_giver_services].each do |local_params|
        if local_params[:assigned].present? and local_params[:assigned]
          service = CareGiverService.new({:approved => false, :approved_by => current_user.id}) do |s|
            s.service_master_id = local_params[:service_master_id];
            s.care_giver_master_id = @care_giver_master.id
          end
          care_giver_services.push(service)
        end
      end
      if care_giver_services.map(&:valid?).all?
        @status = care_giver_services.map(&:save).all?
        @care_giver_master.check_children_approvals_and_update_self_approval
      end
    end
    respond_to do |format|
      format.html{}
      format.json do
        render :json => {:status => @status}
      end
    end
  end

  def approve
    respond_to do |format|
      format.html do
      end
      format.json do
        status = @care_giver_master.care_giver_services.map do |cs| 
          cs.update({:approved => true, :approved_by => current_user.id})
        end
        render :json => {:status => status}
      end
    end
  end


  private
  
  def care_giver_service_params(local_params)
    local_params.permit(:care_giver_master_id, :service_master_id)
  end
  
  def load_care_giver_master
    @care_giver_master = CareGiverMaster.find(params[:care_giver_master_id])
  end

end
