require 'test_helper'

class PaymentBillsControllerTest < ActionController::TestCase
  setup do
    @payment_bill = payment_bills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:payment_bills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create payment_bill" do
    assert_difference('PaymentBill.count') do
      post :create, payment_bill: { amount_paid: @payment_bill.amount_paid, description_1: @payment_bill.description_1, description_2: @payment_bill.description_2, payment_date: @payment_bill.payment_date, payment_done_by: @payment_bill.payment_done_by, payment_master_id: @payment_bill.payment_master_id, payment_mode: @payment_bill.payment_mode, status: @payment_bill.status }
    end

    assert_redirected_to payment_bill_path(assigns(:payment_bill))
  end

  test "should show payment_bill" do
    get :show, id: @payment_bill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @payment_bill
    assert_response :success
  end

  test "should update payment_bill" do
    patch :update, id: @payment_bill, payment_bill: { amount_paid: @payment_bill.amount_paid, description_1: @payment_bill.description_1, description_2: @payment_bill.description_2, payment_date: @payment_bill.payment_date, payment_done_by: @payment_bill.payment_done_by, payment_master_id: @payment_bill.payment_master_id, payment_mode: @payment_bill.payment_mode, status: @payment_bill.status }
    assert_redirected_to payment_bill_path(assigns(:payment_bill))
  end

  test "should destroy payment_bill" do
    assert_difference('PaymentBill.count', -1) do
      delete :destroy, id: @payment_bill
    end

    assert_redirected_to payment_bills_path
  end
end
