require 'test_helper'

class CityMastersControllerTest < ActionController::TestCase
  setup do
    @city_master = city_masters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:city_masters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create city_master" do
    assert_difference('CityMaster.count') do
      post :create, city_master: { latitude: @city_master.latitude, longitude: @city_master.longitude, name: @city_master.name }
    end

    assert_redirected_to city_master_path(assigns(:city_master))
  end

  test "should show city_master" do
    get :show, id: @city_master
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @city_master
    assert_response :success
  end

  test "should update city_master" do
    patch :update, id: @city_master, city_master: { latitude: @city_master.latitude, longitude: @city_master.longitude, name: @city_master.name }
    assert_redirected_to city_master_path(assigns(:city_master))
  end

  test "should destroy city_master" do
    assert_difference('CityMaster.count', -1) do
      delete :destroy, id: @city_master
    end

    assert_redirected_to city_masters_path
  end
end
