require 'test_helper'

class CareGiverEquipmentsControllerTest < ActionController::TestCase
  setup do
    @care_giver_equipment = care_giver_equipments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:care_giver_equipments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create care_giver_equipment" do
    assert_difference('CareGiverEquipment.count') do
      post :create, care_giver_equipment: { name: @care_giver_equipment.name, status: @care_giver_equipment.status }
    end

    assert_redirected_to care_giver_equipment_path(assigns(:care_giver_equipment))
  end

  test "should show care_giver_equipment" do
    get :show, id: @care_giver_equipment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @care_giver_equipment
    assert_response :success
  end

  test "should update care_giver_equipment" do
    patch :update, id: @care_giver_equipment, care_giver_equipment: { name: @care_giver_equipment.name, status: @care_giver_equipment.status }
    assert_redirected_to care_giver_equipment_path(assigns(:care_giver_equipment))
  end

  test "should destroy care_giver_equipment" do
    assert_difference('CareGiverEquipment.count', -1) do
      delete :destroy, id: @care_giver_equipment
    end

    assert_redirected_to care_giver_equipments_path
  end
end
