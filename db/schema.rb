# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160301163912) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "area_master_id"
    t.integer  "city_master_id"
    t.string   "state"
    t.string   "country"
    t.string   "pin_code"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "line1"
    t.string   "line2"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "city"
  end

  create_table "application_defaults", force: :cascade do |t|
    t.string   "description"
    t.string   "value"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "area_masters", force: :cascade do |t|
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "city_master_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "care_giver_additional_details", force: :cascade do |t|
    t.integer "care_giver_master_id"
    t.integer "height_in_cms"
    t.string  "hair"
    t.string  "eyes_color"
    t.string  "complexion"
    t.string  "identification_tatoo"
    t.string  "any_deoformity"
    t.string  "blood_group"
    t.string  "known_allergy"
    t.string  "current_health_problem"
    t.string  "past_health_problem"
    t.boolean "smoking_habit"
    t.boolean "chewing_tobacco"
    t.boolean "consume_liquor"
    t.boolean "convicted_any_court_of_law"
    t.boolean "civil_or_criminal_proceeding_in_court_of_law"
    t.boolean "physical_description_approved",                default: false
    t.integer "physical_description_approved_by"
    t.boolean "health_approved",                              default: false
    t.integer "health_approved_by"
  end

  create_table "care_giver_availability_masters", force: :cascade do |t|
    t.integer  "care_giver_master_id"
    t.string   "day"
    t.datetime "time_from"
    t.datetime "time_to"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "availability",         default: true
  end

  create_table "care_giver_charge_buckets", force: :cascade do |t|
    t.integer  "hrs_from"
    t.integer  "hrs_to"
    t.float    "charge_percentage"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "care_giver_charges", force: :cascade do |t|
    t.integer  "care_giver_master_id"
    t.integer  "base_charge"
    t.float    "margin"
    t.integer  "modified_by"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "margin_type"
    t.integer  "total_charge"
    t.boolean  "approved",             default: false
    t.integer  "approved_by"
  end

  create_table "care_giver_daily_attendances", force: :cascade do |t|
    t.integer  "care_giver_master_id"
    t.integer  "patient_id"
    t.date     "date"
    t.time     "from_time"
    t.time     "to_time"
    t.time     "reached_from_time"
    t.time     "left_to_time"
    t.string   "actual_latitude"
    t.string   "actual_longitude"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "status"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "enquiry_id"
  end

  create_table "care_giver_daily_budget_masters", force: :cascade do |t|
    t.integer  "upper_limit"
    t.integer  "lower_limit"
    t.string   "symbol"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "care_giver_documents", force: :cascade do |t|
    t.integer  "care_giver_master_id"
    t.string   "document_name"
    t.string   "attachment"
    t.date     "date_of_creation"
    t.boolean  "approved"
    t.integer  "approved_by"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "care_giver_educations", force: :cascade do |t|
    t.string  "institute"
    t.date    "period_from"
    t.date    "period_to"
    t.string  "exam_passed"
    t.float   "marks_percentage"
    t.boolean "highest_qualification"
    t.boolean "professional_nursing_qualification"
    t.string  "attachment"
    t.integer "care_giver_master_id"
    t.boolean "approved",                           default: false
    t.integer "approved_by"
  end

  create_table "care_giver_employers", force: :cascade do |t|
    t.string   "employer"
    t.date     "period_from"
    t.string   "period_to"
    t.string   "nature_of_work"
    t.string   "contact_no"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "care_giver_master_id"
    t.string   "attachment"
    t.boolean  "verified"
    t.boolean  "approved",             default: false
    t.integer  "approved_by"
    t.string   "fresher"
  end

  create_table "care_giver_enquiries", force: :cascade do |t|
    t.string   "name"
    t.string   "location"
    t.date     "dob"
    t.string   "phone"
    t.string   "email"
    t.date     "apt_date"
    t.time     "apt_time"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "status"
    t.string   "reason_for_closing"
    t.string   "highest_qualification"
    t.integer  "closed_by"
    t.date     "closed_at"
    t.string   "current_tag"
    t.string   "gender"
    t.string   "city"
  end

  create_table "care_giver_equipments", force: :cascade do |t|
    t.string   "name"
    t.string   "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "care_giver_languages", force: :cascade do |t|
    t.integer  "care_giver_master_id"
    t.integer  "language_master_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "care_giver_leaves", force: :cascade do |t|
    t.integer "care_giver_master_id"
    t.date    "applied_at"
    t.text    "description"
    t.string  "status"
    t.date    "from_date"
    t.date    "to_date"
  end

  create_table "care_giver_masters", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "recruitment_mode"
    t.string   "gender"
    t.string   "father_name"
    t.string   "mother_name"
    t.string   "place_of_birth"
    t.date     "dob"
    t.integer  "address_id"
    t.string   "telephone_no"
    t.string   "maritual_status"
    t.string   "id_proof_no"
    t.string   "id_proof_doc"
    t.boolean  "id_proof_verified"
    t.integer  "id_proof_verified_by"
    t.string   "address_proof_id"
    t.string   "address_proof_doc"
    t.boolean  "address_proof_verified"
    t.integer  "address_proof_verified_by"
    t.string   "status"
    t.string   "job_type"
    t.string   "mentioned_registration_type"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "id_proof_type"
    t.string   "address_proof_type"
    t.string   "heighest_qualification"
    t.string   "profile_pic"
    t.integer  "expected_sal_per_day"
    t.string   "reference_1"
    t.string   "reference_2"
    t.boolean  "approved"
    t.integer  "permanent_address_id"
    t.string   "reason_for_status_change"
    t.integer  "status_changed_by"
    t.date     "status_changed_at"
    t.integer  "care_giver_enquiry_id"
    t.integer  "city_master_id"
  end

  create_table "care_giver_relatives", force: :cascade do |t|
    t.string  "name"
    t.integer "age"
    t.string  "occupation"
    t.string  "contact_no"
    t.string  "relation"
    t.text    "address"
    t.boolean "emergency_contact"
    t.integer "care_giver_master_id"
    t.boolean "approved",             default: false
    t.integer "approved_by"
  end

  create_table "care_giver_return_equipments", force: :cascade do |t|
    t.integer  "care_giver_master_id"
    t.integer  "care_giver_equipment_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "care_giver_services", force: :cascade do |t|
    t.integer  "care_giver_master_id"
    t.integer  "service_master_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.boolean  "approved",             default: false
    t.integer  "approved_by"
  end

  create_table "care_to_be_provided_plans", force: :cascade do |t|
    t.string   "care_time"
    t.string   "comments"
    t.integer  "enrollment_id"
    t.integer  "created_by"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "cg_new_city_requests", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "city"
    t.string   "area"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "city_masters", force: :cascade do |t|
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "address"
  end

  create_table "credit_notes", force: :cascade do |t|
    t.integer  "payment_master_id"
    t.text     "reason_for_creadit_note"
    t.integer  "value_of_credit_note"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "status"
    t.date     "credit_note_date"
    t.integer  "enrollment_id"
  end

  create_table "daily_checks", force: :cascade do |t|
    t.text     "description"
    t.integer  "patient_id"
    t.integer  "care_giver_master_id"
    t.integer  "entered_by"
    t.datetime "entered_at"
    t.string   "tag"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "disability_masters", force: :cascade do |t|
    t.string   "disability"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "drug_masters", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "route_master_id"
    t.integer  "form_master_id"
    t.integer  "unit_master_id"
  end

  create_table "drug_quantity_masters", force: :cascade do |t|
    t.integer  "quantity"
    t.integer  "drug_master_id"
    t.integer  "enrollment_id"
    t.integer  "created_by"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "sos"
    t.integer  "dose"
    t.date     "review_date"
    t.date     "refill_date"
  end

  create_table "email_templates", force: :cascade do |t|
    t.string   "text1"
    t.string   "text2"
    t.string   "name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "template_id"
    t.integer  "template_text_id"
  end

  create_table "enquiries", force: :cascade do |t|
    t.string   "registered_name"
    t.string   "registered_by"
    t.string   "mentioned_registration_type"
    t.integer  "patient_id"
    t.integer  "area_master_id"
    t.string   "care_giver_gender"
    t.date     "service_required_from"
    t.date     "service_required_to"
    t.integer  "no_of_care_givers"
    t.boolean  "require_heavy_patient_lifting"
    t.string   "duty_station"
    t.string   "caregiver_stay"
    t.string   "language_profficiency"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.datetime "appointment_time"
    t.string   "status"
    t.text     "closing_comments"
    t.date     "closed_at"
    t.integer  "closed_by"
    t.boolean  "open_term",                     default: false
    t.string   "affordable_price"
    t.integer  "travel_distance"
    t.boolean  "consider_modified_charge",      default: false
    t.string   "mobile_no"
    t.string   "email"
    t.string   "requested_user_name"
    t.integer  "address_id"
    t.string   "current_tag"
    t.string   "form_filled_location"
    t.string   "type_of_enquiry"
    t.string   "appointment_status"
  end

  create_table "enquiry_care_giver_histories", force: :cascade do |t|
    t.integer "enquiry_history_id"
    t.integer "care_giver_master_id"
    t.integer "caregiver_charge"
    t.integer "transport_charge"
    t.integer "approx_travel_distance"
    t.integer "adjustment_amount"
  end

  create_table "enquiry_care_givers", force: :cascade do |t|
    t.integer  "enquiry_id"
    t.integer  "care_giver_master_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "caregiver_charge"
    t.integer  "transport_charge"
    t.integer  "approx_travel_distance"
  end

  create_table "enquiry_conversations", force: :cascade do |t|
    t.text     "description"
    t.integer  "conversation_master_id"
    t.integer  "entered_by"
    t.datetime "entered_at"
    t.string   "conversation_master_type"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "tag"
  end

  create_table "enquiry_histories", force: :cascade do |t|
    t.integer "enquiry_id"
    t.date    "service_required_from"
    t.date    "service_required_to"
    t.integer "no_of_care_givers"
    t.string  "duty_station"
    t.string  "caregiver_stay"
    t.date    "migration_date"
    t.boolean "consider_modified_charge", default: false
  end

  create_table "enquiry_language_histories", force: :cascade do |t|
    t.integer "enquiry_history_id"
    t.integer "language_master_id"
  end

  create_table "enquiry_languages", force: :cascade do |t|
    t.integer  "enquiry_id"
    t.integer  "language_master_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "enquiry_reference_masters", force: :cascade do |t|
    t.string   "reference"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enrollment_histories", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.string   "note"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "created_by"
  end

  create_table "enrollment_safety_violations", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.integer  "enrolled_by"
    t.text     "comments"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer  "enquiry_id"
    t.integer  "patient_id"
    t.string   "patient_bmi"
    t.string   "patient_home_setting"
    t.boolean  "cg_work_alone_with_patient"
    t.boolean  "suffering_from_infection_disease"
    t.boolean  "safe_and_well_connected_locality"
    t.boolean  "provides_personal_protective_equipment"
    t.boolean  "family_member_helps"
    t.integer  "report_prepared_by"
    t.integer  "report_audited_by"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "email_for_official_comminication"
    t.string   "primary_contact"
    t.integer  "enrollment_fee"
    t.integer  "caregiver_charge"
    t.integer  "discount"
    t.string   "discount_type"
    t.string   "status"
    t.string   "application_no"
    t.boolean  "discount_every_time"
    t.date     "close_request_date"
    t.date     "notification_given_date"
    t.text     "comments_on_notification"
    t.text     "closing_comments"
    t.string   "reason_for_closing"
    t.date     "closed_date"
    t.integer  "super_care_giver_id"
  end

  create_table "form_masters", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "guardians", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mobile_no"
    t.string   "relation"
    t.boolean  "address_same_as_patient"
    t.integer  "address_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "alternate_contact_no"
    t.integer  "age"
    t.string   "gender"
  end

  create_table "job_runs", force: :cascade do |t|
    t.string   "job_code"
    t.datetime "started_on"
    t.datetime "finished_on"
    t.string   "status"
    t.integer  "scrolled_by"
    t.date     "job_date"
  end

  create_table "key_vitals_masters", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "unit"
    t.string   "upper_limit"
    t.string   "lower_limit"
  end

  create_table "language_masters", force: :cascade do |t|
    t.string   "language"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "life_circle_agents", force: :cascade do |t|
    t.string   "name"
    t.string   "mobile_no"
    t.integer  "user_id"
    t.integer  "address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lost_reason_masters", force: :cascade do |t|
    t.string   "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "menu_bars", force: :cascade do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.string   "url"
    t.string   "mark_as_parent"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "missed_call_requests", force: :cascade do |t|
    t.string   "mobile_no"
    t.datetime "requested_at"
    t.string   "status"
    t.string   "processed_by"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "current_tag"
    t.integer  "enquiry_id"
  end

  create_table "new_city_requests", force: :cascade do |t|
    t.string   "city"
    t.string   "area"
    t.string   "email"
    t.string   "mobile_no"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

  create_table "patient_adaptive_equipment_histories", force: :cascade do |t|
    t.integer  "enrollment_history_id"
    t.integer  "patient_adaptive_equipment_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "patient_adaptive_equipment_masters", force: :cascade do |t|
    t.string   "adaptive_equipment"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "patient_adaptive_equipments", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.integer  "created_by"
    t.integer  "patient_adaptive_equipment_master_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "patient_disabilities", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "disability_master_id"
    t.integer  "enrollment_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "patient_rate_masters", force: :cascade do |t|
    t.integer  "from"
    t.integer  "to"
    t.integer  "percent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "patient_routines", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.integer  "created_by"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "routine_master_id"
    t.string   "time"
  end

  create_table "patient_selfcare_abilities", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.integer  "patient_selfcare_ability_master_id"
    t.integer  "created_by"
    t.boolean  "independent"
    t.string   "description"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "patient_selfcare_ability_histories", force: :cascade do |t|
    t.integer  "enrollment_history_id"
    t.integer  "patient_selfcare_ability_master_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.boolean  "independent"
  end

  create_table "patient_selfcare_ability_masters", force: :cascade do |t|
    t.string   "selfcare_ability"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "patients", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.date     "dob"
    t.string   "gender"
    t.string   "mobile_no"
    t.string   "land_line_no"
    t.string   "alternate_contact_no"
    t.string   "bmi"
    t.integer  "guardian_id"
    t.integer  "address_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_id"
    t.integer  "age"
    t.integer  "city_master_id"
  end

  create_table "payment_bills", force: :cascade do |t|
    t.integer  "payment_master_id"
    t.integer  "amount_paid"
    t.string   "payment_mode"
    t.string   "description_1"
    t.string   "description_2"
    t.integer  "payment_done_by"
    t.date     "payment_date"
    t.string   "status"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "payment_masters", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.integer  "amount_in_rupees"
    t.date     "period_from"
    t.date     "period_to"
    t.string   "status"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "discount"
    t.string   "discount_type"
    t.string   "invoice_no"
    t.date     "invoice_generation_date"
    t.integer  "invoice_generated_by"
    t.string   "description"
    t.date     "due_date"
    t.integer  "rate_per_day"
    t.integer  "job_run_id"
    t.boolean  "mail_sent",               default: false
    t.boolean  "nullified",               default: false
  end

  create_table "pcp_services", force: :cascade do |t|
    t.integer  "care_to_be_provided_plan_id"
    t.integer  "sevice_id"
    t.integer  "sub_service_master_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "service_master_id"
    t.integer  "enrollment_id"
  end

  create_table "requested_service_histories", force: :cascade do |t|
    t.integer "enquiry_history_id"
    t.integer "service_master_id"
  end

  create_table "requested_services", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "enquiry_id"
    t.integer  "service_master_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "requested_time_slot_histories", force: :cascade do |t|
    t.integer  "enquiry_history_id"
    t.datetime "time_from"
    t.datetime "time_to"
    t.datetime "booked_at"
    t.integer  "care_giver_master_id"
  end

  create_table "requested_time_slots", force: :cascade do |t|
    t.datetime "time_from"
    t.datetime "time_to"
    t.integer  "enquiry_id"
    t.integer  "patient_id"
    t.integer  "care_giver_master_id"
    t.datetime "booked_at"
    t.string   "status"
  end

  create_table "role_menu_mappings", force: :cascade do |t|
    t.integer  "role_id"
    t.integer  "menu_bar_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "code"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "city_master_id"
    t.boolean  "missed_call"
  end

  create_table "route_masters", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "routine_masters", force: :cascade do |t|
    t.string   "name"
    t.integer  "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_masters", force: :cascade do |t|
    t.string   "service"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "sub_service_masters", force: :cascade do |t|
    t.string   "sub_service"
    t.integer  "service_master_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "template_texts", force: :cascade do |t|
    t.string   "text1"
    t.string   "text2"
    t.string   "text3"
    t.string   "text4"
    t.string   "text5"
    t.string   "text6"
    t.string   "sms1"
    t.string   "sms2"
    t.string   "sms3"
    t.string   "sms4"
    t.string   "sms5"
    t.string   "sms6"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ticket_category_masters", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "category_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "ticket_conversations", force: :cascade do |t|
    t.text     "description"
    t.integer  "ticket_id"
    t.integer  "entered_by"
    t.datetime "entered_at"
    t.string   "tag"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.string   "role"
    t.integer  "patient_id"
    t.integer  "care_giver_master_id"
    t.text     "description"
    t.string   "raised_by"
    t.integer  "ticket_category_master_id"
    t.string   "status"
    t.date     "raised_date"
    t.date     "opened_date"
    t.date     "closed_date"
    t.string   "current_tag"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "time_of_administrations", force: :cascade do |t|
    t.integer  "drug_quantity_master_id"
    t.datetime "time"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "track_vitals", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.integer  "vital_trend_id"
    t.date     "entered_at"
    t.string   "value"
    t.string   "bp1"
    t.string   "bp2"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "transport_charge_masters", force: :cascade do |t|
    t.integer  "from_distance"
    t.integer  "to_distance"
    t.integer  "charge"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "unit_masters", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "mobile_no"
    t.string   "role"
    t.string   "authentication_token"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "user_name"
    t.integer  "role_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["user_name"], name: "index_users_on_user_name", unique: true, using: :btree

  create_table "vital_trends", force: :cascade do |t|
    t.integer  "key_vitals_master_id"
    t.string   "value"
    t.string   "date"
    t.integer  "enrollment_id"
    t.integer  "created_by"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "upper_limit"
    t.string   "lower_limit"
    t.string   "note"
  end

end
