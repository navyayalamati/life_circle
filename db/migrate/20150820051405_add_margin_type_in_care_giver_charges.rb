class AddMarginTypeInCareGiverCharges < ActiveRecord::Migration
  def change
    add_column :care_giver_charges, :margin_type, :string
  end
end
