class CreatePatientSelfcareAbilities < ActiveRecord::Migration
  def change
    create_table :patient_selfcare_abilities do |t|
      t.integer :enrollment_id
      t.integer :patient_selfcare_ability_masters_id
      t.integer :created_by
      t.boolean :independent
      t.string :description
      t.timestamps null: false
    end
  end
end
