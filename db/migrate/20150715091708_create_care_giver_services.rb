class CreateCareGiverServices < ActiveRecord::Migration
  def change
    create_table :care_giver_services do |t|
      t.integer :care_giver_master_id
      t.integer :service_master_id

      t.timestamps null: false
    end
  end
end
