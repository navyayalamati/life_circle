class AddIdProofTypeAndAddressProofTypeToCareGiverMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :id_proof_type, :string
    add_column :care_giver_masters, :address_proof_type, :string
  end
end
