class RemoveDebitAmountColumnsFromPaymentMastersAndAddDescriptionField < ActiveRecord::Migration
  def change
    remove_column :payment_masters, :prev_month_debit_amount, :integer
    remove_column :payment_masters, :prev_month_debit_comments, :string
    remove_column :payment_masters, :credit_note_amount, :integer
    add_column :payment_masters, :decription, :string
    add_column :payment_masters, :due_date, :date
  end
end
