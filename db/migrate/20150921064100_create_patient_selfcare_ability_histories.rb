class CreatePatientSelfcareAbilityHistories < ActiveRecord::Migration
  def change
    create_table :patient_selfcare_ability_histories do |t|
      t.integer :enrollment_history_id
      t.integer :patient_selfcare_ability_master_id
      t.timestamps null: false
    end
  end
end
