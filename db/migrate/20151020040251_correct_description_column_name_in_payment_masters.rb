class CorrectDescriptionColumnNameInPaymentMasters < ActiveRecord::Migration
  def change
    rename_column :payment_masters, :decription, :description
  end
end
