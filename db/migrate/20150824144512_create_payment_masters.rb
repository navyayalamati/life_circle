class CreatePaymentMasters < ActiveRecord::Migration
  def change
    create_table :payment_masters do |t|
      t.integer :enrollment_id
      t.string :payment_mode
      t.integer :amount_in_rupees
      t.date :period_from
      t.date :period_to
      t.string :description_1
      t.string :description_2
      t.string :status
      t.integer :total_amount_in_rupees

      t.timestamps null: false
    end
  end
end
