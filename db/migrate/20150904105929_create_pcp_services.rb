class CreatePcpServices < ActiveRecord::Migration
  def change
    create_table :pcp_services do |t|
      t.integer :care_to_be_provided_plan_id
      t.integer :sevice_id
      t.integer :sub_service_id
      t.timestamps null: false
    end
  end
end
