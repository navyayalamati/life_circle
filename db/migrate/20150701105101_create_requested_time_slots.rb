class CreateRequestedTimeSlots < ActiveRecord::Migration
  def change
    create_table :requested_time_slots do |t|
      t.datetime :from_time
      t.datetime :to_time
      t.integer :enquiry_id
      t.integer :patient_id
    end
  end
end
