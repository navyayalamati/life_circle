class AddEnquiryIdToCareGiverDailyAttendances < ActiveRecord::Migration
  def change
    add_column :care_giver_daily_attendances, :enquiry_id, :integer
  end
end
