class CreateApplicationDefaults < ActiveRecord::Migration
  def change
    create_table :application_defaults do |t|
      t.string :description
      t.string :value

      t.timestamps null: false
    end
  end
end
