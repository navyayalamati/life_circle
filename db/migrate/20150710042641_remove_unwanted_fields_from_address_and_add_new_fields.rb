class RemoveUnwantedFieldsFromAddressAndAddNewFields < ActiveRecord::Migration
  def change
    remove_column :addresses, :house_no
    remove_column :addresses, :apartment_name
    remove_column :addresses, :lane_name
    remove_column :addresses, :plot_no
    remove_column :addresses, :street
    add_column :addresses, :line1, :string
    add_column :addresses, :line2, :string
  end
end
