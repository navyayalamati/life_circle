class AddAppointmentTimeToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :appointment_time, :datetime
    add_column :enquiries, :status, :string
  end
end
