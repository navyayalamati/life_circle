class AddAvailableToCareGiverAvailabilityMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_availability_masters, :availability, :boolean, :default => true
  end
end
