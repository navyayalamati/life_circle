class AddJobRunIdToPaymentMasters < ActiveRecord::Migration
  def change
    add_column :payment_masters, :job_run_id, :integer
  end
end
