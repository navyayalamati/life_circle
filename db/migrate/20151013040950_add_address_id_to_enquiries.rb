class AddAddressIdToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :address_id, :integer
  end
end
