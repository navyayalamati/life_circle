class AddPaymentFieldsToEnrollment < ActiveRecord::Migration
  def change
    add_column :enrollments, :enrollment_fee, :integer
    add_column :enrollments, :caregiver_charge, :integer
    add_column :enrollments, :discount, :integer
    add_column :enrollments, :discount_type, :string
    add_column :enrollments, :status, :string
    add_column :enrollments, :application_no, :string
  end
end
