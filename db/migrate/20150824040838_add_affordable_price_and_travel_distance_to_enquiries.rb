class AddAffordablePriceAndTravelDistanceToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :affordable_price, :integer
    add_column :enquiries, :travel_distance, :integer
    remove_column :enquiries, :long_term, :boolean
  end
end
