class CreateReasonForComings < ActiveRecord::Migration
  def change
    create_table :reason_for_comings do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
