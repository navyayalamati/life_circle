class AddPermanentAddressIdToCareGiverMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :permanent_address_id, :integer
  end
end
