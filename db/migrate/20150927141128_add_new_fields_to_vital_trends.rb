class AddNewFieldsToVitalTrends < ActiveRecord::Migration
  def change
    add_column :vital_trends, :upper_limit, :string
    add_column :vital_trends, :lower_limit, :string
    add_column :vital_trends, :note, :string
  end
end
