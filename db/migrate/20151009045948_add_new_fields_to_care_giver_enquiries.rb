class AddNewFieldsToCareGiverEnquiries < ActiveRecord::Migration
  def change
    add_column :care_giver_enquiries, :highest_qualification, :string
    add_column :care_giver_enquiries, :closed_by, :integer
    add_column :care_giver_enquiries, :closed_at, :date
  end
end
