class AddCreatedByToEnrollmentHistories < ActiveRecord::Migration
  def change
     add_column :enrollment_histories, :created_by, :integer
  end
end
