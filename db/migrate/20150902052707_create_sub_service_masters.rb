class CreateSubServiceMasters < ActiveRecord::Migration
  def change
    create_table :sub_service_masters do |t|
      t.string :sub_service
      t.integer :service_master_id
      t.timestamps null: false
    end
  end
end
