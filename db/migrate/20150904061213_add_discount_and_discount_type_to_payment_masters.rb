class AddDiscountAndDiscountTypeToPaymentMasters < ActiveRecord::Migration
  def change
    add_column :payment_masters, :discount, :integer
    add_column :payment_masters, :discount_type, :string
    add_column :payment_masters, :prev_month_debit_amount, :integer
    add_column :payment_masters, :prev_month_debit_comments, :string
    add_column :payment_masters, :credit_note_amount, :integer
    add_column :payment_masters, :invoice_no, :string
    add_column :payment_masters, :invoice_generation_date, :date
  end
end
