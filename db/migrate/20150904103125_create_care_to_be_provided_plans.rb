class CreateCareToBeProvidedPlans < ActiveRecord::Migration
  def change
    create_table :care_to_be_provided_plans do |t|
      t.string :care_time
      t.string :comments
      t.integer :enrollment_id
      t.integer :created_by
      t.timestamps null: false
    end
  end
end
