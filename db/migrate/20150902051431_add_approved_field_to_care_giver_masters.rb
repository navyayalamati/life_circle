class AddApprovedFieldToCareGiverMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :approved, :boolean
  end
end
