class AddTagFieldsToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :current_tag, :string
    add_column :missed_call_requests, :current_tag, :string
    add_column :enquiry_conversations, :tag, :string
  end
end
