class CreateCreditNotes < ActiveRecord::Migration
  def change
    create_table :credit_notes do |t|
      t.integer :payment_master_id
      t.text :reason_for_creadit_note
      t.integer :days_of_service_provided
      t.integer :days_of_service_invoiced
      t.integer :daily_service_rate
      t.string :value_details
      t.integer :value_of_credit_note
      t.integer :no_days_service_as_per_client_register
      t.integer :no_days_service_as_per_cg_register
      t.boolean :is_notice_period_amount_adjusted
      t.boolean :is_payment_made_on_client_or_patient_name
      t.text :authorization_token_from_client

      t.timestamps null: false
    end
  end
end
