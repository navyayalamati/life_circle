class AddAttachementToCareGiverEducations < ActiveRecord::Migration
  def change
    add_column :care_giver_educations, :attachment, :string
    add_column :care_giver_educations, :care_giver_master_id, :integer
  end
end
