class CreateUnitMasters < ActiveRecord::Migration
  def change
    create_table :unit_masters do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
