class CreateTimeOfAdministrations < ActiveRecord::Migration
  def change
    create_table :time_of_administrations do |t|
      t.integer :drug_quantity_master_id
      t.datetime :time
      t.timestamps null: false
    end
  end
end
