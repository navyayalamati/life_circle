class CreatePatientAdaptiveEquipments < ActiveRecord::Migration
  def change
    create_table :patient_adaptive_equipments do |t|
      t.integer :enrollment_id
      t.integer :created_by
      t.integer :patient_adaptive_equipment_master_id
      t.timestamps null: false
    end
  end
end
