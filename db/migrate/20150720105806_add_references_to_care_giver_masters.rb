class AddReferencesToCareGiverMasters < ActiveRecord::Migration
  def change
    add_column :care_giver_masters, :reference_1, :string
    add_column :care_giver_masters, :reference_2, :string
  end
end
