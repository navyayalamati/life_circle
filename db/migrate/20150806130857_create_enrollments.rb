class CreateEnrollments < ActiveRecord::Migration
  def change
    create_table :enrollments do |t|
      t.integer :enquiry_id
      t.integer :patient_id
      t.string :patient_bmi
      t.string :patient_home_setting
      t.boolean :cg_work_alone_with_patient
      t.boolean :suffering_from_infection_disease
      t.boolean :safe_and_well_connected_locality
      t.boolean :provides_personal_protective_equipment
      t.boolean :family_member_helps
      t.integer :report_prepared_by
      t.integer :report_audited_by
      t.timestamps null: false
    end
  end
end
