class RenameSubServiceIdInPcpServices < ActiveRecord::Migration
  def change
    rename_column :pcp_services, :sub_service_id, :sub_service_master_id
  end
end
