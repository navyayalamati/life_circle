class CreateKeyVitalsMasters < ActiveRecord::Migration
  def change
    create_table :key_vitals_masters do |t|
      t.string :name
      t.string :tracking_frequency
      t.timestamps null: false
    end
  end
end
