class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :first_name
      t.string :last_name
      t.date :dob
      t.string :gender
      t.string :mobile_no
      t.string :land_line_no
      t.string :alternate_contact_no
      t.string :bmi
      t.integer :guardian_id
      t.integer :address_id

      t.timestamps null: false
    end
  end
end
