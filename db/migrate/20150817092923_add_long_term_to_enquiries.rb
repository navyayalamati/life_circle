class AddLongTermToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :long_term, :boolean
  end
end
