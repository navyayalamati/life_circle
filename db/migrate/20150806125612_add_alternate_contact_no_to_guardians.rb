class AddAlternateContactNoToGuardians < ActiveRecord::Migration
  def change
    add_column :guardians, :alternate_contact_no, :string
  end
end
