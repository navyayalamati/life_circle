class AddPaymentDateToPaymentMasters < ActiveRecord::Migration
  def change
    add_column :payment_masters, :payment_date, :date
    add_column :payment_masters, :invoice_generated_by, :integer
    add_column :payment_masters, :payment_done_by, :integer
  end
end
