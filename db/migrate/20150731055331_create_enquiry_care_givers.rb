class CreateEnquiryCareGivers < ActiveRecord::Migration
  def change
    create_table :enquiry_care_givers do |t|
      t.integer :enquiry_id
      t.integer :care_giver_master_id

      t.timestamps null: false
    end
  end
end
