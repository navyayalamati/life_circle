class CreateCareGiverReturnEquipments < ActiveRecord::Migration
  def change
    create_table :care_giver_return_equipments do |t|
      t.integer :care_giver_master_id
      t.integer :care_giver_equipment_id

      t.timestamps null: false
    end
  end
end
