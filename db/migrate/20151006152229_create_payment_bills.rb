class CreatePaymentBills < ActiveRecord::Migration
  def change
    create_table :payment_bills do |t|
      t.integer :payment_master_id
      t.integer :amount_paid
      t.string :payment_mode
      t.string :description_1
      t.string :description_2
      t.integer :payment_done_by
      t.date :payment_date
      t.string :status

      t.timestamps null: false
    end
  end
end
