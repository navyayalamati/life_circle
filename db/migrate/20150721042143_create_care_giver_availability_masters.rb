class CreateCareGiverAvailabilityMasters < ActiveRecord::Migration
  def change
    create_table :care_giver_availability_masters do |t|
      t.integer :care_giver_master_id
      t.string :day
      t.datetime :time_from
      t.datetime :time_to

      t.timestamps null: false
    end
  end
end
