class CreateDisablitiesMasters < ActiveRecord::Migration
  def change
    create_table :disability_masters do |t|
      t.string :disability
      t.string :description

      t.timestamps null: false
    end
  end
end
