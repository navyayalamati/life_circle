class AddCareGiverAndBookingTimeToTimeSlots < ActiveRecord::Migration
  def change
    add_column :requested_time_slots, :care_giver_master_id, :integer
    add_column :requested_time_slots, :booked_at, :datetime
    add_column :requested_time_slots, :status, :string
  end
end
