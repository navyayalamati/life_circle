class CreateLanguageMasters < ActiveRecord::Migration
  def change
    create_table :language_masters do |t|
      t.string :language

      t.timestamps null: false
    end
  end
end
