class CreateCareGiverEducations < ActiveRecord::Migration
  def change
    create_table :care_giver_educations do |t|
      t.string :institute
      t.date :period_from
      t.date :period_to
      t.string :exam_passed
      t.float :marks_percentage
      t.boolean :highest_qualification
      t.boolean :professional_nursing_qualification
    end
  end
end
