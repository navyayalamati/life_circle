class CreateCareGiverCharges < ActiveRecord::Migration
  def change
    create_table :care_giver_charges do |t|
      t.integer :care_giver_master_id
      t.integer :base_charge
      t.float :margin_in_prcentage
      t.integer :modified_by

      t.timestamps null: false
    end
  end
end
