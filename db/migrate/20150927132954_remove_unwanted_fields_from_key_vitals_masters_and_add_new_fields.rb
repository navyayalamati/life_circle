class RemoveUnwantedFieldsFromKeyVitalsMastersAndAddNewFields < ActiveRecord::Migration
  def change
    remove_column :key_vitals_masters, :tracking_frequency
    
        
    add_column :key_vitals_masters, :unit, :string
    add_column :key_vitals_masters, :upper_limit, :string
    add_column :key_vitals_masters, :lower_limit, :string
  end
end
