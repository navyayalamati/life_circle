class CreatePatientAdaptiveEquipmentMasters < ActiveRecord::Migration
  def change
    create_table :patient_adaptive_equipment_masters do |t|
      t.string :adaptive_equipment
      t.timestamps null: false
    end
  end
end
