class RenameMarginInPercentageInCareGiverCharges < ActiveRecord::Migration
  def change
    rename_column :care_giver_charges, :margin_in_prcentage, :margin
  end
end
