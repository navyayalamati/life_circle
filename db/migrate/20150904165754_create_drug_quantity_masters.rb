class CreateDrugQuantityMasters < ActiveRecord::Migration
  def change
    create_table :drug_quantity_masters do |t|
      t.integer :quantity
      t.integer :drug_masters_id
      t.integer :enrollment_id
      t.integer :created_by      
      t.timestamps null: false
    end
  end
end
