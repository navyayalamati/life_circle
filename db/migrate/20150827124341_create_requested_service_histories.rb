class CreateRequestedServiceHistories < ActiveRecord::Migration
  def change
    create_table :requested_service_histories do |t|
      t.integer :enquiry_history_id
      t.integer :service_master_id
    end
  end
end
