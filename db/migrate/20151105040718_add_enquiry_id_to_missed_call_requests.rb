class AddEnquiryIdToMissedCallRequests < ActiveRecord::Migration
  def change
    add_column :missed_call_requests, :enquiry_id, :integer
  end
end
