class AddAdditinalFieldsToCareGiverEmployers < ActiveRecord::Migration
  def change
    add_column :care_giver_employers, :care_giver_master_id, :integer
    add_column :care_giver_employers, :attachment, :string
    add_column :care_giver_employers, :verified, :boolean
  end
end
