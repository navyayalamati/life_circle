class AddEmailForOfficialComminicationAndPrimaryContactToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :email_for_official_comminication, :string
    add_column :enrollments, :primary_contact, :string
  end
end
