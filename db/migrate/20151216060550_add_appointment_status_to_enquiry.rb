class AddAppointmentStatusToEnquiry < ActiveRecord::Migration
  def change
    add_column :enquiries, :appointment_status, :string
  end
end
