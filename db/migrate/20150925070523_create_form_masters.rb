class CreateFormMasters < ActiveRecord::Migration
  def change
    create_table :form_masters do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
