class CreatePendingBillAfterPayments < ActiveRecord::Migration
  def change
    create_table :pending_bill_after_payments do |t|
      t.integer :payment_master_id
      t.integer :enrollment_id
      t.integer :amount
      t.string :status

      t.timestamps null: false
    end
  end
end
