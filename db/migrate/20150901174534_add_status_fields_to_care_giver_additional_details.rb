class AddStatusFieldsToCareGiverAdditionalDetails < ActiveRecord::Migration
  def change
    add_column :care_giver_additional_details, :physical_description_approved, :boolean, :default => false
    add_column :care_giver_additional_details, :physical_description_approved_by, :integer
    add_column :care_giver_additional_details, :health_approved, :boolean, :default => false
    add_column :care_giver_additional_details, :health_approved_by, :integer
  end
end
