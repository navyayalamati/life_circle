class CreateTrackVitals < ActiveRecord::Migration
  def change
    create_table :track_vitals do |t|
      t.integer :enrollment_id
      t.integer :vital_trend_id
      t.date :entered_at
      t.string :value
      t.string :bp1
      t.string :bp2
      t.timestamps null: false
    end
  end
end
