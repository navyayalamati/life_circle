class CreateCareGiverAdditionalDetails < ActiveRecord::Migration
  def change
    create_table :care_giver_additional_details do |t|
      t.integer :care_giver_master_id
      t.integer :height_in_cms
      t.string :hair
      t.string :eyes_color
      t.string :complexion
      t.string :identification_tatoo
      t.string :any_deoformity
      t.string :blood_group
      t.string :known_allergy
      t.string :current_health_problem
      t.string :past_health_problem
      t.boolean :smoking_habit
      t.boolean :chewing_tobacco
      t.boolean :consume_liquor
      t.boolean :convicted_any_court_of_law
      t.boolean :civil_or_criminal_proceeding_in_court_of_law
    end
  end
end
