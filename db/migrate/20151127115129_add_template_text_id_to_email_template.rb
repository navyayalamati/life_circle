class AddTemplateTextIdToEmailTemplate < ActiveRecord::Migration
  def change
    add_column :email_templates, :template_text_id, :integer
  end
end
