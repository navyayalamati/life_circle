class AddCaregiverChargeToEnquiryCareGivers < ActiveRecord::Migration
  def change
    add_column :enquiry_care_givers, :caregiver_charge, :integer
    add_column :enquiry_care_givers, :transport_charge, :integer
    add_column :enquiry_care_givers, :approx_travel_distance, :integer
  end
end
