class RenameTimeSlotsInRequestedTimeSlots < ActiveRecord::Migration
  def change
    rename_column :requested_time_slots, :from_time, :time_from
    rename_column :requested_time_slots, :to_time, :time_to
  end
end
