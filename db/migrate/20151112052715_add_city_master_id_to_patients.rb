class AddCityMasterIdToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :city_master_id, :integer
  end
end
