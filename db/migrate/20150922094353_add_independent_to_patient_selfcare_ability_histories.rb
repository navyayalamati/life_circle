class AddIndependentToPatientSelfcareAbilityHistories < ActiveRecord::Migration
  def change
  add_column :patient_selfcare_ability_histories, :independent, :boolean  
  end
end
