class CreateLifeCircleAgents < ActiveRecord::Migration
  def change
    create_table :life_circle_agents do |t|
      t.string :name
      t.string :mobile_no
      t.integer :user_id
      t.integer :address_id

      t.timestamps null: false
    end
  end
end
