class CreatePatientRateMasters < ActiveRecord::Migration
  def change
    create_table :patient_rate_masters do |t|
      t.integer :from
      t.integer :to
      t.integer :percent
      t.timestamps null: false
    end
  end
end
