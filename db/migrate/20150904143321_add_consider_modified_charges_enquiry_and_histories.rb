class AddConsiderModifiedChargesEnquiryAndHistories < ActiveRecord::Migration
  def change
    add_column :enquiries, :consider_modified_charge, :boolean, :default => false
    add_column :enquiry_histories, :consider_modified_charge, :boolean, :default => false
  end
end
