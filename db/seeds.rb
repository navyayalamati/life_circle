def seed_lc_agent
  l_c_agent = User.find_by_role(User::L_C_AGENT)
  unless l_c_agent.present?
    user = User.create(:user_name => "lc_agent", :email => "l_c_agent@gmail.com" , :mobile_no => "9951442002",:password => "welcome123" , :role => User::L_C_AGENT)
    LifeCircleAgent.create(:name => "lcagent", :mobile_no => "123456", :user_id => user.id)
  end
end

def seed_disablities
  DisabilityMaster.where(disability: "Arthritis").first_or_create({disability: "Arthritis"})
  DisabilityMaster.where(disability: "Balance Problem/Fallsrisk").first_or_create({disability: "Balance Problem/Fallsrisk"})
  DisabilityMaster.where(disability: "Bed ridden").first_or_create({disability: "Bed ridden"})
  DisabilityMaster.where(disability: "Blind/Visually challenged").first_or_create({disability: "Blind/Visually challenged"})
  DisabilityMaster.where(disability: "Cancer").first_or_create({disability: "Cancer"})
  DisabilityMaster.where(disability: "Colostomy").first_or_create({disability: "Colostomy"})
  DisabilityMaster.where(disability: "Comatose").first_or_create({disability: "Comatose"})
  DisabilityMaster.where(disability: "Dementia").first_or_create({disability: "Dementia"})
  DisabilityMaster.where(disability: "Diabetes").first_or_create({disability: "Diabetes"})
  DisabilityMaster.where(disability: "Fainting (Syncope)").first_or_create({disability: "Fainting (Syncope)"})
  DisabilityMaster.where(disability: "Feeding through NG tube").first_or_create({disability: "Feeding through NG tube"})
  DisabilityMaster.where(disability: "Foot ulcer/problem").first_or_create({disability: "Foot ulcer/problem"})
  DisabilityMaster.where(disability: "Fracture").first_or_create({disability: "Fracture"})
  DisabilityMaster.where(disability: "Functional decline (getting on and off bed)").first_or_create({disability: "Functional decline (getting on and off bed)"})
  DisabilityMaster.where(disability: "Hearing defect").first_or_create({disability: "Hearing defect"})
  DisabilityMaster.where(disability: "Hypertensive").first_or_create({disability: "Hypertensive"})
  DisabilityMaster.where(disability: "Incontinence faecal").first_or_create({disability: "Incontinence faecal"})
  DisabilityMaster.where(disability: "Incontinence Urinary").first_or_create({disability: "Incontinence Urinary"})
  DisabilityMaster.where(disability: "Nutritional IV feeding").first_or_create({disability: "Nutritional IV feeding"})
  DisabilityMaster.where(disability: "Parkinson's (movement disorder)").first_or_create({disability: "Parkinson's (movement disorder)"})
  DisabilityMaster.where(disability: "Post-Operative").first_or_create({disability: "Post-Operative"})
  DisabilityMaster.where(disability: "Pressure Ulcers").first_or_create({disability: "Pressure Ulcers"})
  DisabilityMaster.where(disability: "Stroke").first_or_create({disability: "Stroke"})
  DisabilityMaster.where(disability: "Under Dialysis").first_or_create({disability: "Under Dialysis"})
  DisabilityMaster.where(disability: "Urinary Catheter").first_or_create({disability: "Urinary Catheter"})
end

def seed_services
  ServiceMaster.where(service: "Assisted feeding").first_or_create({service: "Assisted feeding"})
  ServiceMaster.where(service: "Bed bath/Sponge bath").first_or_create({service: "Bed bath/Sponge bath"})
  ServiceMaster.where(service: "Blood sugar check").first_or_create({service: "Blood sugar check"})
  ServiceMaster.where(service: "Colostomy care").first_or_create({service: "Colostomy care"})
  ServiceMaster.where(service: "Dialysis care").first_or_create({service: "Dialysis care"})
  ServiceMaster.where(service: "Eye care").first_or_create({service: "Eye care"})
  ServiceMaster.where(service: "Falls prevention").first_or_create({service: "Falls prevention"})
  ServiceMaster.where(service: "Injection").first_or_create({service: "Injection"})
  ServiceMaster.where(service: "Insulin administration").first_or_create({service: "Insulin administration"})
  ServiceMaster.where(service: "IV cannula care").first_or_create({service: "IV cannula care"})
  ServiceMaster.where(service: "Medication").first_or_create({service: "Medication"})
  ServiceMaster.where(service: "NG feeding").first_or_create({service: "NG feeding"})
  ServiceMaster.where(service: "Oral care/Dental care").first_or_create({service: "Oral care/Dental care"})
  ServiceMaster.where(service: "Oxygen administration").first_or_create({service: "Oxygen administration"})
  ServiceMaster.where(service: "Patient lifting").first_or_create({service: "Patient lifting"})
  ServiceMaster.where(service: "Pressure ulcer care").first_or_create({service: "Pressure ulcer care"})
  ServiceMaster.where(service: "Suctioning").first_or_create({service: "Suctioning"})
  ServiceMaster.where(service: "Tracheostomy care").first_or_create({service: "Tracheostomy care"})
  ServiceMaster.where(service: "Urinary Catheter care").first_or_create({service: "Urinary Catheter care"})
  ServiceMaster.where(service: "Vitals checking").first_or_create({service: "Vitals checking"})
  ServiceMaster.where(service: "Walking assistance").first_or_create({service: "Walking assistance"})
  ServiceMaster.where(service: "Wound dressings").first_or_create({service: "Wound dressings"})
end

def seed_enquiry_references
  EnquiryReferenceMaster.where(reference: "Website").first_or_create({reference: "Website"})
  EnquiryReferenceMaster.where(reference: "Google Ad").first_or_create({reference: "Google Ad"})
  EnquiryReferenceMaster.where(reference: "Existing Client").first_or_create({reference: "Existing Client"})
  EnquiryReferenceMaster.where(reference: "Life Circle Employee").first_or_create({reference: "Life Circle Employee"})
  EnquiryReferenceMaster.where(reference: "Just dial").first_or_create({reference: "Just dial"})
  EnquiryReferenceMaster.where(reference: "Newspaper Insert").first_or_create({reference: "Newspaper Insert"})
  EnquiryReferenceMaster.where(reference: "Hospital").first_or_create({reference: "Hospital"})
  EnquiryReferenceMaster.where(reference: "Caregiver").first_or_create({reference: "Caregiver"})
end

def seed_lost_reasons
  LostReasonMaster.where(reason: "Phone Non Responsive").first_or_create({reason: "Phone Non Responsive"})
  LostReasonMaster.where(reason: "Wants lower price").first_or_create({reason: "Wants lower price"})
  LostReasonMaster.where(reason: "Wants male Caregiver").first_or_create({reason: "Wants male Caregiver"})
  LostReasonMaster.where(reason: "Found alternate provider").first_or_create({reason: "Found alternate provider"})
  LostReasonMaster.where(reason: "Non Home Health Enquiry").first_or_create({reason: "Non Home Health Enquiry"})
  LostReasonMaster.where(reason: "Outside Service area").first_or_create({reason: "Outside Service area"})
  LostReasonMaster.where(reason: "Prospect non responsive").first_or_create({reason: "Prospect non responsive"})
end

def seed_languages
  LanguageMaster.where(language: "English").first_or_create({language: "English"})
  LanguageMaster.where(language: "Hindi").first_or_create({language: "Hindi"})
  LanguageMaster.where(language: "Telugu").first_or_create({language: "Telugu"})
end


def seed_templates
  unless TemplateText.first.present?
    TemplateText.create(:id => 1 , :text1 => "Hai Patient Name", :text2  => "Caregiver Details" , :text3 => "Caregiver Name" , :text4 => "Caregiver Charges" , :text5 => "Start date and End date" , :sms1 => "Hai Patient Name", :sms2  => "Caregiver Details" , :sms3 => "Caregiver Name" , :sms4 => "Caregiver Charges" , :sms5 => "Start date and End date"  )
    TemplateText.create(:id => 2 , :text1 => "Hai Patient Name",  :sms1 => "Hai Patient Name" )
    TemplateText.create(:id => 3 , :text1 => "Hai Patient Name", :text2  => "Login id" , :text3 => "Password" , :text4 => "Download App Link" , :text5 => "Start date and End date" , :sms1 => "Hai Patient Name", :sms2  => "Login id" , :sms3 => "Password" , :sms4 => "Download App Link" , :sms5 => "Start date and End date"  )
    TemplateText.create(:id => 4 , :text1 => "Hai Caregiver Name", :text2  => "Login id" , :text3 => "Password" , :text4 => "Download App Link" , :sms1 => "Hai Caregiver Name", :sms2  => "Login id" , :sms3 => "Password" , :sms4 => "Download App Link"  )
    TemplateText.create(:id => 5 , :text1 => "Hai Patient Name", :text2  => "Attachments" , :text3 => "PCP")
    TemplateText.create(:id => 6 , :sms2 => "Caregiver Name", :sms3  => "Time In" )
    TemplateText.create(:id => 7 , :sms2 => "Caregiver Name", :sms3  => "Time In" )
    TemplateText.create(:id => 8 , :text1 => "Hai Patient Name", :text2  => "Medicine Name" , :text3 => "Available Stock" , :sms1 => "Hai Patient Name", :sms2  => "Medicine Name" , :sms3 => "Available Stock"   )
    TemplateText.create(:id => 9 , :text1 => "Hai Caregiver Name", :text2  => "Medicine Name" , :text3 => "Available Stock" , :sms1 => "Hai Caregiver Name", :sms2  => "Medicine Name" , :sms3 => "Available Stock"   )
    TemplateText.create(:id => 10 ,  :text2  => "Patient Name" , :text3 => "Email" , :text4 => "Mobile No" ,  :sms2  => "Patient Name" , :sms3 => "Email"  , :sms4 => "Mobile No"  )
    TemplateText.create(:id => 11 ,  :text1  => "Dear Patient Name" , :sms1  => "Dear Patient Name" )
    TemplateText.create(:id => 12 ,  :text1  => "Hai Patient/CG Name" , :text2 => "Category" , :text3 => "Raised Date" ,  :sms1  => "Patient/CG Name" , :sms2 => "Category"  , :sms3 => "Raised Date"  )
    TemplateText.create(:id => 13 ,  :text2  => "Patient/CG Name" , :text3 => "Patient/CG Role" , :text4 => "Category" ,:text5 => "Raised Date" ,  :sms2  => "Patient/CG Name" , :sms3 => "Patient/CG Role",:sms4 => "Category"  , :sms5 => "Raised Date"  )
    TemplateText.create(:id => 14 ,  :text1  => "Patient Name" , :text2 => "Start Date" , :text3 => "End Date" , :text4 => "Reason", :sms1  => "Patient Name" , :sms2 => "Start Date"  , :sms3 => "End Date" , :sms4 => "Reason"  )
    TemplateText.create(:id => 15 ,  :text2  => "Patient Name" , :text3 => "Address" ,  :text4 => "Emergency Mobile#", :sms2  => "Patient Name" , :sms3 => "Address" , :sms4 => "Emergency Mobile#"  )
    TemplateText.create(:id => 16 ,  :text1  => "Patient Name" ,   :text2 => "CG Mobile#", :sms1  => "Patient Name" , :sms2 => "CG Mobile#"  )
    TemplateText.create(:id => 17 ,  :text2  => "Patient Name" , :text3 => "Address" ,  :text4 => "Emergency Mobile#",  :sms2  => "Patient Name" , :sms3 => "Address" , :sms4 => "Emergency Mobile#" )
    TemplateText.create(:id => 18 ,  :text2  => "CG Name" , :text3 => "CG Mobile#" ,  :text4 => "Patient Name", :text5 => "Patient Address", :sms2  => "CG Name" , :sms3 => "CG Mobile#" , :sms4 => "Patient Name" , :sms5 => "Patient Address"  )
    TemplateText.create(:id => 19 ,  :text1  => "Hai Patient Name" ,  :text2 => "CG Name", :text3 => "CG Mobile#", :sms1  => "Patient Name" , :sms2 => "CG Name",:sms3 => "CG Mobile#" )
    TemplateText.create(:id => 20 ,  :text1  => "Hai CG Name" ,  :text2 => "Patient Name", :text3 => "Patient Mobile#", :sms1  => "CG Name" , :sms2 => "Patient Name",:sms3 => "Patient Mobile#"  )
    TemplateText.create(:id => 21 ,  :text1  => "Hai CG/admin Name" ,  :text2 => "Patient Name" ,:text3 => "Vital Description", :text4 => "Vital Value", :text5 => "Vital Exp Value", :sms1  => "Hai CG/admin Name" , :sms2 => "Patient Name",:sms3 => "Vital Description" , :sms4 => "Vital Value", :sms5 => "Vital Exp Value"  )
    TemplateText.create(:id => 22 ,  :text2 => "Vital Description", :text3 => "Vital Value", :text4 => "Vital Exp Value", :sms2 => "Vital Description" , :sms3 => "Vital Value", :sms4 => "Vital Exp Value"  )
    TemplateText.create(:id => 23 ,   :sms1 => "Hai CG Name" , :sms2 => "Current Status", :sms3 => "Date"  )
    TemplateText.create(:id => 24 ,   :sms1 => "Applicant Name"  )
    TemplateText.create(:id => 25 ,   :sms1 => "Hai Patient Name" , :sms2 => "Appointment Date", :sms3 => "Appointment Time"  )
    TemplateText.create(:id => 26 ,   :sms1 => "Hai Patient Name" , :sms2 => "CG Name", :sms3 => "Start Date" , :sms4 => "End Date" )
    TemplateText.create(:id => 27 ,   :sms1 => "Hai CG Name" , :sms2 => "Patient Name", :sms3 => "Start Date" , :sms4 => "End Date" )
    TemplateText.create(:id => 28 ,  :text1  => "Hai Patient Name" ,  :text2 => "Invoice Date" ,:text3 => "Amount", :text4 => "Due Date", :text5 => "Attachment of Invoice", :sms1  => "Hai Patient Name" , :sms2 => "Invoice Date",:sms3 => "Amount" , :sms4 => "Due Date" )
    TemplateText.create(:id => 29 ,  :text2 => "Patient Name", :text3 => "Invoice#", :text4 => "Invoice Date", :text5 => "Amount", :text6 => "Received Amount",:sms2 => "Patient Name" , :sms3 => "Invoice#", :sms4 => "Invoice Date", :sms5 => "Amount" ,:sms6 => "Received Amount")
    TemplateText.create(:id => 30 ,  :text1 => "Hai Patient Name", :text2 => "Invoice#", :text3 => "Invoice Date", :text4 => "Amount" ,:text5 => "Received Amount",:sms1 => "Hai Patient Name" , :sms2 => "Invoice#", :sms3 => "Invoice Date", :sms4 => "Amount" ,:sms5 => "Received Amount")
    TemplateText.create(:id => 31 ,  :text1 => "Hai Patient Name", :text2 => "CG Name", :text3 => "From Date", :text4 => "To Date" ,:text5 => "Attachment Of Report")
    
  end
end



def seed_menu
  unless MenuBar.first.present?
    MenuBar.create(:id => 1 , :name => "Enquiry Management", :url => "#", :parent_id => 0 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 2 , :name => "Patient Management", :url => "#", :parent_id => 0 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 3 , :name => "Caregiver Management", :url => "#", :parent_id => 0 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 4 ,:name => "Tickets", :url => "#", :parent_id => 0 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 5 ,:name => "Masters", :url => "#", :parent_id => 0 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 6 , :name => "Create Enquiry", :url => "/enquiry#/city_and_area", :parent_id => 1 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 7 , :name => "Appointments", :url => "/enquiries/appointments", :parent_id => 1 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 8 , :name => "Enquiries", :url => "/enquiries", :parent_id => 1 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 9 , :name => "Prospects", :url => "#", :parent_id => 1 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 10 , :name => "High Potential", :url => "/enquiries?status=Potential-High", :parent_id => 9 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 11 , :name => "Low Potential", :url => "/enquiries?status=Potential-Low", :parent_id => 9 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 12 , :name => "Non-Prospects", :url => "/enquiries?status=Non-Prospect", :parent_id => 1 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 13 , :name => "Audit", :url => "/enquiries?status=To+Audit", :parent_id => 1 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 14 , :name => "Closed", :url => "/enquiries?status=Closed", :parent_id => 1 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 15 , :name => "Enrolled Patients", :url => "/enrollments", :parent_id => 2 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 16 , :name => "Incomplete PCP", :url => "/enrollments/incomplete_pcp", :parent_id => 2 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 17 , :name => "Closed Patients", :url => "/enrollments/closed_form", :parent_id => 2 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 18 , :name => "Empanel", :url => "#", :parent_id => 3 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 19 , :name => "New Walk In ", :url => "/care_giver_enquiries/new", :parent_id => 18 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 20 , :name => "Job Applicant List ", :url => "/care_giver_enquiries", :parent_id => 18 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 21 , :name => "Recruit List ", :url => "/care_giver_enquiries/recruitment", :parent_id => 18 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 22 , :name => "Approval List ", :url => "/care_giver_masters?status=Documents+Collected", :parent_id => 18 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 23 , :name => "Hold List", :url => "/care_giver_enquiries/hold", :parent_id => 18 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 24 , :name => "New City Request", :url => "/care_giver_enquiries/cg_city_request", :parent_id => 18 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 25 , :name => "Reject List", :url => "/care_giver_enquiries/closed", :parent_id => 18 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 26 , :name => "Update Profile", :url => "/care_giver_masters", :parent_id => 3 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 27 , :name => "Active", :url => "/care_giver_masters?status=Active", :parent_id => 26 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 28 , :name => "Inactive", :url => "/care_giver_masters?status=Leave", :parent_id => 26 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 29 , :name => "Working", :url => "/care_giver_masters?status=Working", :parent_id => 26 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 30 , :name => "Terminated", :url => "/care_giver_masters?status=Resign", :parent_id => 26 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 31 , :name => "Time & Attendance", :url => "#", :parent_id => 3 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 32 , :name => "Payroll", :url => "/care_giver_masters/payroll", :parent_id => 31 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 33 , :name => "Attendance", :url => "/care_giver_daily_attendances", :parent_id => 31 , :mark_as_parent => 'NO')
    
    MenuBar.create(:id => 34 , :name => "Patient", :url => "#", :parent_id => 4 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 35 , :name => "New Tickets", :url => "/tickets?current_tag=New&role=Patient", :parent_id => 34 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 36 , :name => "Open Tickets", :url => "/tickets?current_tag=Open&role=Patient", :parent_id => 34 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 37 , :name => "Waiting For Inputs", :url => "/tickets?current_tag=Waiting+for+Inputs&role=Patient", :parent_id => 34 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 38 , :name => "Waiting For Closed", :url => "/tickets?current_tag=Waiting+for+Closed&role=Patient", :parent_id => 34 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 39 , :name => "Closed Tickets", :url => "/tickets?current_tag=Closed&role=Patient", :parent_id => 34 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 40 , :name => "Daily Check", :url => "/daily_checks?role=Patient", :parent_id => 34 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 41 , :name => "Care Giver", :url => "#", :parent_id => 4 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 42 , :name => "New Tickets", :url => "/tickets?current_tag=New&role=Caregiver", :parent_id => 41 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 43 , :name => "Open Tickets", :url => "/tickets?current_tag=Open&role=Caregiver", :parent_id => 41 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 44 , :name => "Waiting For Inputs", :url => "/tickets?current_tag=Waiting+for+Inputs&role=Caregiver", :parent_id => 41 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 45 , :name => "Waiting For Closed", :url => "/tickets?current_tag=Waiting+for+Closed&role=Caregiver", :parent_id => 41 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 46 , :name => "Closed Tickets", :url => "/tickets?current_tag=Closed&role=Caregiver", :parent_id => 41 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 47 , :name => "Daily Check", :url => "/daily_checks?role=Caregiver", :parent_id => 41 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 48, :name => "Dashboard", :url => "/count_enquiry_managements/index", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 49, :name => "Roles", :url => "/role_creations", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 50, :name => "Users", :url => "/user_creations", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 51, :name => "Services", :url => "/service_masters", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 52, :name => "Email Templates", :url => "/email_templates", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 53, :name => "Ticket Category", :url => "/ticket_category_masters", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 54, :name => "City Masters", :url => "/city_masters", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 55, :name => "Languages", :url => "/language_masters", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 56, :name => "PCP Masters", :url => "#", :parent_id => 5 , :mark_as_parent => 'YES')
    MenuBar.create(:id => 57, :name => "Disabilities", :url => "/disability_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 58, :name => "Adaptive Equipments", :url => "/patient_adaptive_equipment_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 59, :name => "Selfcare Abilities", :url => "/patient_selfcare_ability_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 60, :name => "Routines", :url => "/routine_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 61, :name => "Forms", :url => "/form_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 62, :name => "Routes", :url => "/route_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 63, :name => "Units", :url => "/unit_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 64, :name => "Drugs", :url => "/drug_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 65, :name => "Key Vitals", :url => "/key_vitals_masters", :parent_id => 56 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 66, :name => "Charge Buckets", :url => "/care_giver_charge_buckets", :parent_id => 5 , :mark_as_parent => 'NO')
    MenuBar.create(:id => 67 , :name => "Map", :url => "/care_giver_daily_attendances/new", :parent_id => 31 , :mark_as_parent => 'NO')
  end    
  



end


def seed_default_cc_email
  ApplicationDefault.first_or_create!(:description => "default_cc_email", :value => "srikanth@ostrylabs.com")
end

seed_menu
seed_lc_agent
seed_disablities
seed_services
seed_enquiry_references
seed_lost_reasons
seed_languages
seed_templates
seed_default_cc_email
# d1 = Date.strptime("2016-1-1", "%Y-%m-%d")
# p d1
# d2 = Date.strptime("2016-1-20", "%Y-%m-%d")
# p d2
# (d1..d2).each do |date|
#   EnrollmentAttendance.create(:enrollment_id => 5, :login_time => date, :logout_time => DateTime.now, :notes => "Testing")
# end
