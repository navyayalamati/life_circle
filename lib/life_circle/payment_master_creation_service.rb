module LifeCircle
  class PaymentMasterCreationService
    attr_accessor :enrollment, :payment_master, :errors
    def initialize(params, enrollment, user)
      @params = params
      @enrollment = enrollment
      @user = user
      errors = []
    end
    
    def build_payment(generation_date, start_date, end_date, job_run_id = nil)
      payment_calculator = PaymentCalculator.new(@enrollment, generation_date)
      payment_masters = payment_calculator.new_payment(start_date, end_date)
      payment_masters.each do |payment_master|
        payment_master.invoice_generated_by = @user.id
        payment_master.invoice_generation_date = generation_date
        payment_master.invoice_no = generate_invoice_no(generation_date)
        payment_master.job_run_id = job_run_id if job_run_id.present?
        ActiveRecord::Base.transaction do
          payment_master.credit_notes.each do |cr|
            cr.status = "ISSUED"
            cr.payment_master = payment_master
          end
          
          if payment_master.save
            if payment_master.credit_notes.map(&:valid?).all?
            else
              raise ActiveRecord::Rollback
            end
          else
            puts "Payment Not Generetad...!!"
            self.errors.push(payment_master.errors)
          end
        end
      end
      payment_masters
    end
    
    def save
      @payment_master = PaymentMaster.new(payment_master_params)
      @payment_master.status = "COMPLETED"
      @payment_master.invoice_no = generate_invoice_no(Date.today)
      status = false
      ActiveRecord::Base.transaction do
        unless @enrollment.payments_exists?
          return false unless make_new_payment_entries
        end
        status = @payment_master.save
      end
      status
    end

    def save_recurring_payment
      @payment_master = PaymentMaster.new(payment_master_params)
      @payment_master.status = "COMPLETED"
      @payment_master.invoice_no = generate_invoice_no
      status = false
      prev_month_credit_note = CreditNote.new(credit_note_params(@params[:payment_master]))
      @payment_master.prev_payment_credit_note = prev_month_credit_note
      ActiveRecord::Base.transaction do
        if prev_month_credit_note.save
          status = @payment_master.save
        end
      end
      status
    end
    
    def generate_and_return_closing_payment_atributes(close_date)
      attributes = LifeCircle::PaymentCalculator.new(@enrollment, close_date).build_closing_payment
      closing_payment_attributes = {}
      payment_master = attributes[:excess_payments].first
      if payment_master.present?
        payment_master.invoice_no = generate_invoice_no(close_date)
        #payment_master.save
        closing_payment_attributes[:payment_master] = payment_master
      end
      credite_notes = attributes[:credit_notes].map do |cr|
        cr.payment_master = payment_master
        cr
      end
      credit_notes_pending = []
      credit_notes_to_be_issued = []
      credit_notes_issued = []
      attributes[:credit_notes].map do |cr|
        existed_credit_note_match = @enrollment.credit_notes.credit_notes_on_date(cr.credit_note_date).having_reason(cr.reason_for_creadit_note).first
        unless existed_credit_note_match.present?
          cr.payment_master = payment_master
          credit_notes_pending.push(cr)
        else
          if(existed_credit_note_match.status == "ISSUED")
            credit_notes_issued.push(existed_credit_note_match)
          else
            credit_notes_to_be_issued.push(existed_credit_note_match)
          end
        end
      end
      closing_payment_attributes[:credit_notes_pending] = credit_notes_pending
      closing_payment_attributes[:credit_notes_to_be_issued] = credit_notes_to_be_issued
      closing_payment_attributes[:credit_notes_issued] = credit_notes_issued
      closing_payment_attributes
    end

    def save_closing_payment
      ActiveRecord::Base.transaction do
        if(@params[:payment_master].present?)

          @payment_master = PaymentMaster.new(payment_master_params)
          @payment_master.invoice_no = @params[:payment_master][:invoice_no]
          @payment_master.save
        else
          @payment_master = @enrollment.payment_masters.last
        end
        @params[:credit_notes].each do |cr_params|
          credit_note = CreditNote.new(credit_attributes(cr_params))
          credit_note.payment_master = @payment_master
          credit_note.save
        end
      end
      true
    end

    private

    def make_new_payment_entries
      @enrollment.enrollment_fee = @params[:payment_master][:enrollment_fee]
      @enrollment.discount = @params[:payment_master][:discount]
      @enrollment.discount_type = @params[:payment_master][:discount_type]
      @enrollment.discount_every_time  = @params[:payment_master][:discount_every_time]
      @enrollment.save
      status_list = @params[:enquiry_care_givers].map do |key, val|
        enquiry_care_giver = EnquiryCareGiver.find(key)
        enquiry_care_giver.update(enquiry_care_giver_params(val))
      end
      status_list.all?
    end
    
    def payment_master_params
      @params.require(:payment_master).permit(:enrollment_id, :payment_mode, :amount_in_rupees, :period_from, :period_to, :description_1, :description_2, :status, :total_amount_in_rupees, :discount, :discount_type, :rate_per_day)
    end

    def credit_note_params(params)
      credit_attributes(params.require(:prev_payment_credit_note))
    end

    def credit_attributes(params)
      params.permit(:payment_master_id, :reason_for_creadit_note, :days_of_service_provided, :days_of_service_invoiced, :daily_service_rate, :value_details, :value_of_credit_note, :no_days_service_as_per_client_register, :no_days_service_as_per_cg_register, :is_notice_period_amount_adjusted, :is_payment_made_on_client_or_patient_name, :authorization_token_from_client)
    end

    def enquiry_care_giver_params(params)
      params.permit(:caregiver_charge, :approx_travel_distance, :transport_charge)
    end

    def generate_invoice_no(generation_date)
      last_payment = PaymentMaster.order("invoice_no DESC").first
      prev_invoice = (last_payment.present? ? last_payment.invoice_no : nil)
      year, month, alphabet, counter = "00", "00", "A", "000"
      if(prev_invoice.present?)
        year = format('%02d', prev_invoice[0..1])
        month = format('%02d', prev_invoice[2..3])
        alphabet = prev_invoice[4]
        counter = format('%03d', prev_invoice[5..prev_invoice.length].to_i).to_i
      end
      year = format('%02d', generation_date.strftime("%y").to_i) if(generation_date.strftime("%y").to_i > year.to_i)
      month = format('%02d', generation_date.strftime("%m").to_i) if(generation_date.strftime("%m").to_i > month.to_i)
      counter = format('%03d', counter.to_i+1)
      if(counter.to_i > 999)
        alphabet =  alphabet.next
        counter = format('%03d', 1)
      end
      "#{year}#{month}#{alphabet}#{counter}"
    end
  end
end
