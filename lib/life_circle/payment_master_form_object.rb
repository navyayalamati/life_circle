module LifeCircle
  class PaymentMasterFormObject
    include Virtus
    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations
    attribute :payment_master, PaymentMaster
    attribute :enquiry_care_givers, [EnquiryCareGiver]
  end
end
