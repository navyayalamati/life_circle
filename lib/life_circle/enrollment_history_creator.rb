module LifeCircle
  class EnrollmentHistoryCreator
    
    def initialize(enrollment, current_user)
      @enrollment = enrollment
      @enquiry = @enrollment.enquiry
      @current_user = current_user
    end

    def update_enrollment_plan(enquiry_params, search_params)
      open_transaction do
        enquiry_history = make_enquiry_history
        if enquiry_history.save
          if (make_services_history(enquiry_history) and
              make_timeslots_history(enquiry_history) and
              make_languages_history(enquiry_history))
            enquiry_creator = LifeCircle::EnquiryCreator.new(enquiry_params, search_params, @enquiry.care_giver_masters, @current_user, @enquiry)
            status = enquiry_creator.update
            unless status
              raise ActiveRecord::Rollback
            end
          else
            raise ActiveRecord::Rollback
          end
        end
      end
      true
    end
    
    def excecute_care_giver_replacement(new_enquiry_care_giver_params_list)
      status = false
      open_transaction do
        enquiry_history = make_enquiry_history
        status = enquiry_history.save
        
        unless status
          return false 
        end
        new_enquiry_care_giver_params_list.each do |new_enquiry_care_giver_params|
          old_enquiry_care_giver = EnquiryCareGiver.find(new_enquiry_care_giver_params[:old_enquiry_care_giver_id])
          reflect_the_cg_charges_change_at_new_enquiry(new_enquiry_care_giver_params[:consider_modified_charge])

          enquiry_care_giver_history = make_enquiry_care_giver_history(old_enquiry_care_giver, enquiry_history, new_enquiry_care_giver_params[:adjustment_amount])
          if enquiry_care_giver_history.save
            status = old_enquiry_care_giver.update(filter_enquiry_care_giver_params(new_enquiry_care_giver_params))
            if status
              requested_time_slots = @enrollment.requested_time_slots.where(:care_giver_master_id => enquiry_care_giver_history.care_giver_master_id)
              status = requested_time_slots.update_all({:care_giver_master_id => old_enquiry_care_giver.care_giver_master_id})
            end
          end
          status = make_timeslots_history(enquiry_history)
          unless status
            raise ActiveRecord::Rollback
            return false
          end
        end
      end
      return status
    end

    def check_and_update_care_giver_charge_changes(enquiry_care_giver_params)
      return true unless is_charges_modified(enquiry_care_giver_params)
      excecute_care_giver_replacement(enquiry_care_giver_params.values)
    end

    private

    def is_charges_modified(enquiry_care_giver_params)
      p '11111111111'
      p enquiry_care_giver_params
      enquiry_care_giver_params.each do |key, val_params|
        care_giver = EnquiryCareGiver.find(key)
        return true if care_giver.caregiver_charge.to_i !=  val_params[:caregiver_charge].to_i
        return true if care_giver.approx_travel_distance.to_i !=  val_params[:approx_travel_distance].to_i
        return true if care_giver.transport_charge.to_i !=  val_params[:transport_charge].to_i
      end
      return false
    end

    def reflect_the_cg_charges_change_at_new_enquiry(consider_modified_charges)
      if consider_modified_charges 
        @enquiry.update(:consider_modified_charge => true)
      else
        @enquiry.update(:consider_modified_charge => false)
      end
    end

    def make_enquiry_history
      history_params = [:service_required_from, :service_required_to, :no_of_care_givers, :duty_station, :caregiver_stay]
      new_enquiry_history = EnquiryHistory.new
      history_params.each do |param|
        new_enquiry_history.send("#{param.to_s}=".to_sym, @enquiry.send(param))
      end
      new_enquiry_history.enquiry_id = @enquiry.id
      new_enquiry_history.migration_date = Date.today
      new_enquiry_history
    end

    def make_enquiry_care_giver_history(old_enquiry_care_giver, new_enquiry_history, adjustment_amount = 0)
      enquiry_cg_history_params = [:care_giver_master_id, :caregiver_charge, :transport_charge, :approx_travel_distance]
      new_enquiry_care_giver_history = EnquiryCareGiverHistory.new
      enquiry_cg_history_params.each do |param|
        new_enquiry_care_giver_history.send("#{param.to_s}=".to_sym, old_enquiry_care_giver.send(param))
      end
      new_enquiry_care_giver_history.enquiry_history = new_enquiry_history
      new_enquiry_care_giver_history.adjustment_amount = adjustment_amount
      new_enquiry_care_giver_history
    end

    def make_services_history(new_enquiry_history)
      old_services = @enquiry.requested_services.map do |service|
        RequestedServiceHistory.new({ :enquiry_history_id => new_enquiry_history.id, :service_master_id => service.service_master_id })
      end
      old_services.map(&:save).all?
    end

    def make_timeslots_history(new_enquiry_history)
      old_slots = @enquiry.requested_time_slots.map do |slot|
        RequestedTimeSlotHistory.new({ :enquiry_history_id => new_enquiry_history.id,  :time_from => slot.time_from, :time_to => slot.time_to, :booked_at => slot.booked_at, :care_giver_master_id => slot.care_giver_master_id})
      end
      old_slots.map(&:save).all?
    end

    def make_languages_history(new_enquiry_history)
      old_languages = @enquiry.enquiry_languages.map do |lang|
        EnquiryLanguageHistory.new({ :enquiry_history_id => new_enquiry_history.id,  :language_master_id => lang.language_master_id})
      end
      old_languages.map(&:save).all?
    end


    def filter_enquiry_care_giver_params(params)
      params.permit(:care_giver_master_id, :approx_travel_distance, :caregiver_charge, :transport_charge)
    end
    
    def open_transaction
      ActiveRecord::Base.transaction do
        yield if block_given?
      end
    end
  end
end
