module LifeCircle
  class EnquiryCreator
    attr_accessor :enquiry, :errors, :params
    
    def initialize(enquiry_params, search_params, care_givers, created_by, enquiry = nil)
      @enquiry_params = enquiry_params
      @search_params = search_params
      @enquiry = enquiry
      @care_givers = care_givers
      @created_by = created_by
      @errors = []
    end

    def update
      open_transaction do 
        @enquiry.assign_attributes(@enquiry_params)
        @enquiry.status = Enquiry::STATUS[:wait_for_appointment]
        @enquiry.appointment_status = "OPEN"
        care_giver_matacher = LifeCircle::SingleCareGiverMatcher.new(@search_params, @care_givers, @enquiry)
        if care_giver_matacher.availabile_to_patient_on_changed_criteria?
          if @enquiry.save
            save_services
            save_languages
            save_time_slots
            return true
          else
            errors.push("Error in enquiry")
            return false
          end
        else
          errors.push("Caregivers not available on the given time slots")
          return false
        end
      end
    end
    
    def save
      open_transaction do 
        @enquiry = get_enquiry
        carry_from_caller = false
        carry_from_caller = yield(@enquiry) if block_given?
        @enquiry.assign_attributes(@enquiry_params)
        p "==================="
        p @enquiry_params[:appointment_time]
        if @enquiry_params[:appointment_time] != '-'
          @enquiry.appointment_time = DateTime.strptime("#{@enquiry_params[:appointment_time]}", '%d/%m/%Y-%H:%M') 
        end
        if @care_givers.length > 0
          @enquiry.status = Enquiry::STATUS[:wait_for_appointment]
          @enquiry.appointment_status = "OPEN"
          if @enquiry.missed_call_request_id.present? and !@enquiry.type_of_enquiry
            @enquiry.type_of_enquiry = "Missed Call"
          elsif !@enquiry.type_of_enquiry
            @enquiry.type_of_enquiry = "Online"
          end
        else
          @enquiry.status = Enquiry::STATUS[:empty_results]
          @enquiry.appointment_status = "OPEN"
          @enquiry.type_of_enquiry = "No Caregiver Matched"
        end
        care_giver_available = true
        unless carry_from_caller
          care_giver_matacher = LifeCircle::SingleCareGiverMatcher.new(@search_params, @care_givers)
          care_giver_available = care_giver_matacher.availabile?
        end
        if (care_giver_available or carry_from_caller)
          if @enquiry.save
            save_dependencies
            @enquiry.reload
            return true
          else
            errors.push("Error in enquiry")
            return false
          end
        else
          errors.push("Caregivers not available on the given time slots")
          return false
        end
      end
    end

    def update_pre_enquiry
      save do |enquiry|
        enquiry.address.try(:destroy)
        enquiry.enquiry_care_givers.destroy_all
        # enquiry.requested_services.destroy_all
        # enquiry.enquiry_languages.destroy_all
        # enquiry.requested_time_slots.destroy_all
        false
      end
    end

    def save_enquiry_without_cgs
      save do |enquiry|
        enquiry.status = Enquiry::STATUS[:empty_results]
        true
      end
    end

    private

    def get_enquiry
      @enquiry ||= Enquiry.new
    end

    def save_dependencies
      save_services
      save_languages
      save_time_slots
      save_enquiry_care_givers
      update_missed_call_request if @enquiry.missed_call_request_id.present?
    end

    
    def open_transaction
      ActiveRecord::Base.transaction do
        yield if block_given?
      end
    end

    def update_missed_call_request
      missed_call_request = MissedCallRequest.find(@enquiry.missed_call_request_id)
      missed_call_request.update({:enquiry_id => @enquiry.id, :status => MissedCallRequest::ENQUIRY_CAPTURED })
    end

    def save_enquiry_care_givers
      care_giver_enquiries = @care_givers.map do |care_giver|
        cost_per_slot = CareGiverChargeBucket.calculated_charge_on_given_duration(@enquiry.total_work_hours, care_giver.care_giver_charge.total_charge)
        ecg = EnquiryCareGiver.new({:care_giver_master_id => care_giver.id, :enquiry_id => @enquiry.id, :caregiver_charge => cost_per_slot, :approx_travel_distance => care_giver.approximate_travel_distance})
        ecg.transport_charge = (care_giver.approximate_travel_distance.present? ? (TransportChargeMaster.having_distance(care_giver.approximate_travel_distance).first.try(:charge).to_i) : 0)
        ecg
      end
      
      if care_giver_enquiries.map(&:valid?).all?
        care_giver_enquiries.map(&:save)
        return true
      else
        errors.push("Error in services")
        return false
      end
    end

    def save_services
      services = @search_params[:requested_services].map do |service_id|
        RequestedService.new({:enquiry_id => @enquiry.id, :service_master_id => service_id})
      end
      if services.map(&:valid?).all?
        @enquiry.requested_services.destroy_all
        services.map(&:save)
        return true
      else
        errors.push("Error in services")
        return false
      end
    end

    def save_languages
      languages = @search_params[:languages].map do |lang_id|
        EnquiryLanguage.new do |en|
          en.language_master_id = lang_id
          en.enquiry_id = @enquiry.id
        end
      end
      if languages.map(&:valid?).all?
        @enquiry.enquiry_languages.destroy_all
        languages.map(&:save)
        return true
      else
        errors.push("Error in languages")
        return false
      end

    end

    def save_time_slots
      time_slots = []
      if @enquiry.live_in?
        time_slots = @care_givers.map do |care_giver|
          time_slot = RequestedTimeSlot.new #(slot.permit(:time_from, :time_to))
          time_slot.time_from = "#{@enquiry.service_required_from.strftime('%Y-%m-%d')} #{Enquiry::LIVE_IN_CHECK_IN_TIME}"
          time_slot.time_to = "#{@enquiry.service_required_from.strftime('%Y-%m-%d')} #{Enquiry::LIVE_IN_CHECK_OUT_TIME}"
          time_slot.enquiry_id = @enquiry.id
          time_slot.patient_id = @enquiry.patient_id
          time_slot.care_giver_master_id = care_giver.id
          time_slot.booked_at = DateTime.now.strftime('%Y-%m-%d %H:%M')
          time_slot
          #time_slot.status = RequestedTimeSlot::ENQUIRY_INIT_STATUS
        end
      else
        time_slots = @care_givers.map do |care_giver|
          @search_params[:time_slots].map do |slot|
            time_slot = RequestedTimeSlot.new #(slot.permit(:time_from, :time_to))
            time_slot.time_from = "#{@enquiry.service_required_from.strftime('%Y-%m-%d')} #{slot[:time_from]}"
            time_slot.time_to = "#{@enquiry.service_required_from.strftime('%Y-%m-%d')} #{slot[:time_to]}"
            time_slot.enquiry_id = @enquiry.id
            time_slot.patient_id = @enquiry.patient_id
            time_slot.care_giver_master_id = care_giver.id
            time_slot.booked_at = DateTime.now.strftime('%Y-%m-%d %H:%M')
            #time_slot.status = RequestedTimeSlot::ENQUIRY_INIT_STATUS
            time_slot
          end
        end.flatten
      end
      if time_slots.map(&:valid?)
        @enquiry.requested_time_slots.destroy_all
        time_slots.map(&:save)
        @enquiry.requested_time_slots = time_slots
        return true
      else
        errors.push("Error in languages")
        return false
      end
    end
  end
end
