module LifeCircle
  class CareGiverMonthwiseCalander

    def initialize(care_giver_master)
      @care_giver_master = care_giver_master
    end
    
    def calander_for_range(start_date, end_date)
      @care_giver_master.enquiries.active_enquiries_in_date_range(start_date, end_date).each do |enquiry|
        enquiry.requested_time_slots.each do |time_slot|
          range_start_at = start_date
          if(start_date > enquiry.service_rquire_from)
          end
          if enquiry.service_rquire_to.present?
          else
            (enquiry.service_rquire_from..enquiry.service_rquire_from)
          end
        end
      end
    end

    class FullCalanderDataObject
      include Virtus
      attribute :title
      attribute :allday, Boolean, :default => false
      attribute :borderColor, String, :default => "#5173DA"
      attribute :color, String, :default => "#99ABEA"
      attribute :textColor, String, :default => "#000000"
      attribute :description, String
      attribute :start, DateTime
      attribute :end, DateTime
      attribute :url, String, :default => "#"
    end
  end
end
