module LifeCircle
  class SingleCareGiverMatcher < LifeCircle::CareGiverMatcher

    def initialize(params, care_givers, enquiry = nil)
      super(params)
      @care_givers = care_givers
      @enquiry = enquiry
    end

    def availabile?
      care_givers = excecute()
      existed_ids = care_givers.map(&:id)
      @care_givers.each do |cg|
        unless existed_ids.include?cg.id
          return false
        end
      end
      return true
    end

    def availabile_to_patient_on_changed_criteria? 
      p "==========slots_with_other_patients_on_criteria"
      p slots_with_other_patients_on_criteria
      slots_with_other_patients_on_criteria.count == 0
    end

    protected
   
    def excecute
      super do |care_givers|
        care_givers.where(:id => @care_givers.map(&:id))
      end
    end

    # Check whether the given caregivers having slots with other
    # patients at the given criteria.
    def slots_with_other_patients_on_criteria
      is_live_in = (params[:caregiver_stay] == "LIVE_IN")
      
      primary_serach_query = "SELECT a.id FROM (#{match_care_givers_attendance}) a , (#{match_services_query}) b, (#{match_languages_query}) c WHERE a.id = b.id and b.id = c.id"
      
      # p "=============primary_serach_query"
      # p primary_serach_query
      existing_time_slot_query = availability_match_query(@from_date, @to_date, params[:time_slots], is_live_in) do |query|
        ## Checking for the time slots of the caregiver whho are not assigned with the given patient
        query += " and e.patient_id != #{@enquiry.patient_id}" if @enquiry.patient_id.present?
        query
      end

      # p "=============existing_time_slot_query"
      # p existing_time_slot_query

      
      care_givers = CareGiverMaster.where("id IN (#{primary_serach_query}) and id IN (#{existing_time_slot_query})")
      care_givers = care_givers.where(:id => @care_givers.map(&:id))
      care_givers
    end
  end
end
