module LifeCircle
  class CareGiverCreationService
    attr_accessor :params, :care_giver_master, :errors
    
    def initialize(params)
      @params = params
      @errors = []
    end
    
    def create
      status = true
      ActiveRecord::Base.transaction do
        begin
          @care_giver_master = CareGiverMaster.new(care_giver_master_params)
          @care_giver_master.status = 'not_approved'
          user = User.new(user_params(params[:care_giver_master]))
          p 'navyanvayanavyanavya'
          p params[:type_of_care_giver]
          
          if params[:type_of_care_giver] == 'true'
            
            user.role = 'super_care_giver'
          end
          @care_giver_master.user = user
          if user.save
            @care_giver_master.user = user
            status = @care_giver_master.save
            if status
              if params[:care_giver_master][:languages_known].present?
                care_giver_languages = params[:care_giver_master][:languages_known].map do |lang_id|
                  CareGiverLanguage.new do |cgl|
                    cgl.care_giver_master_id = @care_giver_master.id
                    cgl.language_master_id = lang_id
                  end
                end
                @care_giver_master.care_giver_languages.destroy_all
                status = care_giver_languages.map(&:save).all?
                unless status
                  raise ActiveRecord::Rollback, "Error in language saving"
                end
              end
            end
          else
            status = false
          end
        rescue Exception => e
          status = false
          errors.push(e.message)
        end
      end
      status
    end
    
    private

    def user_params(care_giver_params)
      care_giver_params.require(:user).permit(:email, :user_name, :password, :password_confirmation)
    end

  
    def care_giver_master_params
      params.require(:care_giver_master).permit(:first_name, :last_name, :father_name, :mother_name, :place_of_birth, :dob, :telephone_no, :id_proof_type, :id_proof_no, :id_proof_doc, :address_proof_type,:address_proof_id, :address_proof_doc, :maritual_status, :status, :gender, :profile_pic ,:address_attributes => [:line1, :line2, :city_master_d ,:city, :area, :state, :country, :pin_code], :permanent_address_attributes => [:line1, :line2, :city, :area, :state, :country, :pin_code , :city_master_id])
    end
  end
end
