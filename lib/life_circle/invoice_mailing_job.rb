module LifeCircle
  class InvoiceMailingJob
    
  def initialize(scrolled_by, date, payment_masters)
    @scrolled_by = scrolled_by
    @date = date
    @job_run_id = JobRun.schedule(JobRun::INVOICE_GENERATION, scrolled_by, date).id
    @payment_masters = payment_masters
  end

  def perform
    ActiveRecord::Base.transaction do
      @payment_masters.each do |enrollment|
        payment_date_period = enrollment.generate_payment_date_period
        payment_creation_service = LifeCircle::PaymentMasterCreationService.new({}, enrollment, @scrolled_by)
        
        @payment_master = payment_creation_service.build_payment(generation_date, payment_date_period.first, payment_date_period.last, @job_run_id) #Date.strptime("25-11-2015", "%d-%m-%Y")
      end
    end
  end
  
  def success(job)
    JobRun.finish_as_success(@job_run_id)
  end

  def error(job, error)
    JobRun.finish_as_error(@job_run_id)
  end

  def failure(job)
    JobRun.finish_as_failed(@job_run_id)
  end

  def job_run_id
    @job_run_id
  end

  end
end
