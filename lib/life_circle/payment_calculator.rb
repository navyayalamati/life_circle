module LifeCircle
  class PaymentCalculator
    attr_accessor :payment_master
    attr_accessor :payment_masters
    
    def initialize(enrollment, generation_date)
      @enrollment = enrollment
      @enquiry = enrollment.enquiry
      @generation_date = generation_date
      self.payment_masters = []
    end
    
    def new_payment(start_date, end_date)
      new_payment = nil
      unless @enrollment.payments_exists?
        new_payment = build_first_payment(start_date, end_date) 
        [new_payment]
      else
        build_payment(start_date, end_date)
      end
    end

    def build_closing_payment
      prev_month_payment = last_payment
      get_payment_revision_attributes(@generation_date, prev_month_payment)
    end


    private
    
    def last_payment
      @enrollment.payment_masters.last
    end

    def first_payment?
      @first_payment ||= (@enrollment.payment_masters.not_failed.count == 0)
    end

    def build_first_payment(start_date, end_date)
      cost_per_slot = @enrollment.enquiry.total_charge
      new_payment_master(start_date, end_date, cost_per_slot) do |payment_master|
        add_enrollment_fee_To_enrollment
      end
    end

    def add_enrollment_fee_To_enrollment
      @enrollment.enrollment_fee = Enrollment::ENROLLMENT_FEE
      @enrollment.save
    end

    def build_payment(start_date, end_date)
      prev_payment = last_payment
      prev_paymement_upper_limit_date =  (@generation_date > prev_payment.period_to ? prev_payment.period_to : @generation_date)
      payment_revision_attributes = get_payment_revision_attributes(prev_paymement_upper_limit_date, last_payment, start_date, end_date)
      new_payment = new_payment_master(start_date, end_date, payment_revision_attributes[:cost_per_slot])
      new_payment.credit_notes = payment_revision_attributes[:credit_notes]
      
      if new_payment.enrollment.discount_every_time?
        new_payment.discount = new_payment.enrollment.discount
        new_payment.discount_type = new_payment.enrollment.discount_type
      else
        new_payment.discount = 0;
      end
      new_payment
      payment_masters = []
      payment_masters += payment_revision_attributes[:excess_payments]
      payment_masters.push(new_payment) if new_payment.present?
      payment_masters
    end

    def get_payment_revision_attributes(generation_date, last_month_payment, start_date =nil, end_date =nil)
      # date_range = generate_payment_date_period
      # start_date = date_range.first
      # end_date = date_range.last
      #last_credit_note = CreditNote.credit_note_of_enrollment(@enrollment).first
      history_list = enquiry_histories_from_date(last_month_payment.period_from)
      cost_per_day = cost_per_day_of_enquiry(history_list)
      
      credit_note_start_date = (@enrollment.payment_masters.count <= 1 ? last_month_payment.period_from : last_month_payment.invoice_generation_date)

      payment_revision_attributes = revise_prev_payment(credit_note_start_date, generation_date) 
      ## This line commented because it should be handled by caller
      #if end_date > generation_date
      uncovered_days = (last_month_payment.period_to - generation_date).to_i
      if @enrollment.close_pending? 
        if @enrollment.close_request_registered?
          uncovered_days -= @enrollment.notice_period_days_when_close_request_registered(generation_date)
        else
          uncovered_days -= @enrollment.reason_based_notice_period_days 
        end
      end
      payment_revision_attributes[:total_charge] += (cost_per_day.to_i*uncovered_days)
      payment_revision_attributes[:total_worked_days] += uncovered_days
      #end 
      payment_revision_attributes = apply_discount_if_any_on_payment_revision_attributes(payment_revision_attributes, last_month_payment)
      
      cross_check_final_payment(payment_revision_attributes, last_month_payment, start_date, end_date) 
      payment_revision_attributes[:cost_per_slot] = cost_per_day
      payment_revision_attributes
    end

    
    # Get all the history enquiries after the start date
    # calculate cost_per_day till the migration_date
    # go to the next enquiry
    def revise_prev_payment(start_date, end_date)
      enquiry_histories = enquiry_histories_from_date(start_date)
      credit_notes = []
      iter_start_date = start_date
      total_charge = 0
      total_worked_days = 0
      credit_note_days = 0
      credit_note_value = 0
      absent_days_list = []
      absent_charges_list = []

      enquiry_histories.each_with_index do |temp_enquiry, index|
        iter_end_date = end_date
        if temp_enquiry.respond_to?:migration_date and temp_enquiry.migration_date.present?
          iter_end_date = (temp_enquiry.migration_date > end_date ?  end_date : temp_enquiry.migration_date)
        end
        cost_per_day = 0
        cost_per_day = cost_per_day_of_enquiry(enquiry_histories, index).to_i
        cost_per_slot = temp_enquiry.total_charge #CareGiverChargeBucket.calculated_charge_on_given_duration(temp_enquiry.total_work_hours, cost_per_day)
        (iter_start_date..iter_end_date).each do |date|
          total_charge += cost_per_day
          if attended_enrollment_on_day?(date)
            total_worked_days += 1
          else
            new_credit_note = CreditNote.new do |credit_note|
              credit_note.reason_for_creadit_note = CreditNote::ABSENCE_REASON_MSG_FOR_SINGLE_DAY
              credit_note.value_of_credit_note = cost_per_day
              credit_note.status = "PENDING"
              credit_note.credit_note_date = date
              credit_note.enrollment = @enrollment
              yield(credit_note) if block_given?
            end
            credit_notes.push(new_credit_note)
          end
        end
        iter_start_date = (iter_end_date+1)
      end
      excess_amount_paid_pending_credi_notes = @enrollment.credit_notes.excess_amount_paid_pendings.map do |cr| 
        yield(cr) if block_given?
        cr.status = "PENDING"
        cr
      end
      {:total_charge => total_charge, :total_worked_days => total_worked_days, :credit_notes => (credit_notes + excess_amount_paid_pending_credi_notes)}
    end


    def cost_per_day_of_enquiry(history_list, index = nil)
      i = (index.nil?) ? history_list.length-1 : index
      cost_per_day = 0
      while(i >= 0)
        iter_enquiry = history_list[i]
        if iter_enquiry.consider_modified_charge
          cost_per_day = iter_enquiry.total_charge
          return cost_per_day
        end
        if i == 0
          cost_per_day = iter_enquiry.total_charge
        end
        i -= 1
      end
      return cost_per_day
    end


    def attended_enrollment_on_day?(date)
      @enrollment.care_giver_daily_attendances.attendance_on_date(date).count == 1
    end

    def get_credit_note(credit_note_attributes, payment_master)
      return nil if credit_note_attributes[:credit_note_days] <= 0
      CreditNote.new do |crn|
        crn.payment_master_id = payment_master.id
        crn.days_of_service_provided = credit_note_attributes[:credit_note_days]
        crn.reason_for_creadit_note = CreditNote.compose_reason_msg(credit_note_attributes[:credit_note_days], credit_note_attributes[:absent_days_list])
        crn.value_of_credit_note = credit_note_attributes[:credit_note_value]
        group_by_chares = credit_note_attributes[:absent_charges_list].group_by{|charge| charge}
        crn.value_details = group_by_chares.map{|key, val| "#{val.length} * #{key}"}.join(" + ")
        crn.no_days_service_as_per_client_register = credit_note_attributes[:total_worked_days]
      end
    end

    def new_payment_master(start_date, end_date, care_giver_charge)
      payment_master = PaymentMaster.new do |payment|
        payment.enrollment = @enrollment
      end
      payment_master.period_from = start_date
      payment_master.period_to = end_date
      payment_master.no_of_days = (start_date..end_date).count
      payment_master.caregiver_charge = care_giver_charge
      payment_master.rate_per_day = care_giver_charge
      payment_master.amount_in_rupees = (payment_master.no_of_days * payment_master.caregiver_charge)
      payment_master.description = "Invoice for the month #{start_date}"
      payment_master.status = "CREATED"
      payment_master.due_date = ApplicationDefault.invoice_due_date(@generation_date)
      yield(payment_master) if block_given?
      payment_master
    end

    ## This method is to cross check the calculated previous month payment and the actual amount he has paid
    ## This will generate a credit or debit note if any
    def cross_check_final_payment(payment_revision_attributes, prev_payment, start_date, end_date) 
      payment_revision_attributes[:excess_payments] = []
      difference_other_than_credit_note = 0
      difference_other_than_credit_note =  payment_revision_attributes[:total_charge] - prev_payment.amount_after_discount
      if difference_other_than_credit_note > 0 
        excess_amount_payment_master = PaymentMaster.new do |payment|
          payment.enrollment = @enrollment
          payment.amount_in_rupees = difference_other_than_credit_note
          payment.status = "PENDING"
          payment.period_from = start_date
          payment.period_to = end_date
          payment.description = "Invoice for the replacement with higher skill caregiver #{payment_revision_attributes[:exces_amount_description]}"
          payment.due_date = ApplicationDefault.invoice_due_date(@generation_date)
        end
        payment_revision_attributes[:excess_payments].push(excess_amount_payment_master)
      elsif difference_other_than_credit_note < 0
        mismatch_credit_note = CreditNote.new do |credit_note|
          credit_note.value_of_credit_note = difference_other_than_credit_note
          credit_note.reason_for_creadit_note = "#{PaymentMaster::PREV_MONTH_AMOUNT_SETTLE_COMMENTS}"
          credit_note.status = "PENDING"
          credit_note.enrollment = @enrollment
          credit_note.credit_note_date = @generation_date
          yield(credit_note) if block_given? 
        end
        payment_revision_attributes[:credit_notes].push(mismatch_credit_note)
      end
    end
    

    # This method is to cut the discount from credit note which has been deducted from invoice
    def apply_discount_if_any_on_payment_revision_attributes(attributes, payment_master)
      if(payment_master.discount.present?)
        discount_percentage = payment_master.discount_in_percentage
        attributes[:total_charge] = (attributes[:total_charge] - (attributes[:total_charge]*(discount_percentage.to_f/100))).to_i
        attributes[:credit_notes].each do |credit_note|
          credit_note.value_of_credit_note = (credit_note.value_of_credit_note - (credit_note.value_of_credit_note * (discount_percentage.to_f/100))).to_i
        end
      end
      attributes
    end

    def existing_payments
      @existing_payments ||= @enrollment.payment_masters.order("created_at asc")
    end

    def enquiry_histories_from_date(start_date)
      @enquiry_histories ||= (histories = @enquiry.enquiry_histories.having_migration_date_on_or_after(start_date).order("migration_date asc"); histories.push(@enquiry); histories;)
    end

    

  end
end
