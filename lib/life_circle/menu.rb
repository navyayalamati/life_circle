module LifeCircle
  class Menu
    
    def initialize(params,array , current_user)
      @params = params
      @array = array
      @current_user = current_user
    end

    def create
      ActiveRecord::Base.transaction do
        @role  = Role.new()
        @role.name = @params[:role]
        @role.description = @params[:role]
        @role.code = @params[:role]
        @role.city_master_id = @params[:branch]
        @role.missed_call = @params[:missed_call]
        @role.save
        if @array
          @array.each do |x|
            @m = MenuBar.find_by_id(x[:id])
            @me  = RoleMenuMapping.new()
            @me.role_id = @role.id
            @me.menu_bar_id = x[:id]
            @me.save
            if @m.parent_id != 0
              @men  = RoleMenuMapping.new()
              @men.role_id = @role.id
              @men.menu_bar_id = @m.parent_id
              @men.save
              @n = MenuBar.find_by_id(@m.parent_id)
              if @n.parent_id != 0 
                @menu = RoleMenuMapping.new()
                @menu.role_id = @role.id
                @menu.menu_bar_id = @n.parent_id
                @menu.save
              end
            end
          end
        end
        # @user = new_user
        # p '222222222222'
        # p @user.password
        # p @user.email
        # @user.save
        # UserMailer.welcome_email(@user , @user.password).deliver_now
      end
      
    end
    private
    
      
    def new_user
      User.new(:user_name => @role.name, :email => @params[:email] , :password =>User.generate_password_on_admin_creation, :role => @role.name , :role_id => @role.id)
    end
     
    
  end
end
